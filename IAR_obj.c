#include "IAR_obj.h"
#include "IAR_global.h"
#include "IAR_general.h"
#include <stdio.h>

extern glob_ds global;

void _IAR_PlayerDefaultStep(IAR_Obj* self) {
	
	if (self->Class.Player.StopFrames == 0) {
	
		self->Class.Player.vspeed = iclamp(self->Class.Player.vspeed,-99*IAR_FIXED_SCALE, 20*IAR_FIXED_SCALE);
		
		if (fixed_cast(self->Class.Player.y+self->Class.Player.vspeed) > STAGE_GROUND) {
			const int MAX_LANDING_FRAMES = 2; //it's actually this -1 since defaultMovement() will subtract 1 inmediatly
			const int MAX_TRIPGUARD_FRAMES = 4;
			self->Class.Player.vspeed = 0;
			self->Class.Player.hspeed = 0;
			self->Class.Player.y = STAGE_GROUND*IAR_FIXED_SCALE;
			self->Class.Player.AirReset = false;
			self->Class.Player.AllowedBuffers = 0;
			self->Class.Player.CanOutputBuffer = false;
			self->Class.Player.ApplyGrav = true;
			
			self->Class.Player.JuggleCounter = IAR_MAX_JUGGLE_COUNTER;
			
			
			self->Class.Player.JumpCounter = 0;
			
			if (self->Class.Player.Juggled > 0) {
				_IAR_PlayerResetStateDescriptors(self);
				self->Class.Player.spState = IAR_PLAYER_KNOCKDOWN_TIME;
				self->Class.Player.LastAction = self->Class.Player.KnockdownStateRef;
				self->Class.Player.DefenseFlags = IAR_DEFENSE_THROW_INV | IAR_DEFENSE_CANBLOCK;
				self->Class.Player.CanKara = false;
				self->Class.Player.AllowedBuffers = 0;
				
			}
			else {
				self->Class.Player.AllowedBuffers = 0;
				self->Class.Player.spState = 0;
				if (self->Class.Player.AirAction) {
					self->Class.Player.LandingFrames = MAX_LANDING_FRAMES;
					self->Class.Player.DefenseFlags = IAR_DEFENSE_CANBLOCK;
					
				}
				else {
					self->Class.Player.LandingFrames = MAX_TRIPGUARD_FRAMES;
					self->Class.Player.DefenseFlags = 0;
					if (self->Class.Player.LastAttackLanded) {
						self->Class.Player.AllowedBuffers = 0xFF;
						
						self->Class.Player.CanOutputBuffer = true;
					}
				}
			}

			self->Class.Player.AirAction = true;
			self->Class.Player.AirAttackSlowdown = false;
			self->Class.Player.inAir = false;
		}
		
		self->Class.Player.x += self->Class.Player.hspeed;
		self->Class.Player.y += self->Class.Player.vspeed;
		
		self->Class.Player.FrameIndex += self->Class.Player.FrameSpeed;
		
		if (self->Class.Player.spState == 0 && !self->Class.Player.inAir && self->Class.Player.LandingFrames == 0) {
			self->Class.Player.DefenseFlags = (IAR_DEFENSE_CANBLOCK); //turn canblock on
			self->Class.Player.spFriction = 1.0;
			self->Class.Player.CounterPushback = false;
		}
		
	}
	
	if (self->Class.Player.CanTech > 0)
	if (
		(
		IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_BUTTON_LP)
		&&
		IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LK)
		)
		||
		(
		IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP)
		&&
		IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_BUTTON_LK)
		)
	) {
		IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID, self->gs);
		if (enemy != NULL) {
			IAR_PlayerTechGrab(self);
			IAR_PlayerTechGrab(enemy);
		}
		
		
	}
	
	if (self->Class.Player.WakeupThrowInv > 0)
		self->Class.Player.DefenseFlags = self->Class.Player.DefenseFlags | IAR_DEFENSE_THROW_INV;
	
}


void _IAR_PlayerDefaultFetchInput(IAR_Obj* self) { //work in progress

	switch(self->Class.Player.InputPolicy) {
		
		case IAR_PLAYER_INPUT_POLICY_HARDWARE:

		IAR_GetInputsFromHardware(self->Class.Player.Pside, &self->Class.Player.CurrentFrameInputs,
									 &self->Class.Player.CurrentFrameInputsPressed,
									 &self->Class.Player.CurrentFrameInputsReleased);
									 
		break;
		
		case IAR_PLAYER_INPUT_POLICY_CPU:
			self->Class.Player.CurrentFrameInputs = global.CPU.CurrentFrameInputs[self->Class.Player.Pside-1];
			self->Class.Player.CurrentFrameInputsPressed = global.CPU.CurrentFrameInputsPressed[self->Class.Player.Pside-1];
			self->Class.Player.CurrentFrameInputsReleased = global.CPU.CurrentFrameInputsReleased[self->Class.Player.Pside-1];
		break;

	}

	
	if (
	   ((self->Class.Player.CurrentFrameInputsPressed  & (DHEX_DIR_R | DHEX_DIR_L | DHEX_DIR_U | DHEX_DIR_D)) != 0)
		||
	   ((self->Class.Player.CurrentFrameInputsReleased & (DHEX_DIR_R | DHEX_DIR_L | DHEX_DIR_U | DHEX_DIR_D)) != 0)
		)
		
		{ //any dir change this frame
		
		for (uint8_t k = IAR_PLAYER_INPUT_HIST_SIZE-1; k > 0; k--) { //shift inputs back
			self->Class.Player.InputHistList[k]= self->Class.Player.InputHistList[k-1];
			
		}
		
		uint8_t tmpFlags = 0;
		if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_R))  tmpFlags = tmpFlags | _IAR_INPUT_HIST_R;
		if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_L))  tmpFlags = tmpFlags | _IAR_INPUT_HIST_L;
		if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_U))  tmpFlags = tmpFlags | _IAR_INPUT_HIST_U;
		if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_D))  tmpFlags = tmpFlags | _IAR_INPUT_HIST_D;
		
		self->Class.Player.InputHistList[0].flaggedInputs = tmpFlags;
		self->Class.Player.InputHistList[0].deltaT = self->Class.Player.LastHistElapsed;
		self->Class.Player.LastHistElapsed = 0;
		
	}
	
	if (self->Class.Player.LastHistElapsed < 255) self->Class.Player.LastHistElapsed++;
	
	
	
}


void _IAR_PlayerDefaultPreStep(IAR_Obj* self) {
	
	IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID, self->gs);
	if (enemy != NULL) {
		const fixed tempXtarget = enemy->Class.Player.x;
		if (self->Class.Player.x < tempXtarget) self->Class.Player.xdir = 1;
		if (self->Class.Player.x > tempXtarget) self->Class.Player.xdir =-1;	
	}
	
	if (self->Class.Player.StopFrames > 0) {
		self->Class.Player.StopFrames--;
	}
	
	if (self->Class.Player.JuggleCounter <= 0 && self->Class.Player.Juggled > 0)
		self->Class.Player.DefenseFlags = IAR_DEFENSE_INVINCIBLE;
	
	if (self->Class.Player.WakeupThrowInv > 0) self->Class.Player.WakeupThrowInv --; 
		
	if (self->Class.Player.StopFrames == 0) {
		
		if (self->Class.Player.spState > 0) 	self->Class.Player.spState--;
		if (self->Class.Player.ProxyBlock > 0)  self->Class.Player.ProxyBlock--;
		if (self->Class.Player.MultiHitDelay > 0) self->Class.Player.MultiHitDelay--;
		if (self->Class.Player.invisible > 0) self->Class.Player.invisible--; 
		if (self->Class.Player.CollisionLess > 0) self->Class.Player.CollisionLess--;
		if (self->Class.Player.CanTech > 0) self->Class.Player.CanTech--;
		
		
		if (self->Class.Player.Juggled > 0) 	{
			self->Class.Player.Juggled--;
			self->Class.Player.AllowedBuffers = 0;
			self->Class.Player.CanOutputBuffer = false;
		}
		
		if (self->Class.Player.AirReset && self->Class.Player.Juggled == 0) {
			self->Class.Player.DefenseFlags = IAR_DEFENSE_INVINCIBLE;
		}
		
		if (self->Class.Player.ApplyProxyBlock) {
			if (enemy != NULL) {
				int x1, x2; //x1 should be < than x2
				
				if (self->Class.Player.xdir == 1) { //aka we are x1
					x1 = fixed_roundCast(self->Class.Player.x)  + self->Class.Player.PushBoxW;
					x2 = fixed_roundCast(enemy->Class.Player.x) - enemy->Class.Player.PushBoxW;
				}
				else {
					x1 = fixed_roundCast(enemy->Class.Player.x) + enemy->Class.Player.PushBoxW;
					x2 = fixed_roundCast(self->Class.Player.x)  - self->Class.Player.PushBoxW;					
				}
				
				#define PLAYER_MAX_PROXY_BLOCK 5
				if ((x2 - x1) < self->Class.Player.ApplyProxyBlockDistance) {
					if (enemy->Class.Player.inAir == false && enemy->Class.Player.PreJump == -1
					 && enemy->Class.Player.spState == 0 && !IAR_IsInputFlagged(enemy->Class.Player.CurrentFrameInputs,DHEX_DIR_D)
					 &&
					 ( (enemy->Class.Player.xdir ==-1 && IAR_IsInputFlagged(enemy->Class.Player.CurrentFrameInputs,DHEX_DIR_R))
					|| (enemy->Class.Player.xdir == 1 && IAR_IsInputFlagged(enemy->Class.Player.CurrentFrameInputs,DHEX_DIR_L)))
					 ) {
						//enemy->Class.Player.hspeed = 0;
						enemy->Class.Player.ProxyBlock = PLAYER_MAX_PROXY_BLOCK;
					}
				}
				#undef PLAYER_MAX_PROXY_BLOCK
			}
		}
		
		self->Class.Player.ApplyProxyBlock = false;
		
		if (self->Class.Player.spState == 1 && self->Class.Player.LastAction == self->Class.Player.KnockdownStateRef) {
			_IAR_PlayerResetStateDescriptors(self);
			self->Class.Player.spState = self->Class.Player.WakeupTime;
			self->Class.Player.LastAction = self->Class.Player.WakeupStateRef;
			self->Class.Player.AllowedBuffers = 0;
			self->Class.Player.CanOutputBuffer = false;		
			self->Class.Player.DefenseFlags = IAR_DEFENSE_THROW_INV;
			self->Class.Player.CanKara = false;
	
		}
		
		if (self->Class.Player.spState == 1 && self->Class.Player.LastAction == self->Class.Player.WakeupStateRef) {
			self->Class.Player.WakeupThrowInv = 6;
			
		}
		
		
		self->Class.Player.inAir = false;
		if (fixed_cast(self->Class.Player.y) < STAGE_GROUND) {
			self->Class.Player.inAir = true;
			self->Class.Player.JumpCounter++;
		}
		
		self->Class.Player.AnimationLoops = true;
		
		if (self->Class.Player.spState > 0 && self->Class.Player.state != self->Class.Player.WhiffGrabStateRef)  {
			self->Class.Player.CanOutputBuffer = false;
			self->Class.Player.AllowedBuffers = 0;
		}
		else {
			
			if (self->Class.Player.LandingFrames == 0 && self->Class.Player.PreJump == -1) {

				self->Class.Player.CanOutputBuffer = true;
				self->Class.Player.AllowedBuffers = 0xFF;
			}
		}
		
	}
	
	else {
		self->Class.Player.CanOutputBuffer = false;
		
	}
	
	if (self->Class.Player.AirReset) {
		self->Class.Player.CanOutputBuffer = false;
		self->Class.Player.AllowedBuffers = 0;
	}
	
}


void _IAR_PlayerSetState(IAR_Obj* self, int state) {
	if (self->Class.Player.state != state) {
		self->Class.Player.FrameIndex = 0;
	}
	self->Class.Player.state = state;
	
}

void _IAR_PlayerDefaultMovement(IAR_Obj* self, const fixed WalkSpeed, const fixed JumpSpeed, const fixed grav, const fixed JumpHspeed) {
	
	const fixed Accel = WalkSpeed/2;
	const int MAX_PREJUMP_FRAMES = 3;
	
	if (self->Class.Player.PreJump >= 0) 		self->Class.Player.PreJump--;
	if (self->Class.Player.LandingFrames > 0) 	self->Class.Player.LandingFrames--;
	
	if (self->Class.Player.spState == 0) {
		
		if (self->Class.Player.PreJump == 0) { //launch the jump
			self->Class.Player.vspeed = JumpSpeed;
			self->Class.Player.hspeed = JumpHspeed*self->Class.Player.JumpDir;
			self->Class.Player.xdraw = self->Class.Player.xdir;
			self->Class.Player.inAir = true;
		}
		
		
		if ((self->Class.Player.xdir != self->Class.Player.xdraw) && self->Class.Player.TurnAround == 0
			&& self->Class.Player.LandingFrames == 0 && self->Class.Player.inAir == false) {
			//const int MAX_TURN_AROUND = 6;
			
			self->Class.Player.TurnAround = self->Class.Player.MAX_TURN_AROUND + 1;
			
		}
		
		if (self->Class.Player.TurnAround > 0) self->Class.Player.TurnAround--;
		
		if (self->Class.Player.LandingFrames == 0) {
			
				if (!self->Class.Player.inAir && self->Class.Player.PreJump == -1 && self->Class.Player.Juggled == 0) {
					if (self->Class.Player.CrouchPhase == self->Class.Player.MAX_CROUCH_PHASE) {
						
						if ((
						!IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs,DHEX_DIR_R)
						&&
						!IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs,DHEX_DIR_L)
						)
						|| self->Class.Player.ProxyBlock > 0)
						{																	///////////////// deaccel walking
							int8_t prev_sign = isign(self->Class.Player.hspeed);
							self->Class.Player.hspeed -= prev_sign*Accel;
							
							if (prev_sign !=  isign(self->Class.Player.hspeed))
								self->Class.Player.hspeed = 0;
							
							
						}
						
						if (
							IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs,DHEX_DIR_R)
							&&
							(self->Class.Player.ProxyBlock == 0 || (self->Class.Player.xdir == 1))
							) { //walk
							if (self->Class.Player.hspeed < 0) self->Class.Player.hspeed = 0;
							self->Class.Player.hspeed += Accel;
							self->Class.Player.ProxyBlock = 0;
						}
						
						if (
							IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs,DHEX_DIR_L)
							&&
							(self->Class.Player.ProxyBlock == 0 || (self->Class.Player.xdir ==-1))
							)  {
							if (self->Class.Player.hspeed > 0) self->Class.Player.hspeed = 0;
							self->Class.Player.hspeed -= Accel;
							self->Class.Player.ProxyBlock = 0;
						}
						
						self->Class.Player.hspeed = iclamp(self->Class.Player.hspeed, -WalkSpeed, WalkSpeed);
						
								
						
					}
					
					if (self->Class.Player.TurnAround == 0)
					self->Class.Player.xdraw = self->Class.Player.xdir;
					
					if ( IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs,DHEX_DIR_D) && self->Class.Player.CrouchPhase > 0) {
						self->Class.Player.CrouchPhase--;
					}
					
					if (!IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs,DHEX_DIR_D) && self->Class.Player.CrouchPhase < self->Class.Player.MAX_CROUCH_PHASE) {
						self->Class.Player.CrouchPhase++;
					}
					
					if (self->Class.Player.ProxyBlock > 0) self->Class.Player.CrouchPhase = self->Class.Player.MAX_CROUCH_PHASE;
					
					if (self->Class.Player.CrouchPhase < self->Class.Player.MAX_CROUCH_PHASE) {
						self->Class.Player.hspeed = 0;
					}
					
				}
				

			
			
		}
		
		if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs,DHEX_DIR_U) && !self->Class.Player.inAir
			&& self->Class.Player.PreJump == -1) { //setup prejump
			self->Class.Player.PreJump = MAX_PREJUMP_FRAMES;
			self->Class.Player.JumpDir = 0;
			self->Class.Player.hspeed = 0;
			self->Class.Player.JumpCounter = 0;
			self->Class.Player.LandingFrames = 0;
			self->Class.Player.TurnAround = 0;
			self->Class.Player.ProxyBlock = 0;
			self->Class.Player.CanOutputBuffer = false;
			
		}
		
		if (self->Class.Player.PreJump >= (MAX_PREJUMP_FRAMES-1) && self->Class.Player.JumpDir == 0) { //setup jump dir
			if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_L)) self->Class.Player.JumpDir =-1;
			if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_R)) self->Class.Player.JumpDir = 1;
		}
		

		
		
	} else {
		self->Class.Player.hspeed = self->Class.Player.hspeed * self->Class.Player.spFriction;
		
	}
	
	if (self->Class.Player.inAir && self->Class.Player.ApplyGrav) { //apply grav
		self->Class.Player.vspeed += grav;
	}
	
	if (!self->Class.Player.AirAttackSlowdown && self->Class.Player.inAir && self->Class.Player.LastAttackLanded && self->Class.Player.vspeed > 0) {
		self->Class.Player.AirAttackSlowdown = true;
		self->Class.Player.vspeed /= 2;
	}
	
	
}


IAR_Obj* IAR_InstanceFind(uint32_t ID, IAR_GameState* gs) {
	clist_iterator it = clist_start(gs->objs);
	while (it != NULL)  {
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		if (o->InstanceID == ID) {
			return o;
		}
		it = clist_next(it);
	}
	return NULL;
}


IAR_Obj* IAR_InstanceCreate(IAR_Obj* (*constructor)(void* data), void* data, IAR_GameState* gs) {
	
	IAR_Obj* o = constructor(data);
	o->InstanceID = gs->InstanceCounter;
	o->gs = gs;
	gs->InstanceCounter++;
	clist_push(gs->CreationQueue, &o);
	return o;
}


void IAR_InstanceDestroy(uint32_t ID, IAR_GameState* gs) {
	clist_iterator it = clist_start(gs->objs);
	while (it != NULL)  {
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		if (o->InstanceID == ID) {
			o->DestroyFlag = true;
		}
		it = clist_next(it);
	}
}

void IAR_CreateSpark(int type, int16_t x, int16_t y, float xscale, IAR_GameState* gs) {
	
	
	
	type = iclamp(type, 0, IAR_HITSPARK4_IMAGES); //validate type into usable range
	
	const int imgWidths[]  = {40, 47, 63, 57, 98, 132, 132};
	const int imgHeights[] = {32, 47, 56, 50, 75, 92,  90};
	
	DHEX_Sprite** refList[] = {
		global.resources.BlockSpark1Images,
		global.resources.BlockSpark2Images,
		global.resources.BlockSpark3Images,
		
		global.resources.HitSpark1Images,
		global.resources.HitSpark2Images,
		global.resources.HitSpark3Images,
		global.resources.HitSpark4Images
		
	};
	
	const int nImagesList[] = {
		IAR_BLOCKSPARK1_IMAGES,
		IAR_BLOCKSPARK2_IMAGES,
		IAR_BLOCKSPARK3_IMAGES,
		
		IAR_HITSPARK1_IMAGES,
		IAR_HITSPARK2_IMAGES,
		IAR_HITSPARK3_IMAGES,
		IAR_HITSPARK4_IMAGES
	};
	
	int nImages = -1; //if this is still -1 after the switch, an invalid type was sent
	DHEX_Sprite** ref = NULL;
	int xOffset, yOffset; 
	
	xOffset = imgWidths[type]/2;
	yOffset = imgHeights[type]/2;
	ref = refList[type];
	nImages = nImagesList[type];
	
	IAR_Obj* spark = IAR_InstanceCreate(IAR_CreateTempVisual, NULL, gs);
	
	spark->Class.TempVisual.TTL = 2*nImages; //time to live
	spark->Class.TempVisual.AnimationLoops = false;
	
	spark->Class.TempVisual.images = ref;
	spark->Class.TempVisual.nImages = nImages;
	
	spark->Class.TempVisual.xscale = xscale;
	spark->Class.TempVisual.yscale = 1;
	
	spark->Class.TempVisual.x = IAR_FIXED_SCALE*x;
	spark->Class.TempVisual.y = IAR_FIXED_SCALE*y;
	
	spark->Class.TempVisual.xOffset = xOffset;
	spark->Class.TempVisual.yOffset = yOffset;
	
	spark->Class.TempVisual.ImageIndex = 0;
	spark->Class.TempVisual.ImageSpeed = IAR_FIXED_SCALE/2;
	spark->Class.TempVisual.layer = DHEX_LAYER_OBJ6;
}



void _IAR_PlayerResetStateDescriptors(IAR_Obj* self) {
	
	self->Class.Player.PreJump = -1; //frames left, acts at 0, stops at -1
	self->Class.Player.JumpDir = 0; //-1, 0 or 1. xdraw independent, it just means absolute speed
	self->Class.Player.LandingFrames = 0; //frames left. Stops at 0
	self->Class.Player.TurnAround = 0; //frames left of turning animation. Stops at 0
	self->Class.Player.CrouchPhase = self->Class.Player.MAX_CROUCH_PHASE;
	self->Class.Player.spState = 0;
	self->Class.Player.LastAction = -1;
	self->Class.Player.CanOutputBuffer = true;
	self->Class.Player.AllowedBuffers = 0xFF;
	self->Class.Player.StopFrames = 0; //frames left, stops at 0. Hitstop (on dealt or received hit)
	self->Class.Player.DefenseFlags = IAR_DEFENSE_CANBLOCK;
	self->Class.Player.BufferedAction = -1;
	self->Class.Player.state = -1;
	self->Class.Player.spFriction  = 1.0;
	self->Class.Player.CounterPushback = false;
	self->Class.Player.ProxyBlock = 0;
	self->Class.Player.Juggled = 0;
	self->Class.Player.AirReset = false;
	self->Class.Player.LastAttackLanded = false;
	self->Class.Player.AirAction = true;
	self->Class.Player.AirAttackSlowdown = false;
	self->Class.Player.JuggleCounter = IAR_MAX_JUGGLE_COUNTER;
	self->Class.Player.ApplyGrav = true;
	self->Class.Player.invisible = 0; 
	self->Class.Player.CollisionLess = 0;
	self->Class.Player.CanTech = 0;
	self->Class.Player.WakeupThrowInv = 0;
	self->Class.Player.CanKara = true;
	
}

void _IAR_PlayerDefaultPostChar(IAR_Obj* self) {
	
	fixed prevX = self->Class.Player.x;
	self->Class.Player.x = iclamp(self->Class.Player.x, (self->gs->cam_x+self->Class.Player.PushBoxW)*IAR_FIXED_SCALE,
													    ((self->gs->cam_x+SCREEN_WIDTH) - (self->Class.Player.PushBoxW))*IAR_FIXED_SCALE);
														
	if (abs(self->Class.Player.x - prevX) != 0 && self->Class.Player.CounterPushback) {
		IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID, self->gs);
		if (enemy != NULL) {
			if (self->Class.Player.inAir == false && enemy->Class.Player.inAir == false)
			enemy->Class.Player.x += abs(self->Class.Player.x - prevX)*self->Class.Player.xdraw;
			
		}
	}
	
	if (self->Class.Player.state == self->Class.Player.WhiffGrabStateRef) {
		int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
		if (spIndex == 3) {
			IAR_PlayerTryGrab(self);
		} 
	}
}


void IAR_PlayerApplyStun(int type, uint16_t nframes, IAR_Obj* self) {
	
	int last = self->Class.Player.LastAction;
	int crPhase = self->Class.Player.CrouchPhase;
	_IAR_PlayerResetStateDescriptors(self);
	
	self->Class.Player.CrouchPhase = self->Class.Player.MAX_CROUCH_PHASE;
	self->Class.Player.DefenseFlags = 0;
	
	if (last == self->Class.Player.StBlockStateRef || last == self->Class.Player.CrBlockStateRef) {
		if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_D)) {
			self->Class.Player.CrouchPhase = 0;
			
		}
		else {
			self->Class.Player.CrouchPhase = self->Class.Player.MAX_CROUCH_PHASE;
			
		}
		crPhase = self->Class.Player.CrouchPhase;
	}
	
	int st = self->Class.Player.GutStunStateRef;
	int maxFr = self->Class.Player.GutStunMaxFrameRef;
	IAR_Frame* setRef = self->Class.Player.GutStunFramesRef;

	if (type == IAR_STUNTYPE_HEAD) {
		st = self->Class.Player.HeadStunStateRef;
		maxFr = self->Class.Player.HeadStunMaxFrameRef;
		setRef = self->Class.Player.HeadStunFramesRef;		
		
	}
	
	if (type == IAR_STUNTYPE_BLOCK) {
		st = self->Class.Player.StBlockStateRef;
		maxFr = self->Class.Player.StBlockMaxFrameRef;
		setRef = self->Class.Player.StBlockFramesRef;	
		self->Class.Player.DefenseFlags = IAR_DEFENSE_CANBLOCK;
		
	}	
	
	if (crPhase < self->Class.Player.MAX_CROUCH_PHASE) {
		if (type != IAR_STUNTYPE_BLOCK) {
			st = self->Class.Player.CrStunStateRef;
			maxFr = self->Class.Player.CrStunMaxFrameRef;
			setRef = self->Class.Player.CrStunFramesRef;
			self->Class.Player.CrouchPhase = 0;
		}
		else {
			st = self->Class.Player.CrBlockStateRef;
			maxFr = self->Class.Player.CrBlockMaxFrameRef;
			setRef = self->Class.Player.CrBlockFramesRef;	
			self->Class.Player.DefenseFlags = IAR_DEFENSE_CANBLOCK;			
			
		}
	}
	_IAR_PlayerSetState(self, st);
	self->Class.Player.FrameSpeed = 0;
	self->Class.Player.CurrentFrameSet = setRef;
	self->Class.Player.CurrentMaxFrame = maxFr;
	self->Class.Player.FrameIndex = 0;
	
	self->Class.Player.CurrentMaxSpState = nframes;
	self->Class.Player.spState = self->Class.Player.CurrentMaxSpState;
	self->Class.Player.LastAction = st;
	self->Class.Player.xdraw = self->Class.Player.xdir;
	self->Class.Player.AllowedBuffers = 0;
	self->Class.Player.CanOutputBuffer = false;
	self->Class.Player.DefenseFlags = self->Class.Player.DefenseFlags | IAR_DEFENSE_THROW_INV;
	
}


void IAR_PlayerRefreshCurrentBXG(IAR_Obj* self) {
	if (self->Class.Player.AnimationLoops) {
		self->Class.Player.CurrentFrame =
		self->Class.Player.CurrentFrameSet[abs(fixed_cast(self->Class.Player.FrameIndex)) % self->Class.Player.CurrentMaxFrame];
	}
	else {
		self->Class.Player.CurrentFrame =
		self->Class.Player.CurrentFrameSet[iclamp(fixed_cast(self->Class.Player.FrameIndex),0,self->Class.Player.CurrentMaxFrame-1)];
		
	}
}


void IAR_CollideChars(IAR_Obj* P1, IAR_Obj* P2) {
	
	if (P1->Class.Player.CollisionLess > 0 || P2->Class.Player.CollisionLess > 0) return;
	
	fixed x1, y1, x2, y2;
	fixed w1, h1, w2, h2;
	
	x1 = P1->Class.Player.x - IAR_FIXED_SCALE*P1->Class.Player.PushBoxW;
	y1 = P1->Class.Player.y - IAR_FIXED_SCALE*P1->Class.Player.PushBoxH;
	w1 = IAR_FIXED_SCALE*P1->Class.Player.PushBoxW*2;
	h1 = IAR_FIXED_SCALE*P1->Class.Player.PushBoxH;

	x2 = P2->Class.Player.x - IAR_FIXED_SCALE*P2->Class.Player.PushBoxW;
	y2 = P2->Class.Player.y - IAR_FIXED_SCALE*P2->Class.Player.PushBoxH;
	w2 = IAR_FIXED_SCALE*P2->Class.Player.PushBoxW*2;
	h2 = IAR_FIXED_SCALE*P2->Class.Player.PushBoxH;
	
	bool tmpHadCollision = false;
	
	if(AABB(x1, y1, w1, h1, x2, y2, w2, h2)) {
		fixed x1Front = x1 + IAR_FIXED_SCALE*P1->Class.Player.PushBoxW + IAR_FIXED_SCALE*P1->Class.Player.PushBoxW*P1->Class.Player.xdir;
		fixed x2Front = x2 + IAR_FIXED_SCALE*P2->Class.Player.PushBoxW + IAR_FIXED_SCALE*P2->Class.Player.PushBoxW*P2->Class.Player.xdir;		
		
		const int tmpMAX_DIST = 20;
		int ClipDistance = iclamp(abs(x1Front - x2Front), IAR_FIXED_SCALE, IAR_FIXED_SCALE*tmpMAX_DIST);
		
		int newX1 = (x1 + w1/2) - (ClipDistance/2)*P1->Class.Player.xdir;
		int newX2 = (x2 + w2/2) - (ClipDistance/2)*P2->Class.Player.xdir;
		
		P1->Class.Player.x = newX1;
		P2->Class.Player.x = newX2;
		
		tmpHadCollision = true;
		
		
	}
	
	if (tmpHadCollision)
	for (uint8_t pside = 1; pside <= 2; pside++) {
		IAR_Obj* P = P1;
		if (pside == 2) P = P2;
		
		fixed prevX = P->Class.Player.x;
		P->Class.Player.x = iclamp(P->Class.Player.x, (P->gs->cam_x+P->Class.Player.PushBoxW)*IAR_FIXED_SCALE,
													    ((P->gs->cam_x+SCREEN_WIDTH) - (P->Class.Player.PushBoxW))*IAR_FIXED_SCALE);
		
		fixed Dist = abs(P->Class.Player.x - prevX);
		if (Dist != 0) {
			IAR_Obj* enemy = IAR_InstanceFind(P->Class.Player.OpponentID, P->gs);
			if (enemy != NULL) {
				 
				enemy->Class.Player.x += Dist*P->Class.Player.xdir;
					//P->Class.Player.x += Dist*P->Class.Player.xdir;
					
					break;
			}
		}
	}
	
	
	
}


void IAR_PlayerApplyHit(IAR_Obj* self, bool Blocked, bool Launcher, bool ForceStand, int StunType, uint16_t OnHit, uint16_t OnBlock,
						uint16_t AppliedStopFrames, int16_t AppliedPushback, int16_t AppliedJuggleHs, int16_t AppliedJuggleVs, uint16_t AppliedJuggleTime, int8_t HitDir,
						int8_t JP, uint16_t Damage, uint16_t Chip) {
	
	uint16_t StunTime = OnHit;
	int OriginalJC = self->Class.Player.JuggleCounter;
	
	int WorkingDamage = Damage;
	
	 if (self->Class.Player.health <= Chip) Blocked = false;
	
	if (Blocked) {
		StunType = IAR_STUNTYPE_BLOCK;
		StunTime = OnBlock;
		WorkingDamage = Chip;
	}	
	
	bool Launched = false;
	
	if (!Blocked && Launcher) Launched = true;
	
	if (self->Class.Player.inAir == false && !Launched) { ///////GROUNDED HIT
		if (ForceStand && !Blocked) self->Class.Player.CrouchPhase = self->Class.Player.MAX_CROUCH_PHASE;
		
		IAR_PlayerApplyStun(StunType, StunTime, self);
		
		
		self->Class.Player.hspeed = HitDir*AppliedPushback*IAR_FIXED_SCALE;
		self->Class.Player.spFriction = 0.9;
		self->Class.Player.CounterPushback = true;
		

	}
	else {  ////ANTI AIR HIT
		_IAR_PlayerResetStateDescriptors(self);
		self->Class.Player.JumpDir = HitDir; //aka the direction the hitter is facing
		self->Class.Player.inAir = true;
		self->Class.Player.y -= IAR_FIXED_SCALE;
		self->Class.Player.Juggled = AppliedJuggleTime;
		self->Class.Player.hspeed = HitDir*AppliedJuggleHs*IAR_FIXED_SCALE;
		self->Class.Player.vspeed = AppliedJuggleVs*IAR_FIXED_SCALE;
		self->Class.Player.JuggleCounter = OriginalJC;
		
		self->Class.Player.JumpCounter = 30;
		
		self->Class.Player.FrameSpeed = 0;
		self->Class.Player.CurrentFrameSet = self->Class.Player.JuggledFramesRef;
		self->Class.Player.CurrentMaxFrame = self->Class.Player.JuggledMaxFrameRef;
		self->Class.Player.FrameIndex = 0;
		self->Class.Player.AirReset = true;
		
		self->Class.Player.JuggleCounter -= JP;
	}

	self->Class.Player.StopFrames = AppliedStopFrames;
	self->Class.Player.xdraw = -HitDir;
	self->Class.Player.health -= WorkingDamage;
	self->Class.Player.CanKara = false;
	self->Class.Player.DefenseScore += 20;
	

}




bool IAR_PlayerCheckQC(int8_t direction, IAR_Obj* self) {

	const char* motions[] = {
		"236",
		"256",
		"214",
		"254"
	};
	
	int full = 0;
	int simple = 1;
	
	if (direction == -1) {
		full = 2;
		simple = 3;		
	}
	
	if (IAR_PlayerCheckMotion(self, motions[full])
		||
	   (IAR_PlayerCheckMotion(self, motions[simple] ) && self->Class.Player.spState > 0)
		
		)
		return true;
		
	return false;
}


bool IAR_PlayerCheckDPM(IAR_Obj* self) {
	const char* neutralMotions[] = {
		"6523",
		"65236",
		"654123",
		"65123",
		"65123",
		
		"623",
		"6236",
		"62123",
		
		"6323",
		"63236",
		"632123",
		"63214123",
		
		
		"6123",
		"64123"
	
	};	
	
	const char* cancelMotions[] = {
		"65256"
	};	
	
	for (uint8_t k = 0;  k < sizeof(neutralMotions)/sizeof(const char*); k++) {
		if (IAR_PlayerCheckMotion(self, neutralMotions[k])) {
			return true;
		}
	}
	
	if (self->Class.Player.spState > 0)
	for (uint8_t k = 0;  k < sizeof(cancelMotions)/sizeof(const char*); k++) {
		if (IAR_PlayerCheckMotion(self, cancelMotions[k])) {
			return true;
		}
	}
	
	return false;
	
}

static uint8_t _local_GetHistMaskFromChar(char c, int8_t xdir) {
	uint8_t FW = _IAR_INPUT_HIST_R;
	uint8_t BK = _IAR_INPUT_HIST_L;
	
	if (xdir < 0) {
		FW = _IAR_INPUT_HIST_L;
		BK = _IAR_INPUT_HIST_R;		
	}
	
	
	switch(c) {
		case '1':
			return (BK | _IAR_INPUT_HIST_D);
		break;
		
		case '2':
			return (_IAR_INPUT_HIST_D);
		break;
		
		case '3':
			return (FW | _IAR_INPUT_HIST_D);
		break;
		
		case '4':
			return (BK);
		break;
		
		case '5':
			return 0;
		break;
		
		case '6':
			return (FW);
		break;
		
		case '7':
			return (BK | _IAR_INPUT_HIST_U);
		break;
		
		case '8':
			return (_IAR_INPUT_HIST_U);
		break;
		
		case '9':
			return (FW | _IAR_INPUT_HIST_U);
		break;
		
	}
	
	return 0xFF;
	
}

bool IAR_PlayerCheckMotion(IAR_Obj* self, const char* notation) {
	
	//validate string
	int N = strlen(notation);
	for (uint16_t k = 0; k < N; k++) {
		if (_local_GetHistMaskFromChar(notation[k], 1) == 0xFF)
			return false;
		
	}
	
	const int MAX_DELTA = 12;
	
	InputHist* InputHistList = self->Class.Player.InputHistList;
	int16_t k = N - 1;
	int16_t i = 0;
	uint16_t Matches = 0;
	int NeutralDelta = 0;

	
	//printf("\n -------------------- \n");
	while (k >= 0) {
		if (i > IAR_PLAYER_INPUT_HIST_SIZE) return  false;
		
		if (k == N - 1 && InputHistList[i].flaggedInputs == 0) {
			i++;
		}
		else {
			
			
			int WaitTime = MAX_DELTA;
			if (k == 0 || k == 1) WaitTime = 1000;
			
			//printf("\nChecking %c, hist index %d, with wait time %d", notation[k], i, WaitTime);
			if ((InputHistList[i].deltaT + NeutralDelta) <= WaitTime
				&&
				(InputHistList[i].flaggedInputs == _local_GetHistMaskFromChar(notation[k], self->Class.Player.xdir)  )) {
				Matches++;
				//printf("    MATCH OK");
			}			
			
			
			NeutralDelta = 0;
			i++;
			k--;
			
		}
	}
	
	//printf("%d",Matches);
	
	if (self->Class.Player.LastHistElapsed <= MAX_DELTA && Matches == N) return true;
	
	return false;
	
}


void IAR_PlayerTryGrab(IAR_Obj* self) {
	IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID, self->gs);
	
	if (enemy != NULL) {
		int x1, x2;
		
		
		if (self->Class.Player.xdir == 1) { //aka we are x1
			x1 = fixed_roundCast(self->Class.Player.x)  + self->Class.Player.PushBoxW;
			x2 = fixed_roundCast(enemy->Class.Player.x) - enemy->Class.Player.PushBoxW;
		}
		else {
			x1 = fixed_roundCast(enemy->Class.Player.x) + enemy->Class.Player.PushBoxW;
			x2 = fixed_roundCast(self->Class.Player.x)  - self->Class.Player.PushBoxW;					
		}
		
		int dist = x2 - x1;
		
		int minDist = self->Class.Player.GRAB_RANGE;
		if (enemy->Class.Player.GRAB_RANGE < minDist) minDist = enemy->Class.Player.GRAB_RANGE;
		
		if (minDist >= dist && !enemy->Class.Player.inAir && (enemy->Class.Player.DefenseFlags & IAR_DEFENSE_INVINCIBLE) == 0
			&& (enemy->Class.Player.DefenseFlags & IAR_DEFENSE_THROW_INV) == 0 && enemy->Class.Player.PreJump == -1) {
				
			//printf("Grabable state ");
				
			//Check for tech
			int enemySpIndex = 1 + enemy->Class.Player.CurrentMaxSpState -  enemy->Class.Player.spState;
			if (enemySpIndex <= 3 && enemy->Class.Player.state == enemy->Class.Player.WhiffGrabStateRef) {
				IAR_PlayerTechGrab(self);
				IAR_PlayerTechGrab(enemy);
			}
			else {
				
				if (enemy->Class.Player.spState == 0)
					enemy->Class.Player.CanTech = 5;
				
				_IAR_PlayerResetStateDescriptors(enemy);
				enemy->Class.Player.spState = 100;
				enemy->Class.Player.StopFrames = 20;
				enemy->Class.Player.CurrentMaxSpState = enemy->Class.Player.spState;
				enemy->Class.Player.CollisionLess = 100;
				enemy->Class.Player.LastAction = enemy->Class.Player.GutStunStateRef;
				enemy->Class.Player.DefenseFlags = IAR_DEFENSE_INVINCIBLE;
				enemy->Class.Player.AllowedBuffers = 0;
				enemy->Class.Player.CanOutputBuffer = false;
				enemy->Class.Player.hspeed = 0;
				
				
				self->Class.Player.DefenseFlags = IAR_DEFENSE_INVINCIBLE;
				self->Class.Player.AllowedBuffers = 0;
				self->Class.Player.CanOutputBuffer = true;
				self->Class.Player.hspeed = 0;
				
				int action = self->Class.Player.ThrowForwardStateRef;
				if
				(
				(self->Class.Player.xdir == 1 && IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_L))
				||
				(self->Class.Player.xdir ==-1 && IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_R))
				) {
					action = self->Class.Player.ThrowBackStateRef;
				}
				
				self->Class.Player.BufferedAction = action;
			
			}
			
			
			
		}
		
	}
	
}

void IAR_PlayerTechGrab(IAR_Obj* self) {
	
	_IAR_PlayerResetStateDescriptors(self);
	self->Class.Player.xdraw = self->Class.Player.xdir;
	self->Class.Player.JumpDir = -self->Class.Player.xdir;
	self->Class.Player.AllowedBuffers = 0;
	self->Class.Player.CanOutputBuffer = 0;
	self->Class.Player.y -= IAR_FIXED_SCALE;
	self->Class.Player.inAir = true;
	self->Class.Player.AirAction = false;
	self->Class.Player.vspeed = -8*IAR_FIXED_SCALE;
	self->Class.Player.hspeed = 3*self->Class.Player.JumpDir*IAR_FIXED_SCALE;
	self->Class.Player.DefenseScore += 70;
	
}

void IAR_DrawHealthBars(IAR_Obj* P1, IAR_Obj* P2) {
	DHEX_DrawLayer(global.app, DHEX_LAYER_HUD0);
	
	for (uint8_t pside = 0; pside < 2; pside++) {
		
		IAR_Obj* P = P1;
		
		const int BorderDistance = 6;
		const int ClockSpace = 40;
		
		int w = - BorderDistance - ClockSpace/2 + SCREEN_WIDTH/2;
		const int h = 8;
		
		
		int x,y;
		int xBase = BorderDistance;
		x = (int)w*( (float)(P->Class.Player.MAX_HEALTH - P->Class.Player.health) / (float)P->Class.Player.MAX_HEALTH);
		x = iclamp(x, 0, w);
		y = 10;
		
		//printf("x %d  ", x);
		
		int w2 = w - x;
		
		if (pside == 1) {
			P = P2;
			xBase = SCREEN_WIDTH - w - BorderDistance;
			x = 0;
			w2 = (int)w*((float)P->Class.Player.health / (float)P->Class.Player.MAX_HEALTH);
			w2 = iclamp(w2, 0, w);
			
		}
		
		DHEX_FillRect(global.app, xBase, y, w, h, 0,0,0,255);
		
		DHEX_FillRect(global.app, xBase + x, y, w2, h, 0,200,0,255);
		DHEX_FillRect(global.app, xBase + x, y+6, w2, h-6, 0,160,0,255);
		
		DHEX_DrawRect(global.app, xBase, y, w, h, 255,255,255,255);
		DHEX_DrawRect(global.app, xBase-1, y-1, w+2, h+2, 0,0,0,255);
		
	}
	
}