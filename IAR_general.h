#ifndef _IAR_GENERAL_INCLUDED
#define _IAR_GENERAL_INCLUDED

#include <stdint.h>
#include "DHEX.h"
#include <stdbool.h>

//#define IAR_FIXED_SCALE 65536
#define IAR_FIXED_SCALE 8192

typedef int32_t fixed;
//fixed points are just integers. The value they represent is = to fixed/IAR_FIXED_SCALE

int32_t fixed_cast (fixed n);
fixed   fixed_round(fixed n);
int32_t fixed_roundCast(fixed n); //round and then cast

void IAR_DrawBXG(DHEX_FrameBoxes bxg, int x, int y); //will use the currently set DHEX draw layer

float clamp (float value, float min, float max);
int32_t iclamp (int32_t value, int32_t min, int32_t max);
int8_t isign(int32_t n);

DHEX_FrameBoxes IAR_ScaleBXG(DHEX_FrameBoxes bxg, float xscale, float yscale);

void IAR_TestBXG_Collision(DHEX_FrameBoxes P1, int x1, int y1, DHEX_FrameBoxes P2, int x2, int y2, char* P1Scored, char* P2Scored);
//test hit and hurtboxes to know of collisions
//PNScored are pointers to a char (this is just a char, not a Null terminated string)
//that will indicate if player N scored a hit (so P1Scored would mean
//player 1 managed to hit player 2.
//The char this vars are pointing to will store 0 if no score was found, 1 otherwise

bool AABB(int x1, int y1, int w1, int h1,
		  int x2, int y2, int w2, int h2);

bool IAR_IsInputFlagged(uint16_t state, uint16_t mask);
void IAR_GetInputsFromHardware(uint8_t pside, uint16_t* held, uint16_t* pressed, uint16_t* released);

int xorRand();

#endif