//clist.h - C linked list for any data type

/*

Create a list:
	clist* List = clist_create(data_type);

Add an element:
	data_type element = value;
	clist_push(List, &element);

Iterate list:
	clist_iterator it = clist_start(List);
	while (it != NULL) {
		<...>
		it = clist_next(it);
	}
	
be careful though, if you delete an element while doing this iteration,
be sure to store a reference to the next element of the iteration before
deleting the current node.
	
	
Cast an element:
	clist_get(clist_iterator, data_type);
	
Delete an element:
	clist_delete(clist_iterator);
	
Delete all elements:
	clist_clear(List);
	
Free list from memory:
	clist_free(List);
	
If your data type requires a more complex function call for deallocating it than just free()
you can asign a function callback to your clist (clist->free_element_callback). By default
this callback points to free()

Create a copy of a list:
	clist* Copy = clist_clone(L);

Only supports trivial copying


*/

#ifndef _CLIST_H_INCLUDED
#define _CLIST_H_INCLUDED

#define clist_create(x) ___clist_create(sizeof(x))
#define clist_get(x,y) (*(y*)(x->data))

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

struct ____clist;

typedef struct ____clist_node {
	void*  data;
	struct ____clist_node*  next; //next clist node
	struct ____clist_node*  prev;
	struct ____clist* list; //address of the list this belongs to
	
} clist_node;

typedef struct ____clist {
	clist_node* first;
	clist_node* last;
	uint16_t element_size; //in bytes
	void (*free_element_callback)(void*);
	
} clist;



typedef clist_node* clist_iterator;

clist* ___clist_create(uint16_t e_size);

void clist_clear(clist* L);
void clist_free(clist* L);
void clist_push(clist* L, void* data);
void clist_delete(clist_node* E);

clist_iterator clist_start(clist* L); //get iterator at the start of the list
clist_iterator clist_next(clist_node* E); //advance the iterator 1 step

clist* clist_clone(clist *L);

void clist_print(clist* L); //print each node's void* value into stdout

#endif
