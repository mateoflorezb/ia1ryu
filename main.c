#include "IAR_DataGeneration.h"
#include <stdio.h>
#include "DHEX.h"
#include "IAR_general.h"
#include "IAR_obj.h"
#include "IAR_global.h"
#include "IAR_gamestate.h"
#include "IAR_CPU.h"
#include <math.h>

#define MS_PER_FRAME 16

glob_ds global;

enum _CLI_Op {
	_IAR_CLI_P1_RAND = 0,
	_IAR_CLI_P1_TRAINED,
	_IAR_CLI_P2_RAND,
	_IAR_CLI_P2_TRAINED,
	_IAR_CLI_HIDEVISUALS,
	_IAR_CLI_SKIP,
	_IAR_CLI_AUTOMATCH,
	_IAR_CLI_SHOWBOX
};

static const char* const CLI_Options[] = {
	"-p1rand",
	"-p1trained",
	"-p2rand",
	"-p2trained",
	"-hidevisuals", //implies -skipdelay and -autorematch
	"-skipdelay",
	"-autorematch",
	"-showboxes"
};

static void NotifyUsage() {
	printf("\n\n");
	printf("IAR - Inteligencia Artificial 2021-1 Ryu (Universidad Industrial de Santander)\n");
	printf("Autor: Mateo F. - 2170077\n");
	printf("Uso: IAR.exe <opciones...>\n");
	printf("-p<x>rand\n	Configurar al jugador <x> como un jugador controlado aleatoriamente\n");
	printf("	Ejemplo: -p2rand\n\n");
	printf("-p<x>trained\n	Configurar al jugador <x> como un jugador entrenado\n\n");
	printf("%s\n	Se ignora el cap de FPS\n\n", CLI_Options[5]);
	printf("%s\n	Se ignora el output visual. Implica %s y %s\n\n", CLI_Options[4], CLI_Options[5], CLI_Options[6]);
	printf("%s\n	Al final de una partida se reinicia inmediatamente el estado\n\n", CLI_Options[6]);
	printf("%s\n	Muestra las hitboxes\n\n", CLI_Options[7]);
}

static void __local_LoadImageList(DHEX_Sprite** list,const char* basePath, int nframes) {
	
	for (uint8_t k = 0; k < nframes; k++) {
		char* tempStr = malloc(strlen(basePath)+7); //AA.png + null
		sprintf(tempStr,"%s%u.png\0",basePath, k+1);
		list[k] =  DHEX_GetSpriteRaw(global.app, tempStr);
		free(tempStr);
	}
	
}

static PyObject* _local_ImportAndPromptErrors(const char* module) {
	PyObject* _pyMod = PyImport_ImportModule(module);
	if (PyErr_Occurred() != NULL) {
		printf("(Python) ");
		PyErr_Print();
		return NULL;
	}
	else {
		printf("Loaded module: %s\n", module);
	}
	
	return _pyMod;
}

static PyObject* _local_GetFunctionFromModule(PyObject* module, const char* attr) {
	PyObject* func = PyObject_GetAttrString(module, attr);
	if (PyErr_Occurred() != NULL) {
		printf("(Python) ");
		PyErr_Print();
		return NULL;
	}
	else {
		printf("Loaded function: %s\n", attr);
	}
	
	return func;
}

int main(int argc, char* argv[]) {
	Py_Initialize();
	
	printf("Loading modules...\n");
	
	global.Python.IAR_Scripts = _local_ImportAndPromptErrors("IAR_Scripts");
	global.Python.ZerosDataDict = _local_GetFunctionFromModule(global.Python.IAR_Scripts, "ZerosDataDict");
	global.Python.SetRow = _local_GetFunctionFromModule(global.Python.IAR_Scripts, "SetRow");
	global.Python.SaveDataDictAsCSV = _local_GetFunctionFromModule(global.Python.IAR_Scripts, "SaveDataDictAsCSV");
	global.Python.GetEstimation = _local_GetFunctionFromModule(global.Python.IAR_Scripts, "GetEstimation");
	PyObject* tJoblib = _local_ImportAndPromptErrors("joblib");
	
	global.Python.RW = PyObject_CallMethod(tJoblib, "load", "s", "RWestimator.joblib");
	printf("RW loaded at: %p\n", global.Python.RW);
	global.Python.RS = PyObject_CallMethod(tJoblib, "load", "s", "RSestimator.joblib");
	printf("RS loaded at: %p\n", global.Python.RS);
	
    global.app = DHEX_Init(NULL, "AI Ryu", SCREEN_WIDTH, SCREEN_HEIGHT);
	
	 __local_LoadImageList(global.resources.BlockSpark1Images,"data/blockspark1/", IAR_BLOCKSPARK1_IMAGES);
	 __local_LoadImageList(global.resources.BlockSpark2Images,"data/blockspark2/", IAR_BLOCKSPARK2_IMAGES);
	 __local_LoadImageList(global.resources.BlockSpark3Images,"data/blockspark3/", IAR_BLOCKSPARK3_IMAGES);
	 __local_LoadImageList(global.resources.HitSpark1Images,"data/hitspark1/", IAR_HITSPARK1_IMAGES);
	 __local_LoadImageList(global.resources.HitSpark2Images,"data/hitspark2/", IAR_HITSPARK2_IMAGES);
	 __local_LoadImageList(global.resources.HitSpark3Images,"data/hitspark3/", IAR_HITSPARK3_IMAGES);
	 __local_LoadImageList(global.resources.HitSpark4Images,"data/hitspark4/", IAR_HITSPARK4_IMAGES);	 

	global.resources.font = DHEX_CreateFont(global.app,"data/fonts/small_font",6);
	DHEX_SetWindowSize(global.app, 3*SCREEN_WIDTH, 3*SCREEN_HEIGHT);
	DHEX_CenterWindow(global.app);
	DHEX_LoadKeyConfig(global.app, "DHEX_KeyConfig");
	global.resources.background = DHEX_GetSpriteRaw(global.app, "data/stage1.png");	
	global.CPU.CurrentRow = 0;
	
	IAR_GameState* MainGameState = IAR_NewGameState();
	
	IAR_CPU_AI* AI  = IAR_Create_CPU_AI(0, 1);
	IAR_CPU_AI* AI2 = IAR_Create_CPU_AI(0, 2);
	AI ->ExportData = false;
	AI2->ExportData = false;
	
	//check CLI Options
	global.Settings.DelayFrame = true;
	global.Settings.DrawFrame = true;
	global.Settings.ShowBoxes = false;
	global.Settings.AutoMatch = false;
	
	bool Notify = true;
	
	for (int option = 1; option < argc; option++) {
		if (option == 1) Notify = false;
		
		int DefinedOptions = sizeof(CLI_Options)/sizeof(CLI_Options[0]);
		int found = 0;
		for (int k = 0; k < DefinedOptions; k++) {
			if (strcmp (argv[option], CLI_Options[k]) == 0) {
				found = 1;
				switch(k) {
					case _IAR_CLI_P1_RAND:
						AI->Policy = IAR_CPU_RANDOM;
						MainGameState->P1->Class.Player.InputPolicy = IAR_PLAYER_INPUT_POLICY_CPU;
					break;
					
					case _IAR_CLI_P2_RAND:
						AI2->Policy = IAR_CPU_RANDOM;
						MainGameState->P2->Class.Player.InputPolicy = IAR_PLAYER_INPUT_POLICY_CPU;
					break;		

					case _IAR_CLI_P1_TRAINED:
						AI->Policy = IAR_CPU_INSTANT_REACTION;
						MainGameState->P1->Class.Player.InputPolicy = IAR_PLAYER_INPUT_POLICY_CPU;
					break;
					
					case _IAR_CLI_P2_TRAINED:
						AI2->Policy = IAR_CPU_INSTANT_REACTION;
						MainGameState->P2->Class.Player.InputPolicy = IAR_PLAYER_INPUT_POLICY_CPU;
					break;
					
					case _IAR_CLI_HIDEVISUALS:
						global.Settings.DelayFrame = false;
						global.Settings.DrawFrame = false;		
						global.Settings.AutoMatch = true;
					break;
					
					case _IAR_CLI_SKIP:
						global.Settings.DelayFrame = false;
					break;
					
					case _IAR_CLI_AUTOMATCH:
						global.Settings.AutoMatch = true;
					break;
					
					case _IAR_CLI_SHOWBOX:
						global.Settings.ShowBoxes = true;
					break;
					
				}
				
				break;
			} 
			
		}
		
		if (found == 0) Notify = true;
		
	}
	
	if (Notify) NotifyUsage();
	
	IAR_GameState* SaveState = IAR_CloneGameState(MainGameState);
	
	srand(time(NULL));

    uint32_t FrameStart = 0;
    uint32_t FrameEnd = 0;
	uint8_t LagFrame = 0;
	uint32_t P1_Wins = 0, P2_Wins = 0;
	int Matches = 50;
	
	DHEX_DrawEnable(global.app, global.Settings.DrawFrame);
    while (DHEX_IsRunning(global.app)) {
		FrameStart = DHEX_GetTicks();
		LagFrame = ~LagFrame;
		
        DHEX_UpdateState(global.app);
		
		int mouse_x, mouse_y;
		DHEX_GetMouseState(global.app, &mouse_x, &mouse_y, NULL, NULL);
		
		if (global.Settings.DrawFrame)
		DHEX_ClearScreen(global.app,0,0,0);
		
		/*if (DHEX_InputCheckPressed(global.app, DHEX_BUTTON_START, 1)) { //save
			if (SaveState != NULL) 
				IAR_FreeGameState(SaveState);
			
			SaveState = IAR_CloneGameState(MainGameState);
		}*/
		
		if (global.Settings.DrawFrame)
		if (DHEX_InputCheckPressed(global.app, DHEX_BUTTON_START, 2)) { //load
			if (SaveState != NULL) {
				IAR_FreeGameState(MainGameState);
				MainGameState = IAR_CloneGameState(SaveState);	
				AI->reset(AI, MainGameState);
				AI2->reset(AI2, MainGameState);		
				//global.CPU.CurrentRow = 0;
			}
		}
		
		if (global.Settings.AutoMatch)
		if (MainGameState->P1->Class.Player.health <= 0 || MainGameState->P2->Class.Player.health <= 0) {
			Matches--;
			printf("%d matches to go...\n",Matches);
			if (MainGameState->P2->Class.Player.health <= 0) {
				P1_Wins++;
			}
			if (MainGameState->P1->Class.Player.health <= 0) {
				P2_Wins++;
			}
			IAR_FreeGameState(MainGameState);
			MainGameState = IAR_CloneGameState(SaveState);	
			AI ->reset(AI,  MainGameState);			
			AI2->reset(AI2, MainGameState);		
			
			if (Matches <= 0) break;
		}
		
		AI-> step(AI,  MainGameState);
		AI2->step(AI2, MainGameState);
		
		IAR_AdvanceGameState(MainGameState); 	
		
		DHEX_UpdateScreen(global.app);
		
		FrameEnd = DHEX_GetTicks();
		if (global.Settings.DelayFrame)
		DHEX_Delay(iclamp((MS_PER_FRAME + (LagFrame & 0b00000001) )-((int)FrameEnd-(int)FrameStart), 0, MS_PER_FRAME+1));
	}
	
	DHEX_FreeApp(global.app);
	
	printf("Matchup metrics-----------\nP1 wins: %u\nP2 wins: %u", P1_Wins, P2_Wins);
	//scanf("*");
    return 0;
}
