#include "box_engine.h"
#include <fstream>
#include <iostream>


void bk_box_engine::save_frame(bk_box_engine::frame_file *frame, const char* filename) {
	if (frame == NULL)
	return;

	std::ofstream file(filename, std::ios::out | std::ios::binary);
	if (!file) {
		std::cout << "Couldn't open file " << filename << std::endl;
		return;
	}
	
	for (uint8_t ch = 0; ch < BK_BOX_ENGINE_MAX_SPRITE_NAME; ch++) { //write the null terminated string
		file.write(&(frame->sprite_name[ch]),1);
		if ((frame->sprite_name[ch]) == 0) break; //0 aka NULL. Did this so that the compiler doesn't whine
	}
	
	for (uint8_t b = 0; b < BK_BOX_ENGINE_MAX_BOXES; b++) { //write null terminated hurtbox list
		file.write((char*)&(frame->hurtboxes[b]),sizeof(box));
		if (frame->hurtboxes[b].w == 0) break;
	}
	
	for (uint8_t b = 0; b < BK_BOX_ENGINE_MAX_BOXES; b++) { //write null terminated hitbox list
		file.write((char*)&(frame->hitboxes[b]),sizeof(box));
		if (frame->hitboxes[b].w == 0) break;
	}
	
	file.write((char*)&(frame->xcenter),sizeof(int16_t));
	file.write((char*)&(frame->ycenter),sizeof(int16_t));		

	file.close();
	
	return;
}



bk_box_engine::frame_file* bk_box_engine::load_frame(const char* filename) {
	
	std::ifstream file(filename, std::ios::out | std::ios::binary);
	if(!file) {
		std::cout << "Couldn't open file " << filename << std::endl;
		return NULL;
	}
	
	frame_file* frame = new frame_file;
	
	for (uint8_t ch = 0; ch < BK_BOX_ENGINE_MAX_SPRITE_NAME; ch++) { //write the null terminated string
		file.read(&(frame->sprite_name[ch]),1);
		if ((frame->sprite_name[ch]) == 0) break; //0 aka NULL. Did this so that the compiler doesn't whine
	}
	
	for (uint8_t b = 0; b < BK_BOX_ENGINE_MAX_BOXES; b++) { //write null terminated hurtbox list
		file.read((char*)&(frame->hurtboxes[b]),sizeof(box));
		if (frame->hurtboxes[b].w == 0) break;
	}
	
	for (uint8_t b = 0; b < BK_BOX_ENGINE_MAX_BOXES; b++) { //write null terminated hitbox list
		file.read((char*)&(frame->hitboxes[b]),sizeof(box));
		if (frame->hitboxes[b].w == 0) break;
	}
	
	file.read((char*)&(frame->xcenter),sizeof(int16_t));
	file.read((char*)&(frame->ycenter),sizeof(int16_t));		
	
	file.close();
	
	return frame;
}