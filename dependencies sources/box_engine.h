#ifndef BOX_ENGINE_H_
#define BOX_ENGINE_H_

#define BK_BOX_ENGINE_MAX_SPRITE_NAME 80
#define BK_BOX_ENGINE_MAX_BOXES 10

#include <stdint.h>

namespace bk_box_engine {

	struct box {
		int16_t x,y;
		uint16_t w,h;
	};
	
	struct frame_file {
		char sprite_name[BK_BOX_ENGINE_MAX_SPRITE_NAME];
		box hurtboxes[BK_BOX_ENGINE_MAX_BOXES];
		box hitboxes[BK_BOX_ENGINE_MAX_BOXES];
		int16_t xcenter, ycenter;
	};
	
	void save_frame(frame_file *frame, const char* filename);
	frame_file* load_frame(const char* filename);
}



#endif /* BOX_ENGINE_H_ */