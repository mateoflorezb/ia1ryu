#ifndef PALETTE_ENGINE_H_INCLUDED
#define PALETTE_ENGINE_H_INCLUDED

#include <SDL.h>

namespace palette_engine {

	struct sprite {
		SDL_Texture* tex;
		int x_offset;
		int y_offset; //used to center the sprite
		int w;
		int h;
	};

	struct palette {
		SDL_Color colors[256];
		unsigned char max_index{0};
	};

	SDL_Surface* load_palettized_surface(const char* filename, SDL_Color* palette);
	
	sprite* create_sprite(SDL_Renderer* renderer, const char* filename, int xoff, int yoff);

	//takes R channel as palette index. Must work from PNGs
	sprite* create_sprite(SDL_Renderer* renderer, const char* filename, int xoff, int yoff, SDL_Color* palette);

	void free_sprite(sprite* spr); //also frees it's texture

	int draw_sprite(SDL_Renderer* renderer, sprite* spr, int x, int y, double xscale, double yscale, double angle, int view_x, int view_y);
	int draw_sprite(SDL_Renderer* renderer, sprite* spr, int x, int y, double xscale, double yscale, double angle, uint8_t alpha, int view_x, int view_y);	

	bool save_palette(palette* pal, const char* filename);

	palette* load_palette(const char* filename);
}


#endif // PALETTE_ENGINE_H_INCLUDED