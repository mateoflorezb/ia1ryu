/*

palette format:
first byte: uint8_t that defines the largest defined index
(0 is always defined, since it must be (0,255,255) )
then the indexes are defined in order (starting from index 1,
as index 0 is always predefined), each one taking 3 bytes 

*/

#include "palette_engine.h"
#include "SDL_image.h"
#include <iostream>
#include "SDL_ttf.h"
#include <string>
#include <cstring>
#include <cmath>

#define SCREEN_WIDTH 880
#define SCREEN_HEIGHT 660
#define AUTO_PALGEN_FILENAME "auto_palgen_out"
#define CUSTOM_PALGEN_FILENAME "custom_palgen_out"
#define PALETTE_EXTENSION ".palgen"
#define SLIDER_MIN 0.5
#define SLIDER_RANGE 20.5
#define FONT_PATH ((std::string)SDL_GetBasePath()+"data/dogicapixel.ttf").c_str()
#define MS_PER_FRAME 20

//undefine if no output of internal process is to be shown
//#define VERBOSAL_MODE
#ifdef VERBOSAL_MODE
#include <bitset>
#endif


using namespace palette_engine;


void notice_usage() {
		std::cout << "\npal_gen: palette generator for palette_engine.h\n\nusage:\n\n"
		"pal_gen -auto <files>... \n"
		"Generates palettized versions of <files>.\nThe palette is automatically generated from the first image and exported\n"
		"as " << AUTO_PALGEN_FILENAME PALETTE_EXTENSION << "." << std::endl << std::endl << "Remarks:" << std::endl << "Only PNG format is supported."
		<< std::endl << std::endl <<

		"pal_gen -plt <palette> <files>... \n"
		"Same as auto but instead of generating a new palette,\nit starts with an already defined one.\nIf the palette"
		" still has room for indexes,\nnew colors found in the sprite will be added to the palette. " << std::endl << std::endl <<

		"pal_gen -GUI <sprite> <palette> \n"
		"Special mode that loads a GUI. Shows <sprite>, palettized with <palette>.\nAllows to select any index from the "
		"palette and change it's RGB value.\nThese changes are shown in the sprite once applied.\nWhen the desired palette "
		"has been created,\nthere's a button to export it as " << CUSTOM_PALGEN_FILENAME PALETTE_EXTENSION << " in the current dir." << std::endl << std::endl;
}


sprite* create_text(SDL_Renderer* renderer, const char* txt, TTF_Font* font, SDL_Color color) {
	if (txt == NULL || font == NULL) return NULL;
	
	auto txt_surf = TTF_RenderText_Solid(font, txt, color);
	auto txt_texture = SDL_CreateTextureFromSurface(renderer, txt_surf);
	
	sprite* spr = new sprite;
	spr->tex = txt_texture;
	spr->w = txt_surf->w;
	spr->h = txt_surf->h;
	spr->x_offset = 0;
	spr->y_offset = 0;
	SDL_FreeSurface(txt_surf);
	return spr;
}

void update_rgb_text(SDL_Renderer* renderer, TTF_Font* font, SDL_Color color, sprite* &Rtext, sprite* &Gtext, sprite* &Btext) {
	if (renderer == NULL || font == NULL) return;
	free_sprite(Rtext);
	free_sprite(Gtext);
	free_sprite(Btext);
	SDL_Color white{255,255,255,255};
		
	std::string support = "R "+std::to_string(color.r);
	Rtext = create_text(renderer,support.c_str(),font,white);
	
	support = "G "+std::to_string(color.g);
	Gtext = create_text(renderer,support.c_str(),font,white);

	support = "B "+std::to_string(color.b);
	Btext = create_text(renderer,support.c_str(),font,white);
}

void draw_rect(SDL_Renderer* renderer, int x, int y, int w, int h, uint8_t r, uint8_t g, uint8_t b) {
	SDL_Rect rc{x,y,w,h};
	SDL_SetRenderDrawColor(renderer,r,g,b,255);
	SDL_RenderFillRect(renderer,&rc);
			

}

int main(int argc, char** argv) {

	if (argc == 1) { //executed without arguments
		notice_usage();
		return 0;
	}

	int video_init = SDL_Init(SDL_INIT_VIDEO);
	int png_init = -1;

	if (video_init < 0) {
		std::cout << "Failed to initializate video display: " << SDL_GetError() << std::endl;
		SDL_ClearError();
	}
	else {
		png_init = IMG_Init(IMG_INIT_PNG);
		if((png_init & IMG_INIT_PNG) != IMG_INIT_PNG) {
			std::cout << "IMG_Init: Failed to init PNG support." << std::endl;
			std::cout << "IMG_Init: " << IMG_GetError() << std::endl;
			// handle error
		}
	
	}


	if ((strcmp(argv[1],"-auto") == 0 && argc > 2) || (strcmp(argv[1],"-plt") == 0) && argc > 3) {

		palette *pal{NULL};
		unsigned char start_point = 2;

		if (strcmp(argv[1],"-plt") == 0) {

			start_point = 3;
			pal = load_palette(argv[2]);

			if (pal == NULL) {
				return -1;
			}

		}
		else {
			pal = new palette;
			pal->colors[0].r = 0;
			pal->colors[0].g = 255;
			pal->colors[0].b = 255;
			pal->max_index = 0;
		}

		for (unsigned int i = start_point; i < argc; i++) {
			//if (pal->max_index == 255) break;

			std::cout << "Palettizing " << argv[i] << std::endl;
			SDL_Surface* newSurface = IMG_Load(argv[i]);

			if (newSurface != NULL) {

				uint8_t* pixels = (uint8_t*)newSurface->pixels;
				uint8_t* found_indexes = new uint8_t[newSurface->w*newSurface->h];

				SDL_LockSurface(newSurface); 

				#ifdef VERBOSAL_MODE
					std::cout << "Surface format: " << std::endl;
					std::cout << "Rmask: " << std::bitset<32>(newSurface->format->Rmask) << std::endl;
					std::cout << "Gmask: " << std::bitset<32>(newSurface->format->Gmask) << std::endl;
					std::cout << "Bmask: " << std::bitset<32>(newSurface->format->Bmask) << std::endl;
					std::cout << "Amask: " << std::bitset<32>(newSurface->format->Amask) << std::endl;
					std::cout << "BPP: " << (uint16_t)newSurface->format->BytesPerPixel << std::endl;
				#endif
				uint32_t pixel_index = 0;
				for (uint32_t py = 0; py < newSurface->h; py++)
				for (uint32_t px = 0; px < newSurface->w; px++) {
					//AAAAAAAA BBBBBBBB GGGGGGGG RRRRRRRR
					//BBBBBBBB GGGGGGGG RRRRRRRR (missing A)
					//*
					//take the rgb/rgba value of the current pixel
					//since rgb takes 3 bytes and rgba takes 4,
					//it's assumed that the pixel will need 4 bytes (int32)
					//however if this wasn't the case, then the last, wrong byte
					//is masked to all 1s so that it will always match regardless
					//of what the next color was.
					
					uint32_t* pixel = (uint32_t*)&(pixels[py*newSurface->pitch + px*newSurface->format->BytesPerPixel]);
					//if (BytePP == 3) *pixel = (*pixel | 0x000000FF) >> 8;
					SDL_Color current_color;
					SDL_GetRGB(*pixel,newSurface->format,&current_color.r,&current_color.g,&current_color.b);
					
					//#ifdef VERBOSAL_MODEAA
						//std::cout << "Pixel " << px << " colors: " << (unsigned int)current_color.r << "," << (unsigned int)current_color.g << "," << (unsigned int)current_color.b << std::endl;
					//#endif

					bool found_match = false;
					if (current_color.r == 0 && current_color.g == 255 && current_color.b == 255) {
						found_match = true;
						found_indexes[pixel_index] = 0;				
					} 
					for (unsigned short pal_index = 0; pal_index <= pal->max_index; pal_index++) {
						if (found_match) break;

						
						if (pal->colors[pal_index].r == current_color.r && pal->colors[pal_index].g == current_color.g
						&& pal->colors[pal_index].b == current_color.b) {

							found_match = true;
							found_indexes[pixel_index] = pal_index;

						}
					}
					if (!found_match) {
						if (pal->max_index < 255) {
							pal->colors[pal->max_index+1] = current_color;
							pal->max_index++;
							found_indexes[pixel_index] = pal->max_index;
						}
						else {
							found_indexes[pixel_index] = 0;
						}
						
					}
					
					pixel_index++;
				}
				SDL_UnlockSurface(newSurface); 

				int w{newSurface->w},h{newSurface->h};
				SDL_FreeSurface(newSurface);
				newSurface = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0);

				SDL_LockSurface(newSurface); 

				pixels = (uint8_t*)newSurface->pixels;
				
				pixel_index = 0;
				for (uint32_t py = 0; py < newSurface->h; py++)
				for (uint32_t px = 0; px < newSurface->w; px++) {
					uint32_t* pixel = (uint32_t*)&pixels[py*newSurface->pitch + px*newSurface->format->BytesPerPixel];
					uint8_t G{200},B{0};
					if (found_indexes[pixel_index] == 0) {G = 255; B = 255;}
					*pixel = SDL_MapRGB(newSurface->format,found_indexes[pixel_index],G,B);
					pixel_index++;
				}

				SDL_UnlockSurface(newSurface); 
				delete[] found_indexes;
				char support_str[200] = "pl_";
				strcat(support_str,argv[i]);
				IMG_SavePNG(newSurface, support_str);
				SDL_FreeSurface(newSurface);
						
			} 
			else {
				std::cout << "Couldn't load image " << argv[i] << std::endl;
				return -1;
			}
		} //end for images

		save_palette(pal,AUTO_PALGEN_FILENAME PALETTE_EXTENSION);
		delete pal;
		return 0;
	}

	if (strcmp(argv[1],"-GUI") == 0 && argc == 4) {
		

		SDL_Window *gWindow = NULL;

		if (video_init >= 0) {
			gWindow = SDL_CreateWindow("PALETTE GENERATOR",
				                               SDL_WINDOWPOS_UNDEFINED,
				                               SDL_WINDOWPOS_UNDEFINED,
				                               SCREEN_WIDTH,
				                               SCREEN_HEIGHT,
				                               SDL_WINDOW_RESIZABLE);
			if (gWindow == NULL) {
				std::cout << "Failed to create window: " << SDL_GetError() << std::endl;
				SDL_ClearError();
			}

		}
		
		if (gWindow == NULL) {
			IMG_Quit();
			SDL_Quit();
			return -1;
		}

		SDL_Renderer *mainRender = SDL_CreateRenderer(gWindow,-1,SDL_RENDERER_ACCELERATED);
		SDL_RenderSetLogicalSize(mainRender,SCREEN_WIDTH,SCREEN_HEIGHT);

		if(TTF_Init() < 0) {
    		std::cout << "TTF_Init: " << TTF_GetError() << std::endl;
    		return -1;
		}	


		palette* edited_palette = load_palette(argv[3]);
		if (edited_palette == NULL) return -1;
		uint8_t currently_edited_index = 0;
		SDL_Color currently_edited_color = edited_palette->colors[currently_edited_index];
		SDL_Color new_edited_color = currently_edited_color;

		TTF_Font* font = TTF_OpenFont(FONT_PATH, 8);
		if (font == NULL) {
			std::cout << "Couldn't open " << FONT_PATH << std::endl;
			return -1;
		}
		
		sprite *edited_R{NULL}, *edited_G{NULL}, *edited_B{NULL};


		update_rgb_text(mainRender, font, currently_edited_color, edited_R, edited_G, edited_B);


		sprite* edited_sprite = create_sprite(mainRender, argv[2], 0, 0, edited_palette->colors);
		if (edited_sprite == NULL) return -1;
		edited_sprite->x_offset = edited_sprite->w/2;
		edited_sprite->y_offset = edited_sprite->h/2;
		
		SDL_Surface* indexed_surface = IMG_Load(argv[2]);
		if (indexed_surface == NULL) {
			std::cout << SDL_GetError() << std::endl;
			SDL_ClearError();
			return -1;
		}

		sprite* arrow = create_text(mainRender, "->", font, SDL_Color{255,255,255});


		double edited_sprite_scale = 1.0;		

		const int palette_section_x = 60+SCREEN_WIDTH/2;
		const int palette_section_y = 0;
		const int square_len = 18;
		const short palette_section_offset = 20;

		const short current_color_distance = 40;
		const short current_color_size = 64;
		const short new_color_dist = 120;

		const short text_distance = 30;
		const short text_offset_distance = 130;
		
		const uint8_t zoom_area_height = 125;
		
		const short img_xcoord = SCREEN_WIDTH/4;
		const short img_ycoord = SCREEN_HEIGHT/2;
		
		SDL_Rect displacement_area{300, SCREEN_HEIGHT-zoom_area_height+10,100,100};

		sprite* apply_txt = create_text(mainRender, "Apply", font, SDL_Color{0,0,0});
		apply_txt->x_offset = apply_txt->w/2;
		apply_txt->y_offset = apply_txt->h/2;

		SDL_Rect apply_button{240+palette_section_x+palette_section_offset,palette_section_y+current_color_distance+square_len*16,80,32};


		sprite* undo_txt = create_text(mainRender, "Undo", font, SDL_Color{0,0,0});
		undo_txt->x_offset = undo_txt->w/2;
		undo_txt->y_offset = undo_txt->h/2;
		SDL_Rect undo_button{240+palette_section_x+palette_section_offset,36+palette_section_y+current_color_distance+square_len*16,80,32};


		sprite* export_txt = create_text(mainRender, "Export palette", font, SDL_Color{0,0,0});
		export_txt->x_offset = export_txt->w/2;
		export_txt->y_offset = export_txt->h/2;
		SDL_Rect export_button{palette_section_x+palette_section_offset,210+palette_section_y+current_color_distance+square_len*16,200,32};

		sprite* export_png = create_text(mainRender, "Export PNG", font, SDL_Color{0,0,0});
		export_png->x_offset = export_png->w/2;
		export_png->y_offset = export_png->h/2;
		SDL_Rect export_png_button{palette_section_x+palette_section_offset,260+palette_section_y+current_color_distance+square_len*16,200,32};

		SDL_Color export_txt_color = SDL_Color{255,255,255,255};
		SDL_Color export_png_color = SDL_Color{255,255,255,255};

		uint8_t undo_index = currently_edited_index;
		SDL_Color undo_color = currently_edited_color;

		bool can_undo = false;



		SDL_Rect rgb_slider_area[3];
		for (uint8_t sl = 0; sl < 3; sl++) {
			rgb_slider_area[sl] = SDL_Rect{palette_section_x+100,-26+palette_section_y+palette_section_offset+square_len*16+text_offset_distance+text_distance*sl,
									256,26};
		}

		int mouse_x,mouse_y;
		bool mouse_held = false;
		bool mouse_pressed = false; 
		bool quit = false;		
		//SDL_CaptureMouse(SDL_TRUE);

		double logic_aspect_ratio = (double)SCREEN_WIDTH/(double)SCREEN_HEIGHT;

		int window_w,window_h;	
		
		while (!quit) {
			
			SDL_Event e;
			while (SDL_PollEvent(&e)) {
			
				switch(e.type) {
					case SDL_QUIT:
						quit = true;
					break;

					default: break;

					case SDL_MOUSEBUTTONDOWN:
						if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) {
							mouse_held = true;
							mouse_pressed = true;
						}
					break;

					case SDL_MOUSEBUTTONUP:
						if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) {
							mouse_held = false;
						}
					break;

				}

			}
				
								
			uint32_t mouse_state = SDL_GetMouseState(&mouse_x,&mouse_y);
			SDL_GetWindowSize(gWindow, &window_w, &window_h);
			double window_aspect_ratio = (double)window_w/(double)window_h;

			if (window_aspect_ratio >= logic_aspect_ratio) { //longer window horizontally, thus the vertical side is fit to the screen's height
				mouse_y = (int)(SCREEN_HEIGHT*((double)mouse_y/(double)window_h));
				double screen_scale = (double)window_h/(double)SCREEN_HEIGHT;
				int black_border = (int)std::floor((double)window_w - (screen_scale*(double)SCREEN_WIDTH))/2;
				mouse_x = (int)std::max(mouse_x - black_border, 0);
				mouse_x = std::min(mouse_x,window_w - black_border); //clamp mouse in between non blackbar space. Mouse 0 starts from where the screen is drawn
				mouse_x = (int)(SCREEN_WIDTH*((double)mouse_x/(double)(window_w - 2*black_border)));				
			}
			else {
				mouse_x = (int)(SCREEN_WIDTH*((double)mouse_x/(double)window_w));
				double screen_scale = (double)window_w/(double)SCREEN_WIDTH;
				int black_border = (int)std::floor((double)window_h - (screen_scale*(double)SCREEN_HEIGHT))/2;
				mouse_y = (int)std::max(mouse_y - black_border, 0);
				mouse_y = std::min(mouse_y,window_h - black_border); //clamp mouse in between non blackbar space. Mouse 0 starts from where the screen is drawn
				mouse_y = (int)(SCREEN_HEIGHT*((double)mouse_y/(double)(window_h - 2*black_border)));				
			}

			SDL_Rect slider_area{20,SCREEN_HEIGHT-80,200,32};


			if (mouse_held) {
				//check mouse inside slider
				if (mouse_x >= slider_area.x && mouse_x <= slider_area.x+slider_area.w
					&&
					mouse_y >= slider_area.y && mouse_y <= slider_area.y+slider_area.h) {
					
					//scaler goes from SLIDER_MIN to SLIDER_RANGE, so range is 3.5(slider pos) + 0.5(defacto)
					edited_sprite_scale = SLIDER_MIN+SLIDER_RANGE*(double)(mouse_x-slider_area.x)/(double)slider_area.w;
					//cheap clamp
					if (edited_sprite_scale < SLIDER_MIN)   edited_sprite_scale  = SLIDER_MIN;
					if (edited_sprite_scale > SLIDER_RANGE+SLIDER_MIN) edited_sprite_scale  = SLIDER_RANGE+SLIDER_MIN;
				}

				bool need_update = false;
				if (currently_edited_index > 0)
				for (uint8_t sl = 0; sl < 3; sl++) {
					if (mouse_x >= rgb_slider_area[sl].x && mouse_x <= rgb_slider_area[sl].x+rgb_slider_area[sl].w
						&&
						mouse_y >= rgb_slider_area[sl].y && mouse_y <= rgb_slider_area[sl].y+rgb_slider_area[sl].h) {
						need_update = true;
					
						switch(sl) {
							case 0: {
								uint16_t clamp = mouse_x-rgb_slider_area[sl].x;
								if (clamp > 255) clamp = 255;
								new_edited_color.r = (uint8_t)clamp;	
							}
							break;		
							case 1: {
								uint16_t clamp = mouse_x-rgb_slider_area[sl].x;
								if (clamp > 255) clamp = 255;
								new_edited_color.g = (uint8_t)clamp;	
							}
							break;	
							case 2: {
								uint16_t clamp = mouse_x-rgb_slider_area[sl].x;
								if (clamp > 255) clamp = 255;
								new_edited_color.b = (uint8_t)clamp;	
							}
							break;					
						}
					}
					
				}
				if (need_update) update_rgb_text(mainRender, font, new_edited_color, edited_R, edited_G, edited_B);
				
				
				
				
				if (mouse_x >= displacement_area.x && mouse_x <= displacement_area.x+displacement_area.w
					&&
					mouse_y >= displacement_area.y && mouse_y <= displacement_area.y+displacement_area.h)  {
					
					edited_sprite->x_offset = (int)(edited_sprite->w * ((double)(mouse_x - displacement_area.x)/(double)displacement_area.w));
					edited_sprite->y_offset = (int)(edited_sprite->h * ((double)(mouse_y - displacement_area.y)/(double)displacement_area.h));
				
				}

			}

			if (mouse_pressed) {
				//check mouse inside palette selector
				int xOri{palette_section_x+palette_section_offset},yOri{palette_section_y+palette_section_offset};
				if (mouse_x >= xOri && mouse_x <= xOri+16*square_len
					&&
					mouse_y >= yOri && mouse_y <= yOri+16*square_len) {	
					
					double xpos,ypos;
					xpos = (mouse_x-xOri)/square_len;
					ypos = (mouse_y-yOri)/square_len;
					xpos = std::floor(xpos);
					ypos = std::floor(ypos);

					uint8_t index = 16*(int)ypos + (int)xpos;
					if (index <= edited_palette->max_index) { //clicked a valid (defined) palette index
						currently_edited_index = index;
						currently_edited_color = edited_palette->colors[currently_edited_index];
						new_edited_color = currently_edited_color;
						update_rgb_text(mainRender, font, currently_edited_color, edited_R, edited_G, edited_B);
					}
					
				}	



				if ((mouse_x >= apply_button.x && mouse_x <= apply_button.x+apply_button.w
					&&
					mouse_y >= apply_button.y && mouse_y <= apply_button.y+apply_button.h)
					&& currently_edited_index > 0) {	
					
					int prev_x_offset,prev_y_offset;
					prev_x_offset = edited_sprite->x_offset;
					prev_y_offset = edited_sprite->y_offset;
					
					undo_color = edited_palette->colors[currently_edited_index];
					undo_index = currently_edited_index;
					edited_palette->colors[currently_edited_index] = new_edited_color;
					currently_edited_color = new_edited_color;
					free_sprite(edited_sprite);
					edited_sprite = create_sprite(mainRender, argv[2], 0, 0, edited_palette->colors);
					edited_sprite->x_offset = prev_x_offset;
					edited_sprite->y_offset = prev_y_offset;			
					can_undo = true;

					export_txt_color = SDL_Color{255,255,255,255};
					export_png_color = SDL_Color{255,255,255,255};
					
				}	


				if ((mouse_x >= undo_button.x && mouse_x <= undo_button.x+undo_button.w
					&&
					mouse_y >= undo_button.y && mouse_y <= undo_button.y+undo_button.h) 
					&& can_undo) {	
					
					int prev_x_offset,prev_y_offset;
					prev_x_offset = edited_sprite->x_offset;
					prev_y_offset = edited_sprite->y_offset;
					
					
					edited_palette->colors[undo_index] = undo_color;
					currently_edited_color = edited_palette->colors[currently_edited_index];
					new_edited_color = currently_edited_color;
					update_rgb_text(mainRender, font, currently_edited_color, edited_R, edited_G, edited_B);
					free_sprite(edited_sprite);
					edited_sprite = create_sprite(mainRender, argv[2], 0, 0, edited_palette->colors);
					edited_sprite->x_offset = prev_x_offset;
					edited_sprite->y_offset = prev_y_offset;
					can_undo = false;

					export_txt_color = SDL_Color{255,255,255,255};
					export_png_color = SDL_Color{255,255,255,255};
					
				}	



				if (mouse_x >= export_button.x && mouse_x <= export_button.x+export_button.w
					&&
					mouse_y >= export_button.y && mouse_y <= export_button.y+export_button.h)  {	

					save_palette(edited_palette,CUSTOM_PALGEN_FILENAME PALETTE_EXTENSION);
					export_txt_color = SDL_Color{100,100,100,255};

				}	


				if (mouse_x >= export_png_button.x && mouse_x <= export_png_button.x+export_png_button.w
					&&
					mouse_y >= export_png_button.y && mouse_y <= export_png_button.y+export_png_button.h)  {	
			
					SDL_Surface* pngSurf = load_palettized_surface(argv[2], edited_palette->colors);
					char support_str[200] = "custom_";
					strcat(support_str,argv[2]);
					IMG_SavePNG(pngSurf, support_str);
					export_png_color = SDL_Color{100,100,100,255};
					SDL_FreeSurface(pngSurf);
					
				}	
				
				
				//select color from the image itself
				if (mouse_x >= 0 && mouse_x <= palette_section_x
					&&
					mouse_y >= 0 && mouse_y <= SCREEN_HEIGHT-zoom_area_height)  {				
				
					int img_origin_x, img_origin_y;
					//draw_sprite() draw coords for the edited image: 
					//edited_sprite_scale
					img_origin_x = img_xcoord - (edited_sprite->x_offset * edited_sprite_scale);
					img_origin_y = img_ycoord - (edited_sprite->y_offset * edited_sprite_scale);
					
					int xpixel, ypixel;
					xpixel = std::floor((double)(mouse_x - img_origin_x)/edited_sprite_scale);
					ypixel = std::floor((double)(mouse_y - img_origin_y)/edited_sprite_scale);					
					
					if (xpixel >= 0 && xpixel < indexed_surface->w 
						&&
						ypixel >= 0 && ypixel < indexed_surface->h) {
							
						SDL_LockSurface(indexed_surface);
						
						uint8_t* pixels = (uint8_t*)indexed_surface->pixels;			
						
						uint32_t* pixel = (uint32_t*)&(pixels[ypixel*indexed_surface->pitch + xpixel*indexed_surface->format->BytesPerPixel]);
						
						SDL_Color picked_color;
						SDL_GetRGB(*pixel,indexed_surface->format,&picked_color.r,&picked_color.g,&picked_color.b);					
						
						currently_edited_index = picked_color.r;
						currently_edited_color = edited_palette->colors[currently_edited_index];
						new_edited_color = currently_edited_color;
						update_rgb_text(mainRender, font, currently_edited_color, edited_R, edited_G, edited_B);						
						
						SDL_UnlockSurface(indexed_surface);
						
							
					} 
				}

			}
			
			//---------------------------------------------------------------- DRAW SECTION
			
			SDL_Color soft_color{60,60,60,255};

			SDL_SetRenderDrawColor(mainRender,0,0,0,0);//fill whole screen with black pixels
			SDL_RenderClear(mainRender);
			
			{
			SDL_Rect rc{0,0,SCREEN_WIDTH,SCREEN_HEIGHT};//fill actual room space with light gray
			SDL_SetRenderDrawColor(mainRender,0,255,255,255);//200,200,200,255);
			SDL_RenderFillRect(mainRender,&rc);
			}
			
			//currently edited sprite
			draw_sprite(mainRender, edited_sprite, img_xcoord,img_ycoord,edited_sprite_scale,edited_sprite_scale,0.0,0,0);

			draw_rect(mainRender, 0, SCREEN_HEIGHT-zoom_area_height,  palette_section_x, zoom_area_height, soft_color.r,soft_color.g,soft_color.b);
			SDL_SetRenderDrawColor(mainRender,255,255,255,255);
			SDL_RenderFillRect(mainRender,&displacement_area);
			
			SDL_SetRenderDrawColor(mainRender,0,0,0,255);
			SDL_RenderFillRect(mainRender,&slider_area);
			
			draw_rect(mainRender,
					(int)(slider_area.x+slider_area.w*(float)(edited_sprite_scale/(SLIDER_RANGE+SLIDER_MIN)))-8,
					-16+slider_area.y+slider_area.h/2,16,32, 255, 255, 255);	

		

			draw_rect(mainRender, palette_section_x,0,SCREEN_WIDTH-palette_section_x,SCREEN_HEIGHT, soft_color.r,soft_color.g,soft_color.b);
			
			//if (false)
			for (short ypalette = 0; ypalette < 16; ypalette++)
			for (short xpalette = 0; xpalette < 16; xpalette++) {
				uint8_t index = 16*ypalette + xpalette;
				if (index > edited_palette->max_index) break;
				SDL_Color baseColor{255,255,255};
							
				if (currently_edited_index == index) baseColor = SDL_Color{0,255,255};

				int xOri{palette_section_offset+palette_section_x},yOri{palette_section_offset+palette_section_y};
				SDL_Color frontColor = edited_palette->colors[index];

				draw_rect(mainRender, xOri+square_len*xpalette,yOri+square_len*ypalette,square_len-2,square_len-2, 
					baseColor.r, baseColor.g ,baseColor.b);	

				draw_rect(mainRender, 2+xOri+square_len*xpalette,2+yOri+square_len*ypalette,square_len-4,square_len-4, 
					frontColor.r, frontColor.g ,frontColor.b);									

			}

			//show currently edited color

			draw_rect(mainRender, -4+palette_section_x+palette_section_offset,-4+palette_section_y+current_color_distance+square_len*16,8+current_color_size,8+current_color_size,
					255,255,255);

			draw_rect(mainRender, palette_section_x+palette_section_offset,palette_section_y+current_color_distance+square_len*16,current_color_size,current_color_size,
					currently_edited_color.r,currently_edited_color.g,currently_edited_color.b);


			draw_rect(mainRender, new_color_dist-4+palette_section_x+palette_section_offset,-4+palette_section_y+current_color_distance+square_len*16,8+current_color_size,8+current_color_size,
					255,255,255);

			draw_rect(mainRender, new_color_dist+palette_section_x+palette_section_offset,palette_section_y+current_color_distance+square_len*16,current_color_size,current_color_size,
					new_edited_color.r,new_edited_color.g,new_edited_color.b);

			draw_sprite(mainRender, arrow, 80+palette_section_x+palette_section_offset,24+palette_section_y+current_color_distance+square_len*16,2.0,2.0,0.0,0,0);


			draw_sprite(mainRender, edited_R, palette_section_x+palette_section_offset,palette_section_y+text_offset_distance+square_len*16+text_distance*0,2.0,2.0,0.0,0,0);
			draw_sprite(mainRender, edited_G, palette_section_x+palette_section_offset,palette_section_y+text_offset_distance+square_len*16+text_distance*1,2.0,2.0,0.0,0,0);
			draw_sprite(mainRender, edited_B, palette_section_x+palette_section_offset,palette_section_y+text_offset_distance+square_len*16+text_distance*2,2.0,2.0,0.0,0,0);

			if (currently_edited_index > 0)
			for (uint8_t sl = 0; sl < 3; sl++) {
				uint8_t slider_pos;
				switch(sl) {
					case 0:
						slider_pos = new_edited_color.r;
					break;
					case 1:
						slider_pos = new_edited_color.g;
					break;
					case 2:
						slider_pos = new_edited_color.b;
					break;
				}
				draw_rect(mainRender, rgb_slider_area[sl].x, rgb_slider_area[sl].y, rgb_slider_area[sl].w, rgb_slider_area[sl].h, 255, 255, 255);
				draw_rect(mainRender, slider_pos+rgb_slider_area[sl].x, rgb_slider_area[sl].y, 16, rgb_slider_area[sl].h, 0, 0, 0);
			}

			if (currently_edited_index > 0) {
			
				draw_rect(mainRender, apply_button.x, apply_button.y, apply_button.w, apply_button.h, 255,255,255);	
				draw_sprite(mainRender, apply_txt, apply_button.x+apply_button.w/2, apply_button.y+apply_button.h/2,2.0,2.0,0.0,0,0);

			}

			if (can_undo) {

				draw_rect(mainRender, undo_button.x, undo_button.y, undo_button.w, undo_button.h, 255,255,255);	
				draw_sprite(mainRender, undo_txt, undo_button.x+undo_button.w/2, undo_button.y+undo_button.h/2,2.0,2.0,0.0,0,0);
			}

			draw_rect(mainRender, export_button.x, export_button.y, export_button.w, export_button.h, export_txt_color.r,export_txt_color.g,export_txt_color.b);	
			draw_sprite(mainRender, export_txt, export_button.x+export_button.w/2, export_button.y+export_button.h/2,2.0,2.0,0.0,0,0);

			draw_rect(mainRender, export_png_button.x, export_png_button.y, export_png_button.w, export_png_button.h, export_png_color.r,export_png_color.g,export_png_color.b);	
			draw_sprite(mainRender, export_png, export_png_button.x+export_png_button.w/2, export_png_button.y+export_png_button.h/2,2.0,2.0,0.0,0,0);
	

			mouse_pressed = false;
		
			SDL_RenderPresent(mainRender);
			SDL_UpdateWindowSurface(gWindow);

			
			SDL_Delay(MS_PER_FRAME);
		
		} //end main while

		free_sprite(export_txt);
		free_sprite(undo_txt);
		free_sprite(apply_txt);
		free_sprite(arrow);
		free_sprite(edited_R);
		free_sprite(edited_G);
		free_sprite(edited_B);
		free_sprite(edited_sprite);
		SDL_FreeSurface(indexed_surface);
		TTF_CloseFont(font);


		TTF_Quit();
		IMG_Quit();
		SDL_Quit();

		return 0;	
	}	

	TTF_Quit();
	IMG_Quit();
	SDL_Quit();

	notice_usage();	
	return 0;
}