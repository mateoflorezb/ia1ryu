#include "palette_engine.h"
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>

#define TRANSPARENCY_COLOR_R 0
#define TRANSPARENCY_COLOR_G 255
#define TRANSPARENCY_COLOR_B 255

palette_engine::sprite* palette_engine::create_sprite(SDL_Renderer* renderer, const char* filename, int xoff, int yoff) {
    SDL_Surface* newSurface = IMG_Load(filename);
    SDL_Texture* newTexture = NULL;
	palette_engine::sprite* spr = NULL;

    if (newSurface != NULL) {

    	newTexture = SDL_CreateTextureFromSurface(renderer,newSurface);
		spr = new palette_engine::sprite{newTexture,xoff,yoff,newSurface->w,newSurface->h};
		SDL_FreeSurface(newSurface);
	}
	else {
		std::cout << SDL_GetError() << std::endl;
		SDL_ClearError();
	}
	
	return spr;
}


SDL_Surface* palette_engine::load_palettized_surface(const char* filename, SDL_Color* palette) {
    SDL_Surface* newSurface = IMG_Load(filename);

    if (newSurface != NULL) {	

		SDL_LockSurface(newSurface); ///////////
	
		SDL_Surface* lowdepthSurface = SDL_CreateRGBSurface(0,newSurface->w,newSurface->h,8,0,0,0,0);
		uint8_t* lowdepth_indexes = new uint8_t[newSurface->w*newSurface->h];

		

		//check this in case this function doesn't work on other OS's
		/*
		std::cout << "Surface format: " << std::endl;
		std::cout << "Rmask: " << std::bitset<32>(newSurface->format->Rmask) << std::endl;
		std::cout << "Gmask: " << std::bitset<32>(newSurface->format->Gmask) << std::endl;
		std::cout << "Bmask: " << std::bitset<32>(newSurface->format->Bmask) << std::endl;
		std::cout << "Amask: " << std::bitset<32>(newSurface->format->Amask) << std::endl;
		std::cout << "Bytes per pixel: " << (unsigned int)newSurface->format->BytesPerPixel << std::endl;
		*/

		uint32_t pixel_index = 0;
		
		uint8_t* pixels = (uint8_t*)newSurface->pixels;

		//w x h to get # pixels
		for (uint32_t py = 0; py < newSurface->h; py++)
		for (uint32_t px = 0; px < newSurface->w; px++) {
			
			uint32_t* pixel = (uint32_t*)&(pixels[py*newSurface->pitch + px*newSurface->format->BytesPerPixel]);
			SDL_Color current_color;
			SDL_GetRGB(*pixel,newSurface->format,&current_color.r,&current_color.g,&current_color.b);
			lowdepth_indexes[pixel_index] = current_color.r;
			pixel_index++;
		} 
		
		SDL_UnlockSurface(newSurface); ///////////

		///////////
	
		SDL_LockSurface(lowdepthSurface); ///////////
			
			//wipe the image to all 0s
			memset(lowdepthSurface->pixels, 0, lowdepthSurface->h * lowdepthSurface->pitch);
	
			pixel_index = 0;
			pixels = (uint8_t*)lowdepthSurface->pixels;
			for (uint32_t py = 0; py < newSurface->h; py++)
			for (uint32_t px = 0; px < newSurface->w; px++) {
				
				pixels[py*lowdepthSurface->pitch + px*lowdepthSurface->format->BytesPerPixel] = lowdepth_indexes[pixel_index];
				pixel_index++;
			}
	
		SDL_UnlockSurface(lowdepthSurface); ///////////

		delete[] lowdepth_indexes;

		SDL_Color fixed_palette[256];
		for (short i = 1; i < 256; i++) {
			fixed_palette[i] = palette[i];
		}
		fixed_palette[0].r = TRANSPARENCY_COLOR_R;
		fixed_palette[0].g = TRANSPARENCY_COLOR_G;
		fixed_palette[0].b = TRANSPARENCY_COLOR_B;
		

		SDL_SetPaletteColors(lowdepthSurface->format->palette,(const SDL_Color*) fixed_palette,0,256);

		//had to create a new surface instead of using the already created "newSurface"
		//otherwise it didn't work for some reason idk.
		SDL_Surface* highdepthColorized = SDL_CreateRGBSurface(0,lowdepthSurface->w,lowdepthSurface->h,32,0,0,0,0);
		SDL_Rect dest{0,0,lowdepthSurface->w,lowdepthSurface->h};
		SDL_BlitSurface(lowdepthSurface,NULL,highdepthColorized,&dest);

		
		SDL_SetColorKey(highdepthColorized,SDL_TRUE,SDL_MapRGB(highdepthColorized->format,
																TRANSPARENCY_COLOR_R,
																TRANSPARENCY_COLOR_G,
																TRANSPARENCY_COLOR_B)); 

	

		
		SDL_FreeSurface(newSurface);
		SDL_FreeSurface(lowdepthSurface);
		newSurface = highdepthColorized;
	}
	else {
		std::cout << SDL_GetError() << std::endl;
		SDL_ClearError();
	}
	
	return newSurface;



}

palette_engine::sprite* palette_engine::create_sprite(SDL_Renderer* renderer, const char* filename, int xoff, int yoff, SDL_Color* palette) {
    SDL_Surface* newSurface = palette_engine::load_palettized_surface(filename, palette);
    SDL_Texture* newTexture = NULL;
	palette_engine::sprite* spr = NULL;

    if (newSurface != NULL) {	

		newTexture = SDL_CreateTextureFromSurface(renderer,newSurface);	
		spr = new palette_engine::sprite{newTexture,xoff,yoff,newSurface->w,newSurface->h};
		SDL_FreeSurface(newSurface);
	}
	else {
		std::cout << SDL_GetError() << std::endl;
		SDL_ClearError();
	}
	
	return spr;
}


void palette_engine::free_sprite(palette_engine::sprite* spr) {
	if (spr != NULL) {
		SDL_DestroyTexture(spr->tex);
		delete spr;
		spr = NULL;
	}	
}


int palette_engine::draw_sprite(SDL_Renderer* renderer,
palette_engine::sprite* spr, int32_t x, int32_t y, double xscale, double yscale, double angle, int32_t view_x, int32_t view_y) {
	//view x,y represent the top left corner of the camera.
	
	if (spr == NULL) return -1;	
	
	int w{spr->w},h{spr->h};
	w = (int)(w*std::abs(xscale));
	h = (int)(h*std::abs(yscale));
	

	int xCenter{spr->x_offset},yCenter{spr->y_offset};
	xCenter = (int)(xCenter*std::abs(xscale));
	yCenter = (int)(yCenter*std::abs(yscale));	
	
	SDL_RendererFlip flip = SDL_FLIP_NONE;

	if (xscale < 0) {
		xscale =-xscale;
		flip = (SDL_RendererFlip)(flip | SDL_FLIP_HORIZONTAL);
		x += xCenter - (w-xCenter);
	}

	if (yscale < 0) {
		yscale =-yscale;
		flip = (SDL_RendererFlip)(flip | SDL_FLIP_VERTICAL);
		y += yCenter - (h-yCenter);
	}

	SDL_Rect dest{x-xCenter-view_x, y-yCenter-view_y,w,h};
	const SDL_Point center{xCenter,yCenter};
	

	return SDL_RenderCopyEx(renderer,
                     spr->tex,
                     NULL,
                     (const SDL_Rect*) &dest,
                     (const double) -angle,
                     &center,
                     flip);
}


int palette_engine::draw_sprite(SDL_Renderer* renderer, palette_engine::sprite* spr, int x, int y, double xscale, double yscale, double angle, uint8_t alpha, int view_x, int view_y) {
	if (spr->tex == NULL) return -1;		
	SDL_SetTextureAlphaMod(spr->tex, alpha);
	auto rt = draw_sprite(renderer, spr, x, y, xscale, yscale, angle, view_x, view_y);
	SDL_SetTextureAlphaMod(spr->tex, 255);
	return rt;
}



bool palette_engine::save_palette(palette_engine::palette* pal, const char* filename) {
	if (pal == NULL)
	return false;

	std::ofstream file(filename, std::ios::out | std::ios::binary);
	if (!file) {
		std::cout << "Couldn't open file " << filename << std::endl;
		return false;
	}

	file.write((char*)&pal->max_index,1); //write the number of defined colors

	for (short i = 1; i <= pal->max_index; i++) {
		file.write((char*)&pal->colors[i],sizeof(SDL_Color));

	}

	if(!file.good()) {
		std::cout << "Error occurred while writing " << filename << std::endl;
		return false;
	}

	file.close();
	return true;
	
}


palette_engine::palette* palette_engine::load_palette(const char* filename) {
	std::ifstream file(filename, std::ios::out | std::ios::binary);
	if(!file) {
		std::cout << "Couldn't open file " << filename << std::endl;
		return NULL;
	}
	
	palette_engine::palette* pal = new palette_engine::palette;

	file.read((char*)&pal->max_index,1); //read the number of defined colors

	for (short i = 1; i <= pal->max_index; i++) {
		file.read((char*)&pal->colors[i],sizeof(SDL_Color));

	}

	if(!file.good()) {
		std::cout << "Error occurred while reading " << filename << std::endl;
		delete pal;
		return NULL;
	}

	file.close();
	pal->colors[0].r = 0;
	pal->colors[0].g = 255;
	pal->colors[0].b = 255;
	return pal;
	
}