/*

struct box {
	int16_t x,y;
	uint16_t w,h;
};

frame file format:
C-style string terminated in NULL (sprite name)

these can be "null" terminated (if w == 0, it's assumed that's the end of the array)
so not all of them will necessarily be defined/stored in the file
box[10] hurtboxes
box[10] hitboxes

int32 x center
int32 y center

*/

#include "SDL.h"
#include "palette_engine.h"
#include <iostream>
#include "SDL_image.h"
#include "SDL_ttf.h"
#include <cstring>
#include <fstream>
#include <cmath>
#include "box_engine.h"

#define SCREEN_WIDTH 880
#define SCREEN_HEIGHT 660

#define MAX_SPRITE_NAME 80
#define MAX_BOXES 10

#define NEW_FRAME_OUT "new_frame_out"
#define FRAME_EXTENSION ".bxg"

#define MS_PER_FRAME 16

#define FONT_PATH ((std::string)SDL_GetBasePath()+"data/dogicapixel.ttf").c_str()


using namespace palette_engine;
using namespace bk_box_engine;

enum WINDOW_TYPE {
	WDW_HURTBOXES = 0,
	WDW_HITBOXES//,
	//WDW_MISC
	
};

struct button {
	sprite* txt;
	SDL_Rect area;
};


void notice_usage() {
	std::cout <<
	"box_gen   hit/hurt box generator for Dynamic Hitline"
	"\n\n" "usage:"
	"\n\n" "box_gen -new <palette> <sprite>"
	"\n" "Create a new frame. Once defined, it can be saved as " NEW_FRAME_OUT FRAME_EXTENSION
	"\n\n" "box_gen -edit <palette> <frames...>"
	"\n" "Edit one or more existing frames. They can be saved as their original filenames."
	"\n";
}


void draw_rect(SDL_Renderer* renderer, int x, int y, int w, int h, uint8_t r, uint8_t g, uint8_t b) {
	SDL_Rect rc{x,y,w,h};
	SDL_SetRenderDrawColor(renderer,r,g,b,255);
	SDL_RenderFillRect(renderer,&rc);		

}

void draw_rect(SDL_Renderer* renderer, int x, int y, int w, int h, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
	SDL_Rect rc{x,y,w,h};
	SDL_SetRenderDrawColor(renderer,r,g,b,a);
	SDL_RenderFillRect(renderer,&rc);		

}

void draw_rect(SDL_Renderer* renderer, const SDL_Rect rect, uint8_t r, uint8_t g, uint8_t b) {
	SDL_SetRenderDrawColor(renderer,r,g,b,255);
	SDL_RenderFillRect(renderer,&rect);		

}


void draw_rect(SDL_Renderer* renderer, const SDL_Rect rect, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
	SDL_SetRenderDrawColor(renderer,r,g,b,a);
	SDL_RenderFillRect(renderer,&rect);		

}

void draw_box(SDL_Renderer* renderer, const SDL_Rect rect, uint8_t r, uint8_t g, uint8_t b) {
	SDL_SetRenderDrawColor(renderer,r,g,b,255);
	SDL_RenderDrawLine(renderer,rect.x,rect.y,rect.x+rect.w,rect.y);
	SDL_RenderDrawLine(renderer,rect.x,rect.y+rect.h,rect.x+rect.w,rect.y+rect.h);
	SDL_RenderDrawLine(renderer,rect.x,rect.y,rect.x,rect.y+rect.h);
	SDL_RenderDrawLine(renderer,rect.x+rect.w,rect.y,rect.x+rect.w,rect.y+rect.h);
}

bool point_in_rect(SDL_Rect r, int x, int y) {
	if (x >= r.x && x <= r.x + r.w &&
		y >= r.y && y <= r.y + r.h)
		return true;
		return false;
	
}

sprite* create_text(SDL_Renderer* renderer, const char* txt, TTF_Font* font, SDL_Color color) {
	if (txt == NULL || font == NULL) return NULL;
	
	auto txt_surf = TTF_RenderText_Solid(font, txt, color);
	auto txt_texture = SDL_CreateTextureFromSurface(renderer, txt_surf);
	
	sprite* spr = new sprite;
	spr->tex = txt_texture;
	spr->w = txt_surf->w;
	spr->h = txt_surf->h;
	spr->x_offset = spr->w/2;
	spr->y_offset = spr->h/2;
	SDL_FreeSurface(txt_surf);
	return spr;
}


button* create_button(SDL_Renderer* renderer, const char* txt, TTF_Font* font, int x, int y) {
	sprite* spr_txt = create_text(renderer,txt,font,SDL_Color{0,0,0,255});
	if (spr_txt == NULL) return NULL;
	button* b = new button;
	b->txt = spr_txt;
	b->area = SDL_Rect{x,y,10+2*b->txt->w,10+2*b->txt->h};
	return b;
}

void draw_button(SDL_Renderer* renderer, button* btn, uint8_t r, uint8_t g, uint8_t b) {
	if (btn == NULL) return;
	draw_rect(renderer,btn->area,r,g,b);
	draw_sprite(renderer, btn->txt, btn->area.x+btn->area.w/2, btn->area.y+btn->area.h/2, 2.0, 2.0, 0.0, 0, 0);
	
}

void update_current_box_txt(SDL_Renderer* renderer, TTF_Font* font, uint8_t num, sprite* &cbt) {
	free_sprite(cbt);
	cbt = create_text(renderer,("box: "+std::to_string(num)).c_str(), font, SDL_Color{255,255,255,255});
	cbt->x_offset = 0;
	cbt->y_offset = 0;
}

void update_sprite_dimentions_txt(SDL_Renderer* renderer, TTF_Font* font, sprite* img, sprite* &cbt) {
	free_sprite(cbt);
	if (img == NULL) return;
	cbt = create_text(renderer,("w = "+std::to_string(img->w)+"\n"+
								"h = "+std::to_string(img->h)+"\n"+	
								"x = "+std::to_string(img->x_offset)+"\n"+
								"y = "+std::to_string(img->y_offset)).c_str(), font, SDL_Color{255,255,255,255});
	cbt->x_offset = 0;
	cbt->y_offset = 0;
}

void update_frame_label_txt(SDL_Renderer* renderer, TTF_Font* font, sprite* &cbt, uint8_t current, uint8_t max) {
	free_sprite(cbt);
	cbt = create_text(renderer,(std::to_string(current+1)+"/"+std::to_string(max)).c_str(), font, SDL_Color{255,255,255,255});
}


void update_pushbox_label(SDL_Renderer* renderer, TTF_Font* font, sprite* &cbt, uint16_t w, uint16_t h) {
	free_sprite(cbt);
	cbt = create_text(renderer,("pushbox (w: "+std::to_string(w)+" ; h: "+std::to_string(h)+")").c_str(), font, SDL_Color{255,255,255,255});
	cbt->x_offset = cbt->w;
}


int main (int argc, char** argv) {
	
	frame_file* working_frames;
	sprite** working_sprites;
	palette* working_pal;
	
	uint8_t n_frames = 0; //number of working frames
	uint8_t current_frame = 0;
	
	if (argc == 1) {
		notice_usage();
		return 0;
	}
	
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		std::cout << "SDL_Init: " << SDL_GetError() << std::endl;
		return -1;
	}

	if((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
		std::cout << "IMG_Init: " << IMG_GetError() << std::endl;
		return -1;
	}
	
	
	if(TTF_Init() < 0) {
		std::cout << "TTF_Init: " << TTF_GetError() << std::endl;
		return -1;
	}	
	
	TTF_Font* font = TTF_OpenFont(FONT_PATH, 8);
	if (font == NULL) {
		std::cout << "Couldn't open " << FONT_PATH << std::endl;
		return -1;
	}
	
	
	bool new_frame = true;
	
	if ((strcmp(argv[1],"-new")  == 0 && argc == 4)
	 || (strcmp(argv[1],"-edit") == 0 && argc >= 4)) {
		 
		 working_pal = load_palette(argv[2]);
		 if (working_pal == NULL) return -1;
		
		if (strcmp(argv[1],"-new")  == 0) {
			n_frames = 1;
			working_frames = new frame_file[n_frames];
			working_sprites = new sprite*[n_frames];
			for (uint8_t i = 0; i < MAX_BOXES; i++) {
				working_frames[0].hurtboxes[i] = box{0,0,0,0};
				working_frames[0].hitboxes[i]  = box{0,0,0,0};
			}
			
			strcpy(working_frames[0].sprite_name, argv[3]);
			working_frames[0].xcenter = 0;
			working_frames[0].ycenter = 0;
			
		}
		else { //then we are on edit mode
			new_frame = false;
			n_frames = argc - 3;
			working_frames = new frame_file[n_frames];
			working_sprites = new sprite*[n_frames];
			
			for (uint8_t i = 0; i < n_frames; i++) {
				frame_file* new_frame = load_frame(argv[3+i]);
				if (new_frame == NULL) return -1;
				memcpy(&working_frames[i],new_frame,sizeof(frame_file));
				delete new_frame;
			}
			
		}
			
	}
	else {
		notice_usage();
		TTF_Quit();
		IMG_Quit();
		SDL_Quit();
		return 0;
	}
	
	SDL_Window* window = SDL_CreateWindow("BOX GENERATOR",
				                               SDL_WINDOWPOS_UNDEFINED,
				                               SDL_WINDOWPOS_UNDEFINED,
				                               SCREEN_WIDTH,
				                               SCREEN_HEIGHT,
				                               SDL_WINDOW_RESIZABLE);
											   
	if (window == NULL) {
		std::cout << "Failed to create window: " << SDL_GetError() << std::endl;
		IMG_Quit();
		SDL_Quit();
		return -1;
	}
	
	//frames have already been created or loaded, proceed with event loop
	
	SDL_Renderer *mainRender = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
	SDL_RenderSetLogicalSize(mainRender,SCREEN_WIDTH,SCREEN_HEIGHT);
	SDL_SetRenderDrawBlendMode(mainRender,SDL_BLENDMODE_BLEND);
	
	for (uint8_t i = 0; i < n_frames; i++) {
		working_sprites[i] = create_sprite(mainRender, working_frames[i].sprite_name, 0, 0, working_pal->colors);
		if (new_frame) {
			working_sprites[i]->x_offset = working_sprites[i]->w/2;
			working_sprites[i]->y_offset = working_sprites[i]->h;
			working_frames[current_frame].xcenter = (int16_t)working_sprites[current_frame]->x_offset;
			working_frames[current_frame].ycenter = (int16_t)working_sprites[current_frame]->y_offset;
		}
		else {
			working_sprites[i]->x_offset = working_frames[i].xcenter;
			working_sprites[i]->y_offset = working_frames[i].ycenter;		
		}

	}

	const double logic_aspect_ratio = (double)SCREEN_WIDTH/(double)SCREEN_HEIGHT;
	int window_w,window_h;	
	int mouse_x,mouse_y;
	
	const short draw_xcenter = SCREEN_WIDTH/2;
	const short draw_ycenter = SCREEN_HEIGHT-150;
	
	const uint8_t cross_length = 32;
	
	double sprite_scale = 1.0;
	const uint8_t max_scale = 10;
	
	bool mouse_held = false;
	bool mouse_pressed = false;
	bool quit = false;
	
	SDL_Rect size_slider{20,SCREEN_HEIGHT-60,200,30};
	
	const uint8_t center_slider_size = 100;
	SDL_Rect center_slider{SCREEN_WIDTH-center_slider_size-20,SCREEN_HEIGHT-center_slider_size-20,center_slider_size,center_slider_size};
	
	uint8_t current_window = WDW_HURTBOXES; //see the window enum for values
	uint8_t current_box = 0;
	
	uint32_t frame_counter = 0;
	
	const uint32_t border_offset = 30;
	
	sprite* current_sprite_dimentions = NULL;
	update_sprite_dimentions_txt(mainRender, font, working_sprites[current_frame], current_sprite_dimentions);
	
	const uint16_t offset_buttons_x = 680;
	const uint16_t offset_buttons_y = 580;
	const uint8_t offset_buttons_distance = 24;
	
	button* btn_center_offset = create_button(mainRender,"center", font, SCREEN_WIDTH-100,500);
	button* btn_offset_r = create_button(mainRender,">", font, offset_buttons_x+offset_buttons_distance,offset_buttons_y);
	button* btn_offset_l = create_button(mainRender,"<", font, offset_buttons_x-offset_buttons_distance,offset_buttons_y);
	button* btn_offset_u = create_button(mainRender,"^", font, offset_buttons_x,offset_buttons_y-offset_buttons_distance);
	button* btn_offset_d = create_button(mainRender,"v", font, offset_buttons_x,offset_buttons_y+offset_buttons_distance);
	
	const uint16_t wdw_button_distance = 150;
	button* hurt_btn  = create_button(mainRender, "hurtboxes" , font, border_offset, 30);
	button* hit_btn   = create_button(mainRender, "hitboxes"  , font, border_offset+wdw_button_distance, 30);
	
	button* btn_next_box = create_button(mainRender, ">", font, 100+border_offset+30,80);
	button* btn_prev_box = create_button(mainRender, "<", font, 100+border_offset,80);
	
	sprite* current_box_txt = NULL;
	update_current_box_txt(mainRender, font, current_box, current_box_txt);
	
	const uint16_t box_pos_buttons_x = 180, box_pos_buttons_y = 150;
	const uint8_t box_pos_buttons_distance = 24;

	button* btn_box_pos_r = create_button(mainRender,">", font, box_pos_buttons_x+box_pos_buttons_distance,box_pos_buttons_y);
	button* btn_box_pos_l = create_button(mainRender,"<", font, box_pos_buttons_x-box_pos_buttons_distance,box_pos_buttons_y);
	button* btn_box_pos_u = create_button(mainRender,"^", font, box_pos_buttons_x,box_pos_buttons_y-box_pos_buttons_distance);
	button* btn_box_pos_d = create_button(mainRender,"v", font, box_pos_buttons_x,box_pos_buttons_y+box_pos_buttons_distance);	
	
	
	sprite* size_label = create_text(mainRender, "size", font, SDL_Color{255,255,255,255});
	size_label->x_offset = 0;
	size_label->y_offset = 0;
	const uint16_t box_size_buttons_x = 30, box_size_buttons_y = 300;
	const uint8_t  box_size_buttons_distance = 26;
	
	button* btn_box_hsize_minus_fast = create_button(mainRender,"<",font, box_size_buttons_x,box_size_buttons_y);
	button* btn_box_hsize_minus      = create_button(mainRender,"-",font, box_size_buttons_x+box_size_buttons_distance*1,box_size_buttons_y);
	button* btn_box_hsize_plus       = create_button(mainRender,"+",font, box_size_buttons_x+box_size_buttons_distance*2,box_size_buttons_y);
	button* btn_box_hsize_plus_fast  = create_button(mainRender,">",font, box_size_buttons_x+box_size_buttons_distance*3,box_size_buttons_y);
	
	button* btn_box_vsize_minus_fast = create_button(mainRender,"<",font, box_size_buttons_x,box_size_buttons_y+box_size_buttons_distance);
	button* btn_box_vsize_minus      = create_button(mainRender,"-",font, box_size_buttons_x+box_size_buttons_distance*1,box_size_buttons_y+box_size_buttons_distance);
	button* btn_box_vsize_plus       = create_button(mainRender,"+",font, box_size_buttons_x+box_size_buttons_distance*2,box_size_buttons_y+box_size_buttons_distance);
	button* btn_box_vsize_plus_fast  = create_button(mainRender,">",font, box_size_buttons_x+box_size_buttons_distance*3,box_size_buttons_y+box_size_buttons_distance);
	
	sprite* position_label = create_text(mainRender, "position", font, SDL_Color{255,255,255,255});
	position_label->x_offset = 0;
	position_label->y_offset = 0;
	
	SDL_Rect position_slider{(int)border_offset,150,100,100};
	
	sprite* frame_label_txt = NULL;
	update_frame_label_txt(mainRender, font, frame_label_txt, current_frame, n_frames);
	
	const int16_t frame_label_x = SCREEN_WIDTH/2, frame_label_y = SCREEN_HEIGHT-100;
	const uint8_t frame_label_button_distance = 20;
	const uint8_t frame_label_button_yoffset = 35;
	const int8_t  frame_label_button_xoffset = -5;
	
	button* btn_next_frame = create_button(mainRender,">", font, frame_label_button_xoffset+frame_label_x+frame_label_button_distance, frame_label_y+frame_label_button_yoffset);
	button* btn_prev_frame = create_button(mainRender,"<", font, frame_label_button_xoffset+frame_label_x-frame_label_button_distance, frame_label_y+frame_label_button_yoffset);
		
	button* btn_save_frames = create_button(mainRender,"save changes", font, 30, 450);
	bool changes_since_last_save = false;
	
	
	const int16_t pushbox_area_x = SCREEN_WIDTH - 20, pushbox_area_y = 80;
	int16_t pushbox_w = 16, pushbox_h = 16;
	int8_t pushbox_buttons_xoffset = -60, pushbox_buttons_yoffset = 20;
	uint8_t pushbox_buttons_distance = 30;
	
	
	sprite* pushbox_label = NULL;
	update_pushbox_label(mainRender, font, pushbox_label, pushbox_w, pushbox_h);
	
	
	button* btn_pushbox_wminus = create_button(mainRender, "-", font, pushbox_area_x+pushbox_buttons_xoffset+pushbox_buttons_distance*0, pushbox_area_y+pushbox_buttons_yoffset);
	button* btn_pushbox_wplus  = create_button(mainRender, "+", font, pushbox_area_x+pushbox_buttons_xoffset+pushbox_buttons_distance*1, pushbox_area_y+pushbox_buttons_yoffset);	
	
	button* btn_pushbox_hminus = create_button(mainRender, "-", font, pushbox_area_x+pushbox_buttons_xoffset+pushbox_buttons_distance*0, pushbox_buttons_distance+pushbox_area_y+pushbox_buttons_yoffset);
	button* btn_pushbox_hplus  = create_button(mainRender, "+", font, pushbox_area_x+pushbox_buttons_xoffset+pushbox_buttons_distance*1, pushbox_buttons_distance+pushbox_area_y+pushbox_buttons_yoffset);
	
	while(!quit) {
		
		SDL_Event e;
		while (SDL_PollEvent(&e)) {
		
			switch(e.type) {
				case SDL_QUIT:
					quit = true;
				break;

				default: break;

				case SDL_MOUSEBUTTONDOWN:
					if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) {
						mouse_held = true;
						mouse_pressed = true;
					}
				break;

				case SDL_MOUSEBUTTONUP:
					if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) {
						mouse_held = false;
					}
				break;

			}

		}
		
		
		SDL_GetMouseState(&mouse_x,&mouse_y);
		SDL_GetWindowSize(window, &window_w, &window_h);
		double window_aspect_ratio = (double)window_w/(double)window_h;

		if (window_aspect_ratio >= logic_aspect_ratio) { //longer window horizontally, thus the vertical side is fit to the screen's height
			mouse_y = (int)(SCREEN_HEIGHT*((double)mouse_y/(double)window_h));
			double screen_scale = (double)window_h/(double)SCREEN_HEIGHT;
			int black_border = (int)std::floor((double)window_w - (screen_scale*(double)SCREEN_WIDTH))/2;
			mouse_x = (int)std::max(mouse_x - black_border, 0);
			mouse_x = std::min(mouse_x,window_w - black_border); //clamp mouse in between non blackbar space. Mouse 0 starts from where the screen is drawn
			mouse_x = (int)(SCREEN_WIDTH*((double)mouse_x/(double)(window_w - 2*black_border)));				
		}
		else {
			mouse_x = (int)(SCREEN_WIDTH*((double)mouse_x/(double)window_w));
			double screen_scale = (double)window_w/(double)SCREEN_WIDTH;
			int black_border = (int)std::floor((double)window_h - (screen_scale*(double)SCREEN_HEIGHT))/2;
			mouse_y = (int)std::max(mouse_y - black_border, 0);
			mouse_y = std::min(mouse_y,window_h - black_border); //clamp mouse in between non blackbar space. Mouse 0 starts from where the screen is drawn
			mouse_y = (int)(SCREEN_HEIGHT*((double)mouse_y/(double)(window_h - 2*black_border)));				
		}
		
		
		box* box_reference = working_frames[current_frame].hurtboxes;
		if (current_window == WDW_HITBOXES) //********************************************
		box_reference = working_frames[current_frame].hitboxes;
		
		if (mouse_held) {
			if (point_in_rect(size_slider,mouse_x,mouse_y)) {
				sprite_scale = 0.5+(double)max_scale*((double)(mouse_x-size_slider.x)/(double)(size_slider.w));
			}
			
			if (point_in_rect(center_slider,mouse_x,mouse_y)) {
			working_sprites[current_frame]->x_offset = working_sprites[current_frame]->w*((double)(mouse_x-center_slider.x)/(double)center_slider.w);
			working_sprites[current_frame]->y_offset = working_sprites[current_frame]->h*((double)(mouse_y-center_slider.y)/(double)center_slider.h);	
			update_sprite_dimentions_txt(mainRender, font, working_sprites[current_frame], current_sprite_dimentions);			
			working_frames[current_frame].xcenter = (int16_t)working_sprites[current_frame]->x_offset;
			working_frames[current_frame].ycenter = (int16_t)working_sprites[current_frame]->y_offset;
			changes_since_last_save = true;
			}
			
			
			if (point_in_rect(position_slider,mouse_x,mouse_y)) {
				box_reference[current_box].x = working_sprites[current_frame]->w*((double)(mouse_x-position_slider.x)/(double)position_slider.w)
																			-working_sprites[current_frame]->w/2;
				box_reference[current_box].y = working_sprites[current_frame]->h*((double)(mouse_y-position_slider.y)/(double)position_slider.h)
																			-working_sprites[current_frame]->h;
				changes_since_last_save = true;
			}
			
			if (frame_counter % 2 == 0) {
				auto prev_w = box_reference[current_box].w, prev_h = box_reference[current_box].h;
				
				if (point_in_rect(btn_box_hsize_minus_fast->area,mouse_x,mouse_y)) {
					if (box_reference[current_box].w > 0)
						box_reference[current_box].w--;								
				}
				
				if (point_in_rect(btn_box_hsize_plus_fast->area,mouse_x,mouse_y)) box_reference[current_box].w++;
				
				if (point_in_rect(btn_box_vsize_minus_fast->area,mouse_x,mouse_y)) {
					if (box_reference[current_box].h > 0)
						box_reference[current_box].h--;										
				}
				
				if (point_in_rect(btn_box_vsize_plus_fast->area,mouse_x,mouse_y)) box_reference[current_box].h++;
				
				if (prev_w != box_reference[current_box].w || prev_h != box_reference[current_box].h)
				changes_since_last_save = true;
			
			}
			
			
		}
		
		if (mouse_pressed) {
			if (point_in_rect(hurt_btn->area,mouse_x,mouse_y)) {
				current_window = WDW_HURTBOXES; 
			}
			
			if (point_in_rect(hit_btn->area,mouse_x,mouse_y)) {
				current_window = WDW_HITBOXES; 
			}

			
			if (point_in_rect(btn_center_offset->area,mouse_x,mouse_y)) {
				working_sprites[current_frame]->x_offset = working_sprites[current_frame]->w/2;
				working_sprites[current_frame]->y_offset = working_sprites[current_frame]->h;
				update_sprite_dimentions_txt(mainRender, font, working_sprites[current_frame], current_sprite_dimentions);			
				working_frames[current_frame].xcenter = (int16_t)working_sprites[current_frame]->x_offset;
				working_frames[current_frame].ycenter = (int16_t)working_sprites[current_frame]->y_offset;			
				changes_since_last_save = true;				
			}
			
			bool offset_move = false;
			
			if (point_in_rect(btn_offset_r->area,mouse_x,mouse_y)) {
				working_sprites[current_frame]->x_offset++;
				offset_move = true;
			}
			
			if (point_in_rect(btn_offset_l->area,mouse_x,mouse_y)) {
				working_sprites[current_frame]->x_offset--;
				offset_move = true;
			}
			
			if (point_in_rect(btn_offset_u->area,mouse_x,mouse_y)) {
				working_sprites[current_frame]->y_offset--;
				offset_move = true;
			}
			
			if (point_in_rect(btn_offset_d->area,mouse_x,mouse_y)) {
				working_sprites[current_frame]->y_offset++;
				offset_move = true;
			}
			
			if (offset_move) {
				update_sprite_dimentions_txt(mainRender, font, working_sprites[current_frame], current_sprite_dimentions);			
				working_frames[current_frame].xcenter = (int16_t)working_sprites[current_frame]->x_offset;
				working_frames[current_frame].ycenter = (int16_t)working_sprites[current_frame]->y_offset;		
				changes_since_last_save = true;
			}
		
			auto prev_x = box_reference[current_box].x, prev_y = box_reference[current_box].y;
			
			if (point_in_rect(btn_box_pos_r->area,mouse_x,mouse_y)) box_reference[current_box].x++;
			if (point_in_rect(btn_box_pos_l->area,mouse_x,mouse_y)) box_reference[current_box].x--;
			if (point_in_rect(btn_box_pos_u->area,mouse_x,mouse_y)) box_reference[current_box].y--;
			if (point_in_rect(btn_box_pos_d->area,mouse_x,mouse_y)) box_reference[current_box].y++;
			
			if (prev_x != box_reference[current_box].x || prev_y != box_reference[current_box].y) {
				changes_since_last_save = true;
			}
			
			auto prev_w = box_reference[current_box].w, prev_h = box_reference[current_box].h; 
			
			if (point_in_rect(btn_box_hsize_minus->area,mouse_x,mouse_y)) {
				if (box_reference[current_box].w > 0)
					box_reference[current_box].w--;						
			}
			
			if (point_in_rect(btn_box_hsize_plus->area,mouse_x,mouse_y)) box_reference[current_box].w++;
			
			if (point_in_rect(btn_box_vsize_minus->area,mouse_x,mouse_y)) {
				if (box_reference[current_box].h > 0)
					box_reference[current_box].h--;										
			}
			
			if (point_in_rect(btn_box_vsize_plus->area,mouse_x,mouse_y)) box_reference[current_box].h++;			
	
			if (prev_h != box_reference[current_box].h || prev_w != box_reference[current_box].w) {
				changes_since_last_save = true;
			}
	
			auto prev_frame_box = current_box;
			if (point_in_rect(btn_next_box->area,mouse_x,mouse_y) && current_box < MAX_BOXES-1) current_box++; 
			if (point_in_rect(btn_prev_box->area,mouse_x,mouse_y) && current_box > 0) current_box--; 			
			
			if (current_box != prev_frame_box) update_current_box_txt(mainRender, font, current_box, current_box_txt);
			
			
			bool frame_change = false;
			if (point_in_rect(btn_next_frame->area,mouse_x,mouse_y) && current_frame < n_frames-1) {
				frame_change = true;
				current_frame++;
			}
			if (point_in_rect(btn_prev_frame->area,mouse_x,mouse_y) && current_frame > 0) {
				frame_change = true;
				current_frame--;
			}
		
			if (frame_change) {
				update_frame_label_txt(mainRender, font, frame_label_txt, current_frame, n_frames);
			}
			
			if (point_in_rect(btn_save_frames->area,mouse_x,mouse_y)) {
				changes_since_last_save = false;
				if (new_frame) {
					save_frame(&working_frames[0], NEW_FRAME_OUT FRAME_EXTENSION);
				}
				else {
					for (uint8_t fr = 0; fr < n_frames; fr++) {
						save_frame(&working_frames[fr], argv[3+fr]);					
					}
					
				}
				
				
			}
			
			auto prev_push_w = pushbox_w, prev_push_h = pushbox_h;
			
			if (point_in_rect(btn_pushbox_wminus->area,mouse_x,mouse_y)) pushbox_w-=2; 
			if (point_in_rect(btn_pushbox_wplus->area,mouse_x,mouse_y))  pushbox_w+=2; 		

			if (point_in_rect(btn_pushbox_hminus->area,mouse_x,mouse_y)) pushbox_h-=1; 
			if (point_in_rect(btn_pushbox_hplus->area,mouse_x,mouse_y))  pushbox_h+=1;

			if (pushbox_h < 0) pushbox_h = 0;
			if (pushbox_w < 0) pushbox_w = 0;	

			if (prev_push_w != pushbox_w || prev_push_h != pushbox_h) {
				update_pushbox_label(mainRender, font, pushbox_label, pushbox_w, pushbox_h);
			}
		}
		
		
		//draw section ---------------------------------------------------------------------------------------
		SDL_SetRenderDrawColor(mainRender,0,0,0,0);//fill whole screen with black pixels
		SDL_RenderClear(mainRender);
		
		draw_rect(mainRender,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,60,60,60); // funny gray.
		
		draw_box(mainRender,SDL_Rect{(int)(draw_xcenter-working_sprites[current_frame]->x_offset*sprite_scale),
									 (int)(draw_ycenter-working_sprites[current_frame]->y_offset*sprite_scale),
									 (int)(working_sprites[current_frame]->w*sprite_scale),
									 (int)(working_sprites[current_frame]->h*sprite_scale)},255,255,255);
									
		draw_sprite(mainRender, working_sprites[current_frame], draw_xcenter, draw_ycenter, sprite_scale, sprite_scale, 0.0, 0, 0);
		
		for (uint8_t box_draw_step = 0; box_draw_step <= 1; box_draw_step++)
		for (uint8_t i = 0; i < MAX_BOXES; i++) {
			bool highlight_type = false;
			if ((box_draw_step == 0 && current_window == WDW_HURTBOXES) || (box_draw_step == 1 && current_window == WDW_HITBOXES)) //********************************************
				highlight_type = true;
				
			SDL_Color local_color{0,100,255};
			box* local_box_reference = working_frames[current_frame].hurtboxes;
			if (box_draw_step == 1) {
				local_box_reference = working_frames[current_frame].hitboxes;
				local_color = SDL_Color{255,10,0};
			}

			if (local_box_reference[i].w == 0) break;
			int32_t xbox,ybox, wbox, hbox;
			xbox = draw_xcenter + sprite_scale*(double)local_box_reference[i].x;
			ybox = draw_ycenter + sprite_scale*(double)local_box_reference[i].y;
			wbox = sprite_scale*(double)local_box_reference[i].w;
			hbox = sprite_scale*(double)local_box_reference[i].h;
			SDL_Rect rc{xbox,ybox,wbox,hbox};
			if (frame_counter % 2 == 0 || i != current_box || !highlight_type) {
				draw_rect(mainRender,rc,local_color.r,local_color.g,local_color.b,128);
				draw_box(mainRender,rc,local_color.r,local_color.g,local_color.b);
			}
			
		}
		
		
		//if ((frame_counter % 2) == 0)
		{
		int _xx = draw_xcenter-sprite_scale*(double)(pushbox_w/2),
			_yy = draw_ycenter-sprite_scale*(double)pushbox_h,
			_ww = sprite_scale*pushbox_w,
			_hh = sprite_scale*pushbox_h;
			
		draw_rect(mainRender, SDL_Rect{_xx,_yy,_ww,_hh}, 0, 255, 0, 128);
		draw_box (mainRender, SDL_Rect{_xx,_yy,_ww,_hh}, 0, 255, 0);
		
		}
		
		//center cross---
		//black border
		draw_rect(mainRender,draw_xcenter-1,draw_ycenter-cross_length/2,3,cross_length,0,0,0);
		draw_rect(mainRender,draw_xcenter-cross_length/2,draw_ycenter-1,cross_length,3,0,0,0);
		//white center
		draw_rect(mainRender,draw_xcenter,draw_ycenter-cross_length/2,1,cross_length,255,255,255);
		draw_rect(mainRender,draw_xcenter-cross_length/2,draw_ycenter,cross_length,1,255,255,255);	

		draw_rect(mainRender,size_slider,255,255,255);
		draw_rect(mainRender,10+(double)size_slider.w*(sprite_scale-0.5)/(double)max_scale,size_slider.y,20,size_slider.h,0,0,0);
		draw_rect(mainRender,11+(double)size_slider.w*(sprite_scale-0.5)/(double)max_scale,size_slider.y+1,18,size_slider.h-2,255,255,255);
		
		draw_rect(mainRender,center_slider,255,255,255);
		draw_button(mainRender,btn_center_offset,255,255,255);
		draw_button(mainRender,btn_offset_r,255,255,255);
		draw_button(mainRender,btn_offset_l,255,255,255);
		draw_button(mainRender,btn_offset_u,255,255,255);
		draw_button(mainRender,btn_offset_d,255,255,255);
		
		draw_sprite(mainRender, current_sprite_dimentions, SCREEN_WIDTH - 2*current_sprite_dimentions->w - 20, 30, 2.0,2.0,0.0,0,0);
		
		uint8_t hurtbox_color = 120;
		uint8_t hitbox_color  = 120;

		
		switch(current_window) {
			case WDW_HURTBOXES:
				hurtbox_color = 255;
			break;
			
			case WDW_HITBOXES:
				hitbox_color = 255;
			break;		
			
		}
		
		draw_button(mainRender,hurt_btn,hurtbox_color,hurtbox_color,hurtbox_color);
		draw_button(mainRender,hit_btn,hitbox_color,hitbox_color,hitbox_color);	
	
		if (current_box < MAX_BOXES-1) draw_button(mainRender,btn_next_box,255,255,255);
		if (current_box > 0) draw_button(mainRender,btn_prev_box,255,255,255);
		
		draw_sprite(mainRender,current_box_txt, border_offset, 80, 2.0, 2.0, 0.0, 0, 0);
		
		draw_sprite(mainRender, position_label, border_offset, 120, 2.0, 2.0, 0.0, 0, 0);	
		draw_rect(mainRender,position_slider,255,255,255);
	
		draw_button(mainRender,btn_box_pos_r,255,255,255);
		draw_button(mainRender,btn_box_pos_l,255,255,255);
		draw_button(mainRender,btn_box_pos_u,255,255,255);
		draw_button(mainRender,btn_box_pos_d,255,255,255);
	
		draw_sprite(mainRender, size_label, box_size_buttons_x, box_size_buttons_y-30, 2.0,2.0,0.0,0,0);
	
		draw_button(mainRender,btn_box_hsize_minus_fast,255,255,255);
		draw_button(mainRender,btn_box_hsize_minus,255,255,255);
		draw_button(mainRender,btn_box_hsize_plus,255,255,255);
		draw_button(mainRender,btn_box_hsize_plus_fast,255,255,255);
		
		draw_button(mainRender,btn_box_vsize_minus_fast,255,255,255);
		draw_button(mainRender,btn_box_vsize_minus,255,255,255);
		draw_button(mainRender,btn_box_vsize_plus,255,255,255);
		draw_button(mainRender,btn_box_vsize_plus_fast,255,255,255);
		
		
		draw_sprite (mainRender, frame_label_txt, frame_label_x, frame_label_y, 2.0, 2.0, 0.0, 0, 0);
		if (current_frame < n_frames-1)
		draw_button(mainRender,btn_next_frame,255,255,255);
		if (current_frame > 0)
		draw_button(mainRender,btn_prev_frame,255,255,255);
		
		uint8_t save_color = 255;
		if (!changes_since_last_save) save_color = 128;
	 	
		draw_button(mainRender,btn_save_frames,save_color,save_color,save_color);
		
		
		draw_sprite(mainRender, pushbox_label, pushbox_area_x, pushbox_area_y, 2.0, 2.0, 0.0, 0,0);
		
		draw_button(mainRender, btn_pushbox_wminus, 255,255,255);
		draw_button(mainRender, btn_pushbox_wplus,  255,255,255);
		draw_button(mainRender, btn_pushbox_hminus, 255,255,255);
		draw_button(mainRender, btn_pushbox_hplus,  255,255,255);		
		
		SDL_RenderPresent(mainRender);
		//SDL_UpdateWindowSurface(gWindow);
		
		
		mouse_pressed = false;
		
		frame_counter++;
		SDL_Delay(MS_PER_FRAME);
	}
	
	for (uint8_t i = 0; i < n_frames; i++) {
		free_sprite(working_sprites[i]);
	}
	
	delete[] working_sprites;
	delete[] working_frames;
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
	return 0;
}