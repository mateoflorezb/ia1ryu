#ifndef _IAR_GAMESTATE_INCLUDED
#define _IAR_GAMESTATE_INCLUDED

#include "IAR_obj.h"

struct _IAR_Obj;
typedef struct _IAR_Obj IAR_Obj;

typedef struct _IAR_GameState {
	clist* objs;
	clist* CreationQueue;
	int cam_x, cam_y;
	uint8_t SecondTimer;
	uint8_t FrameTimer;
	IAR_Obj* P1;
	IAR_Obj* P2;
	uint32_t InstanceCounter;
	
} IAR_GameState;

IAR_GameState* IAR_CloneGameState(IAR_GameState* gs);
void IAR_FreeGameState(IAR_GameState* gs);
void IAR_AdvanceGameState(IAR_GameState* gs); //aka step
IAR_GameState* IAR_NewGameState(); //starts a game state with initial conditions (player distance, timer, health, etc)


#endif
