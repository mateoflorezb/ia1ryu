import os
import pandas as pd
from sklearn.metrics import *
from sklearn.tree import DecisionTreeRegressor
import numpy as np
import joblib
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
import math

data = pd.read_csv("IAR_dataset.csv")
data = data.drop(data.columns[[0]], 1)

X_trainRW = data.values[:,0:12] 
y_trainRW = data.values[:,-2] 

testDepth = 15

RWestimator = DecisionTreeRegressor(max_depth=testDepth)

def ShowRegressorMetrics(rx, X, y):
    s = cross_val_score(rx, X, y, cv=KFold(10, shuffle=True), scoring=make_scorer(mean_absolute_error))
    print ("MAE "+ str(testDepth) +"depth " + str(np.mean(s)))

    s = cross_val_score(rx, X, y, cv=KFold(10, shuffle=True), scoring=make_scorer(mean_squared_error))
    print ("sqrt(MSE) "+ str(testDepth) +"depth " + str(math.sqrt(np.mean(s))))

print(os.linesep + "Metrics with 10-Fold cross validation" + os.linesep)

print("RW metrics:")
ShowRegressorMetrics(RWestimator, X_trainRW, y_trainRW)

X_trainRS = data.values[:,0:12] 
y_trainRS = data.values[:,-1] 

RSestimator = DecisionTreeRegressor(max_depth=testDepth)


print("RS metrics:")
ShowRegressorMetrics(RSestimator, X_trainRS, y_trainRS)

RWestimator.fit(X_trainRW, y_trainRW)
RSestimator.fit(X_trainRS, y_trainRS)
joblib.dump(RWestimator,"RWestimator.joblib")
joblib.dump(RSestimator,"RSestimator.joblib")

input()
