import os
import pandas as pd
from sklearn.metrics import *
from sklearn.svm import SVR
import numpy as np
import joblib

print(os.getcwd())

data = pd.read_csv("IAR_dataset.csv")
data = data.drop(data.columns[[0]], 1)

X_trainRW = data.values[:,0:12] 
y_trainRW = data.values[:,-2] 


RWestimator = SVR()
RWestimator.fit(X_trainRW, y_trainRW)

predictInput = np.array(data[["SelfDistCorner", "EnemyDistCorner", "SelfDistEnemy", "SelfDistFireball", "EnemyDistFireball",
                                    "FrameAdvantage", "SelfLife", "EnemyLife", "InCombo", "EnemyInCombo", "EnemyY", "Option"]])

print ("MAE %.3f"% mean_absolute_error(data['RW'],
RWestimator.predict(predictInput)))
print ("MSE %.3f"% mean_squared_error(data['RW'],
RWestimator.predict(predictInput)))
print ("MAX %.3f"% max_error(data['RW'],
RWestimator.predict(predictInput)))
print("Now RS...")
X_trainRS = data.values[:,0:12] 
y_trainRS = data.values[:,-1] 

RSestimator = SVR()
RSestimator.fit(X_trainRS, y_trainRS)

predictInput = np.array(data[["SelfDistCorner", "EnemyDistCorner", "SelfDistEnemy", "SelfDistFireball", "EnemyDistFireball",
                                    "FrameAdvantage", "SelfLife", "EnemyLife", "InCombo", "EnemyInCombo", "EnemyY", "Option"]])

print ("MAE %.3f"% mean_absolute_error(data['RS'],
RSestimator.predict(predictInput)))
print ("MSE %.3f"% mean_squared_error(data['RS'],
RSestimator.predict(predictInput)))
print ("MAX %.3f"% max_error(data['RS'],
RSestimator.predict(predictInput)))



#f = open('RWestimator2', 'wb')
#pickle.dump(RWestimator,f, protocol=pickle.HIGHEST_PROTOCOL)
#f.close()

joblib.dump(RWestimator,"RWestimatorSVR.joblib")
joblib.dump(RSestimator,"RSestimatorSVR.joblib")

#f = open('RSestimator2', 'wb')
#pickle.dump(RSestimator,f, protocol=pickle.HIGHEST_PROTOCOL)
#f.close()
