import os
import pandas as pd
from sklearn.metrics import *
from sklearn.ensemble import RandomForestRegressor
import numpy as np
import joblib

print(os.getcwd())

data = pd.read_csv("IAR_dataset.csv")
data = data.drop(data.columns[[0]], 1)

X_trainRW = data.values[:,0:12] 
y_trainRW = data.values[:,-2] 

testDepth = 14
n_DTs = 4

RWestimator = RandomForestRegressor(n_estimators=n_DTs, max_depth=testDepth, criterion="absolute_error")
RWestimator.fit(X_trainRW, y_trainRW)

predictInput = np.array(data[["SelfDistCorner", "EnemyDistCorner", "SelfDistEnemy", "SelfDistFireball", "EnemyDistFireball",
                                    "FrameAdvantage", "SelfLife", "EnemyLife", "InCombo", "EnemyInCombo", "EnemyY", "Option"]])

print ("MAE "+ str(testDepth) +"depth "+str(n_DTs)+"trees  %.3f"% mean_absolute_error(data['RW'],
RWestimator.predict(predictInput)))
print ("MSE "+ str(testDepth) +"depth "+str(n_DTs)+"trees  %.3f"% mean_squared_error(data['RW'],
RWestimator.predict(predictInput)))
print ("MAX "+ str(testDepth) +"depth "+str(n_DTs)+"trees  %.3f"% max_error(data['RW'],
RWestimator.predict(predictInput)))

X_trainRS = data.values[:,0:12] 
y_trainRS = data.values[:,-1] 

RSestimator = RandomForestRegressor(n_estimators=n_DTs, max_depth=testDepth, criterion="absolute_error")
RSestimator.fit(X_trainRS, y_trainRS)

predictInput = np.array(data[["SelfDistCorner", "EnemyDistCorner", "SelfDistEnemy", "SelfDistFireball", "EnemyDistFireball",
                                    "FrameAdvantage", "SelfLife", "EnemyLife", "InCombo", "EnemyInCombo", "EnemyY", "Option"]])
print("Now RS...")
print ("MAE "+ str(testDepth) +"depth "+str(n_DTs)+"trees  %.3f"% mean_absolute_error(data['RS'],
RSestimator.predict(predictInput)))
print ("MSE "+ str(testDepth) +"depth "+str(n_DTs)+"trees  %.3f"% mean_squared_error(data['RS'],
RSestimator.predict(predictInput)))
print ("MAX "+ str(testDepth) +"depth "+str(n_DTs)+"trees  %.3f"% max_error(data['RS'],
RSestimator.predict(predictInput)))

joblib.dump(RWestimator,"RWestimatorRF.joblib")
joblib.dump(RSestimator,"RSestimatorRF.joblib")
