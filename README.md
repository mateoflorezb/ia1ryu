# Desarrollo de un jugador controlado por máquina para videojuegos del género de lucha
<b>Autor: Mateo Flórez Bacca</b>
<br>
<img src="misc/IA1RyuBanner.png">

<b>Objetivo: Implementar un jugador controlado por máquina para juegos de lucha, basado en machine learning</b>
<ul>
<li>Dataset: En este repositorio, DT Generation/IAR_dataset.csv
<li>Modelo: DecisionTreeRegressor, SupportVectorRegressor, RandomForestRegressor
<li>Vídeo explicando el algoritmo: https://youtu.be/1CREtg24Bj8
<li>Vídeo mostrando el jugador controlado por máquina funcionando: https://youtu.be/0KXm5rmIDKA
</ul>

## Arquitectura del algoritmo

<p align="center">
<img src="misc/iar_arch.png">
</p>

## ¿Por qué este no es un problema de regresión o clasificación?

<br><br><br>

<p align="center">
<img src="misc/iar_notaregression.drawio.png">
</p>

<br><br><br>

## Cómo entender el source code

## DHEX

DHEX es una librería para la creación de juegos de pelea. Se encarga de simplificar aspectos técnicos como cargar imágenes y sonidos, aplicar paletas de color a una imágen, detectar cualquier tipo de control y teclado, etc. Es de mi autoría y está basada en la librería de multimedia general, SDL2.

## IAR

IAR es el nombre simplificado de este proyecto, ia1ryu. La implementación del juego, algoritmo del jugador controlado por máquina y algoritmo para generación del dataset se encuentran en estos archivos.

## Archivos relacionados con IA

#### DT Generation/generateDT.py

Script que se encarga de generar los regresores de RW y RS y serializarlos como archivos.

#### IAR_CPU.h y IAR_CPU.c

Se encarga de definir al jugador aleatorio y al jugador controlado por máquina.
Las funciones _local_CPU_DumpData(...), _local_ExtractDataRow(...) y IAR_SetLocalRow(...) invocadas desde IAR_CPU.c son las principales que se encargan de la generación del dataset.

En la línea 184 de IAR_CPU.c inicia el proceso de calcular las regresiones y con estas definir las probabilidades de cada opción.

#### IAR_DataGeneration.h y IAR_DataGeneration.c

Se encarga de definir la interfaz o protocolo entre Python y C. Esto es importante porque Python se encarga de algunas tareas críticas, tales como exportar información local (es decir, en RAM) a un CSV, o calcular las regresiones.

#### IAR_Scripts.py

Es la contraparte de Python para IAR_DataGeneration. Es aquí donde se invocan los regresores (con GetEstimation(...)).


## Archivos no relacionados con IA

### main.c

Aplicación.

#### clist.c/clist.h

Implementación en C de una lista enlazada para cualquier tipo de dato.

#### IAR_obj.c

Describe algunas funciones para el motor del juego (como crear y destruir objetos). Principalmente describe la interacción entre los jugadores (por ejemplo, si un jugador es golpeado por otro, ¿qué ocurre?)

#### IAR_obj.h

Define la jerarquía de los objetos. Define una estructura de datos que imita el comportamiento del paradigma orientado a objetos.

#### IAR_gamestate.h y IAR_gamestate.c

Define el loop jugable. Permite guardar, cargar y avanzar estados de la partida, fundamental para las simulaciones que generan las métricas.

#### IAR_general.h y IAR_general.c

Define algunas funciones que no tiene un énfasis específico. Algunos ejemplos son definir números de punto fijo (no se usaron floats), definir una función aleatoria uniforme para números enteros, calcular colisiones entre rectángulos y conjuntos de rectángulos, entre otras.

#### IAR_global.h

Interfaz para acceder a algunos recursos globales. Cosas como imágenes y referencias a objetos de Python.

#### IAR_Ryu.h y IAR_Ryu.c

Se encarga de definir las características de los ataques (cuánto daño hacen, qué tan rápidos son, etc) y animaciones del personaje.

#### IAR_Hadoken.c

Se encarga de definir las características y funcionamiento del proyectil especial que pueden lanzar los personajes.

#### IAR_TempVisual.c

Se encarga de definir ciertas partículas del juego (efectos visuales que mejoran la identificación del estado de la partida para los jugadores humanos).



## Instalación

<p>El ejecutable incluido en el repositorio (IAR.exe) requiere de varias dependencias en runtime<p> <br>
<b>Librerías compartidas</b>
<ul>
<li>libogg-0.dll</li>
<li>libpng16-16.dll</li>
<li>libvorbis-0.dll</li>
<li>libvorbisfile-3.dll</li>
<li>SDL2.dll</li>
<li>SDL2_image.dll</li>
<li>SDL2_mixer.dll</li>
<li>zlib1.dll</li>
</ul>

<p>Adicionalmente, Python debe estar instalado. Para este proyecto se utilizó Python 3.8. Los siguientes modulos de terceros deben estar instalados en el entorno global de Python:</p>
<ul>
<li>joblib</li>
<li>numpy</li>
<li>pandas</li>
<li>sklearn</li>
</ul>

<p>Si se cuenta con todas estas dependencias es posible correr directamente IAR.exe. También es posible ejecutar algunos de los modos predeterminados:</p>
<ul>
<li>Jugar PVP.bat: Permite jugar contra otro rival humano.</li>
<li>Jugar VS entrenado.bat: Permite jugar contra un jugador controlado por máquina entrenado con un Decision Tree.</li>
<li>Jugar VS random.bat: Permite jugar contra un jugador aleatorio (su función de masa de probabilidad es uniforme para cualquier contexto)</li>
<li>Ver entrenado vs random.bat: Permite ver varias partidas entre un jugador controlado por máquina entrenado y un jugador aleatorio</li>
<li>Ver entrenado vs random (rápido).bat: Igual que el anterior, pero cada fotograma se calcula tan rápido como el PC permita.</li> 
<li>Simular entrenado VS random (muy rápido - 50 partidas).bat: Simula 50 partidas entre un jugador controlado por máquina entrenado y un jugador aleatorio. No se genera ningún fotograma, por lo que esta simulación es mucho más rápida que visualizar la partida.</li>
</ul>

### Generar el ejecutable manualmente

Si se desea, es posible utilizar clCompile.bat para compilar el proyecto. Sin embargo, esto requiere que se modifique el script para reemplazar las rutas correspondientes de include y link. Compilar requiere contar con:

<ul>
<li>SDL2 (developer) https://www.libsdl.org/download-2.0.php</li>
<li>SDL_image 2.0 (developer) https://www.libsdl.org/projects/SDL_image/</li>
<li>SDL_mixer 2.0 (developer) https://www.libsdl.org/projects/SDL_mixer/</li>
<li>Compilador de Microsoft (cl.exe). Requiere instalar el módulo "Build Tools" de Visual Studio</li>
</ul>

## Controles para el juego

<p>Por defecto los controles para el juego son:</p>
<b>General</b>
<table>
<tr>
  <th>Acción
  <th>Tecla(s)
</tr>

<tr>
  <th>Reiniciar la partida
  <th>2
<tr>
</table>

<b>Jugador 1</b>
<table>

<tr>
  <th>Acción
  <th>Tecla(s)
</tr>

<tr>
  <th>Direcciones
  <th>flechas del teclado
<tr>

<tr>
  <th>LP (Light Punch)
  <th>A
<tr>

<tr>
  <th>MP (Medium Punch)
  <th>S
<tr>

<tr>
  <th>HP (Hard Punch)
  <th>D
<tr>

<tr>
  <th>LK (Light Kick)
  <th>Z
<tr>

<tr>
  <th>MK (Medium Kick)
  <th>X
<tr>

<tr>
  <th>HK (Hard Kick)
  <th>C
<tr>

</table>

<b>Jugador 2</b>
<p>Indefinido por defecto</p>

<p>Para configurar los controles se puede ejecutar el programa DHEX_SetupKeyConfig.exe. Un detalle importante a tener en cuenta es que este programa usa una notación distinta para referirse a los botones. El programa los va a mostrar como LP, HP, SP, LK, HK, DS. Para estre proyecto, cada uno de estos corresponde a LP, MP, HP, LK, MK, HK respectivamente.
Es posible configurar los controles con el teclado o con un joystick.</p>

### Cómo jugar

<b>Mecánicas generales</b>
<ul>
<li>Movimiento: con atrás y adelante el personaje camina. Con arriba o cualquiera de las diagonales hacia arriba el personaje salta en la dirección indicada.</li>
<li>Bloquear ataques: para bloquear ataques basta con marcar la dirección opuesta al rival. Es posible bloquear agachado al marcar abajo + atrás al mismo tiempo. Si el ataque golpea a la altura de los pies, debe ser bloqueado agachado. Si el ataque viene desde arriba hacia abajo, debe ser bloqueado de pie. No es posible bloquear en el aire.</li>
<li>Atacar: cada botón ejecuta un ataque. Hay ciertas secuencias de movimientos que permiten realizar ataques especiales.</li>
<li>Tech: para defenderse de agarres, hay que intentar agarrar al rival al mismo tiempo.</li>

</ul>

<b>Ataques especiales</b>
<table>
<tr>
  <th>Secuencia
  <th>Ataque
</tr>

<tr>
  <th>Abajo > Adelante > Puño
  <th>Hadoken
</tr>

<tr>
  <th>Abajo > Atrás > Patada
  <th>Tatsumaki
</tr>

<tr>
  <th>Adelante > Abajo > Adelante > Puño
  <th>Shoryuken
</tr>

<tr>
  <th>Puño suave + Patada suave
  <th>Agarre
</tr>

</table>



