#include "DHEX_img.h"

#include "SDL.h"
#include "SDL_image.h"
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#define _DHEX_FONT_FILE_EXTENSION ".png"

#define TRANSPARENCY_COLOR_R 0
#define TRANSPARENCY_COLOR_G 255
#define TRANSPARENCY_COLOR_B 255


static void* 		__local_DHEX_LoadPalettizedPixelData(const char* filename, DHEX_Palette pal);

static DHEX_Sprite* __local_DHEX_CreateSpriteFromPixelData(DHEX_App* app, SDL_Surface* pixelData) {
	
    SDL_Texture* newTexture = NULL;
	DHEX_Sprite* spr = NULL;
	
    if (pixelData != NULL) {

    	newTexture = SDL_CreateTextureFromSurface((SDL_Renderer*)(app->__private.MainRenderer),pixelData);
		spr = malloc(sizeof(DHEX_Sprite));
		spr->tex = (void*)newTexture;
		spr->w = pixelData->w;
		spr->h = pixelData->h;
	}
	
	return spr;
	
}

DHEX_Sprite* _DHEX_CreateSpriteRaw(DHEX_App* app, const char* filename) {
    SDL_Surface* newSurface = IMG_Load(filename);
    if (newSurface == NULL) {
		printf("%s\n", SDL_GetError() );
		SDL_ClearError();
		return NULL;
	}
	
	DHEX_Sprite* spr = __local_DHEX_CreateSpriteFromPixelData(app, newSurface);
	SDL_FreeSurface(newSurface);
	return spr;	
}

DHEX_Sprite* _DHEX_CreateSpritePalettized(DHEX_App* app, const char* filename, DHEX_Palette pal) {
	SDL_Surface* palettizedSurface = (SDL_Surface*)__local_DHEX_LoadPalettizedPixelData(filename, pal);
    if (palettizedSurface == NULL) {
		printf("%s\n", SDL_GetError() );
		SDL_ClearError();
		return NULL;
	}
	
	DHEX_Sprite* spr = __local_DHEX_CreateSpriteFromPixelData(app, palettizedSurface);
	SDL_FreeSurface(palettizedSurface);
	return spr;		
	
}



static void* __local_DHEX_LoadPalettizedPixelData(const char* filename, DHEX_Palette pal) {
    SDL_Surface* newSurface = IMG_Load(filename);

    if (newSurface != NULL) {	

		SDL_LockSurface(newSurface); ///////////
	
		SDL_Surface* lowdepthSurface = SDL_CreateRGBSurface(0,newSurface->w,newSurface->h,8,0,0,0,0);
		uint8_t* lowdepthIndexes = malloc(sizeof(uint8_t)*newSurface->w*newSurface->h);//new uint8_t[newSurface->w*newSurface->h];
		uint32_t pixelIndex = 0;
		uint8_t* pixels = (uint8_t*)newSurface->pixels;

		//w x h to get # pixels
		for (uint32_t py = 0; py < newSurface->h; py++)
		for (uint32_t px = 0; px < newSurface->w; px++) {
			
			uint32_t pixel = *(uint32_t*)&(pixels[py*newSurface->pitch + px*newSurface->format->BytesPerPixel]);
			SDL_Color currentColor;
			SDL_GetRGB(pixel,newSurface->format,&currentColor.r,&currentColor.g,&currentColor.b);
			lowdepthIndexes[pixelIndex] = currentColor.r;
			pixelIndex++;
		} 
		
		SDL_UnlockSurface(newSurface); ///////////

		///////////
	
		SDL_LockSurface(lowdepthSurface); ///////////
			
			//wipe the image to all 0s
			memset(lowdepthSurface->pixels, 0, lowdepthSurface->h * lowdepthSurface->pitch);
	
			pixelIndex = 0;
			pixels = (uint8_t*)lowdepthSurface->pixels;
			for (uint32_t py = 0; py < newSurface->h; py++)
			for (uint32_t px = 0; px < newSurface->w; px++) {
				
				pixels[py*lowdepthSurface->pitch + px*lowdepthSurface->format->BytesPerPixel] = lowdepthIndexes[pixelIndex];
				pixelIndex++;
			}
	
		SDL_UnlockSurface(lowdepthSurface); ///////////

		free(lowdepthIndexes);

		SDL_Color fixedPalette[256];
		for (short i = 1; i < 256; i++) {
			fixedPalette[i].r = pal.colors[i].r;
			fixedPalette[i].g = pal.colors[i].g;
			fixedPalette[i].b = pal.colors[i].b;
			fixedPalette[i].a = 255;//pal.colors[i].a;
		}
		fixedPalette[0].r = TRANSPARENCY_COLOR_R;
		fixedPalette[0].g = TRANSPARENCY_COLOR_G;
		fixedPalette[0].b = TRANSPARENCY_COLOR_B;
		

		SDL_SetPaletteColors(lowdepthSurface->format->palette,(const SDL_Color*) fixedPalette,0,256);

		//had to create a new surface instead of using the already created "newSurface"
		//otherwise it didn't work for some reason idk.
		SDL_Surface* highdepthColorized = SDL_CreateRGBSurface(0,lowdepthSurface->w,lowdepthSurface->h,32,0,0,0,0);
		SDL_Rect dest; dest.x = 0; dest.y = 0; dest.w = lowdepthSurface->w; dest.h = lowdepthSurface->h;
		SDL_BlitSurface(lowdepthSurface,NULL,highdepthColorized,&dest);

		
		SDL_SetColorKey(highdepthColorized,SDL_TRUE,SDL_MapRGB(highdepthColorized->format,
																TRANSPARENCY_COLOR_R,
																TRANSPARENCY_COLOR_G,
																TRANSPARENCY_COLOR_B)); 

	

		
		SDL_FreeSurface(newSurface);
		SDL_FreeSurface(lowdepthSurface);
		newSurface = highdepthColorized;
	}
	else {
		printf("%s\n", SDL_GetError() );
		SDL_ClearError();
	}
	
	return (void*)newSurface;
}



DHEX_Palette* _DHEX_LoadPalette(const char* filename) {
	FILE* file = fopen(filename, "rb");
	
	if(ferror(file) != 0) {
		printf("Couldn't open file %s\n", filename);
		return NULL;
	}
	
	DHEX_Palette* pal = (DHEX_Palette*)malloc(sizeof(DHEX_Palette));

	fread((void*)&pal->max_index, 1, 1, file); //read the number of defined colors
	fread((void*)&pal->colors[1], sizeof(DHEX_Color), pal->max_index, file); //read the colors
	

	if(ferror(file) != 0) {
		printf("Error occurred while reading %s\n", filename);
		free(pal);
		return NULL;
	}

	fclose(file);
	pal->colors[0].r = 0;
	pal->colors[0].g = 255;
	pal->colors[0].b = 255;
	return pal;
}

void DHEX_FreeSprite(DHEX_Sprite* spr) {
	if (spr != NULL) {
		SDL_DestroyTexture((SDL_Texture*)spr->tex);
		free(spr);
	}
	
	return;
}

int _DHEX_DrawSprite(DHEX_App* app, DHEX_Sprite* spr, int x, int y, int xOffset, int yOffset, double xscale, double yscale, double angle) {
	
	if (app == NULL) return -1;
	if (spr == NULL) return -1;
	
	SDL_RendererFlip flip = SDL_FLIP_NONE;

	int w,h;
	w = (int)(spr->w*fabs(xscale));
	h = (int)(spr->h*fabs(yscale));

	int xCenter, yCenter;
	xCenter = (int)(xOffset*fabs(xscale));
	yCenter = (int)(yOffset*fabs(yscale));
	
	if (xscale < 0) {
		flip = flip | SDL_FLIP_HORIZONTAL;
		xCenter = -(spr->w - xOffset)*xscale;
	}
	

	if (yscale < 0) {
		flip = flip | SDL_FLIP_VERTICAL;
		yCenter = -(spr->h - yOffset)*yscale;

	}
	

	SDL_Rect dest = {x - xCenter, y - yCenter, w, h};
	SDL_Point center = {xCenter,yCenter};
	

	return SDL_RenderCopyEx((SDL_Renderer*)app->__private.MainRenderer,
                     (SDL_Texture*)spr->tex,
                     NULL,
                     (const SDL_Rect*) &dest,
                     (const double) -angle,
                     (const SDL_Point*)&center,
                     (const SDL_RendererFlip)flip);
	
	
}



DHEX_Sprite* DHEX_GetSpriteRaw(DHEX_App* app, const char* filename) {
	if (app == NULL)
		return NULL;
	
	clist* ref = (clist*)(app->__private.SpriteRawList);
	clist_iterator it = clist_start(ref);
	while (it != NULL) {
		_DHEX_APP_SPRITE_RAW_LIST_STRUCT st = clist_get(it,_DHEX_APP_SPRITE_RAW_LIST_STRUCT);
		if (strcmp(st.filename,filename) == 0) {
			return st.spr;
		}
	
	it = clist_next(it);
	}
	/*
	unsigned int tmpFilenameLen = strlen(filename)+strlen(app->WorkingDirectory)+1;
	char* tmpFilename = malloc(tmpFilenameLen);
	strcpy(tmpFilename, app->WorkingDirectory);
	strcat(tmpFilename, filename);
	tmpFilename[tmpFilenameLen-1] = '\0';
	
	DHEX_Sprite* spr = _DHEX_CreateSpriteRaw(app, tmpFilename);
	free(tmpFilename);
	*/
	
	DHEX_Sprite* spr = _DHEX_CreateSpriteRaw(app, filename);
	
	if (spr != NULL) {
		_DHEX_APP_SPRITE_RAW_LIST_STRUCT* st = malloc(sizeof(_DHEX_APP_SPRITE_RAW_LIST_STRUCT));
		char* tmpStr = malloc(strlen(filename)+1);
		strcpy(tmpStr, filename);
		strcat(tmpStr, "");
		st->filename = (const char*)tmpStr;
		st->spr = spr;
		clist_push(ref, st);
		
		return spr;
	}
	
	return NULL;
}


DHEX_Sprite* DHEX_GetSpritePalette(DHEX_App* app, const char* spr_filename, const char* pal_filename) {
	
	if (app == NULL)
		return NULL;
	
	clist* ref = (clist*)(app->__private.SpritePalettizedList);
	
	clist_iterator it = clist_start(ref);
	//printf("%p\n", it);	
	
	while (it != NULL) {
		
		_DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT st = clist_get(it,_DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT);
		
		if ((strcmp(st.spr_filename,spr_filename) == 0) && (strcmp(st.pal_filename, pal_filename) == 0)) {
			return st.spr;
		}
	
	it = clist_next(it);
	}
	
	/*
	unsigned int tmpFilenameLen = strlen(pal_filename)+strlen(app->WorkingDirectory)+1;
	char* tmpPalFilename = malloc(tmpFilenameLen);
	strcpy(tmpPalFilename, app->WorkingDirectory);
	strcat(tmpPalFilename, pal_filename);
	tmpPalFilename[tmpFilenameLen-1] = '\0';	
	
	DHEX_Palette* pal = _DHEX_LoadPalette(tmpPalFilename);
	free(tmpPalFilename);
	
	if (pal == NULL) return NULL;
	
	
	tmpFilenameLen = strlen(spr_filename)+strlen(app->WorkingDirectory)+1;
	char* tmpSprFilename = malloc(tmpFilenameLen);
	strcpy(tmpSprFilename, app->WorkingDirectory);
	strcat(tmpSprFilename, spr_filename);
	tmpSprFilename[tmpFilenameLen-1] = '\0';
	
	DHEX_Sprite* spr = _DHEX_CreateSpritePalettized(app, tmpSprFilename, *pal);
	free(tmpSprFilename);
	free(pal);
	*/
	
	DHEX_Palette* pal = _DHEX_LoadPalette(pal_filename);
	
	if (pal == NULL) return NULL;
	
	DHEX_Sprite* spr = _DHEX_CreateSpritePalettized(app, spr_filename, *pal);
	free(pal);
	
	if (spr != NULL) {
		
		_DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT* st = malloc(sizeof(_DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT));
		char* tmpStr = malloc(strlen(spr_filename)+1);
		strcpy(tmpStr, spr_filename);
		strcat(tmpStr, "");
		st->spr_filename = (const char*)tmpStr;
	
		tmpStr = malloc(strlen(pal_filename)+1);
		strcpy(tmpStr, pal_filename);
		strcat(tmpStr, "");
		st->pal_filename = (const char*)tmpStr;
		
		st->spr = spr;
		clist_push(ref, st);
		
		return spr;
		
	}
	
	return NULL;
	
	
}



void DHEX_FlushSprites(DHEX_App* app) {
	
	if (app == NULL) return;
	
	clist_clear((clist*)app->__private.SpriteRawList);
	clist_clear((clist*)app->__private.SpritePalettizedList);
	
}

void _DHEX_FillRect(DHEX_App* app, int x, int y, int w, int h) {
	if (app == NULL) return;	
	SDL_RenderFillRect(app->__private.MainRenderer, &(const SDL_Rect){x,y,w,h});
	
}

enum __DHEX_FONT_NOTABLE_INDEXES {
	__DHEX_FONT_INDEX_NUMBERS_START   = 0,
	__DHEX_FONT_INDEX_LOWERCASE_START = 10,
	__DHEX_FONT_INDEX_UPPERCASE_START = 36,
	
	__DHEX_FONT_INDEX_LESS 	  = 62,
	__DHEX_FONT_INDEX_GREATER = 63,
	__DHEX_FONT_INDEX_PLUS 	  = 64,
	__DHEX_FONT_INDEX_MINUS   = 65,
	__DHEX_FONT_INDEX_COMMA   = 66,
	__DHEX_FONT_INDEX_DOT 	  = 67,
	__DHEX_FONT_INDEX_SLASH   = 68,
	__DHEX_FONT_INDEX_UNKNOWN = 69
	
};

const char* const _DHEX_FontIndexToFilename[_DHEX_FONT_MAX_CHAR] = {
	"0","1","2","3","4","5","6","7","8","9",
	"lowerA","lowerB","lowerC","lowerD","lowerE","lowerF","lowerG","lowerH","lowerI","lowerJ","lowerK","lowerL",
	"lowerM","lowerN","lowerO","lowerP","lowerQ","lowerR","lowerS","lowerT","lowerU","lowerV","lowerW","lowerX",
	"lowerY","lowerZ",
	"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
	"LESS","GREATER","PLUS","MINUS","COMMA","DOT","SLASH","UNKNOWN"
};



DHEX_Font* DHEX_CreateFont(DHEX_App* app, const char* path, unsigned short SpaceSeparation) {
	
	if (app == NULL) return NULL;
	
	DHEX_Font* font = malloc(sizeof(DHEX_Font));
	unsigned short len = strlen(app->WorkingDirectory)+strlen(path)+20;
	char* tmpStr = malloc(len);
	
	for (unsigned char c = 0; c < _DHEX_FONT_MAX_CHAR; c++) {
		//memset(tmpStr, (int)NULL, len);
		//strcpy(tmpStr, app->WorkingDirectory);
		//strcat(tmpStr, path);
		//strcat(tmpStr, "\\");
		//strcat(tmpStr, _DHEX_FontIndexToFilename[c]);
		//strcat(tmpStr, _DHEX_FONT_FILE_EXTENSION);
		sprintf(tmpStr, "%s/%s%s\0",path, _DHEX_FontIndexToFilename[c], _DHEX_FONT_FILE_EXTENSION);
		font->ch[c] = _DHEX_CreateSpriteRaw(app, (const char*)tmpStr);
		if (font->ch[c] == NULL) {
			printf("DHEX: Couldn't load font @ %s\n", path);
			
			for (unsigned char cc = 0; cc < c; cc++) {
				DHEX_FreeSprite(font->ch[cc]);
			}
			
			free(font);
			free(tmpStr);
			return NULL;
		}
		
	}
	
	free(tmpStr);
	font->SpaceSeparation = SpaceSeparation;
	return font;
	
}

static unsigned char __local_DHEX_C_CharToDHEX_FontIndex(char c) {
	if (c >= 48 && c < 58) {
		return (unsigned char) (c - 48 + __DHEX_FONT_INDEX_NUMBERS_START);
	}
	
	if (c >= 97 && c < 123) {
		return (unsigned char) (c - 97 + __DHEX_FONT_INDEX_LOWERCASE_START);
	}
	
	if (c >= 65 && c < 91) {
		return (unsigned char) (c - 65 + __DHEX_FONT_INDEX_UPPERCASE_START);
	}
	
	switch(c) {
		case '<':
			return (unsigned char) __DHEX_FONT_INDEX_LESS;
		break;
		
		case '>':
			return (unsigned char) __DHEX_FONT_INDEX_GREATER;
		break;
		
		case '+':
			return (unsigned char) __DHEX_FONT_INDEX_PLUS;
		break;
		
		case '-':
			return (unsigned char) __DHEX_FONT_INDEX_MINUS;
		break;
		
		case ',':
			return (unsigned char) __DHEX_FONT_INDEX_COMMA;
		break;
		
		case '.':
			return (unsigned char) __DHEX_FONT_INDEX_DOT;
		break;
		
		case '/':
			return (unsigned char) __DHEX_FONT_INDEX_SLASH;
		break;
		
		default:
		break;
		
	}

	return (unsigned char) __DHEX_FONT_INDEX_UNKNOWN;
	
}


void _DHEX_DirectDrawFont(DHEX_App* app, DHEX_Font* font, int x, int y, double xscale, double yscale,
_DHEX_TEXT_HCENTER hcenter, _DHEX_TEXT_VCENTER vcenter, const char* string) {
	
	if (app == NULL) return;
	if (font == NULL) return;
	
	//calculate length
	float txtLen = 0;
	const unsigned int stringLength = strlen(string);
	for (unsigned int i = 0; i < stringLength; i++) {
		
		unsigned char dhex_c = __local_DHEX_C_CharToDHEX_FontIndex(string[i]);
		
		if (string[i] == ' ') 
			txtLen += xscale*(float)font->SpaceSeparation;
		
		else
			txtLen += xscale*(float)font->ch[dhex_c]->w;
	}
	
	float txtHeight = yscale*(float)font->ch[__DHEX_FONT_INDEX_UNKNOWN]->h;
	
	//xcenter,ycenter = 0,0 means the text is centered at the bottom left corner
	//this is done this way as to nicely align text (so that the letter a isn't floating at the level 
	//of letter t or something)
	
	int xCenter = 0;
	switch(hcenter) {
		case DHEX_TEXT_HCENTER_MIDDLE:
			xCenter = (int)txtLen/2;
		break;
		
		case DHEX_TEXT_HCENTER_RIGHT:
			xCenter = (int)txtLen;
		break;
	}
	
	int yCenter = 0;
	switch(vcenter) {
		case DHEX_TEXT_VCENTER_MIDDLE:
			yCenter = (int)txtHeight/2;
		break;
		
		case DHEX_TEXT_VCENTER_TOP:
			yCenter = (int)txtHeight;
		break;
	}
	
	
	txtLen = 0; //lemme just reuse this shit lmao
	for (unsigned int i = 0; i < stringLength; i++) {
		unsigned char dhex_c = __local_DHEX_C_CharToDHEX_FontIndex(string[i]);
		
		if (string[i] == ' ')
			txtLen += xscale*(float)font->SpaceSeparation;
		else {
			_DHEX_DrawSprite(app, font->ch[dhex_c], (int)(x-xCenter+txtLen), y+yCenter, 0, font->ch[dhex_c]->h, xscale, yscale, 0.0);
			txtLen += xscale*(float)font->ch[dhex_c]->w;
		}
		
	}
	
	
}