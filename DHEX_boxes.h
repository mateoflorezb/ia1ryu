#ifndef _DHEX_BOXES_INCLUDED
#define _DHEX_BOXES_INCLUDED

#include "DHEX_base.h"

#include <stdint.h>

#define DHEX_MAX_HBOXES 10

typedef struct {
	int16_t x,y;
	uint16_t w,h;
} DHEX_HBox;

typedef struct {
	DHEX_HBox hurtboxes[DHEX_MAX_HBOXES]; //"Null" terminated list of hurtboxes (Null being box.w = 0)
	DHEX_HBox hitboxes[DHEX_MAX_HBOXES]; //Much the same
	int16_t xcenter, ycenter; //center relative to the sprite's top left
} DHEX_FrameBoxes; //the hit/hurtboxes for a single frame

DHEX_FrameBoxes* DHEX_LoadBXG(const char* filename); //BXG files (.bxg extension) are files produced by the box_gen utility
//The format is as follows:
/*
Null terminated C-style string with the name of the sprite used (R-channel indexed)
for DHEX's purposes, this will be ignored (it is only stored in the file for editing purposes, so that box_gen knows
what image to display)

Array of DHEX_HBoxes, representing the hurtboxes. Null terminated, meaning
the first HBox found with w = 0 will be considered Null, and thus the end of the array.
However, if the 10th HBox is reached, that will be considered the end of the array regardless
of it being Null or not (bxg can only store 10 hurtboxes max).

Much the same but with hitboxes.

int16_t representing xcenter.

int16_t representing ycenter.


*/

typedef struct {
	const char* filename;
	DHEX_FrameBoxes* bxg;
} _DHEX_APP_BXG_LIST_STRUCT;

DHEX_FrameBoxes* DHEX_GetBXG(DHEX_App* app, const char* filename); //mem managed by dhex


#endif