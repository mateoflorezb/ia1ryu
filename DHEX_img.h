#ifndef _DHEX_IMG_INCLUDED
#define _DHEX_IMG_INCLUDED

#include "DHEX_base.h"
#include <stdint.h>

#define _DHEX_FONT_MAX_CHAR 70

typedef struct {
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
} DHEX_Color;

typedef struct {
	DHEX_Color colors[256];
	uint8_t max_index;
} DHEX_Palette;

typedef struct {
	void* tex;
	int w;
	int h;
} DHEX_Sprite;

typedef struct {
	const char* filename;
	DHEX_Sprite* spr;
} _DHEX_APP_SPRITE_RAW_LIST_STRUCT;

typedef struct {
	const char* spr_filename;
	const char* pal_filename;
	DHEX_Sprite* spr;
} _DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT;

typedef enum ___DHEX_TEXT_HCENTER {
	DHEX_TEXT_HCENTER_LEFT,
	DHEX_TEXT_HCENTER_MIDDLE,
	DHEX_TEXT_HCENTER_RIGHT
} _DHEX_TEXT_HCENTER;

typedef enum ___DHEX_TEXT_VCENTER {
	DHEX_TEXT_VCENTER_TOP,
	DHEX_TEXT_VCENTER_MIDDLE,
	DHEX_TEXT_VCENTER_BOTTOM
} _DHEX_TEXT_VCENTER;

//26 letters, so 26+26 for upper+lowercase
//10 digits
//<>+-,./ (unknown char)    --->  8 special chars

extern const char* const _DHEX_FontIndexToFilename[_DHEX_FONT_MAX_CHAR];

typedef struct {
	DHEX_Sprite* ch[70];
	unsigned short SpaceSeparation;
	
} DHEX_Font;

DHEX_Sprite* _DHEX_CreateSpriteRaw(DHEX_App* app, const char* filename);

//takes R channel as palette index. Must work from PNGs
DHEX_Sprite* _DHEX_CreateSpritePalettized(DHEX_App* app, const char* filename, DHEX_Palette pal);

void DHEX_FreeSprite(DHEX_Sprite* spr); //also frees it's texture

int _DHEX_DrawSprite(DHEX_App* app, DHEX_Sprite* spr, int x, int y, int xOffset, int yOffset, double xscale, double yscale, double angle);

DHEX_Palette* _DHEX_LoadPalette(const char* filename);

DHEX_Sprite* DHEX_GetSpriteRaw	  (DHEX_App* app, const char* filename);
DHEX_Sprite* DHEX_GetSpritePalette(DHEX_App* app, const char* spr_filename, const char* pal_filename);


void _DHEX_FillRect(DHEX_App* app, int x, int y, int w, int h);
void DHEX_FlushSprites(DHEX_App* app);

DHEX_Font* DHEX_CreateFont(DHEX_App* app, const char* path, unsigned short SpaceSeparation); //path to a folder with a bunch of png files named after every character (eg. s.png)

void _DHEX_DirectDrawFont(DHEX_App* app, DHEX_Font* font, int x, int y, double xscale, double yscale,
_DHEX_TEXT_HCENTER hcenter, _DHEX_TEXT_VCENTER vcenter, const char* string);

#endif