#include "IAR_obj.h"
#include "IAR_global.h"
#include "DHEX.h"
#include <stdio.h>

extern glob_ds global;


static void _TempVisualStep(IAR_Obj* self) {
	
	uint16_t index = abs(fixed_cast(self->Class.TempVisual.ImageIndex)) % self->Class.TempVisual.nImages;
	if (!self->Class.TempVisual.AnimationLoops)
		index = iclamp(abs(fixed_cast(self->Class.TempVisual.ImageIndex)), 0, self->Class.TempVisual.nImages - 1);
	
	self->Class.TempVisual.CurrentImage = self->Class.TempVisual.images[index];
	
	self->Class.TempVisual.ImageIndex += self->Class.TempVisual.ImageSpeed;
	self->Class.TempVisual.TTL--;
	
	if (self->Class.TempVisual.TTL == 0) {
		self->DestroyFlag = true;
		return;
	}
	
}

static void _TempVisualPostStep(IAR_Obj* self) {return;}

static void _TempVisualDraw(IAR_Obj* self) {
	DHEX_DrawLayer(global.app, self->Class.TempVisual.layer);
	
	DHEX_DrawSprite(global.app, self->Class.TempVisual.CurrentImage,
					fixed_cast(self->Class.TempVisual.x)-self->gs->cam_x,
					fixed_cast(self->Class.TempVisual.y)-self->gs->cam_y,
					self->Class.TempVisual.xOffset,
					self->Class.TempVisual.yOffset,
					self->Class.TempVisual.xscale,self->Class.TempVisual.yscale,0.0);
}

static void _TempVisualDestroy(IAR_Obj* self) {
	free(self);
	return;
}


static IAR_Obj* _TempVisualGetCopy(IAR_Obj* self) {
	IAR_Obj* o = malloc(sizeof(IAR_Obj));
	memcpy(o, self, sizeof(IAR_Obj));
	return o;
}

IAR_Obj* IAR_CreateTempVisual(void* data) {
	IAR_Obj* o = malloc(sizeof(IAR_Obj));
	o->ClassID = IAR_OBJ_TEMPVISUAL;
	o->ClassGroup = IAR_GROUP_NONE;
	o->DestroyFlag = false;
	
	o->step = &_TempVisualStep;
	o->post_step = &_TempVisualPostStep;
	o->pre_step = &_TempVisualPostStep;
	o->draw = &_TempVisualDraw;
	o->destroy = &_TempVisualDestroy;
	o->GetCopy = &_TempVisualGetCopy;
	
	o->Class.TempVisual.TTL = 1; //time to live
	o->Class.TempVisual.AnimationLoops = false;
	
	o->Class.TempVisual.images = NULL;
	o->Class.TempVisual.nImages = 1;
	
	o->Class.TempVisual.xscale = 1.0;
	o->Class.TempVisual.yscale = 1.0;
	
	o->Class.TempVisual.x = 0;
	o->Class.TempVisual.y = 0;
	
	o->Class.TempVisual.xOffset = 0;
	o->Class.TempVisual.yOffset = 0;
	
	o->Class.TempVisual.ImageIndex = 0;
	o->Class.TempVisual.ImageSpeed = 0;
	
	o->Class.TempVisual.layer = DHEX_LAYER_OBJ0;
	
	return o;
	
}