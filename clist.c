#include "clist.h"
#include <stdio.h>

clist* ___clist_create(uint16_t e_size) {
	clist* new = (clist*) malloc(sizeof(clist));
	new->first = NULL;
	new->last = NULL;
	new->element_size = e_size;
	new->free_element_callback = &free;
	return new;
}



clist_node* clist_next(clist_node* E) {
	if (E == NULL) {
		return NULL;
	}
	
	return E->next;
}



void clist_clear(clist* L) {

	clist_iterator it = clist_start(L);
	while (it != NULL) {
		clist_iterator next = it->next;
		clist_delete(it);
		it = next;
	}
}

void clist_free(clist* L) {
	
	if (L == NULL) {
		return;
	}
	
	clist_clear(L);
	free(L);
}



void clist_push(clist* L, void* data) {
	
	if (L == NULL) {
		return;
	}
	
	uint16_t size = L->element_size;
	
	void* data_copy = malloc(size);
	memcpy(data_copy, data, size);
	clist_node* new_node = (clist_node*) malloc(sizeof(clist_node));
	new_node->data = data_copy;
	new_node->next = NULL;
	new_node->prev = NULL;
	new_node->list = L;
	
	if (L->first == NULL) { //empty list
		L->first = new_node;
		L->last = L->first;
		return;
	}
	
	L->last->next = new_node;
	clist_node* prev_to_last = L->last;
	L->last = new_node;
	new_node->prev = prev_to_last;
	
	return;
}



void clist_delete(clist_node* E) {
	
	if (E == NULL || E->list == NULL || E->data == NULL) {
		return;
	}
	
	clist* L = E->list;
	
	if (E->prev == NULL) { //no previous element means we are deleting the very first element, thus we gotta update the list struct itself
		L->first = L->first->next;
	} 
	else {
		clist_node* prev_node = E->prev;
		prev_node->next = E->next;

	}
	
	if (E->next == NULL) { //no next element means we are deleting the last element, thus we gotta update the list struct itself
		L->last = L->last->prev;
	}
	else {
		clist_node* next_node = E->next;
		next_node->prev = E->prev;
	}	
	
	L->free_element_callback(E->data);

	free(E);
	return;
}



clist_node* clist_start(clist* L) {
	return L->first;
}


void clist_print(clist* L) {
	clist_iterator it = clist_start(L);
	unsigned int index = 0;
	printf("Printing clist at %p.\n", L);
	while (it != NULL) {
		printf("%d. %p\n", index, it->data);
		it = clist_next(it);
		index++;
	}
	printf("End of printing.\n");
	
}

clist* clist_clone(clist *L) {
	clist* copy = ___clist_create(L->element_size);
	copy->free_element_callback = L->free_element_callback;
	clist_iterator it = clist_start(L);
	while (it != NULL) {
		
		clist_push(copy, it->data);
		it = clist_next(it);
	}
	
	return copy;
}