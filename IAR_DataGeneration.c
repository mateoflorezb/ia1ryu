#include "IAR_DataGeneration.h"
#include "IAR_global.h"
#include "Python.h"

extern glob_ds global;

void IAR_ExportLocalRowsAsCSV(IAR_DataRow data[], int nrows, const char* filename) {
	
	PyObject* pValue = Py_BuildValue("i", nrows);
	PyObject* pArgs = PyTuple_New(1);
	PyTuple_SetItem(pArgs, 0, pValue);
	PyObject* DataDict = PyObject_CallObject(global.Python.ZerosDataDict, pArgs);
	Py_DECREF(pValue);
	Py_DECREF(pArgs);
	
	for (uint32_t k = 0; k < nrows; k++) {
		PyObject* pArgs_SetRow = PyTuple_New(3); //args for set row
		PyTuple_SetItem(pArgs_SetRow, 0, DataDict);
		
		const int rowMemberValues[] = {
			data[k].SelfDistCorner,
			data[k].EnemyDistCorner,
			data[k].SelfDistEnemy,
			data[k].SelfDistFireball,
			data[k].EnemyDistFireball,
			data[k].FrameAdvantage,
			data[k].SelfLife,
			data[k].EnemyLife,
			data[k].InCombo,
			data[k].EnemyInCombo,
			data[k].EnemyY,
			data[k].Option,
			data[k].RW,
			data[k].RS
		};
		
		PyObject* pArgs_Row = PyTuple_New(_IAR_DATA_ROW_NMEMBERS);
		
		for (uint8_t rMem = 0; rMem < _IAR_DATA_ROW_NMEMBERS; rMem++) {
			PyObject* pValue = PyLong_FromLong( rowMemberValues[rMem] );
			PyTuple_SetItem(pArgs_Row, rMem, pValue);			
		}
		
		PyTuple_SetItem(pArgs_SetRow, 1, pArgs_Row);
		PyObject* pValue = Py_BuildValue("i", k);
		PyTuple_SetItem(pArgs_SetRow, 2, pValue);
		
		PyObject_CallObject(global.Python.SetRow, pArgs_SetRow);
		Py_DECREF(pValue);
		Py_DECREF(pArgs_Row);
		
	}
	
	PyObject* pArgs_CSV = PyTuple_New(2); //dictionary (data) + string (filename)
	PyObject* pPath = Py_BuildValue("s", filename);
	PyTuple_SetItem(pArgs_CSV, 0, DataDict);
	PyTuple_SetItem(pArgs_CSV, 1, pPath);
	PyObject_CallObject(global.Python.SaveDataDictAsCSV, pArgs_CSV);
	
	Py_DECREF(pPath);
	Py_DECREF(pArgs_CSV);
	Py_DECREF(DataDict);
	
	return;
}

void IAR_SetLocalRow(IAR_DataRow row) {
	//printf("Trying to set local row %d\n", global.CPU.CurrentRow);
	if (global.CPU.CurrentRow < IAR_MAX_DATA_ROWS) { 
		global.CPU.LocalDataSet[global.CPU.CurrentRow] = row;
		global.CPU.CurrentRow++;
	}
}

double IAR_GetEstimation(PyObject* rx, IAR_DataRow input) {
	//global.Python.GetEstimation
	
	PyObject* pArgs_Func = PyTuple_New(2);
	PyObject* pArgs = PyTuple_New(_IAR_DATA_ROW_NMEMBERS - 2);
	
	const int rowMemberValues[] = {
		input.SelfDistCorner,
		input.EnemyDistCorner,
		input.SelfDistEnemy,
		input.SelfDistFireball,
		input.EnemyDistFireball,
		input.FrameAdvantage,
		input.SelfLife,
		input.EnemyLife,
		input.InCombo,
		input.EnemyInCombo,
		input.EnemyY,
		input.Option
	};
	
	for (uint8_t rMem = 0; rMem < _IAR_DATA_ROW_NMEMBERS - 2; rMem++) {
		PyObject* pValue = PyLong_FromLong( rowMemberValues[rMem] );
		PyTuple_SetItem(pArgs, rMem, pValue);			
	}
	
	Py_INCREF(rx);
	PyTuple_SetItem(pArgs_Func, 0, rx);
	PyTuple_SetItem(pArgs_Func, 1, pArgs);
	
	
	PyObject* est = PyObject_CallObject(global.Python.GetEstimation, pArgs_Func);
	Py_DECREF(pArgs_Func);
	//Py_DECREF(pArgs);
	
	PyObject* pParsable = PyTuple_New(1);
	PyTuple_SetItem(pParsable, 0, est);
	
	double rv;
	if (PyArg_ParseTuple(pParsable, "d", &rv) == 0) {
		rv = -1.0;
	}
	
	//Py_DECREF(est);
	Py_DECREF(pParsable);
	
	
	
	return rv;
}