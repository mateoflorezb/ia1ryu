#include "IAR_obj.h"
#include "IAR_global.h"
#include "IAR_Ryu.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

extern glob_ds global;

static const char* const _IAR_RYU_IMAGE_FILENAMES[_IAR_RYU_IMG_MAX] = {
	
	"data/ryu/frames/idle/1.png",
	"data/ryu/frames/idle/2.png",
	"data/ryu/frames/idle/3.png",
	"data/ryu/frames/idle/4.png",
	"data/ryu/frames/idle/5.png",
	"data/ryu/frames/idle/6.png",

	"data/ryu/frames/crouch_turn/1.png",
	"data/ryu/frames/crouch_turn/2.png",
	"data/ryu/frames/crouch_turn/3.png",

	"data/ryu/frames/jump_back/1.png",
	"data/ryu/frames/jump_back/2.png",
	"data/ryu/frames/jump_back/3.png",
	"data/ryu/frames/jump_back/4.png",
	"data/ryu/frames/jump_back/5.png",
	"data/ryu/frames/jump_back/6.png",
	"data/ryu/frames/jump_back/7.png",
	"data/ryu/frames/jump_back/8.png",
	
	"data/ryu/frames/jump_forward/1.png",
	"data/ryu/frames/jump_forward/2.png",
	"data/ryu/frames/jump_forward/3.png",
	"data/ryu/frames/jump_forward/4.png",
	"data/ryu/frames/jump_forward/5.png",
	"data/ryu/frames/jump_forward/6.png",
	"data/ryu/frames/jump_forward/7.png",
	"data/ryu/frames/jump_forward/8.png",
	
	"data/ryu/frames/jump_neutral/1.png",
	"data/ryu/frames/jump_neutral/2.png",
	"data/ryu/frames/jump_neutral/3.png",
	"data/ryu/frames/jump_neutral/4.png",
	"data/ryu/frames/jump_neutral/5.png",
	"data/ryu/frames/jump_neutral/6.png",
	
	"data/ryu/frames/to_crouch/1.png",
	"data/ryu/frames/to_crouch/2.png",
	"data/ryu/frames/to_crouch/3.png",

	"data/ryu/frames/stand_turn/1.png",
	"data/ryu/frames/stand_turn/2.png",
	"data/ryu/frames/stand_turn/3.png",

	"data/ryu/frames/walk_back/1.png",
	"data/ryu/frames/walk_back/2.png",
	"data/ryu/frames/walk_back/3.png",
	"data/ryu/frames/walk_back/4.png",
	"data/ryu/frames/walk_back/5.png",
	"data/ryu/frames/walk_back/6.png",

	"data/ryu/frames/walk_forward/1.png",
	"data/ryu/frames/walk_forward/2.png",
	"data/ryu/frames/walk_forward/3.png",
	"data/ryu/frames/walk_forward/4.png",
	"data/ryu/frames/walk_forward/5.png",
	"data/ryu/frames/walk_forward/6.png",
	
	"data/ryu/frames/prejump.png",
	
	"data/ryu/frames/stLP/1.png",
	"data/ryu/frames/stLP/2.png",
	"data/ryu/frames/stLP/3.png",
	
	"data/ryu/frames/stMP/start.png",
	"data/ryu/frames/stMP/act.png",
	"data/ryu/frames/stMP/aa.png",
	"data/ryu/frames/stMP/rec2.png",
	
	"data/ryu/frames/stHP/1.png",
	"data/ryu/frames/stHP/2.png",
	"data/ryu/frames/stHP/3.png",
	"data/ryu/frames/stHP/4.png",
	"data/ryu/frames/stHP/5.png",
	"data/ryu/frames/stHP/6.png",
	
	"data/ryu/frames/gutstun/1.png",
	"data/ryu/frames/gutstun/2.png",
	"data/ryu/frames/gutstun/3.png",
	"data/ryu/frames/gutstun/4.png",
	
	"data/ryu/frames/headstun/1.png",
	"data/ryu/frames/headstun/2.png",
	"data/ryu/frames/headstun/3.png",
	"data/ryu/frames/headstun/4.png",
	
	"data/ryu/frames/cr_stun/1.png",
	"data/ryu/frames/cr_stun/2.png",
	"data/ryu/frames/cr_stun/3.png",

	"data/ryu/frames/st_block/1.png",
	"data/ryu/frames/st_block/2.png",
	
	"data/ryu/frames/cr_block/1.png",
	"data/ryu/frames/cr_block/2.png",
	
	"data/ryu/frames/juggled/1.png",
	"data/ryu/frames/juggled/2.png",
	"data/ryu/frames/juggled/3.png",
	"data/ryu/frames/juggled/4.png",
	"data/ryu/frames/juggled/5.png",
	
	"data/ryu/frames/knockdown/1.png",
	"data/ryu/frames/knockdown/2.png",
	"data/ryu/frames/knockdown/3.png",
	"data/ryu/frames/knockdown/4.png",
	"data/ryu/frames/knockdown/5.png",
	
	"data/ryu/frames/wakeup/1.png",
	"data/ryu/frames/wakeup/2.png",
	"data/ryu/frames/wakeup/3.png",
	"data/ryu/frames/wakeup/4.png",
	"data/ryu/frames/wakeup/5.png",
	"data/ryu/frames/wakeup/6.png",
	"data/ryu/frames/wakeup/7.png",
	
	"data/ryu/frames/crHP/1.png",
	"data/ryu/frames/crHP/2.png",
	"data/ryu/frames/crHP/3.png",
	
	"data/ryu/frames/crLP/1.png",
	"data/ryu/frames/crLP/2.png",
	
	"data/ryu/frames/hadoken/1.png",
	"data/ryu/frames/hadoken/2.png",
	"data/ryu/frames/hadoken/3.png",
	"data/ryu/frames/hadoken/4.png",
	"data/ryu/frames/hadoken/5.png",
	"data/ryu/frames/hadoken/6.png",
	
	"data/ryu/frames/crMP/1.png",
	"data/ryu/frames/crMP/2.png",
	"data/ryu/frames/crMP/3.png",
	
	"data/ryu/frames/stLK/1.png",
	"data/ryu/frames/stLK/2.png",
	"data/ryu/frames/stLK/3.png",
	"data/ryu/frames/stLK/4.png",
	
	"data/ryu/frames/stMK/1.png",
	"data/ryu/frames/stMK/2.png",
	"data/ryu/frames/stMK/3.png",
	"data/ryu/frames/stMK/4.png",
	"data/ryu/frames/stMK/5.png",
	"data/ryu/frames/stMK/6.png",
	"data/ryu/frames/stMK/7.png",
	"data/ryu/frames/stMK/8.png",
	
	"data/ryu/frames/stHK/1.png",
	"data/ryu/frames/stHK/2.png",
	"data/ryu/frames/stHK/3.png",
	"data/ryu/frames/stHK/4.png",
	"data/ryu/frames/stHK/5.png",
	"data/ryu/frames/stHK/6.png",
	
	"data/ryu/frames/crLK/1.png",
	"data/ryu/frames/crLK/2.png",

	"data/ryu/frames/crMK/1.png",
	"data/ryu/frames/crMK/2.png",
	"data/ryu/frames/crMK/3.png",
	
	"data/ryu/frames/crHK/1.png",
	"data/ryu/frames/crHK/2.png",
	"data/ryu/frames/crHK/3.png",
	"data/ryu/frames/crHK/4.png",
	"data/ryu/frames/crHK/5.png",

	"data/ryu/frames/airLP/1.png",
	"data/ryu/frames/airLP/2.png",

	"data/ryu/frames/airMP/1.png",
	"data/ryu/frames/airMP/2.png",
	"data/ryu/frames/airMP/3.png",
	"data/ryu/frames/airMP/4.png",
	"data/ryu/frames/airMP/5.png",
	
	"data/ryu/frames/airHP/1.png",
	"data/ryu/frames/airHP/2.png",
	"data/ryu/frames/airHP/3.png",
	
	"data/ryu/frames/airLK/1.png",
	"data/ryu/frames/airLK/2.png",
	"data/ryu/frames/airLK/3.png",

	"data/ryu/frames/airMHK/1.png",
	"data/ryu/frames/airMHK/2.png",
	"data/ryu/frames/airMHK/3.png",
	
	"data/ryu/frames/fHP/1.png",
	"data/ryu/frames/fHP/2.png",
	"data/ryu/frames/fHP/3.png",
	"data/ryu/frames/fHP/4.png",
	"data/ryu/frames/fHP/5.png",
	"data/ryu/frames/fHP/6.png",
	"data/ryu/frames/fHP/7.png",
	"data/ryu/frames/fHP/8.png",
	"data/ryu/frames/fHP/9.png",
	"data/ryu/frames/fHP/10.png",
	"data/ryu/frames/fHP/11.png",
	
	"data/ryu/frames/shoryuken/1.png",
	"data/ryu/frames/shoryuken/2.png",
	"data/ryu/frames/shoryuken/3.png",
	"data/ryu/frames/shoryuken/4.png",
	"data/ryu/frames/shoryuken/5.png",
	"data/ryu/frames/shoryuken/6.png",
	
	"data/ryu/frames/tatsu/1.png",
	"data/ryu/frames/tatsu/2.png",
	"data/ryu/frames/tatsu/3.png",
	"data/ryu/frames/tatsu/4.png",
	"data/ryu/frames/tatsu/5.png",
	"data/ryu/frames/tatsu/6.png",
	"data/ryu/frames/tatsu/7.png",
	"data/ryu/frames/tatsu/8.png",
	"data/ryu/frames/tatsu/9.png",
	"data/ryu/frames/tatsu/10.png",
	"data/ryu/frames/tatsu/11.png",
	"data/ryu/frames/tatsu/12.png",
	
	"data/ryu/frames/whiff_grab/1.png",
	"data/ryu/frames/whiff_grab/2.png",
	"data/ryu/frames/whiff_grab/3.png",
	"data/ryu/frames/whiff_grab/4.png",
	"data/ryu/frames/whiff_grab/5.png",

	"data/ryu/frames/throw_forward/1.png",
	"data/ryu/frames/throw_forward/2.png",
	"data/ryu/frames/throw_forward/3.png",
	"data/ryu/frames/throw_forward/4.png",
	"data/ryu/frames/throw_forward/5.png",

	"data/ryu/frames/throw_back/1.png",
	"data/ryu/frames/throw_back/2.png",
	"data/ryu/frames/throw_back/3.png",
	"data/ryu/frames/throw_back/4.png",
	"data/ryu/frames/throw_back/5.png",
	"data/ryu/frames/throw_back/6.png"
	
};

static void __local_ApplyProxyBlock(IAR_Obj* self, int state, int frame, int distance) {
	if (self->Class.Player.LastAction == state) {
		if ((self->Class.Player.CurrentMaxSpState - self->Class.Player.spState) > 0
		&&  (self->Class.Player.CurrentMaxSpState - self->Class.Player.spState) <= frame
			) {
			self->Class.Player.ApplyProxyBlock = true;
			self->Class.Player.ApplyProxyBlockDistance = distance;
		}
	}	
	
}

static void __local_SetAllowedBuffers(IAR_Obj* self, int state, int framesFromEnd, uint8_t AllowedBuffers) {
	if (!self->Class.Player.LastAttackLanded) return;
	
	if (self->Class.Player.LastAction == state) {
		if (self->Class.Player.spState >= framesFromEnd)
			self->Class.Player.AllowedBuffers = AllowedBuffers;
		
		
		if (self->Class.Player.spState == framesFromEnd)
			self->Class.Player.CanOutputBuffer = true;
	}	

}

static void _RyuPreStep(IAR_Obj *self) {
	_IAR_PlayerDefaultFetchInput(self);
	
	if (self->Class.Player.spState > 0) {
		
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_ST_HP, 10, 46);
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_ST_MP, 6 , 42);
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_ST_LK, 5 , 50);
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_ST_MK, 16, 70);
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_ST_HK, 12, 50);
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_CR_LK, 5 , 55);
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_CR_MK, 6 , 60);
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_CR_HK, 6 , 52);
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_AIR_HK,7 , 50);
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_F_HP,  17, 34);
		
		__local_ApplyProxyBlock(self, IAR_RYU_STATE_HADOKEN, 30 , STAGE_WIDTH);
	}
	
	if (self->Class.Player.LastAction == IAR_RYU_STATE_SHORYUKEN  && self->Class.Player.LandingFrames >= 3) {
		_IAR_PlayerResetStateDescriptors(self);
		self->Class.Player.spState = 6;
		self->Class.Player.CurrentMaxSpState = self->Class.Player.spState;
		self->Class.Player.DefenseFlags = 0;
		_IAR_PlayerSetState(self, IAR_RYU_STATE_SHORYUKEN_RECOVERY);
		self->Class.Player.CanOutputBuffer = false;
		self->Class.Player.AllowedBuffers = 0;
		
				
	}
	
	_IAR_PlayerDefaultPreStep(self);	
	
	
	if (self->Class.Player.StopFrames == 0) {
	
		const fixed WalkSpeed = 6*IAR_FIXED_SCALE/2;
		const fixed JumpSpeed = -11*IAR_FIXED_SCALE;
		const fixed grav = 4.4*IAR_FIXED_SCALE/8;
		const fixed JumpHspeeed = 3.5*IAR_FIXED_SCALE;//(fixed)4.25*IAR_FIXED_SCALE;
		
		_IAR_PlayerDefaultMovement(self, WalkSpeed, JumpSpeed, grav, JumpHspeeed);
		
	}
}


static void _RyuStep(IAR_Obj *self) {
	self->Class.Player.pDepth = DHEX_LAYER_OBJ4;
	
	if (self->Class.Player.inAir) self->Class.Player.Char.Ryu.tempJumpCounter++;

	
	/*if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LK)) {
		printf("-------------\n");
		printf("elap: %u\n\n", self->Class.Player.LastHistElapsed);
		
		for (uint8_t k = 0; k < IAR_PLAYER_INPUT_HIST_SIZE; k++) {
			printf("n%u: %u  ---  ",k, self->Class.Player.InputHistList[k].flaggedInputs);
			printf("%u\n", self->Class.Player.InputHistList[k].deltaT);
		}
		
		
		//IAR_PlayerCheckDPM(self);
		//printf("%u\n ", self->Class.Player.Char.Ryu.tempJumpCounter);
		self->Class.Player.Char.Ryu.tempJumpCounter = 0;
	}*/
	
	//read inputs and buffer attacks here (I think=?)
	
	//set allowed buffers
	
	__local_SetAllowedBuffers(self, IAR_RYU_STATE_ST_LP,  2, IAR_BUFFER_LIGHT   | IAR_BUFFER_SPECIAL);
	__local_SetAllowedBuffers(self, IAR_RYU_STATE_CR_LP,  2, IAR_BUFFER_LIGHT   | IAR_BUFFER_SPECIAL | IAR_BUFFER_SUPER);
	__local_SetAllowedBuffers(self, IAR_RYU_STATE_ST_MP, 16, IAR_BUFFER_SPECIAL | IAR_BUFFER_SUPER);
	__local_SetAllowedBuffers(self, IAR_RYU_STATE_CR_MP,  7, IAR_BUFFER_SPECIAL | IAR_BUFFER_SUPER);
	__local_SetAllowedBuffers(self, IAR_RYU_STATE_CR_HP, 23, IAR_BUFFER_SPECIAL | IAR_BUFFER_SUPER);
	__local_SetAllowedBuffers(self, IAR_RYU_STATE_ST_LK,  8, IAR_BUFFER_SPECIAL | IAR_BUFFER_SUPER);
	__local_SetAllowedBuffers(self, IAR_RYU_STATE_CR_LK,  7, IAR_BUFFER_LIGHT   | IAR_BUFFER_SPECIAL | IAR_BUFFER_SUPER);
	__local_SetAllowedBuffers(self, IAR_RYU_STATE_CR_MK, 13, IAR_BUFFER_SPECIAL | IAR_BUFFER_SUPER);
	__local_SetAllowedBuffers(self, IAR_RYU_STATE_CR_HK, 19, IAR_BUFFER_SPECIAL | IAR_BUFFER_SUPER);
	
	if ((self->Class.Player.CurrentMaxSpState - self->Class.Player.spState == 1) && self->Class.Player.StopFrames == 0 && self->Class.Player.CanKara) { //allow karas
		self->Class.Player.CanOutputBuffer = true;
		self->Class.Player.AllowedBuffers = IAR_BUFFER_GRAB;
	}
	
	if (self->Class.Player.state == IAR_RYU_STATE_WHIFF_GRAB) {
		self->Class.Player.AllowedBuffers = 0;
	}
		
		
	//if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP)) 
		
	//// Buffer stuff ///////////////////////	--------------------------------------------
	if (self->Class.Player.PreJump == -1 && !self->Class.Player.inAir) { //grounded normals --------------------------
		
		if (!IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_D)) {  //standing --------------------------------
			
			if ((self->Class.Player.AllowedBuffers & (IAR_BUFFER_LIGHT | IAR_BUFFER_NORMAL)) != 0) {
				
				//printf("%u\n", self->Class.Player.CurrentFrameInputsPressed);
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_ST_LP;
					//printf("eleped ");
					 
				}
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LK)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_ST_LK;
				}
				
			}
			
			if ((self->Class.Player.AllowedBuffers & (IAR_BUFFER_NORMAL)) != 0) {
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HP)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_ST_MP;
				}
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_SP)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_ST_HP;
				}
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HK)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_ST_MK;
				}
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_DS)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_ST_HK;
				}
			}
		}
		else { // crouching ---------------------------------------------
			
			if ((self->Class.Player.AllowedBuffers & (IAR_BUFFER_LIGHT | IAR_BUFFER_NORMAL)) != 0) {
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_CR_LP;
				}	
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LK)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_CR_LK;
				}
			}
			
			if ((self->Class.Player.AllowedBuffers & (IAR_BUFFER_NORMAL)) != 0) {
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HP)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_CR_MP;
				}				
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_SP)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_CR_HP;
				}
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HK)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_CR_MK;
				}
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_DS)) {
					self->Class.Player.BufferedAction = IAR_RYU_STATE_CR_HK;
				}
				
			}
			
		}
		
		////// Command normals /// ----------------------
		if (!IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_D)) {
			if ((self->Class.Player.AllowedBuffers & (IAR_BUFFER_NORMAL)) != 0) {
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_SP) &&
					(
						(IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_R) && self->Class.Player.xdir == 1)
						||
						(IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_L) && self->Class.Player.xdir ==-1)
					
					)) 
				
				{
						
					self->Class.Player.BufferedAction = IAR_RYU_STATE_F_HP;
				}
				
			}
		}
		
		
		////// Specials /// ----------------------
		if ((self->Class.Player.AllowedBuffers & (IAR_BUFFER_SPECIAL)) != 0) {
			if ( IAR_PlayerCheckQC(1, self)
				&& (
					IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP)
					||
					IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HP)
					||
					IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_SP)
				   )
			
			
				)  
				
			if (IAR_InstanceFind(self->Class.Player.Char.Ryu.LastHadokenID, self->gs) == NULL)	
			
			{
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP))
					self->Class.Player.BufferedSpecialLevel = 1;
		
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HP))
					self->Class.Player.BufferedSpecialLevel = 2;
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_SP))
					self->Class.Player.BufferedSpecialLevel = 3;
				
				self->Class.Player.BufferedAction = IAR_RYU_STATE_HADOKEN;
				
			}
			
			if ( IAR_PlayerCheckDPM(self)
				&& (
					IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP)
					||
					IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HP)
					||
					IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_SP)
				   )
			
			
				) {
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP))
					self->Class.Player.BufferedSpecialLevel = 1;
		
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HP))
					self->Class.Player.BufferedSpecialLevel = 2;
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_SP))
					self->Class.Player.BufferedSpecialLevel = 3;
				
				self->Class.Player.BufferedAction = IAR_RYU_STATE_SHORYUKEN;				
				
			}
			
			
			if ( IAR_PlayerCheckQC(-1, self)
				&& (
					IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LK)
					||
					IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HK)
					||
					IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_DS)
				   )
			
			
				)  
			
			{
				/*
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LK))
					self->Class.Player.BufferedSpecialLevel = 1;
		
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HK))
					self->Class.Player.BufferedSpecialLevel = 2;
				
				if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_DS))
					self->Class.Player.BufferedSpecialLevel = 3;
				*/
				
				self->Class.Player.BufferedAction = IAR_RYU_STATE_TATSU;
				
			}
			
		}
		
		
		//Grab /////////////
		if ((self->Class.Player.AllowedBuffers & (IAR_BUFFER_GRAB)) != 0) {
			if (
				(
				IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_BUTTON_LP)
				&&
				IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LK)
				)
				||
				(
				IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP)
				&&
				IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_BUTTON_LK)
				)
			) {
				
				self->Class.Player.BufferedAction = IAR_RYU_STATE_WHIFF_GRAB;
			}
			
		}
		
		
	}
	if ((self->Class.Player.inAir || self->Class.Player.PreJump >= 0) && self->Class.Player.AirAction) {  //Buffer air attacks
		if ((self->Class.Player.AllowedBuffers & (IAR_BUFFER_NORMAL)) != 0) {
			
			if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LP)) {
				self->Class.Player.BufferedAction = IAR_RYU_STATE_AIR_LP;
			}

			if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HP)) {
				self->Class.Player.BufferedAction = IAR_RYU_STATE_AIR_MP;
			}	

			if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_SP)) {
				self->Class.Player.BufferedAction = IAR_RYU_STATE_AIR_HP;
			}	

			if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_LK)) {
				self->Class.Player.BufferedAction = IAR_RYU_STATE_AIR_LK;
			}
			
			if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_HK)) {
				self->Class.Player.BufferedAction = IAR_RYU_STATE_AIR_MK;
			}
			
			if (IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputsPressed, DHEX_BUTTON_DS)) {
				self->Class.Player.BufferedAction = IAR_RYU_STATE_AIR_HK;
			}
			
		}
		
	}
	
	/////////////////// output buffered action ////////// -----------------------------------
	if (self->Class.Player.CanOutputBuffer && self->Class.Player.BufferedAction != ___IAR_RYU_STATE_NONE) {
		int tmBff = self->Class.Player.BufferedAction;
		_IAR_PlayerResetStateDescriptors(self);
		self->Class.Player.BufferedAction = tmBff;
		
		self->Class.Player.Launcher = false;
		self->Class.Player.DefenseFlags = 0;
		self->Class.Player.AppliedJuggleTime = 1;
		self->Class.Player.HitStunType = IAR_STUNTYPE_GUT;
		self->Class.Player.ForceStand = false;
		self->Class.Player.AttackHits = 1;
		self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_HIGH | IAR_ATTACK_CANBLOCK_LOW;
		self->Class.Player.LastAttackLanded = false;
		self->Class.Player.JP = 1;
		self->Class.Player.AppliedDamage = 40;
		self->Class.Player.AppliedChip = 0;
		self->Class.Player.CanKara = true;
		
		
		switch(self->Class.Player.BufferedAction) {
			case IAR_RYU_STATE_ST_LP:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 2+_IAR_RYU_ST_LP_FRAMES*2;
				
				self->Class.Player.Spark_xOffset = 60;
				self->Class.Player.Spark_yOffset = -80;
				self->Class.Player.AppliedStopFrames = 7;
				self->Class.Player.HitLevel = IAR_HITLEVEL_LIGHT;
				
				self->Class.Player.AppliedOnHit = 12;
				self->Class.Player.AppliedOnBlock = 11;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 2; 
				self->Class.Player.AppliedJuggleVs = -7;
				self->Class.Player.AppliedDamage = 30;
				
			break;

			case IAR_RYU_STATE_ST_MP:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 22;
				
				self->Class.Player.Spark_xOffset = 50;
				self->Class.Player.Spark_yOffset = -65;
				self->Class.Player.AppliedStopFrames = 11;
				self->Class.Player.HitLevel = IAR_HITLEVEL_MEDIUM;
				
				self->Class.Player.AppliedOnHit = 19;
				self->Class.Player.AppliedOnBlock = 17;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 5; 
				self->Class.Player.AppliedJuggleVs = -6;//-6;
				
				//self->Class.Player.Launcher = true;
				self->Class.Player.AppliedJuggleTime = 5;
				
				self->Class.Player.AppliedDamage = 80;

				
			break;

			case IAR_RYU_STATE_ST_HP:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 4+_IAR_RYU_ST_HP_FRAMES*4;
				
				self->Class.Player.Spark_xOffset = 45;
				self->Class.Player.Spark_yOffset = -80;
				self->Class.Player.AppliedStopFrames = 9;
				self->Class.Player.HitLevel = IAR_HITLEVEL_HARD;
				self->Class.Player.HitStunType = IAR_STUNTYPE_HEAD;
				
				self->Class.Player.AppliedOnHit = 21;
				self->Class.Player.AppliedOnBlock = 19;
				
				self->Class.Player.AppliedPushback = 7; 
				self->Class.Player.AppliedJuggleHs = 7; 
				self->Class.Player.AppliedJuggleVs = -6;
				
				self->Class.Player.AppliedDamage = 150;
			break;
			
			case IAR_RYU_STATE_CR_LP:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = _IAR_RYU_CR_LP_FRAMES*2;
				
				self->Class.Player.Spark_xOffset = 60;
				self->Class.Player.Spark_yOffset = -45;
				self->Class.Player.AppliedStopFrames = 9;
				self->Class.Player.HitLevel = IAR_HITLEVEL_LIGHT;
				
				self->Class.Player.AppliedOnHit = 11;
				self->Class.Player.AppliedOnBlock = 7;
				
				self->Class.Player.AppliedPushback = 4; 
				self->Class.Player.AppliedJuggleHs = 3; 
				self->Class.Player.AppliedJuggleVs = -5;
				
				self->Class.Player.CrouchPhase = 0;
				self->Class.Player.AppliedDamage = 30;
				
			break;
			
			
			case IAR_RYU_STATE_CR_MP:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 4+6+5+4; //startup + active + rec with active pose + rec
				
				self->Class.Player.Spark_xOffset = 60;
				self->Class.Player.Spark_yOffset = -45;
				self->Class.Player.AppliedStopFrames = 7;
				self->Class.Player.HitLevel = IAR_HITLEVEL_MEDIUM;
				
				self->Class.Player.AppliedOnHit = 18;
				self->Class.Player.AppliedOnBlock = 18;
				
				self->Class.Player.AppliedPushback = 4; 
				self->Class.Player.AppliedJuggleHs = 3; 
				self->Class.Player.AppliedJuggleVs = -5;
				
				self->Class.Player.CrouchPhase = 0;
				
				self->Class.Player.AppliedDamage = 80;
				
			break;
			
			case IAR_RYU_STATE_CR_HP:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 29;
				
				self->Class.Player.Spark_xOffset = 40;
				self->Class.Player.Spark_yOffset = -60;
				self->Class.Player.AppliedStopFrames = 12;
				self->Class.Player.HitLevel = IAR_HITLEVEL_HARD;
				
				self->Class.Player.AppliedOnHit = 23;
				self->Class.Player.AppliedOnBlock = 17;
				
				self->Class.Player.AppliedPushback = 6; 
				self->Class.Player.AppliedJuggleHs = 5; 
				self->Class.Player.AppliedJuggleVs = -5;
				
				self->Class.Player.CrouchPhase = 0;
				
				self->Class.Player.ForceStand = true;
				
				self->Class.Player.AppliedDamage = 125;
				
			break;
			
			case IAR_RYU_STATE_ST_LK:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 16;
				
				self->Class.Player.Spark_xOffset = 50;
				self->Class.Player.Spark_yOffset = -20;
				self->Class.Player.AppliedStopFrames = 9;
				self->Class.Player.HitLevel = IAR_HITLEVEL_LIGHT;
				
				self->Class.Player.AppliedOnHit = 16;
				self->Class.Player.AppliedOnBlock = 12;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 2; 
				self->Class.Player.AppliedJuggleVs = -7;
				
				self->Class.Player.AppliedDamage = 40;
				
			break;
			
			case IAR_RYU_STATE_ST_MK:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 8*4 - 2;
				
				self->Class.Player.Spark_xOffset = 50;
				self->Class.Player.Spark_yOffset = -50;
				self->Class.Player.AppliedStopFrames = 9;
				self->Class.Player.HitLevel = IAR_HITLEVEL_MEDIUM;
				
				self->Class.Player.AppliedOnHit = 16;
				self->Class.Player.AppliedOnBlock = 12;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 2; 
				self->Class.Player.AppliedJuggleVs = -7;
				
				self->Class.Player.AttackHits = 2;
				
				self->Class.Player.AppliedDamage = 70;
				
			break;
			
			case IAR_RYU_STATE_ST_HK:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = _IAR_RYU_ST_HK_FRAMES*4;
				
				self->Class.Player.Spark_xOffset = 60;
				self->Class.Player.Spark_yOffset = -80;
				self->Class.Player.AppliedStopFrames = 10;
				self->Class.Player.HitLevel = IAR_HITLEVEL_HARD;
				
				self->Class.Player.AppliedOnHit = 16;
				self->Class.Player.AppliedOnBlock = 12;
				
				self->Class.Player.AppliedPushback = 9; 
				self->Class.Player.AppliedJuggleHs = 7; 
				self->Class.Player.AppliedJuggleVs = -7;
				
				self->Class.Player.AppliedDamage = 150;
				
			break;
			
			case IAR_RYU_STATE_CR_LK:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 4+4+6;
				
				self->Class.Player.Spark_xOffset = 50;
				self->Class.Player.Spark_yOffset = -8;
				self->Class.Player.AppliedStopFrames = 9;
				self->Class.Player.HitLevel = IAR_HITLEVEL_LIGHT;
				
				self->Class.Player.AppliedOnHit = 14;
				self->Class.Player.AppliedOnBlock = 12;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 5; 
				self->Class.Player.AppliedJuggleVs = -5;
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_LOW;
				
				self->Class.Player.CrouchPhase = 0;
				
				self->Class.Player.AppliedDamage = 50;
				
			break;
			
			case IAR_RYU_STATE_CR_MK:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 5+4+11;
				
				self->Class.Player.Spark_xOffset = 50;
				self->Class.Player.Spark_yOffset = -8;
				self->Class.Player.AppliedStopFrames = 9;
				self->Class.Player.HitLevel = IAR_HITLEVEL_MEDIUM;
				
				self->Class.Player.AppliedOnHit = 17;
				self->Class.Player.AppliedOnBlock = 14;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 5; 
				self->Class.Player.AppliedJuggleVs = -5;
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_LOW;
				
				self->Class.Player.CrouchPhase = 0;
				
				self->Class.Player.AppliedDamage = 100;
				
			break;
			
			case IAR_RYU_STATE_CR_HK:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 4+4+19;
				
				self->Class.Player.Spark_xOffset = 50;
				self->Class.Player.Spark_yOffset = -8;
				self->Class.Player.AppliedStopFrames = 13;
				self->Class.Player.HitLevel = IAR_HITLEVEL_HARD;
				
				self->Class.Player.AppliedOnHit = 17;
				self->Class.Player.AppliedOnBlock = 15;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 5; 
				self->Class.Player.AppliedJuggleVs = -3;
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_LOW;
	
				self->Class.Player.CrouchPhase = 0;
	
				self->Class.Player.Launcher = true;
				self->Class.Player.AppliedJuggleTime = 0xFFFF;
				self->Class.Player.JP = 4;
				
				self->Class.Player.AppliedDamage = 140;
				
			break;
			
			case IAR_RYU_STATE_AIR_LP:
				self->Class.Player.CurrentMaxSpState = 3+15+5;
				
				self->Class.Player.Spark_xOffset = 35;
				self->Class.Player.Spark_yOffset = -40;
				self->Class.Player.AppliedStopFrames = 7;
				self->Class.Player.HitLevel = IAR_HITLEVEL_LIGHT;
				
				self->Class.Player.AppliedOnHit = 15;
				self->Class.Player.AppliedOnBlock = 15;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 4; 
				self->Class.Player.AppliedJuggleVs = -8;
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_HIGH;
				self->Class.Player.HitStunType = IAR_STUNTYPE_HEAD;
				
				self->Class.Player.AirAction = false;
				
				self->Class.Player.AppliedDamage = 50;
				
			break;
			
			case IAR_RYU_STATE_AIR_MP:
				self->Class.Player.CurrentMaxSpState = 6+8+6;
				
				self->Class.Player.Spark_xOffset = 35;
				self->Class.Player.Spark_yOffset = -60;
				self->Class.Player.AppliedStopFrames = 7;
				self->Class.Player.HitLevel = IAR_HITLEVEL_MEDIUM;
				
				self->Class.Player.AppliedOnHit = 13;
				self->Class.Player.AppliedOnBlock = 13;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 3; 
				self->Class.Player.AppliedJuggleVs = -8;
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_HIGH;
				self->Class.Player.HitStunType = IAR_STUNTYPE_HEAD;
				
				//self->Class.Player.Launcher = true;
				self->Class.Player.AppliedJuggleTime = 0xFFFF;
				
				self->Class.Player.AirAction = false;
				
				self->Class.Player.JP = 0;
				
				self->Class.Player.AppliedDamage = 45;
				
			break;
			
			case IAR_RYU_STATE_AIR_HP:
				self->Class.Player.CurrentMaxSpState = 5+10+10;
				
				self->Class.Player.Spark_xOffset = 35;
				self->Class.Player.Spark_yOffset = -60;
				self->Class.Player.AppliedStopFrames = 7;
				self->Class.Player.HitLevel = IAR_HITLEVEL_HARD;
				
				self->Class.Player.AppliedOnHit = 17;
				self->Class.Player.AppliedOnBlock = 17;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 6; 
				self->Class.Player.AppliedJuggleVs = -9;
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_HIGH;
				self->Class.Player.HitStunType = IAR_STUNTYPE_HEAD;
				
				self->Class.Player.AirAction = false;
				
				self->Class.Player.AppliedDamage = 140;
				
			break;
			
			case IAR_RYU_STATE_AIR_LK:
				self->Class.Player.CurrentMaxSpState = 2+6+4+10;
				
				self->Class.Player.Spark_xOffset = 35;
				self->Class.Player.Spark_yOffset = -40;
				self->Class.Player.AppliedStopFrames = 7;
				self->Class.Player.HitLevel = IAR_HITLEVEL_LIGHT;
				
				self->Class.Player.AppliedOnHit = 15;
				self->Class.Player.AppliedOnBlock = 15;
				
				self->Class.Player.AppliedPushback = 5; 
				self->Class.Player.AppliedJuggleHs = 4; 
				self->Class.Player.AppliedJuggleVs = -8;
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_HIGH;
				self->Class.Player.HitStunType = IAR_STUNTYPE_HEAD;
				
				self->Class.Player.AirAction = false;
				
				self->Class.Player.AppliedDamage = 60;
				
			break;
			
			case IAR_RYU_STATE_AIR_MK:
				self->Class.Player.CurrentMaxSpState = 6+6+10;
				
				self->Class.Player.Spark_xOffset = 35;
				self->Class.Player.Spark_yOffset = -40;
				self->Class.Player.AppliedStopFrames = 7;
				self->Class.Player.HitLevel = IAR_HITLEVEL_MEDIUM;
				
				self->Class.Player.AppliedOnHit = 15;
				self->Class.Player.AppliedOnBlock = 15;
				
				self->Class.Player.AppliedPushback = 7; 
				self->Class.Player.AppliedJuggleHs = 4; 
				self->Class.Player.AppliedJuggleVs = -8;
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_HIGH;
				self->Class.Player.HitStunType = IAR_STUNTYPE_HEAD;
				
				self->Class.Player.AirAction = false;
				
				self->Class.Player.AppliedDamage = 110;
				
			break;
			
			case IAR_RYU_STATE_AIR_HK:
				self->Class.Player.CurrentMaxSpState = 6+6+10;
				
				self->Class.Player.Spark_xOffset = 60;
				self->Class.Player.Spark_yOffset = -40;
				self->Class.Player.AppliedStopFrames = 8;
				self->Class.Player.HitLevel = IAR_HITLEVEL_HARD;
				
				self->Class.Player.AppliedOnHit = 17;
				self->Class.Player.AppliedOnBlock = 17;
				
				self->Class.Player.AppliedPushback = 7; 
				self->Class.Player.AppliedJuggleHs = 4; 
				self->Class.Player.AppliedJuggleVs = -8;
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_HIGH;
				self->Class.Player.HitStunType = IAR_STUNTYPE_HEAD;
				
				self->Class.Player.AirAction = false;
				
				self->Class.Player.AppliedDamage = 130;
				
			break;
			
			
			case IAR_RYU_STATE_F_HP:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = -6+_IAR_RYU_F_HP_FRAMES*4;
				
				self->Class.Player.Spark_xOffset = 50;
				self->Class.Player.Spark_yOffset = -65;
				self->Class.Player.AppliedStopFrames = 11;
				self->Class.Player.HitLevel = IAR_HITLEVEL_HARD;
				
				self->Class.Player.AppliedOnHit = 22;
				self->Class.Player.AppliedOnBlock = 19;
				
				self->Class.Player.AppliedPushback = 3; 
				self->Class.Player.AppliedJuggleHs = 5; 
				self->Class.Player.AppliedJuggleVs = -6;//-6;
				
				self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_HIGH;
				self->Class.Player.HitStunType = IAR_STUNTYPE_HEAD;
				
				self->Class.Player.AppliedDamage = 85;

				
			break;
			
			case IAR_RYU_STATE_SHORYUKEN:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 100;
				self->Class.Player.Spark_xOffset = 40;
				self->Class.Player.Spark_yOffset = -65;
				self->Class.Player.AppliedStopFrames = 11;
				self->Class.Player.HitLevel = IAR_HITLEVEL_SPECIAL;
				
				self->Class.Player.AppliedOnBlock = 19;
				
				self->Class.Player.AppliedPushback = 6; 
				self->Class.Player.AppliedJuggleHs = 5; 
				self->Class.Player.AppliedJuggleVs = -10;

				self->Class.Player.Launcher = true;
				self->Class.Player.AppliedJuggleTime = 0xFFFF;
				self->Class.Player.AirAction = false;
				
				self->Class.Player.JP = 2;
				
				self->Class.Player.DefenseFlags = IAR_DEFENSE_ANTI_AIR | IAR_DEFENSE_THROW_INV;
				if (self->Class.Player.BufferedSpecialLevel >= 2) 
					self->Class.Player.DefenseFlags = IAR_DEFENSE_INVINCIBLE;
				
				self->Class.Player.AppliedDamage = 140 + 10*self->Class.Player.BufferedSpecialLevel;
				self->Class.Player.AppliedChip = 10;
			
			break;
			
			case IAR_RYU_STATE_TATSU:
				self->Class.Player.hspeed = 4*self->Class.Player.xdir*IAR_FIXED_SCALE;
				self->Class.Player.CurrentMaxSpState = 100;//4+_IAR_RYU_TATSU_FRAMES*2;
				self->Class.Player.Spark_xOffset = 36;
				self->Class.Player.Spark_yOffset = -55;
				self->Class.Player.AppliedStopFrames = 11;
				self->Class.Player.HitLevel = IAR_HITLEVEL_SPECIAL;
				
				self->Class.Player.AppliedOnBlock = 19;
				
				self->Class.Player.AppliedPushback = 6; 
				self->Class.Player.AppliedJuggleHs = 9; 
				self->Class.Player.AppliedJuggleVs = -7;

				self->Class.Player.Launcher = true;
				self->Class.Player.AppliedJuggleTime = 0xFFFF;
				self->Class.Player.AirAction = false;
				
				self->Class.Player.JP = 2;
				
				self->Class.Player.DefenseFlags = IAR_DEFENSE_THROW_INV;
				
				self->Class.Player.AppliedDamage = 160;
				self->Class.Player.AppliedChip = 10;
			
			break;
			
			case IAR_RYU_STATE_HADOKEN:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = 46;//38;
				
			break;
			
			case IAR_RYU_STATE_WHIFF_GRAB:
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = _IAR_RYU_WHIFF_GRAB_FRAMES*4;
				self->Class.Player.AllowedBuffers = 0;
				
			break;
			
			case IAR_RYU_STATE_THROW_FORWARD:
			{
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = _IAR_RYU_THROW_FORWARD_FRAMES*4 + 10;
				IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID, self->gs);
				if (enemy != NULL)
					enemy->Class.Player.invisible = 100;
			}
			break;
			
			case IAR_RYU_STATE_THROW_BACK:
			{
				self->Class.Player.hspeed = 0;
				self->Class.Player.CurrentMaxSpState = _IAR_RYU_THROW_BACK_FRAMES*4;
				IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID, self->gs);
				if (enemy != NULL)
					enemy->Class.Player.invisible = 100;
			}
			break;


			
		}
		
		self->Class.Player.spState = self->Class.Player.CurrentMaxSpState;
		self->Class.Player.LastAction = self->Class.Player.BufferedAction;
		self->Class.Player.BufferedAction = ___IAR_RYU_STATE_NONE;
		if (!self->Class.Player.inAir)
		self->Class.Player.xdraw = self->Class.Player.xdir;
		
		
		
	}
	
	_IAR_PlayerDefaultStep(self);
	
	//define proper state description ///////////////////////------------------------------
	if (self->Class.Player.spState == 0) {
		if (!self->Class.Player.inAir) {
			
			if (self->Class.Player.TurnAround == 0) {
			
				if (!IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_R)
					&& !IAR_IsInputFlagged(self->Class.Player.CurrentFrameInputs, DHEX_DIR_L)) { //not in air and not holding either r or l
					
					_IAR_PlayerSetState(self, IAR_RYU_STATE_IDLE);
					self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/5;
					self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.IdleFrames;
					self->Class.Player.CurrentMaxFrame = _IAR_RYU_IDLE_FRAMES;
				 
				
				}
				
				else {
					if (isign(self->Class.Player.xdraw*self->Class.Player.hspeed) == 1) {
						_IAR_PlayerSetState(self, IAR_RYU_STATE_WALK_FORWARD);	
						self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/5;
						self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.WalkForwardFrames;
						self->Class.Player.CurrentMaxFrame = _IAR_RYU_WALK_FORWARD_FRAMES;
					}
					
					if (isign(self->Class.Player.xdraw*self->Class.Player.hspeed) ==-1){
						_IAR_PlayerSetState(self, IAR_RYU_STATE_WALK_BACK);	
						self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/5;
						self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.WalkBackFrames;
						self->Class.Player.CurrentMaxFrame = _IAR_RYU_WALK_BACK_FRAMES;
					}
					
				}
				
				if (self->Class.Player.PreJump > 0 || self->Class.Player.LandingFrames) {
					_IAR_PlayerSetState(self, IAR_RYU_STATE_PREJUMP);	
					self->Class.Player.FrameSpeed = 0;
					self->Class.Player.CurrentFrameSet = &self->Class.Player.Char.Ryu.PreJumpFrame;
					self->Class.Player.CurrentMaxFrame = 1;
				}
				
				if (self->Class.Player.CrouchPhase < self->Class.Player.MAX_CROUCH_PHASE) {
					_IAR_PlayerSetState(self, IAR_RYU_STATE_TO_CROUCH);
					self->Class.Player.FrameSpeed = 0;
					self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.ToCrouchFrames;
					self->Class.Player.CurrentMaxFrame = _IAR_RYU_TO_CROUCH_FRAMES;
					self->Class.Player.FrameIndex = iclamp(self->Class.Player.MAX_CROUCH_PHASE - (1+self->Class.Player.CrouchPhase), 0, 99)*IAR_FIXED_SCALE;
					self->Class.Player.AnimationLoops = false;
					
				}
				
				
			}
			else {
				
				if (self->Class.Player.CrouchPhase == self->Class.Player.MAX_CROUCH_PHASE) {
				
					_IAR_PlayerSetState(self, IAR_RYU_STATE_STAND_TURN);
					self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.StandTurnFrames;
					self->Class.Player.CurrentMaxFrame = _IAR_RYU_STAND_TURN_FRAMES;

				
				}
				else {
					_IAR_PlayerSetState(self, IAR_RYU_STATE_CROUCH_TURN);
					self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.CrouchTurnFrames;
					self->Class.Player.CurrentMaxFrame = _IAR_RYU_CROUCH_TURN_FRAMES;
						
				}
				
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.FrameIndex =  iclamp(self->Class.Player.MAX_TURN_AROUND - self->Class.Player.TurnAround, 0, 99)*IAR_FIXED_SCALE;
				self->Class.Player.AnimationLoops = false;
			}
			
			if (self->Class.Player.ProxyBlock > 0) {
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.FrameIndex = 0;
				self->Class.Player.AnimationLoops = false;
				_IAR_PlayerSetState(self, IAR_RYU_STATE_PROXY_BLOCK);
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.StBlockFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_ST_BLOCK_FRAMES;
			}
			
			
		}
		else {
			
			if (isign(self->Class.Player.xdraw*self->Class.Player.JumpDir) ==-1) { //back
				_IAR_PlayerSetState(self, IAR_RYU_STATE_JUMP_BACK);	
				self->Class.Player.FrameSpeed = 0;
				
				const int tempTurningPoint = 12;
				
				if (fixed_cast(self->Class.Player.FrameIndex) == 0) self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/3;
				
				if (self->Class.Player.JumpCounter >= tempTurningPoint) self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/2;
				
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.JumpBackFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_JUMP_BACK_FRAMES;	
				self->Class.Player.AnimationLoops = false;
			}
			
			if (isign(self->Class.Player.xdraw*self->Class.Player.JumpDir) == 1) { //forward
				_IAR_PlayerSetState(self, IAR_RYU_STATE_JUMP_FORWARD);	
				self->Class.Player.FrameSpeed = 0;
				
				const int tempTurningPoint = 12;
				
				if (fixed_cast(self->Class.Player.FrameIndex) == 0) self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/3;
				
				if (self->Class.Player.JumpCounter >= tempTurningPoint) self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/2;
				
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.JumpForwardFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_JUMP_FORWARD_FRAMES;	
				self->Class.Player.AnimationLoops = false;
			}
			
			if (self->Class.Player.JumpDir == 0) { //neutral
				_IAR_PlayerSetState(self, IAR_RYU_STATE_JUMP_NEUTRAL);	

				self->Class.Player.FrameSpeed = 0;
				
				const int tempTurningPoint = 7;
				
				if (self->Class.Player.JumpCounter >= tempTurningPoint) self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/4;
				
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.JumpNeutralFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_JUMP_NEUTRAL_FRAMES;	
				self->Class.Player.AnimationLoops = false;
			}
			
			if (self->Class.Player.Juggled > 0) {
				_IAR_PlayerSetState(self, IAR_RYU_STATE_JUGGLED);	
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.JuggledFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_JUGGLED_FRAMES;	
				self->Class.Player.AnimationLoops = false;
				self->Class.Player.pDepth = DHEX_LAYER_OBJ3;
				
				self->Class.Player.FrameIndex = 0;
				if (fixed_roundCast(self->Class.Player.vspeed) > -4) self->Class.Player.FrameIndex = 1*IAR_FIXED_SCALE;
				if (fixed_roundCast(self->Class.Player.vspeed) > -2) self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
				if (fixed_roundCast(self->Class.Player.vspeed) >  0) self->Class.Player.FrameIndex = 3*IAR_FIXED_SCALE;
				if (fixed_roundCast(self->Class.Player.vspeed) >  2) self->Class.Player.FrameIndex = 4*IAR_FIXED_SCALE;
				
				
			}
			
			
		}
		
	} /////////////////////////////// NON SP STATE
	
	else {
		
		switch(self->Class.Player.LastAction) {
			case IAR_RYU_STATE_KNOCKDOWN:
				_IAR_PlayerSetState(self, IAR_RYU_STATE_KNOCKDOWN);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.KnockdownFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_KNOCKDOWN_FRAMES;
				self->Class.Player.AnimationLoops = false;				
			break;
			
			case IAR_RYU_STATE_WAKEUP:
				_IAR_PlayerSetState(self, IAR_RYU_STATE_WAKEUP);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.WakeupFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_WAKEUP_FRAMES;
				self->Class.Player.AnimationLoops = false;			
			break;
			
			case IAR_RYU_STATE_ST_LP:
				
				_IAR_PlayerSetState(self, IAR_RYU_STATE_ST_LP);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/2;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.stLP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_ST_LP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				
			break;
			
			case IAR_RYU_STATE_ST_MP:
				
				_IAR_PlayerSetState(self, IAR_RYU_STATE_ST_MP);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.stMP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_ST_MP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				if (fixed_cast(self->Class.Player.FrameIndex) == 0) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/3; 
				if (fixed_cast(self->Class.Player.FrameIndex) == 1) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/1;
				if (fixed_cast(self->Class.Player.FrameIndex) == 2) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/2;
				if (fixed_cast(self->Class.Player.FrameIndex) == 3) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				if (fixed_cast(self->Class.Player.FrameIndex) == 4) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				if (fixed_cast(self->Class.Player.FrameIndex) == 5) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				if (fixed_cast(self->Class.Player.FrameIndex) == 6) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				
				if (fixed_cast(self->Class.Player.FrameIndex) == 3) self->Class.Player.Spark_yOffset = -90;
				
			break;
			
			case IAR_RYU_STATE_ST_HP:
				
				_IAR_PlayerSetState(self, IAR_RYU_STATE_ST_HP);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				if (fixed_cast(self->Class.Player.FrameIndex) == 4) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/8;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.stHP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_ST_HP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
			break;
			
			case IAR_RYU_STATE_CR_LP:
				
				_IAR_PlayerSetState(self, IAR_RYU_STATE_CR_LP);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/2;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.crLP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_CR_LP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
			break;
			
			
			//4+6+5+4 //startup + active + rec with active pose + rec
			case IAR_RYU_STATE_CR_MP:
			{
				int spIndex = self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				_IAR_PlayerSetState(self, IAR_RYU_STATE_CR_HP);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.crMP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_CR_MP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				if (spIndex >= 0)  self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;
				if (spIndex >= 2)  self->Class.Player.FrameIndex = 1*IAR_FIXED_SCALE;
				if (spIndex >= 4)  self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
				if (spIndex >= 10) self->Class.Player.FrameIndex = 3*IAR_FIXED_SCALE;
				if (spIndex >= 15) self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;

				
			}
			break;
			
			
			case IAR_RYU_STATE_CR_HP:
				
				_IAR_PlayerSetState(self, IAR_RYU_STATE_CR_HP);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.crHP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_CR_HP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				if (fixed_cast(self->Class.Player.FrameIndex) == 0) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4; 
				if (fixed_cast(self->Class.Player.FrameIndex) == 1) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/1;
				if (fixed_cast(self->Class.Player.FrameIndex) == 2) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/2;
				if (fixed_cast(self->Class.Player.FrameIndex) == 3) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				if (fixed_cast(self->Class.Player.FrameIndex) == 4) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/10;
				if (fixed_cast(self->Class.Player.FrameIndex) == 5) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				if (fixed_cast(self->Class.Player.FrameIndex) == 6) self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				
				if (fixed_cast(self->Class.Player.FrameIndex) == 3) self->Class.Player.Spark_yOffset = -100;
				
			break;
			
			case IAR_RYU_STATE_ST_LK:
			{
				
				self->Class.Player.hspeed = 0;
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_ST_LK);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.stLK_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_ST_LK_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				int spIndex = self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				if (spIndex >= 0)   self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;
				if (spIndex >= 2)   self->Class.Player.FrameIndex = 1*IAR_FIXED_SCALE;
				if (spIndex >= 4)   self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
				if (spIndex >= 8)   self->Class.Player.FrameIndex = 3*IAR_FIXED_SCALE;
				if (spIndex >= 12)  self->Class.Player.FrameIndex = 4*IAR_FIXED_SCALE;
				
				const int dist = 20;
				if (spIndex == 0)  self->Class.Player.x += dist*self->Class.Player.xdraw*IAR_FIXED_SCALE;
				if (spIndex == 12) self->Class.Player.x -= dist*self->Class.Player.xdraw*IAR_FIXED_SCALE;
					
			}
			break;
			
			
			case IAR_RYU_STATE_ST_MK:
			{
				
				//self->Class.Player.hspeed = 0;
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_ST_MK);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.stMK_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_ST_MK_FRAMES;
				self->Class.Player.AnimationLoops = false;
			
				int spIndex = self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				switch(spIndex) {
					
					case 0:
						self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;
					break;
					
					case 4:
						self->Class.Player.FrameIndex = 1*IAR_FIXED_SCALE;
					break;
					
					case 5:
						self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
					break;
					
					case 6:
						self->Class.Player.FrameIndex = 1*IAR_FIXED_SCALE;
					break;
					
					case 8:
						self->Class.Player.FrameIndex = 3*IAR_FIXED_SCALE;
					break;
					
					case 10:
						self->Class.Player.FrameIndex = 4*IAR_FIXED_SCALE;
					break;
					
					case 14:
						self->Class.Player.FrameIndex = 5*IAR_FIXED_SCALE;
					break;
					
					case 18:
						self->Class.Player.FrameIndex = 6*IAR_FIXED_SCALE;
					break;
					
					case 22:
						self->Class.Player.FrameIndex = 7*IAR_FIXED_SCALE;
					break;
					
					case 26:
						self->Class.Player.FrameIndex = 8*IAR_FIXED_SCALE;
					break;
					
					default: break;
					
				}
				
				
				if (spIndex == 6) {
					self->Class.Player.AttackHits = 1;
					self->Class.Player.Spark_xOffset = 50;
					self->Class.Player.Spark_yOffset = -20;
					self->Class.Player.HitStunType = IAR_STUNTYPE_HEAD;
					self->Class.Player.AttackFlags = IAR_ATTACK_CANBLOCK_HIGH;
				}
				
			}
			break;
			
			
			case IAR_RYU_STATE_ST_HK:
				
				_IAR_PlayerSetState(self, IAR_RYU_STATE_ST_HK);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.stHK_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_ST_HK_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				
			break;
			
			case IAR_RYU_STATE_CR_LK:
			{
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_CR_LK);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.crLK_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_CR_LK_FRAMES;
				self->Class.Player.AnimationLoops = false;
			
				int spIndex = self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				switch(spIndex) {
					case 0:
						self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;
					break;
					
					case 3:
						self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
					break;
					
					case 4:
						self->Class.Player.FrameIndex = 1*IAR_FIXED_SCALE;
					break;
					
					case 8:
						self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
					break;
					
					case 10:
						self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;
					break;
					
					default: break;
					
				}
				
				
			}
			break;
			
			case IAR_RYU_STATE_CR_MK:
			{
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_CR_MK);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.crMK_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_CR_MK_FRAMES;
				self->Class.Player.AnimationLoops = false;
			
				int spIndex = self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				switch(spIndex) {
					
					case 0:
						self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;
					break;

					case 2:
						self->Class.Player.FrameIndex = 1*IAR_FIXED_SCALE;
					break;

					case 4:
						self->Class.Player.FrameIndex = 3*IAR_FIXED_SCALE;
					break;

					case 5:
						self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
					break;

					case 9:
						self->Class.Player.FrameIndex = 3*IAR_FIXED_SCALE;
					break;

					case 13:
						self->Class.Player.FrameIndex = 1*IAR_FIXED_SCALE;
					break;

					case 17:
						self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;
					break;
					
					default: break;

				}
				
				
			}
			break;
			
			case IAR_RYU_STATE_CR_HK:
			{
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_CR_HK);
				self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/4;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.crHK_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_CR_HK_FRAMES;
				self->Class.Player.AnimationLoops = false;
			
				int spIndex = self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				if (spIndex >= 4)  self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/2;
				if (spIndex >= 8)  self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/5;


				
				
			}
			break;
			
			
			case IAR_RYU_STATE_AIR_LP:
			{
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_AIR_LP);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.airLP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_AIR_LP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				
				int spIndex = self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				switch (spIndex) {
					case 0:
						self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;
					break;
					
					case 3:
						self->Class.Player.FrameIndex = 1*IAR_FIXED_SCALE;
					break;
					
					case 18:
						self->Class.Player.FrameIndex = 0*IAR_FIXED_SCALE;
					break;
					
					default: break;
					
				}

				
				
			}
			break;
			
			case IAR_RYU_STATE_AIR_MP:
			{
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_AIR_MP);
				self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/2;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.airMP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_AIR_MP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				
				int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				if (spIndex >= 13) {
					self->Class.Player.FrameSpeed = 0;
				}
				if (spIndex == 13+8) self->Class.Player.FrameIndex = 4*IAR_FIXED_SCALE;
				
				if (spIndex == 10) {
					self->Class.Player.AttackHits = 1;
					self->Class.Player.Spark_yOffset = -80;
					self->Class.Player.JP = 1;
				}
				
			}
			break;
			
			
			case IAR_RYU_STATE_AIR_HP:
			{
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_AIR_HP);
				self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/5;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.airHP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_AIR_HP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				
			}
			break;
			
			
			case IAR_RYU_STATE_AIR_LK:
			{
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_AIR_LK);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.airLK_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_AIR_LK_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				switch(spIndex) {
					case 1:
						self->Class.Player.FrameIndex = 0;
					break;
					
					case 2:
						self->Class.Player.FrameIndex = IAR_FIXED_SCALE;
					break;
					
					case 3:
						self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
					break;
					
					case (3+6):
						self->Class.Player.FrameIndex = IAR_FIXED_SCALE;
					break;
					
				}
				
				
			}
			break;
			
			
			case IAR_RYU_STATE_AIR_MK:
			{
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_AIR_MK);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.airMHK_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_AIR_MHK_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				switch(spIndex) {
					case 1:
						self->Class.Player.FrameIndex = 0;
					break;
					
					case 4:
						self->Class.Player.FrameIndex = IAR_FIXED_SCALE;
					break;
					
					case 7:
						self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
					break;
					
					case 11:
						self->Class.Player.FrameIndex = 4*IAR_FIXED_SCALE;
					break;
					
					case (11+8):
						self->Class.Player.FrameIndex = IAR_FIXED_SCALE;
					break;
					
				}
				
				
			}
			break;
			
			
			case IAR_RYU_STATE_AIR_HK:
			{
					
				_IAR_PlayerSetState(self, IAR_RYU_STATE_AIR_MK);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.airMHK_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_AIR_MHK_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				switch(spIndex) {
					case 1:
						self->Class.Player.FrameIndex = 0;
					break;
					
					case 4:
						self->Class.Player.FrameIndex = IAR_FIXED_SCALE;
					break;
					
					case 7:
						self->Class.Player.FrameIndex = 3*IAR_FIXED_SCALE;
					break;
					
					case 11:
						self->Class.Player.FrameIndex = 4*IAR_FIXED_SCALE;
					break;
					
					case (11+8):
						self->Class.Player.FrameIndex = IAR_FIXED_SCALE;
					break;
					
				}
				
				
			}
			break;
			
			
			case IAR_RYU_STATE_F_HP:
			{
				
				_IAR_PlayerSetState(self, IAR_RYU_STATE_F_HP);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/4;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.fHP_Frames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_F_HP_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				int spIndex = self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				if (spIndex == 18) self->Class.Player.AttackHits = 1;
				
				if (spIndex >= 18) {
					self->Class.Player.Spark_yOffset = -30;
					
				}
				
				if (spIndex >= 4*4) {
					self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/2;
					
				}
				
				if (spIndex >= 4*4 + 8) {
					self->Class.Player.FrameSpeed = 0;
					
				}
				
				if (self->Class.Player.spState <= 4*2 + 3) {
					self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/8;
					
				}
				
				
					
			}	
			break;
			
			
			case IAR_RYU_STATE_HADOKEN:
			{
				self->Class.Player.pDepth = DHEX_LAYER_OBJ5;
				
				_IAR_PlayerSetState(self, IAR_RYU_STATE_HADOKEN);
				self->Class.Player.FrameSpeed = 1*IAR_FIXED_SCALE/2;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.HadokenFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_HADOKEN_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				if (spIndex >= 7) {
					self->Class.Player.FrameSpeed = 0;
					self->Class.Player.FrameIndex = 3*IAR_FIXED_SCALE;
					const int interval = 4;
					if ((spIndex % (2*interval)) < interval)
						self->Class.Player.FrameIndex = 4*IAR_FIXED_SCALE;
					
					
				}
				
				if (spIndex >= (self->Class.Player.CurrentMaxSpState - 4)) {
					self->Class.Player.FrameIndex = 5*IAR_FIXED_SCALE;
				}
				
				
				if (spIndex == 6) {	//6 since created objs start acting in the next frame
					IAR_Obj* fireball = IAR_InstanceCreate(IAR_CreateHadoken, NULL, self->gs);
					fireball->Class.Projectile.x = self->Class.Player.x + IAR_FIXED_SCALE*(70*self->Class.Player.xdraw); 
					fireball->Class.Projectile.y = self->Class.Player.y + IAR_FIXED_SCALE*(-55); 
					fireball->Class.Projectile.hspeed = ((2.65*IAR_FIXED_SCALE+(self->Class.Player.BufferedSpecialLevel*(IAR_FIXED_SCALE/2)))*self->Class.Player.xdraw); 
					fireball->Class.Projectile.xdraw = self->Class.Player.xdraw;
					fireball->Class.Projectile.Pside = self->Class.Player.Pside;
					self->Class.Player.Char.Ryu.LastHadokenID = fireball->InstanceID;
				}
				
				
			}
			break;
			
			case IAR_RYU_STATE_SHORYUKEN:
			{
				_IAR_PlayerSetState(self, IAR_RYU_STATE_SHORYUKEN);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.ShoryukenFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_SHORYUKEN_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
				int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				if (spIndex == 3) self->Class.Player.FrameIndex = IAR_FIXED_SCALE;
				if (spIndex == 5) {
					self->Class.Player.FrameIndex = 2*IAR_FIXED_SCALE;
					self->Class.Player.vspeed = (-4 -2*self->Class.Player.BufferedSpecialLevel)*IAR_FIXED_SCALE;
					self->Class.Player.hspeed = (self->Class.Player.xdraw)*IAR_FIXED_SCALE;
				}
				
				if (spIndex == 7) self->Class.Player.DefenseFlags = 0;
				
				if (self->Class.Player.inAir) {
					if (fixed_roundCast(self->Class.Player.vspeed) > -1) self->Class.Player.FrameIndex = 3*IAR_FIXED_SCALE;
					if (fixed_roundCast(self->Class.Player.vspeed) >  1) self->Class.Player.FrameIndex = 4*IAR_FIXED_SCALE;
					//if (fixed_roundCast(self->Class.Player.vspeed) >  2) self->Class.Player.FrameIndex = 5*IAR_FIXED_SCALE;
					
					
				}
			
			}
			break;	


			case IAR_RYU_STATE_TATSU:
			{
				const int spd = 3;
				_IAR_PlayerSetState(self, IAR_RYU_STATE_TATSU);
				self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/spd;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.TatsuFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_TATSU_FRAMES;
				self->Class.Player.AnimationLoops = false;
				self->Class.Player.ApplyGrav = false;
				
				int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				if (spIndex < 1+(spd*3)) {
					self->Class.Player.y -= 2*IAR_FIXED_SCALE;
				}
				
				if (spIndex == 1+(spd*7)) {
					self->Class.Player.ApplyGrav = true;
					self->Class.Player.vspeed = 3*IAR_FIXED_SCALE;
				}
				
			
			}
			break;				
			
			case IAR_RYU_STATE_SHORYUKEN_RECOVERY:
			{
				_IAR_PlayerSetState(self, IAR_RYU_STATE_SHORYUKEN_RECOVERY);
				self->Class.Player.FrameSpeed = 0;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.ShoryukenFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_SHORYUKEN_FRAMES;
				self->Class.Player.AnimationLoops = false;
				

				self->Class.Player.FrameIndex = 4*IAR_FIXED_SCALE;
				
			
			}
			break;
			
			case IAR_RYU_STATE_WHIFF_GRAB:
			{
				_IAR_PlayerSetState(self, IAR_RYU_STATE_WHIFF_GRAB);
				self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/4;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.WhiffGrabFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_WHIFF_GRAB_FRAMES;
				self->Class.Player.AnimationLoops = false;
				
			}
			break;
			
			case IAR_RYU_STATE_THROW_FORWARD:
			{
				const int spd = 4;
				_IAR_PlayerSetState(self, IAR_RYU_STATE_THROW_FORWARD);
				self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/spd;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.ThrowForwardFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_THROW_FORWARD_FRAMES;
				self->Class.Player.AnimationLoops = false;		

				int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				
				IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID, self->gs);
				if (enemy != NULL) {
					DHEX_Sprite* img = enemy->Class.Player.GutStunAtFeet.img;
					int Img_xCenter  = enemy->Class.Player.GutStunAtFeet.xcenter;
					int Img_yCenter  = enemy->Class.Player.GutStunAtFeet.ycenter;
					
					int x_offset = 40;
					int y_offset = 0; //distance from self
					int8_t xdraw = -self->Class.Player.xdraw;
					float angle = 0.0;
					
					bool draw = true;
					
					if (spIndex >= 1 + (1*spd)) {
						img = enemy->Class.Player.JuggledAtNeck.img;
						Img_xCenter = enemy->Class.Player.JuggledAtNeck.xcenter;
						Img_yCenter = enemy->Class.Player.JuggledAtNeck.ycenter;
						
						x_offset = -40;
						y_offset = -85;	
						xdraw = -self->Class.Player.xdraw;	
						angle = 0.0;
					}
					
					if (spIndex >= 1 + (2*spd)) {
						img = enemy->Class.Player.JuggledAtNeck.img;
						Img_xCenter = enemy->Class.Player.JuggledAtNeck.xcenter;
						Img_yCenter = enemy->Class.Player.JuggledAtNeck.ycenter;
						
						x_offset = -8;
						y_offset = -82;	
						xdraw = -self->Class.Player.xdraw;	
						angle = 0.0;
					}
					
					if (spIndex >= 1 + (3*spd)) {
						img = enemy->Class.Player.JuggledAtNeck.img;
						Img_xCenter = enemy->Class.Player.JuggledAtNeck.xcenter;
						Img_yCenter = enemy->Class.Player.JuggledAtNeck.ycenter;
						
						x_offset = 30;
						y_offset = -82;	
						xdraw = -self->Class.Player.xdraw;	
						angle = xdraw*90.0;
					}
					
					if (spIndex >= 1 + (4*spd)) {
						enemy->Class.Player.invisible = 0;
						draw = false;
					}
					
					
					DHEX_DrawLayer(global.app, DHEX_LAYER_OBJ5);
					if (draw)
					DHEX_DrawSprite(global.app, img,
									fixed_roundCast(self->Class.Player.x) + x_offset*self->Class.Player.xdraw - self->gs->cam_x,
									fixed_roundCast(self->Class.Player.y) + y_offset - self->gs->cam_y,
									Img_xCenter,
									Img_yCenter,
									xdraw, 1, angle);
									
					if (spIndex == 1 + (4*spd)) {
						_IAR_PlayerResetStateDescriptors(enemy);
						_IAR_PlayerSetState(enemy, enemy->Class.Player.JuggledStateRef);
						enemy->Class.Player.inAir = true;
						enemy->Class.Player.Juggled = 0xFFFF;
						enemy->Class.Player.hspeed = 8*IAR_FIXED_SCALE*self->Class.Player.xdraw;
						enemy->Class.Player.vspeed = -6*IAR_FIXED_SCALE;
						enemy->Class.Player.x = self->Class.Player.x + 40*IAR_FIXED_SCALE*self->Class.Player.xdraw;
						enemy->Class.Player.y = self->Class.Player.y - 20*IAR_FIXED_SCALE;
						enemy->Class.Player.JuggleCounter = 0;
						
						enemy->Class.Player.CurrentFrameSet = enemy->Class.Player.JuggledFramesRef;
						enemy->Class.Player.CurrentMaxFrame = enemy->Class.Player.JuggledMaxFrameRef;
						IAR_PlayerRefreshCurrentBXG(enemy);
						enemy->Class.Player.health -= 120;
						enemy->Class.Player.AirAction = false;
						
						
					}
					
				}
				
				
			}
			break;		
			
			case IAR_RYU_STATE_THROW_BACK:
			{
				const int spd = 4;
				_IAR_PlayerSetState(self, IAR_RYU_STATE_THROW_BACK);
				self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/spd;
				self->Class.Player.CurrentFrameSet = self->Class.Player.Char.Ryu.ThrowBackFrames;
				self->Class.Player.CurrentMaxFrame = _IAR_RYU_THROW_BACK_FRAMES;
				self->Class.Player.AnimationLoops = false;		

				int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
				
				
				IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID, self->gs);
				if (enemy != NULL) {
					DHEX_Sprite* img = enemy->Class.Player.GutStunAtFeet.img;
					int Img_xCenter  = enemy->Class.Player.GutStunAtFeet.xcenter;
					int Img_yCenter  = enemy->Class.Player.GutStunAtFeet.ycenter;
					
					int x_offset = 40;
					int y_offset = 0; //distance from self
					int8_t xdraw = -self->Class.Player.xdraw;
					float angle = 0.0;
					
					bool draw = true;
					
					if (spIndex >= 1 + (1*spd)) {
						img = enemy->Class.Player.GutStunAtChest.img;
						Img_xCenter = enemy->Class.Player.GutStunAtChest.xcenter;
						Img_yCenter = enemy->Class.Player.GutStunAtChest.ycenter;
						
						x_offset =  20;
						y_offset = -60;	
						xdraw = -self->Class.Player.xdraw;	
						angle = 0.0;
					}
					
					if (spIndex >= 1 + (2*spd)) {
						img = enemy->Class.Player.GutStunAtChest.img;
						Img_xCenter = enemy->Class.Player.GutStunAtChest.xcenter;
						Img_yCenter = enemy->Class.Player.GutStunAtChest.ycenter;
						
						x_offset =  -10;
						y_offset = -60;	
						xdraw = -self->Class.Player.xdraw;	
						angle = xdraw*270.0;
					}
					
					if (spIndex >= 1 + (3*spd)) {
						enemy->Class.Player.invisible = 0;
						draw = false;
					}
					
					
					DHEX_DrawLayer(global.app, DHEX_LAYER_OBJ5);
					if (draw)
					DHEX_DrawSprite(global.app, img,
									fixed_roundCast(self->Class.Player.x) + x_offset*self->Class.Player.xdraw - self->gs->cam_x,
									fixed_roundCast(self->Class.Player.y) + y_offset - self->gs->cam_y,
									Img_xCenter,
									Img_yCenter,
									xdraw, 1, angle);
									
					if (spIndex == 1 + (3*spd)) {
						_IAR_PlayerResetStateDescriptors(enemy);
						_IAR_PlayerSetState(enemy, enemy->Class.Player.JuggledStateRef);
						enemy->Class.Player.inAir = true;
						enemy->Class.Player.Juggled = 0xFFFF;
						enemy->Class.Player.hspeed = -8*IAR_FIXED_SCALE*self->Class.Player.xdraw;
						enemy->Class.Player.vspeed = -10*IAR_FIXED_SCALE;
						enemy->Class.Player.x = self->Class.Player.x - 40*IAR_FIXED_SCALE*self->Class.Player.xdraw;
						enemy->Class.Player.y = self->Class.Player.y - 20*IAR_FIXED_SCALE;
						enemy->Class.Player.JuggleCounter = 0;
						enemy->Class.Player.xdraw = self->Class.Player.xdraw;
						
						enemy->Class.Player.CurrentFrameSet = enemy->Class.Player.JuggledFramesRef;
						enemy->Class.Player.CurrentMaxFrame = enemy->Class.Player.JuggledMaxFrameRef;
						IAR_PlayerRefreshCurrentBXG(enemy);
						enemy->Class.Player.health -= 120;
						enemy->Class.Player.AirAction = false;
		
						
					}
					
				}
				
				
			}
			break;
			
		}
		
		if (self->Class.Player.LastAction == IAR_RYU_STATE_GUTSTUN || self->Class.Player.LastAction == IAR_RYU_STATE_HEADSTUN) {
			
			self->Class.Player.pDepth = DHEX_LAYER_OBJ3;
			self->Class.Player.AnimationLoops = false;
			fixed frSpd = -IAR_FIXED_SCALE/4;
			if ((self->Class.Player.CurrentMaxSpState - self->Class.Player.spState) == 0) {
				self->Class.Player.FrameIndex = IAR_FIXED_SCALE*self->Class.Player.CurrentMaxFrame;
			}
			
			self->Class.Player.FrameSpeed = frSpd;		
		}
		
		if (self->Class.Player.LastAction == IAR_RYU_STATE_CR_STUN || self->Class.Player.LastAction == IAR_RYU_STATE_ST_BLOCK ||
			self->Class.Player.LastAction == IAR_RYU_STATE_CR_BLOCK) {
			self->Class.Player.pDepth = DHEX_LAYER_OBJ3;
			self->Class.Player.AnimationLoops = false;
			self->Class.Player.FrameSpeed = IAR_FIXED_SCALE/4;		
			
		}
		
	}
	
	IAR_PlayerRefreshCurrentBXG(self);
	
	return;
}

static void _RyuDraw(IAR_Obj *self) {
	
	if (self->Class.Player.invisible > 0) return;

	IAR_PlayerRefreshCurrentBXG(self);
	
	DHEX_DrawLayer(global.app, DHEX_LAYER_OBJ0);
	const float ShadowShrinkDist = 100.0;
	float ShadowScale = fmax(0.0, ShadowShrinkDist/(ShadowShrinkDist+(fabs(fixed_roundCast(self->Class.Player.y)-STAGE_GROUND))  ));
	
	DHEX_DrawSprite(global.app, self->Class.Player.GroundShadow,
					fixed_roundCast(self->Class.Player.x)-self->gs->cam_x,
					STAGE_GROUND-2-self->gs->cam_y,
					self->Class.Player.GroundShadow->w/2,
					self->Class.Player.GroundShadow->h/2,
					ShadowScale,ShadowScale,0.0);
	

	DHEX_DrawLayer(global.app, self->Class.Player.pDepth);
	DHEX_DrawSprite(global.app, self->Class.Player.CurrentFrame.spr,
					fixed_roundCast(self->Class.Player.x) - self->gs->cam_x,
					fixed_roundCast(self->Class.Player.y) - self->gs->cam_y,
					self->Class.Player.CurrentFrame.x_offset,
					self->Class.Player.CurrentFrame.y_offset,
					self->Class.Player.xdraw,1,0.0);
	
	if (global.Settings.ShowBoxes)	//debug shit
	{ 
		IAR_DrawBXG(IAR_ScaleBXG(*self->Class.Player.CurrentFrame.bxg, self->Class.Player.xdraw, 1),
		fixed_roundCast(self->Class.Player.x)-self->gs->cam_x, fixed_roundCast(self->Class.Player.y)-self->gs->cam_y );
		
		DHEX_DrawLayer(global.app, DHEX_LAYER_HUD0);
		
		char tmp[25];
		IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID, self->gs);
		int adv = (enemy->Class.Player.StopFrames + enemy->Class.Player.spState) - (self->Class.Player.StopFrames + self->Class.Player.spState);
		
		char canBlock = 0;
		if ((self->Class.Player.DefenseFlags & IAR_DEFENSE_CANBLOCK) != 0) canBlock = 1;
		
		int spIndex = 1 + self->Class.Player.CurrentMaxSpState - self->Class.Player.spState;
		
		//sprintf(tmp, "%d    %d\0", self->Class.Player.AllowedBuffers, self->Class.Player.spState);
		//sprintf(tmp, "%d\0", global.CPU.CurrentFrameInputs[self->Class.Player.Pside-1]);
		sprintf(tmp, "%d\0", self->Class.Player.health);
		//IAR_Obj* enemy = IAR_InstanceFind(self->Class.Player.OpponentID);

		int x1, x2; //x1 should be < than x2
		
		if (enemy != NULL) {
			
			if (self->Class.Player.xdir == 1) { //aka we are x1
				x1 = fixed_roundCast(self->Class.Player.x)  + self->Class.Player.PushBoxW;
				x2 = fixed_roundCast(enemy->Class.Player.x) - enemy->Class.Player.PushBoxW;
			}
			else {
				x1 = fixed_roundCast(enemy->Class.Player.x) + enemy->Class.Player.PushBoxW;
				x2 = fixed_roundCast(self->Class.Player.x)  - self->Class.Player.PushBoxW;					
			}
			
			
			
		}
		
		//sprintf(tmp, "%d\0", x2 - x1);
		
		//DHEX_DrawText(global.app, global.resources.font, fixed_roundCast(self->Class.Player.x)-self->gs->cam_x, -20+fixed_roundCast(self->Class.Player.y)-self->gs->cam_y,
		//1,1, DHEX_TEXT_HCENTER_MIDDLE, DHEX_TEXT_VCENTER_MIDDLE, tmp);
		
		DHEX_DrawRect(global.app, fixed_roundCast(self->Class.Player.x) - self->Class.Player.PushBoxW - self->gs->cam_x,
								  fixed_roundCast(self->Class.Player.y) - self->Class.Player.PushBoxH - self->gs->cam_y,
								  self->Class.Player.PushBoxW*2, self->Class.Player.PushBoxH, 200, 10, 200, 255);
							  
	}
	
	
}

static void __local_GenericFrameSetup(IAR_Frame* array, uint16_t nframes, const char* bxgName, uint16_t frame_offset, DHEX_Sprite** images) {
//Create IAR_Frames. All frames will use the same bxg. All frames will use the default center (sprite_w/2, sprite_h)
	for (uint16_t i = 0; i < nframes; i++) {
		array[i].bxg = DHEX_GetBXG(global.app, bxgName);
		array[i].spr = images[frame_offset+i];
		array[i].x_offset = images[frame_offset+i]->w/2;
		array[i].y_offset = images[frame_offset+i]->h;
	}
	
}

static void __local_GenericFrameSetupWOffsets(IAR_Frame* array, uint16_t nframes, const char* bxgFolder, uint16_t frame_offset, DHEX_Sprite** images) {
//Create IAR_Frames. Frames will use dedicated bxgs. The name of the bxgs are expected to be "1.bxg", "2.bxg" and so on.
//the given string refers to a folder where these bxgs lie.
//Frames will use the center specified in their bxg file
	for (uint16_t i = 0; i < nframes; i++) {
		char* tmpStr = malloc(strlen(bxgFolder) + 8); //8 for "/AA.bxg + NULL char"
		sprintf(tmpStr, "%s/%u.bxg\0", bxgFolder, i+1);
		array[i].bxg = DHEX_GetBXG(global.app, tmpStr);
		array[i].spr = images[frame_offset+i];
		array[i].x_offset = array[i].bxg->xcenter;
		array[i].y_offset = array[i].bxg->ycenter;
		free(tmpStr);
	}
	
}

static void __local_FreeIARFrames(IAR_Frame* ref, int nframes) {
	for (uint16_t k = 0; k < nframes; k++) {
		free(ref[k].bxg);
	}
	
}

static void __local_FrameSetup(int nframes, IAR_Frame* array, int imageIndexes[], const char* bxgNames[], DHEX_Sprite** images) {
	for (uint16_t i = 0; i < nframes; i++) {
		array[i].bxg = DHEX_GetBXG(global.app, bxgNames[i]);
		array[i].spr = images[imageIndexes[i]];
		array[i].x_offset = array[i].bxg->xcenter;
		array[i].y_offset = array[i].bxg->ycenter;			
	}
	
}

static void _RyuDestroy(IAR_Obj* self) {
	
	/*
	
	__local_FreeIARFrames(self->Class.Player.Char.Ryu.IdleFrames, _IAR_RYU_IDLE_FRAMES);
	__local_FreeIARFrames(self->Class.Player.Char.Ryu.StandTurnFrames, _IAR_RYU_STAND_TURN_FRAMES);
	__local_FreeIARFrames(self->Class.Player.Char.Ryu.CrouchTurnFrames, _IAR_RYU_CROUCH_TURN_FRAMES);

	
	__local_FreeIARFrames(self->Class.Player.Char.Ryu.WalkForwardFrames, _IAR_RYU_WALK_FORWARD_FRAMES);
	__local_FreeIARFrames(self->Class.Player.Char.Ryu.WalkBackFrames, _IAR_RYU_WALK_BACK_FRAMES);
	__local_FreeIARFrames(self->Class.Player.Char.Ryu.JumpBackFrames, _IAR_RYU_JUMP_BACK_FRAMES);
	__local_FreeIARFrames(self->Class.Player.Char.Ryu.JumpNeutralFrames, _IAR_RYU_JUMP_NEUTRAL_FRAMES);
	__local_FreeIARFrames(self->Class.Player.Char.Ryu.JumpForwardFrames, _IAR_RYU_JUMP_FORWARD_FRAMES);
	__local_FreeIARFrames(self->Class.Player.Char.Ryu.ToCrouchFrames, _IAR_RYU_TO_CROUCH_FRAMES);

	
	free(self->Class.Player.Char.Ryu.PreJumpFrame.bxg);
	*/
	free(self);
	
}


static void _RyuPostStep(IAR_Obj *self) {
	
	_IAR_PlayerDefaultPostChar(self);
	return;
}

static IAR_Obj* _RyuGetCopy(IAR_Obj* self) {
	IAR_Obj* o = malloc(sizeof(IAR_Obj));
	memcpy(o, self, sizeof(IAR_Obj));
	
	o->Class.Player.GutStunFramesRef = o->Class.Player.Char.Ryu.GutStunFrames;
	o->Class.Player.HeadStunFramesRef = o->Class.Player.Char.Ryu.HeadStunFrames;
	o->Class.Player.CrStunFramesRef = o->Class.Player.Char.Ryu.CrStunFrames;
	o->Class.Player.StBlockFramesRef = o->Class.Player.Char.Ryu.StBlockFrames;
	o->Class.Player.CrBlockFramesRef = o->Class.Player.Char.Ryu.CrBlockFrames;
	o->Class.Player.JuggledFramesRef = o->Class.Player.Char.Ryu.JuggledFrames;
	
	return o;
}



IAR_Obj* IAR_CreateRyu(void* data) {
	IAR_Obj* o = (IAR_Obj*)malloc(sizeof(IAR_Obj));
	o->ClassID = IAR_OBJ_RYU;
	o->ClassGroup = IAR_GROUP_CHAR;
	o->pre_step = &_RyuPreStep;
	o->step = &_RyuStep;
	o->post_step = &_RyuPostStep;
	o->draw = &_RyuDraw;
	o->destroy = &_RyuDestroy;
	o->GetCopy = &_RyuGetCopy;
	o->DestroyFlag = false;
	o->Class.Player.x = 200*IAR_FIXED_SCALE;
	o->Class.Player.y = STAGE_GROUND*IAR_FIXED_SCALE;
	o->Class.Player.hspeed = 0;
	o->Class.Player.vspeed = 0;
	o->Class.Player.state = IAR_RYU_STATE_IDLE;
	o->Class.Player.FrameIndex = 0;
	o->Class.Player.FrameSpeed = 2*IAR_FIXED_SCALE/5;
	o->Class.Player.JumpDir = 0;
	o->Class.Player.PreJump = -1;
	o->Class.Player.CurrentFrameInputs = 0;
	o->Class.Player.CurrentFrameInputsPressed = 0;
	o->Class.Player.CurrentFrameInputsReleased = 0;
	o->Class.Player.inAir = false;
	o->Class.Player.LandingFrames = 0;
	o->Class.Player.AnimationLoops = true;
	o->Class.Player.JumpCounter = 0;
	o->Class.Player.TurnAround = 0;
	o->Class.Player.PushBoxW = 22;
	o->Class.Player.PushBoxH = 40;
	o->Class.Player.GroundShadow = DHEX_GetSpriteRaw(global.app, "data/ryu/shadow.png");
	o->Class.Player.Char.Ryu.tempJumpCounter = 0;
	o->Class.Player.xdir = 1;
	o->Class.Player.xdraw = 1;
	o->Class.Player.MAX_CROUCH_PHASE = 3;
	o->Class.Player.CrouchPhase = o->Class.Player.MAX_CROUCH_PHASE;
	o->Class.Player.MAX_TURN_AROUND = 6;
	o->Class.Player.spState = 0;
	o->Class.Player.LastAction = 0;
	o->Class.Player.CanOutputBuffer = true;
	o->Class.Player.BufferedAction = ___IAR_RYU_STATE_NONE;
	o->Class.Player.CurrentMaxSpState = 10; //don't ask why this is a bugfix lmao
	o->Class.Player.AllowedBuffers = 0xFF; //just flag everything up lmao
	o->Class.Player.StopFrames = 0;
	o->Class.Player.DefenseFlags = IAR_DEFENSE_CANBLOCK;
	o->Class.Player.spFriction = 1.0;
	o->Class.Player.pDepth = DHEX_LAYER_OBJ4;//not layer depth, just priority
	o->Class.Player.Juggled = 0;
	o->Class.Player.CounterPushback = false;
	o->Class.Player.ProxyBlock = 0;
	o->Class.Player.ApplyProxyBlock = false;
	o->Class.Player.KnockdownStateRef = IAR_RYU_STATE_KNOCKDOWN;
	o->Class.Player.WakeupStateRef = IAR_RYU_STATE_WAKEUP;
	o->Class.Player.WakeupTime = _IAR_RYU_WAKEUP_FRAMES*4;
	o->Class.Player.MultiHitDelay = 0;
	o->Class.Player.LastHistElapsed = 0;
	o->Class.Player.LastAttackLanded = false;
	o->Class.Player.AirAction = true;
	o->Class.Player.AirAttackSlowdown = false;
	o->Class.Player.JuggleCounter = IAR_MAX_JUGGLE_COUNTER;
	o->Class.Player.AirReset = false;
	o->Class.Player.ApplyGrav = true;
	o->Class.Player.GRAB_RANGE = 8;
	o->Class.Player.invisible = 0; 
	o->Class.Player.CollisionLess = 0;
	o->Class.Player.CanTech = 0;
	o->Class.Player.MAX_HEALTH = 1100;
	o->Class.Player.health = o->Class.Player.MAX_HEALTH;
	o->Class.Player.WakeupThrowInv = 0;
	o->Class.Player.CanKara = true;
	o->Class.Player.InputPolicy = IAR_PLAYER_INPUT_POLICY_HARDWARE;
	o->Class.Player.DefenseScore = 0;
	
	for (uint8_t k = 0; k < IAR_PLAYER_INPUT_HIST_SIZE; k++) {
		o->Class.Player.InputHistList[k].flaggedInputs = 0;
		o->Class.Player.InputHistList[k].deltaT = 255;
	}
	
	int palette = *(int*)data;
	
	const char* pal_original = "data/ryu/pal_original.palgen";
	const char* pal_blue =	   "data/ryu/pal_blue.palgen";
	const char* pal_gray = 	   "data/ryu/pal_gray.palgen";	

	const char* pal_ref = pal_original;
	if (palette == IAR_RYU_PAL_BLUE) pal_ref = pal_blue;
	if (palette == IAR_RYU_PAL_GRAY) pal_ref = pal_gray;
	
	for (uint16_t i = 0; i < _IAR_RYU_IMG_MAX; i++) {
		o->Class.Player.Char.Ryu.images[i] = DHEX_GetSpritePalette(global.app, _IAR_RYU_IMAGE_FILENAMES[i], pal_ref);
	}
	
	
	
	__local_GenericFrameSetup(o->Class.Player.Char.Ryu.IdleFrames, _IAR_RYU_IDLE_FRAMES, "data/ryu/frames/idle.bxg", _IAR_RYU_IDLE1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetup(o->Class.Player.Char.Ryu.StandTurnFrames, _IAR_RYU_STAND_TURN_FRAMES, "data/ryu/frames/idle.bxg", _IAR_RYU_STAND_TURN1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetup(o->Class.Player.Char.Ryu.CrouchTurnFrames, _IAR_RYU_CROUCH_TURN_FRAMES, "data/ryu/frames/cr_turn.bxg", _IAR_RYU_CROUCH_TURN1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetup(o->Class.Player.Char.Ryu.GutStunFrames, _IAR_RYU_GUTSTUN_FRAMES, "data/ryu/frames/idle.bxg", _IAR_RYU_GUTSTUN1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetup(o->Class.Player.Char.Ryu.CrBlockFrames, _IAR_RYU_CR_BLOCK_FRAMES, "data/ryu/frames/cr_turn.bxg", _IAR_RYU_CR_BLOCK1, o->Class.Player.Char.Ryu.images);

	
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.WalkForwardFrames, _IAR_RYU_WALK_FORWARD_FRAMES, "data/ryu/frames/walk_forward", _IAR_RYU_WALK_FORWARD1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.WalkBackFrames, _IAR_RYU_WALK_BACK_FRAMES, "data/ryu/frames/walk_back", _IAR_RYU_WALK_BACK1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.JumpBackFrames, _IAR_RYU_JUMP_BACK_FRAMES, "data/ryu/frames/jump_back", _IAR_RYU_JUMP_BACK1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.JumpNeutralFrames, _IAR_RYU_JUMP_NEUTRAL_FRAMES, "data/ryu/frames/jump_neutral", _IAR_RYU_JUMP_NEUTRAL1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.JumpForwardFrames, _IAR_RYU_JUMP_FORWARD_FRAMES, "data/ryu/frames/jump_forward", _IAR_RYU_JUMP_FORWARD1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.ToCrouchFrames, _IAR_RYU_TO_CROUCH_FRAMES, "data/ryu/frames/to_crouch", _IAR_RYU_TO_CROUCH1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.stHP_Frames, _IAR_RYU_ST_HP_FRAMES, "data/ryu/frames/stHP", _IAR_RYU_ST_HP1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.HeadStunFrames, _IAR_RYU_HEADSTUN_FRAMES, "data/ryu/frames/headstun", _IAR_RYU_HEADSTUN1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.CrStunFrames, _IAR_RYU_CR_STUN_FRAMES, "data/ryu/frames/cr_stun", _IAR_RYU_CR_STUN1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.StBlockFrames, _IAR_RYU_ST_BLOCK_FRAMES, "data/ryu/frames/st_block", _IAR_RYU_ST_BLOCK1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.JuggledFrames, _IAR_RYU_JUGGLED_FRAMES, "data/ryu/frames/juggled", _IAR_RYU_JUGGLED1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.KnockdownFrames, _IAR_RYU_KNOCKDOWN_FRAMES, "data/ryu/frames/knockdown", _IAR_RYU_KNOCKDOWN1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.WakeupFrames, _IAR_RYU_WAKEUP_FRAMES, "data/ryu/frames/wakeup", _IAR_RYU_WAKEUP1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.HadokenFrames, _IAR_RYU_HADOKEN_FRAMES, "data/ryu/frames/hadoken", _IAR_RYU_HADOKEN1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.stHK_Frames, _IAR_RYU_ST_HK_FRAMES, "data/ryu/frames/stHK", _IAR_RYU_ST_HK1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.airLP_Frames, _IAR_RYU_AIR_LP_FRAMES, "data/ryu/frames/airLP", _IAR_RYU_AIR_LP1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.airLK_Frames, _IAR_RYU_AIR_LK_FRAMES, "data/ryu/frames/airLK", _IAR_RYU_AIR_LK1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.fHP_Frames, _IAR_RYU_F_HP_FRAMES, "data/ryu/frames/fHP", _IAR_RYU_F_HP1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.ShoryukenFrames, _IAR_RYU_SHORYUKEN_FRAMES, "data/ryu/frames/shoryuken", _IAR_RYU_SHORYUKEN1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.TatsuFrames, _IAR_RYU_TATSU_FRAMES, "data/ryu/frames/tatsu", _IAR_RYU_TATSU1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.WhiffGrabFrames, _IAR_RYU_WHIFF_GRAB_FRAMES, "data/ryu/frames/whiff_grab", _IAR_RYU_WHIFF_GRAB1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.ThrowForwardFrames, _IAR_RYU_THROW_FORWARD_FRAMES, "data/ryu/frames/throw_forward", _IAR_RYU_THROW_FORWARD1, o->Class.Player.Char.Ryu.images);
	__local_GenericFrameSetupWOffsets(o->Class.Player.Char.Ryu.ThrowBackFrames, _IAR_RYU_THROW_BACK_FRAMES, "data/ryu/frames/throw_back", _IAR_RYU_THROW_BACK1, o->Class.Player.Char.Ryu.images);

	
	{
		int _stLPimgIndexes[] = {_IAR_RYU_ST_LP1, _IAR_RYU_ST_LP2, _IAR_RYU_ST_LP2, _IAR_RYU_ST_LP1, _IAR_RYU_ST_LP3};
		const char* _stLPbxgNames[] = {
			"data/ryu/frames/stLP/1.bxg",
			"data/ryu/frames/stLP/2.bxg",
			"data/ryu/frames/stLP/2.bxg",
			"data/ryu/frames/stLP/1.bxg",
			"data/ryu/frames/stLP/3.bxg"
		};
		
		__local_FrameSetup(_IAR_RYU_ST_LP_FRAMES, o->Class.Player.Char.Ryu.stLP_Frames,
								_stLPimgIndexes, _stLPbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	{
		int _stMPimgIndexes[] = {
			_IAR_RYU_ST_MP_STR, _IAR_RYU_ST_MP_AC, //startup
			_IAR_RYU_ST_MP_AC, _IAR_RYU_ST_MP_AA, //active
			_IAR_RYU_ST_MP_AA, _IAR_RYU_ST_MP_AC, _IAR_RYU_ST_MP_REC2 //recovery
		};
		
		const char* _stMPbxgNames[] = {
			#define _MP_PREF "data/ryu/frames/stMP/"
			_MP_PREF "start1.bxg", _MP_PREF "start2.bxg",
			_MP_PREF "act1.bxg", _MP_PREF "act2.bxg", 
			_MP_PREF "rec1.bxg", _MP_PREF "start2.bxg",  _MP_PREF "rec2.bxg"
			#undef _MP_PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_ST_MP_FRAMES, o->Class.Player.Char.Ryu.stMP_Frames,
								_stMPimgIndexes, _stMPbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	
	{
		int _crLPimgIndexes[] = {
			_IAR_RYU_CR_LP_ST, _IAR_RYU_CR_LP_AC, _IAR_RYU_CR_LP_AC, _IAR_RYU_CR_LP_ST
		};
		
		const char* _crLPbxgNames[] = {
			#define _LP_PREF "data/ryu/frames/crLP/"
			_LP_PREF "s1.bxg", _LP_PREF "ac1.bxg", _LP_PREF "ac1.bxg", _LP_PREF "s1.bxg"
			#undef _LP_PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_CR_LP_FRAMES, o->Class.Player.Char.Ryu.crLP_Frames,
								_crLPimgIndexes, _crLPbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	
	{
		int _crMPimgIndexes[] = {
			_IAR_RYU_CR_MP_S1, _IAR_RYU_CR_MP_S2, //start
			_IAR_RYU_CR_MP_A1, //active
			_IAR_RYU_CR_MP_A1 //recovery
		};
		
		const char* _crMPbxgNames[] = {
			#define _MP_PREF "data/ryu/frames/crMP/"
			_MP_PREF "s1.bxg", _MP_PREF "s2.bxg",
			_MP_PREF "ac1.bxg", 
			_MP_PREF "rec1.bxg"
			#undef _MP_PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_CR_MP_FRAMES, o->Class.Player.Char.Ryu.crMP_Frames,
								_crMPimgIndexes, _crMPbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	
	
	{
		int _crHPimgIndexes[] = {
			_IAR_RYU_CR_HP_ST, _IAR_RYU_CR_HP_AC, //Start
			_IAR_RYU_CR_HP_AC, _IAR_RYU_CR_HP_AA, //Active
			_IAR_RYU_CR_HP_AA, _IAR_RYU_CR_HP_AC, _IAR_RYU_CR_HP_ST //Recovery
		};
		
		const char* _crHPbxgNames[] = {
			#define _HP_PREF "data/ryu/frames/crHP/"
			_HP_PREF "s1.bxg", _HP_PREF "s2.bxg",
			_HP_PREF "act1.bxg", _HP_PREF "aa1.bxg", 
			_HP_PREF "rec1.bxg", _HP_PREF "s2.bxg",  "data/ryu/frames/crHP/s1.bxg"
			#undef _HP_PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_CR_HP_FRAMES, o->Class.Player.Char.Ryu.crHP_Frames,
								_crHPimgIndexes, _crHPbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	
	{
		int _stLKimgIndexes[] = {
			_IAR_RYU_ST_LK1, _IAR_RYU_ST_LK2, //startup
			_IAR_RYU_ST_LK3, //active
			_IAR_RYU_ST_LK3, _IAR_RYU_ST_LK4 //recovery
		};
		
		const char* _stLKbxgNames[] = {
			#define _PREF "data/ryu/frames/stLK/"
			_PREF "s1.bxg", _PREF "s2.bxg",
			_PREF "ac1.bxg",
			_PREF "r1.bxg", _PREF "r2.bxg"
			#undef _PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_ST_LK_FRAMES, o->Class.Player.Char.Ryu.stLK_Frames,
								_stLKimgIndexes, _stLKbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	{
		int _stMKimgIndexes[] = {
			_IAR_RYU_ST_MK_S1, _IAR_RYU_ST_MK_AC1, //startup
			_IAR_RYU_ST_MK_AC1, //active 1
			_IAR_RYU_ST_MK_TO1, _IAR_RYU_ST_MK_TO2, //to active 2
			_IAR_RYU_ST_MK_AC2, //active 2
			_IAR_RYU_ST_MK_R1, _IAR_RYU_ST_MK_R2, _IAR_RYU_ST_MK_R3 //recovery

		};
		
		const char* _stMKbxgNames[] = {
			#define _PREF "data/ryu/frames/stMK/"
			_PREF "s1.bxg" , _PREF "s2.bxg", 
			_PREF "ac1.bxg", 
			_PREF "to1.bxg", _PREF "to2.bxg", 
			_PREF "ac2.bxg", 
			_PREF "r1.bxg" , _PREF "r2.bxg" , _PREF "r3.bxg"
			#undef _PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_ST_MK_FRAMES, o->Class.Player.Char.Ryu.stMK_Frames,
								_stMKimgIndexes, _stMKbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	{
		int _crLKimgIndexes[] = {
			_IAR_RYU_CR_LK_S1, //startup
			_IAR_RYU_CR_LK_AC1, //active
			_IAR_RYU_CR_LK_AC1 //recovery

		};
		
		const char* _crLKbxgNames[] = {
			#define _PREF "data/ryu/frames/crLK/"
			_PREF "s1.bxg",
			_PREF "ac1.bxg",
			_PREF "r1.bxg"
			#undef _PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_CR_LK_FRAMES, o->Class.Player.Char.Ryu.crLK_Frames,
								_crLKimgIndexes, _crLKbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	
	{
		int _crMKimgIndexes[] = {
			_IAR_RYU_CR_MK_S1, _IAR_RYU_CR_MK_S2, //startup
			_IAR_RYU_CR_MK_AC1, //active
			_IAR_RYU_CR_MK_AC1 //recovery

		};
		
		const char* _crMKbxgNames[] = {
			#define _PREF "data/ryu/frames/crMK/"
				_PREF "s1.bxg", _PREF "s2.bxg",
				_PREF "ac1.bxg",
				_PREF "r1.bxg" 
			#undef _PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_CR_MK_FRAMES, o->Class.Player.Char.Ryu.crMK_Frames,
								_crMKimgIndexes, _crMKbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	
	{
		int _crHKimgIndexes[] = {
			_IAR_RYU_CR_HK_S1, //startup
			_IAR_RYU_CR_HK_AC1, //active
			_IAR_RYU_CR_HK_AC1, _IAR_RYU_CR_HK_R2, _IAR_RYU_CR_HK_R3, _IAR_RYU_CR_HK_R4 //recovery
		};
		
		const char* _crHKbxgNames[] = {
			#define _PREF "data/ryu/frames/crHK/"
			_PREF "s1.bxg",
			_PREF "ac1.bxg",
			_PREF "r1.bxg", _PREF "r2.bxg", _PREF "r3.bxg", _PREF "r4.bxg"			
			#undef _PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_CR_HK_FRAMES, o->Class.Player.Char.Ryu.crHK_Frames,
								_crHKimgIndexes, _crHKbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	{
		int _airMPimgIndexes[] = {
			_IAR_RYU_AIR_MP_S1, _IAR_RYU_AIR_MP_S2, _IAR_RYU_AIR_MP_S3, //startup
			_IAR_RYU_AIR_MP_AC1, //active 1
			_IAR_RYU_AIR_MP_AC1, //to active 2
			_IAR_RYU_AIR_MP_AC2, //active 2
			_IAR_RYU_AIR_MP_AC2 //recovery 
		};
		
		const char* _airMPbxgNames[] = {
			#define _PREF "data/ryu/frames/airMP/"
			_PREF "s1.bxg", _PREF "s2.bxg", _PREF "s3.bxg", 
			_PREF "ac1.bxg",
			_PREF "to1.bxg",
			_PREF "ac2.bxg",
			_PREF "r1.bxg"
			#undef _PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_AIR_MP_FRAMES, o->Class.Player.Char.Ryu.airMP_Frames,
								_airMPimgIndexes, _airMPbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	
	{
		int _airHPimgIndexes[] = {
			_IAR_RYU_AIR_HP_S1, 
			_IAR_RYU_AIR_HP_AC1,
			_IAR_RYU_AIR_HP_AC1, _IAR_RYU_AIR_HP_R1
		};
		
		const char* _airHPbxgNames[] = {
			#define _PREF "data/ryu/frames/airHP/"
				_PREF "s1.bxg", 
				_PREF "ac1.bxg",
				_PREF "r1.bxg", _PREF "r2.bxg" 
			#undef _PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_AIR_HP_FRAMES, o->Class.Player.Char.Ryu.airHP_Frames,
								_airHPimgIndexes, _airHPbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	

	{
		int _airMHKimgIndexes[] = {
			_IAR_RYU_AIR_MHK_S1, _IAR_RYU_AIR_MHK_S2, //startup
			_IAR_RYU_AIR_MHK_AC, _IAR_RYU_AIR_MHK_AC, //active(s)
			_IAR_RYU_AIR_MHK_AC //recovery 
		};
		
		const char* _airMHKbxgNames[] = {
			#define _PREF "data/ryu/frames/airMHK/"
				_PREF "s1.bxg", _PREF "s2.bxg",
				_PREF "mac.bxg", _PREF "hac.bxg",
				_PREF "r1.bxg"
			#undef _PREF
		};
		
		
		__local_FrameSetup(_IAR_RYU_AIR_MHK_FRAMES, o->Class.Player.Char.Ryu.airMHK_Frames,
								_airMHKimgIndexes, _airMHKbxgNames, o->Class.Player.Char.Ryu.images);
		
	}
	
	o->Class.Player.Char.Ryu.LastHadokenID = 0xFFFFFFFF; //aka nul
	
	o->Class.Player.Char.Ryu.PreJumpFrame.bxg = DHEX_GetBXG(global.app, "data/ryu/frames/prejump.bxg");
	o->Class.Player.Char.Ryu.PreJumpFrame.spr = o->Class.Player.Char.Ryu.images[_IAR_RYU_PREJUMP];
	o->Class.Player.Char.Ryu.PreJumpFrame.x_offset = o->Class.Player.Char.Ryu.PreJumpFrame.bxg->xcenter;
	o->Class.Player.Char.Ryu.PreJumpFrame.y_offset = o->Class.Player.Char.Ryu.PreJumpFrame.bxg->ycenter;
	
	o->Class.Player.CurrentFrame = o->Class.Player.Char.Ryu.IdleFrames[_IAR_RYU_IDLE1];
	o->Class.Player.CurrentFrameSet = o->Class.Player.Char.Ryu.IdleFrames;
	o->Class.Player.CurrentMaxFrame = 1;
	
	o->Class.Player.GutStunFramesRef = o->Class.Player.Char.Ryu.GutStunFrames;
	o->Class.Player.GutStunStateRef = IAR_RYU_STATE_GUTSTUN;
	o->Class.Player.GutStunMaxFrameRef = _IAR_RYU_GUTSTUN_FRAMES;
	
	o->Class.Player.HeadStunFramesRef = o->Class.Player.Char.Ryu.HeadStunFrames;
	o->Class.Player.HeadStunStateRef = IAR_RYU_STATE_HEADSTUN;
	o->Class.Player.HeadStunMaxFrameRef = _IAR_RYU_HEADSTUN_FRAMES;	
	
	o->Class.Player.CrStunFramesRef = o->Class.Player.Char.Ryu.CrStunFrames;
	o->Class.Player.CrStunStateRef = IAR_RYU_STATE_CR_STUN;
	o->Class.Player.CrStunMaxFrameRef = _IAR_RYU_CR_STUN_FRAMES;
	
	o->Class.Player.StBlockFramesRef = o->Class.Player.Char.Ryu.StBlockFrames;
	o->Class.Player.StBlockStateRef = IAR_RYU_STATE_ST_BLOCK;
	o->Class.Player.StBlockMaxFrameRef = _IAR_RYU_ST_BLOCK_FRAMES;
	
	o->Class.Player.CrBlockFramesRef = o->Class.Player.Char.Ryu.CrBlockFrames;
	o->Class.Player.CrBlockStateRef = IAR_RYU_STATE_CR_BLOCK;
	o->Class.Player.CrBlockMaxFrameRef = _IAR_RYU_CR_BLOCK_FRAMES;
	
	o->Class.Player.JuggledFramesRef = o->Class.Player.Char.Ryu.JuggledFrames;
	o->Class.Player.JuggledStateRef = IAR_RYU_STATE_JUGGLED;
	o->Class.Player.JuggledMaxFrameRef = _IAR_RYU_JUGGLED_FRAMES;
	
	o->Class.Player.WhiffGrabStateRef = IAR_RYU_STATE_WHIFF_GRAB;
	o->Class.Player.ThrowForwardStateRef = IAR_RYU_STATE_THROW_FORWARD;
	o->Class.Player.ThrowBackStateRef = IAR_RYU_STATE_THROW_BACK;
	
	o->Class.Player.GutStunAtFeet.img = DHEX_GetSpritePalette(global.app, _IAR_RYU_IMAGE_FILENAMES[_IAR_RYU_GUTSTUN1], pal_ref);
	o->Class.Player.GutStunAtFeet.xcenter = 35;
	o->Class.Player.GutStunAtFeet.ycenter = 87;
	
	o->Class.Player.GutStunAtChest.img = DHEX_GetSpritePalette(global.app, _IAR_RYU_IMAGE_FILENAMES[_IAR_RYU_GUTSTUN1], pal_ref);
	o->Class.Player.GutStunAtChest.xcenter = 33;
	o->Class.Player.GutStunAtChest.ycenter = 25;
	
	o->Class.Player.JuggledAtNeck.img = DHEX_GetSpritePalette(global.app, _IAR_RYU_IMAGE_FILENAMES[_IAR_RYU_JUGGLED1], pal_ref);
	o->Class.Player.JuggledAtNeck.xcenter = 24;
	o->Class.Player.JuggledAtNeck.ycenter = 18;
	
	
	return o;
}