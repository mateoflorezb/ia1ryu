#ifndef _DHEX_SFX_INCLUDED
#define _DHEX_SFX_INCLUDED

#include "DHEX_base.h"

#define DHEX_SFX_BYTES_PER_FRAME 1470

enum _DHEX_AUDIO_CHANNELS {
	DHEX_AUDIO_CHANNEL_VOICE1 = 0,  //"Hadoken!"   "Ouch!"
	DHEX_AUDIO_CHANNEL_VOICE2 = 1,
	DHEX_AUDIO_CHANNEL_HIT1   = 2,	//(punch impact*)
	DHEX_AUDIO_CHANNEL_HIT2   = 3,
	DHEX_AUDIO_CHANNEL_FX1 	  = 4,  //maybe the fireball does a noise while travelling?
	DHEX_AUDIO_CHANNEL_FX2 	  = 5,
	DHEX_AUDIO_CHANNEL_UNIQUE = 6,	//Narrator saying KO or something
   _DHEX_AUDIO_CHANNEL_MAX    = 7
};

typedef struct {
    void* wav;
    const uint8_t* original_abuf;
    uint32_t original_alen;
} DHEX_Sfx;

typedef struct {
	const char* filename;
	DHEX_Sfx* sfx;
} _DHEX_APP_SFX_LIST_STRUCT;


DHEX_Sfx* _DHEX_LoadSfx(const char* file);

int8_t _DHEX_FreeSfx(DHEX_Sfx* s); //0 on good, -1 on errors

int8_t DHEX_PlaySfx(int channel, DHEX_Sfx* s, int loops, uint32_t skip_bytes); //0 on good, -1 on errors

int8_t _DHEX_SfxInit(void);

DHEX_Sfx* DHEX_GetSfx(DHEX_App* app, const char* file);

//these down below are pretty much SDL mixer wrappers
void* DHEX_LoadMusic(DHEX_App* app, const char* filename);
void  DHEX_FreeMusic(void* music);
int   DHEX_PlayMusic(void* music, int loops); //Returns 0 on success, or -1 on errors.
int8_t DHEX_VolumeMusic(int8_t volume); //accepts values from 0 to 128. Returns current volume if input is -1

void DHEX_PauseMusic();
void DHEX_ResumeMusic();
int DHEX_IsMusicPaused();
int DHEX_SetMusicPosition(double position);

int DHEX_FadeInMusicPos(void* music, int loops, int ms, double position);
/*
From the SDL mixer documentation:

music
Pointer to Mix_Music to play.

loops
number of times to play through the music.
0 plays the music zero times...
-1 plays the music forever (or as close as it can get to that)

ms
Milliseconds for the fade-in effect to complete.

position
Posistion to play from. Since DHEX only supports OGG, 
this is interpreted as seconds from the start of the track
*/

#endif