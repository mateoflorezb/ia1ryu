#ifndef _IAR_GLOBAL_INCLUDED
#define _IAR_GLOBAL_INCLUDED

#include "DHEX.h"
#include "IAR_DataGeneration.h"

//cps2-ish resolution
#define SCREEN_WIDTH 384
#define SCREEN_HEIGHT 224

#define STAGE_WIDTH 730
#define STAGE_GROUND 205

#define IAR_BLOCKSPARK1_IMAGES 5
#define IAR_BLOCKSPARK2_IMAGES 6
#define IAR_BLOCKSPARK3_IMAGES 9

#define IAR_HITSPARK1_IMAGES 6
#define IAR_HITSPARK2_IMAGES 6
#define IAR_HITSPARK3_IMAGES 6
#define IAR_HITSPARK4_IMAGES 6

#define IAR_MAX_DATA_ROWS 100

#include "Python.h"


typedef struct {
	DHEX_App *app;
	
	struct {
		DHEX_Sprite* BlockSpark1Images[IAR_BLOCKSPARK1_IMAGES];
		DHEX_Sprite* BlockSpark2Images[IAR_BLOCKSPARK2_IMAGES];
		DHEX_Sprite* BlockSpark3Images[IAR_BLOCKSPARK3_IMAGES];
		
		DHEX_Sprite* HitSpark1Images[IAR_HITSPARK1_IMAGES];
		DHEX_Sprite* HitSpark2Images[IAR_HITSPARK2_IMAGES];
		DHEX_Sprite* HitSpark3Images[IAR_HITSPARK3_IMAGES];
		DHEX_Sprite* HitSpark4Images[IAR_HITSPARK4_IMAGES];
		
		DHEX_Font* font;
		
		DHEX_Sprite* background;
		
	} resources;
	
	struct {
		PyObject* IAR_Scripts;
		PyObject* ZerosDataDict;
		PyObject* SetRow;
		PyObject* SaveDataDictAsCSV;
		PyObject* RW;
		PyObject* RS;
		PyObject* GetEstimation;
		
	} Python;
	
	struct {
		uint16_t CurrentFrameInputs[2]; //index is pside
		uint16_t CurrentFrameInputsPressed[2];
		uint16_t CurrentFrameInputsReleased[2];
		
		IAR_DataRow LocalDataSet[IAR_MAX_DATA_ROWS];
		int CurrentRow;
		
	} CPU;
	
	struct {
		bool ShowBoxes;
		bool DelayFrame;
		bool DrawFrame;
		bool AutoMatch;
		
	} Settings;
	
} glob_ds;


#endif