#include "IAR_obj.h"
#include "IAR_global.h"
#include "IAR_general.h"
#include "DHEX.h"
#include <stdio.h>

extern glob_ds global;

static void _emptyFunc(IAR_Obj* none) {return;}

static void _HadokenPreStep(IAR_Obj* self) {
	
	clist_iterator it = clist_start(self->gs->objs);
	while (it != NULL) {
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		if (!o->DestroyFlag) {
			if (o->ClassID == IAR_OBJ_PROJECTILE) {
				
				if (o->Class.Projectile.Pside != self->Class.Projectile.Pside)
				if (AABB(fixed_cast(self->Class.Projectile.x)-self->Class.Projectile.HitboxW,
						 fixed_cast(self->Class.Projectile.y)-self->Class.Projectile.HitboxH,
						 self->Class.Projectile.HitboxW*2,
						 self->Class.Projectile.HitboxH*2,
						 fixed_cast(o->Class.Projectile.x)-o->Class.Projectile.HitboxW,
						 fixed_cast(o->Class.Projectile.y)-o->Class.Projectile.HitboxH,
						 o->Class.Projectile.HitboxW*2,
						 o->Class.Projectile.HitboxH*2)) {
							
					IAR_InstanceDestroy(self->InstanceID, self->gs);
					IAR_InstanceDestroy(o->InstanceID, self->gs);
					return;
				}
			}
		}
		
		
		it = clist_next(it);
	}

}

static void _HadokenStep(IAR_Obj* self) {
	self->Class.Projectile.ImageIndex += self->Class.Projectile.ImageSpeed;
	
	const int DESTROY_SELF_OFFSET = 90;
	
	if (
	(fixed_roundCast(self->Class.Projectile.x) < self->gs->cam_x - DESTROY_SELF_OFFSET)
	||
	(fixed_roundCast(self->Class.Projectile.x) > self->gs->cam_x + SCREEN_WIDTH + DESTROY_SELF_OFFSET)
	) {
		IAR_InstanceDestroy(self->InstanceID, self->gs);
		return;
		
	}
	
	if (self->Class.Projectile.TTSmall >  0) self->Class.Projectile.TTSmall--;
	if (self->Class.Projectile.TTSmall == 1) {
		self->Class.Projectile.HitboxH = 4;
		self->Class.Projectile.HitboxW = 13;
	}
	
	self->Class.Projectile.x += self->Class.Projectile.hspeed;
	self->Class.Projectile.CurrentImage = self->Class.Projectile.images[fixed_cast(self->Class.Projectile.ImageIndex) % self->Class.Projectile.nImages];
	
}

static void _HadokenDestroy(IAR_Obj* self) {
	
	//free(self->Class.Projectile.images);
	free(self);
}

static void _HadokenDraw(IAR_Obj* self) {
	DHEX_DrawLayer(global.app, DHEX_LAYER_OBJ6);
	///*
	
	DHEX_DrawSprite(global.app, self->Class.Projectile.CurrentImage,
					fixed_roundCast(self->Class.Projectile.x) - self->gs->cam_x,
					fixed_roundCast(self->Class.Projectile.y) - self->gs->cam_y,
					self->Class.Projectile.xOffset,
					self->Class.Projectile.yOffset,
					self->Class.Projectile.xdraw,1,0.0);
					
	//*/
			
	/*
	DHEX_DrawRect(global.app, fixed_cast(self->Class.Projectile.x)-(int)self->Class.Projectile.HitboxW,
						 fixed_cast(self->Class.Projectile.y)-(int)self->Class.Projectile.HitboxH,
						 (int)self->Class.Projectile.HitboxW*2,
						 (int)self->Class.Projectile.HitboxH*2, 0, 0, 0, 255);
	*/
}

static IAR_Obj* _HadokenGetCopy(IAR_Obj* self) {
	IAR_Obj* newHado = malloc(sizeof(IAR_Obj));
	memcpy(newHado, self, sizeof(IAR_Obj));
	return newHado;
}

IAR_Obj* IAR_CreateHadoken(void* data) {
	IAR_Obj* o = malloc(sizeof(IAR_Obj));
	o->ClassID = IAR_OBJ_PROJECTILE;
	o->ClassGroup = IAR_GROUP_NONE;
	o->DestroyFlag = false;
	o->GetCopy = &_HadokenGetCopy;
	
	o->step = &_HadokenStep;
	o->post_step = &_emptyFunc;
	o->pre_step = &_HadokenPreStep;
	o->draw = &_HadokenDraw;
	o->destroy = &_HadokenDestroy;
	
	//o->Class.Projectile.images = malloc(8*sizeof(DHEX_Sprite*));
	o->Class.Projectile.nImages = HADOKEN_FRAMES;
	
	for (uint8_t k = 0; k < HADOKEN_FRAMES; k++) {
		char tmpStr[50];
		sprintf(tmpStr, "%s/%u.png\0", "data/ryu/fireball", k+1);
		o->Class.Projectile.images[k] = DHEX_GetSpriteRaw(global.app, tmpStr);
		
	}
	
	o->Class.Projectile.CurrentImage = o->Class.Projectile.images[0];
	o->Class.Projectile.ImageIndex = 0;
	o->Class.Projectile.ImageSpeed = IAR_FIXED_SCALE/2;
	
	o->Class.Projectile.HitboxW = 15;
	o->Class.Projectile.HitboxH = 15;
	
	o->Class.Projectile.xOffset = 60;
	o->Class.Projectile.yOffset = 18;
	
	o->Class.Projectile.Hits = 1;
	o->Class.Projectile.Launcher = false;
	
	o->Class.Projectile.AppliedStopFrames = 9; //how much hitstop applied with the current action
	o->Class.Projectile.HitLevel = IAR_HITLEVEL_SPECIAL; //special or super
	o->Class.Projectile.AppliedOnHit = 21;
	o->Class.Projectile.AppliedOnBlock = 18;
	
	o->Class.Projectile.AppliedPushback = 12; //on int units, will get converted to fixed later on
	o->Class.Projectile.AppliedJuggleHs = 5; //same
	o->Class.Projectile.AppliedJuggleVs = -5; //same lmao
	o->Class.Projectile.AppliedJuggleTime = 0xFFFF;
	
	o->Class.Projectile.AppliedDamage = 90;
	o->Class.Projectile.AppliedChip = 15;
	
	o->Class.Projectile.TTSmall = 3;
	
	return o;
	
}