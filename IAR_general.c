#include "IAR_general.h"
#include "IAR_global.h"
#include <math.h>

extern glob_ds global;

int32_t fixed_cast(fixed n) { return n/IAR_FIXED_SCALE; }


void IAR_DrawBXG(DHEX_FrameBoxes bxg, int x, int y) {
	
	for (unsigned char boxtype = 0; boxtype < 2; boxtype++)
	for (unsigned char k = 0; k < DHEX_MAX_HBOXES; k++) {
		DHEX_HBox* reference = bxg.hurtboxes;
		
		uint8_t r = 10, g = 255, b = 100;
		
		if (boxtype == 1) {
			reference = bxg.hitboxes;
			r = 230;
			g = 20;
			b = 20;
		}
		
		if (reference[k].w == 0) break;
		
		DHEX_FillRect(global.app, x + reference[k].x,
								  y + reference[k].y,
								  reference[k].w,
								  reference[k].h,
								  r,g,b,115);
								  
								 
		DHEX_DrawRect(global.app, x + reference[k].x,
								  y + reference[k].y,
								  reference[k].w,
								  reference[k].h,
								  r,g,b,190);
		
	}
	
	
}

float clamp (float value, float min, float max) {
	return fmin(fmax(min, value), max);
	
}

int32_t iclamp (int32_t value, int32_t min, int32_t max) {
	if (value < min) value = min;
	if (value > max) value = max;
	return value;
	
}


DHEX_FrameBoxes IAR_ScaleBXG(DHEX_FrameBoxes bxg, float xscale, float yscale) {
	DHEX_FrameBoxes frame;
	
	for (unsigned char boxtype = 0; boxtype < 2; boxtype++)
	for (unsigned char k = 0; k < DHEX_MAX_HBOXES; k++) {
		DHEX_HBox* ref1 = frame.hurtboxes;
		DHEX_HBox* ref2 =   bxg.hurtboxes;
		
		if (boxtype == 1) {
			ref1 = frame.hitboxes;
			ref2 =   bxg.hitboxes;
		}
		
		ref1[k].x = ref2[k].x;
		ref1[k].y = ref2[k].y;
		ref1[k].w = ref2[k].w;
		ref1[k].h = ref2[k].h;
		
	}
	
	frame.xcenter = bxg.xcenter;
	frame.ycenter = bxg.ycenter;
	
	for (unsigned char boxtype = 0; boxtype < 2; boxtype++)
	for (unsigned char k = 0; k < DHEX_MAX_HBOXES; k++) {
		DHEX_HBox* reference = frame.hurtboxes;
	
		if (boxtype == 1) {
			reference = frame.hitboxes;
		}
		
		reference[k].x = (int)((float)reference[k].x*fabs(xscale));
		reference[k].w = abs((int)((float)reference[k].w*xscale));	
		
		if (xscale < 0) {
			reference[k].x = -((int)reference[k].w + (int)reference[k].x);
		}
		
		reference[k].y = (int)((float)reference[k].y*fabs(yscale));
		reference[k].h = abs((int)((float)reference[k].h*yscale));	
		
		if (yscale < 0) {
			reference[k].y = -((int)reference[k].h + (int)reference[k].y);
		}
		
	
	}
	
	return frame;
	
}

bool AABB(int x1, int y1, int w1, int h1,
		  int x2, int y2, int w2, int h2) {
			  
	if (x1 <= x2 + w2 &&
		x1 + w1 >= x2 &&
		y1 <= y2 + h2 &&
		y1 + h1 >= y2)
		
		return true;
		
	return false;
			  
}


void IAR_TestBXG_Collision(DHEX_FrameBoxes P1, int x1, int y1, DHEX_FrameBoxes P2, int x2, int y2, char* P1Scored, char* P2Scored) {
	if (P1Scored == NULL || P2Scored == NULL) return;
	
	for (unsigned char pside = 1; pside <= 2; pside++) {
		char* score = P1Scored;
		DHEX_HBox* Attacker = P1.hitboxes;
		DHEX_HBox* Defender = P2.hurtboxes;
		int xAt = x1, yAt = y1, xDf = x2, yDf = y2;
		
		if (pside == 2) {
			score = P2Scored;
			Attacker = P2.hitboxes;
			Defender = P1.hurtboxes;

			xAt = x2; yAt = y2; xDf = x1; yDf = y1;
		}
		
		*score = (char)0;
		for (unsigned char at = 0; at < DHEX_MAX_HBOXES; at++) {
			if ((int)Attacker[at].w == 0) break;
			if (*score != 1)
			for (unsigned char df = 0; df < DHEX_MAX_HBOXES; df++) {
				if ((int)Defender[df].w == 0) break;
				
				if (AABB((int)Attacker[at].x+xAt, (int)Attacker[at].y+yAt, (int)Attacker[at].w, (int)Attacker[at].h,
						 (int)Defender[df].x+xDf, (int)Defender[df].y+yDf, (int)Defender[df].w, (int)Defender[df].h)) {
				
					*score = (char)1;
					break;
				}
			}
		}
	}
}

void IAR_GetInputsFromHardware(uint8_t pside, uint16_t* held, uint16_t* pressed, uint16_t* released) {
	
	*held = 0;
	*pressed = 0;
	*released = 0;
	
	uint16_t MaskList[] = {
		DHEX_BUTTON_LP,
		DHEX_BUTTON_HP,
		DHEX_BUTTON_SP,
		DHEX_BUTTON_LK,
		DHEX_BUTTON_HK,
		DHEX_BUTTON_DS,
		
		DHEX_BUTTON_START,
		
		DHEX_DIR_R,
		DHEX_DIR_L,
		DHEX_DIR_U,
		DHEX_DIR_D,
		
		DHEX_MACRO_1,
		DHEX_MACRO_2
	};
	
	for (uint8_t k = 0; k < sizeof(MaskList)/sizeof(MaskList[0]); k++) {
		if (DHEX_InputCheck		   (global.app, MaskList[k], pside)) *held 	   = *held 	   | MaskList[k];
		if (DHEX_InputCheckPressed (global.app, MaskList[k], pside)) *pressed  = *pressed  | MaskList[k];
		if (DHEX_InputCheckReleased(global.app, MaskList[k], pside)) *released = *released | MaskList[k];
	}
	
	for (uint8_t var = 0; var < 3; var++) { //cancel same axis opposing directions
		uint16_t* ref = held;
		if (var == 1) ref = pressed;
		if (var == 2) ref = released;
		
		if (IAR_IsInputFlagged(*ref, DHEX_DIR_L) && IAR_IsInputFlagged(*ref, DHEX_DIR_R)) {
			*ref = *ref & ~(DHEX_DIR_L |  DHEX_DIR_R);
		}
		
		if (IAR_IsInputFlagged(*ref, DHEX_DIR_U) && IAR_IsInputFlagged(*ref, DHEX_DIR_D)) {
			*ref = *ref & ~(DHEX_DIR_U |  DHEX_DIR_D);
		}
		
	}

}


bool IAR_IsInputFlagged(uint16_t state, uint16_t mask) {
	
	return ( (state & mask) == mask );
}



int8_t isign(int32_t n) {
	if (n > 0) return  1;
	if (n < 0) return -1;
	return 0;
}


fixed fixed_round(fixed n) {
	fixed roundDown = fixed_cast(n)*IAR_FIXED_SCALE;
	fixed roundUp = (1+fixed_cast(n))*IAR_FIXED_SCALE;

	int delta1 = abs(n - roundDown);
	int delta2 = abs(n - roundUp);
	
	if (delta1 < delta2) return roundDown;
	
	return roundUp;
}

int32_t fixed_roundCast(fixed n) {return fixed_cast(fixed_round(n));}

int xorRand() {
	int r1 = rand();
	int r2 = rand();
	int r = r1 ^ r2;
	r = abs(r);
	return r;
}