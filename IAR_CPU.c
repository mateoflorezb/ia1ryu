#include "IAR_CPU.h"
#include "clist.h"
#include "IAR_obj.h"
#include "IAR_general.h"
#include "IAR_global.h"

#define _NULL_SCORE (0x7FFFFFFF)

extern glob_ds global;

static IAR_DataRow _local_ExtractDataRow(IAR_GameState* gs, int8_t Pside) {
	IAR_DataRow args;
	//memset(&args, 0x00, sizeof(IAR_DataRow));
	IAR_Obj* P = gs->P1;
	if (Pside == 2) P = gs->P2;	

	if (P != NULL) {
		IAR_Obj* enemy = IAR_InstanceFind(P->Class.Player.OpponentID, gs);
		if (enemy == NULL) return args;
		
		if (P->Class.Player.xdir == 1) {
			args.SelfDistCorner  = fixed_roundCast(P->Class.Player.x);
			args.EnemyDistCorner = STAGE_WIDTH - fixed_roundCast(enemy->Class.Player.x);
		}
		else {
			args.SelfDistCorner  = STAGE_WIDTH - fixed_roundCast(P->Class.Player.x);
			args.EnemyDistCorner = fixed_roundCast(enemy->Class.Player.x);
		}
		
		args.SelfDistEnemy = abs(fixed_roundCast(P->Class.Player.x) - fixed_roundCast(enemy->Class.Player.x));
		
		args.SelfDistFireball = STAGE_WIDTH;
		args.EnemyDistFireball = STAGE_WIDTH;
		
		IAR_Obj* Hado = IAR_InstanceFind(enemy->Class.Player.Char.Ryu.LastHadokenID, gs);
		if (Hado != NULL) 
		args.SelfDistFireball = abs(fixed_roundCast(P->Class.Player.x) - fixed_roundCast(Hado->Class.Projectile.x));
			
		Hado = IAR_InstanceFind(P->Class.Player.Char.Ryu.LastHadokenID, gs);
		if (Hado != NULL) 
		args.EnemyDistFireball = abs(fixed_roundCast(enemy->Class.Player.x) - fixed_roundCast(Hado->Class.Projectile.x));
		
		int selfSp = P->Class.Player.spState;
		int enemySp = enemy->Class.Player.spState;
		
		for (unsigned char k = 0; k < 2; k++) {
			
			int* Sp = &selfSp;
			IAR_Obj* tmpP = P;
			
			if (k == 1) {
				Sp = &enemySp;
				tmpP = enemy;
			}
		
			if (tmpP->Class.Player.KnockdownStateRef == tmpP->Class.Player.state) 
				*Sp += tmpP->Class.Player.WakeupTime;
			
			if (tmpP->Class.Player.GutStunStateRef == tmpP->Class.Player.state && tmpP->Class.Player.DefenseFlags == IAR_DEFENSE_INVINCIBLE) {
				//aka in "grab" stun. Doing this since grab stun has an exaggerated sp value,  I don't want
				//to bias the data that much
				*Sp = 50;
			}
			
			*Sp += iclamp(tmpP->Class.Player.Juggled, 0, 50);
			*Sp = iclamp(*Sp, 0, 50); 			
			
		}
		
		args.FrameAdvantage = (enemy->Class.Player.StopFrames + enemySp) - (P->Class.Player.StopFrames + selfSp);
		
		args.InCombo = 0;
		if ((enemy->Class.Player.DefenseFlags & IAR_DEFENSE_CANBLOCK) == 0)
			args.InCombo = 1;
		
		args.EnemyInCombo = 0;
		if ((P->Class.Player.DefenseFlags & IAR_DEFENSE_CANBLOCK) == 0)
			args.EnemyInCombo = 1;
		
		args.SelfLife = P->Class.Player.health;
		args.EnemyLife = enemy->Class.Player.health;
		args.EnemyY = fixed_roundCast(enemy->Class.Player.y);
		
	}
	
	return args;

}

static int32_t _local_ScoreGameState(IAR_GameState* gs, int8_t Pside) {
	IAR_DataRow args = _local_ExtractDataRow(gs, Pside);
	IAR_Obj* P = gs->P1;
	if (Pside == 2) P = gs->P2;
	
	if (P != NULL) {
		IAR_Obj* enemy = IAR_InstanceFind(P->Class.Player.OpponentID, gs);
		if (enemy == NULL) return _NULL_SCORE;
		
		int LifeLead = P->Class.Player.health - enemy->Class.Player.health;		
		
		int SKO = 0;
		if (P->Class.Player.health <= 0) SKO = 1;

		int EKO = 0;
		if (enemy->Class.Player.health <= 0) EKO = 1;
		
		const int LifeLeadWeight = 3;
		const float CornerWeight = 1.5;
		const float FireballWeight = 0.5;
		const int FrameAdvantageWeight = 70;
		const int KO_Weight = 2000;
		
		int32_t score = LifeLeadWeight*LifeLead;
		score += CornerWeight*(float)args.SelfDistCorner;
		score -= CornerWeight*(float)args.EnemyDistCorner;
		score += FireballWeight*(float)args.SelfDistFireball;
		score -= FireballWeight*(float)args.EnemyDistFireball;
		
		score += FrameAdvantageWeight*(float)args.FrameAdvantage/(1.0 + (float)args.SelfDistEnemy); 
		
		const int InComboWeight = 15;
		
		if (args.FrameAdvantage > 0)
			score += args.FrameAdvantage*args.InCombo*InComboWeight;
		
		if (args.FrameAdvantage < 0)
			score += args.FrameAdvantage*args.EnemyInCombo*InComboWeight;
		
		score += EKO*KO_Weight;
		score -= SKO*KO_Weight;
		
		score += P->Class.Player.DefenseScore;
		
		return score;
	}
	
	
	return _NULL_SCORE;
}

static uint16_t _local_InputSetDir(uint16_t input, int8_t dir) {
	if (dir == -1) {
		if ((input & DHEX_DIR_R) != 0) {
			input = input & (~DHEX_DIR_R);
			input = input | DHEX_DIR_L;
		}
		else
		if ((input & DHEX_DIR_L) != 0) {
			input = input & (~DHEX_DIR_L);
			input = input | DHEX_DIR_R;
		}
		
	}
	
	return input;
}

static void _local_CPU_RefreshCurrentRecord(IAR_CPU_AI* cpu, IAR_GameState* gs) {
	
	cpu->CurrentIndex = 0;
	//printf("Selecting option...\n");
	
	switch(cpu->Policy) {
		case IAR_CPU_RANDOM:
		{
			//printf("Policy random\n");
			int RandomIndex = rand() % cpu->nOptions;
			cpu->CurrentRecord = cpu->OptionGroup[RandomIndex];	
		}
		break;
		
		case IAR_CPU_INSTANT_REACTION:
		{
			//printf("Policy instant\n");
			if (cpu->OptionGroup == cpu->IAR_OptionGroup_Okizeme) {
				int RandomIndex = rand() % cpu->nOptions;
				cpu->CurrentRecord = cpu->OptionGroup[RandomIndex];					
			}
			else {
				//generate score table
				int32_t tScores[IAR_AI_NEUTRAL_MID_OPTIONS]; //mid options since this is the larges group
				int32_t range = 0;
				for (int k = 0; k < cpu->nOptions; k++) {
					double Reward = IAR_GetEstimation(global.Python.RW, _local_ExtractDataRow(gs, cpu->Pside));
					double Risk   = IAR_GetEstimation(global.Python.RS, _local_ExtractDataRow(gs, cpu->Pside));
					
					
					double score = (100.0*Reward);
					score = score/min(1,pow(Risk*0.2,3));
					//printf("Got division done\n");
					//int32_t score = 100;
					tScores[k] = ceil(score);
					range += ceil(score);
				}
				//printf("Got range %d\n", range);
				int32_t tRange = range;
				if (range <= 0) 
					tRange = 1;
				int32_t RandomChoice = rand() % tRange;
				
				int SelectedOption = 0;
				int32_t lower = 0;
				int32_t upper = tScores[0];
				
				if (range > 0) {
					for (int k = 0; k < cpu->nOptions; k++) {
						
						if ((RandomChoice >= lower && RandomChoice < upper) || k == cpu->nOptions-1) {
							SelectedOption = k;
							break;
						}
						
						lower += tScores[k];
						upper = lower + tScores[k+1];
					}
				}
				else {
					SelectedOption = rand() % cpu->nOptions;
				}
				
				cpu->CurrentRecord = cpu->OptionGroup[SelectedOption];
				/*
				printf("Selected option: %d\n", SelectedOption);
				
				if (cpu->OptionGroup == cpu->IAR_OptionGroup_NeutralMid) 
					printf("Selected mid\n");
				if (cpu->OptionGroup == cpu->IAR_OptionGroup_NeutralFar) 
					printf("Selected far\n");
				if (cpu->OptionGroup == cpu->IAR_OptionGroup_Close) 
					printf("Selected close\n");
				if (cpu->OptionGroup == cpu->IAR_OptionGroup_Wakeup) 
					printf("Selected wakeuo\n");
				if (cpu->OptionGroup == cpu->IAR_OptionGroup_Okizeme) 
					printf("Selected oki\n");
				*/

				
			}
		}
		break;
	}
}

static void _local_CPU_PutInput(IAR_CPU_AI* cpu, uint16_t input, int8_t dir) {
	global.CPU.CurrentFrameInputs[cpu->Pside-1] = _local_InputSetDir(input, dir);
	uint16_t FlaggedForChanges = global.CPU.CurrentFrameInputs[cpu->Pside-1] ^ cpu->PreviousItem;
	global.CPU.CurrentFrameInputsPressed[cpu->Pside-1]  = FlaggedForChanges & global.CPU.CurrentFrameInputs[cpu->Pside-1];
	global.CPU.CurrentFrameInputsReleased[cpu->Pside-1] = FlaggedForChanges & cpu->PreviousItem;
	
}

static void _local_CPU_AdvanceRecord(IAR_CPU_AI* cpu, IAR_GameState* gs) {
	IAR_Obj* P = gs->P1;
	if (cpu->Pside == 2) P = gs->P2;
	
	_local_CPU_PutInput(cpu, cpu->CurrentRecord->RecordList[cpu->CurrentIndex], P->Class.Player.xdir);
	
	cpu->PreviousItem = global.CPU.CurrentFrameInputs[cpu->Pside-1];
	cpu->CurrentIndex++;
}

static void _local_CPU_DumpData(IAR_CPU_AI* cpu) {
	printf("Dumping data...\n");
	char tmpStr[100];
	sprintf(tmpStr, "dump/RandomTrain%d.csv\0",cpu->DumpFilenameOffset);
	cpu->DumpFilenameOffset++;
	IAR_ExportLocalRowsAsCSV(global.CPU.LocalDataSet, global.CPU.CurrentRow, tmpStr);
	//DHEX_Delay(2000);
	global.CPU.CurrentRow = 0;
	
	return;
}

static void _local_CPU_ResetState(IAR_CPU_AI* cpu, IAR_GameState* gs) {
	cpu->GotToRecordEnd = false;
	cpu->OptionGroup = cpu->IAR_OptionGroup_NeutralFar;
	cpu->nOptions = IAR_AI_NEUTRAL_FAR_OPTIONS;
	_local_CPU_RefreshCurrentRecord(cpu, gs);
	cpu->InQueue = 0;
	clist_clear(cpu->ScoreQueue);
	cpu->EndgameDump = false;
	
}

static void _local_CPU_Step(void* self, IAR_GameState* gs) {
	IAR_CPU_AI* cpu = (IAR_CPU_AI*)self;
	IAR_Obj* P = gs->P1;
	
	if (cpu->Pside == 2) P = gs->P2;
	IAR_Obj* enemy = IAR_InstanceFind(P->Class.Player.OpponentID, gs);
	if (enemy == NULL) return;
	
	if (cpu->ExportData) {
		clist_iterator it = clist_start(cpu->ScoreQueue);
		while (it != NULL) {
			_IAR_ScoreQueueNode* node = (_IAR_ScoreQueueNode*)it->data;
			clist_iterator next = clist_next(it);
			node->Wait -= 1;
			
			if (node->Wait <= 0) { //time to score 
				int32_t final = _local_ScoreGameState(gs, cpu->Pside);
				int DScore = final - node->InitialScore;
				if (DScore > 0) {
					node->InitialContext.RW = DScore;
					node->InitialContext.RS = 0;
				}
				else {
					node->InitialContext.RS =-DScore;
					node->InitialContext.RW = 0;
				}
				
				//printf("AI: Setting new local row (%d)...\n", global.CPU.CurrentRow);
				IAR_SetLocalRow(node->InitialContext);
				cpu->InQueue = cpu->InQueue - 1;
				
				if (global.CPU.CurrentRow == IAR_MAX_DATA_ROWS) {
					_local_CPU_DumpData(cpu);
				}
				
				clist_delete(it);
			}
			
			it = next;
		}
		
	}
	
	bool GameEnded = false;
	if (P->Class.Player.health <= 0 || enemy->Class.Player.health <= 0)
		GameEnded = true;
	
	if (GameEnded) {
		/*
		if (cpu->InQueue == 0 && cpu->EndgameDump == false && cpu->ExportData == true) {
			printf("Dumping data...\n");
			cpu->EndgameDump = true;
			_local_CPU_DumpData(cpu);
		}
		*/
		_local_CPU_PutInput(cpu, 0, 1);	
		//printf("Game ended pana\n");
	}
	
	if (!GameEnded) {
		
		int EnemyDistance = abs(fixed_roundCast(P->Class.Player.x) - fixed_roundCast(enemy->Class.Player.x));
		
		cpu->OptionGroup = cpu->IAR_OptionGroup_Close;
		cpu->nOptions = IAR_AI_CLOSE_OPTIONS;
		
		if (EnemyDistance > 70) {
			cpu->OptionGroup = cpu->IAR_OptionGroup_NeutralMid;
			cpu->nOptions = IAR_AI_NEUTRAL_MID_OPTIONS;
		}
		
		if (EnemyDistance > 120) {
			cpu->OptionGroup = cpu->IAR_OptionGroup_NeutralFar;
			cpu->nOptions = IAR_AI_NEUTRAL_FAR_OPTIONS;
		}
		
		int TimeUntilOkiFrame = 0;
		if (enemy->Class.Player.spState > 0 &&
		   (enemy->Class.Player.state == enemy->Class.Player.WakeupStateRef || enemy->Class.Player.state == enemy->Class.Player.KnockdownStateRef) ) {
			TimeUntilOkiFrame = enemy->Class.Player.spState;
			if (enemy->Class.Player.state == enemy->Class.Player.KnockdownStateRef)
				TimeUntilOkiFrame += enemy->Class.Player.WakeupTime;	
		}
		
		if (TimeUntilOkiFrame > 0 && EnemyDistance <= 60) {
			cpu->OptionGroup = cpu->IAR_OptionGroup_Okizeme;
			cpu->nOptions = IAR_AI_OKIZEME_OPTIONS;		
		}
		
		if (P->Class.Player.state == P->Class.Player.WakeupStateRef) {
			cpu->OptionGroup = cpu->IAR_OptionGroup_Wakeup;
			cpu->nOptions = IAR_AI_WAKEUP_OPTIONS;			
		}
		
		int spIndex = P->Class.Player.CurrentMaxSpState - P->Class.Player.spState + 1;
		
		if (P->Class.Player.state == IAR_RYU_STATE_ST_LK) {
			cpu->OptionGroup = cpu->IAR_OptionGroup_CheckConfirm;
			cpu->nOptions = IAR_AI_CHECK_CONFIRM_OPTIONS;	
		}			
		
		bool _Playback = true;
		
		if (TimeUntilOkiFrame > 10) {
			_Playback = false;
			_local_CPU_PutInput(cpu, DHEX_DIR_R, P->Class.Player.xdir);
		}
		
		bool ValidPlaybackStart = false;
		if (_Playback) {
			if ((P->Class.Player.AllowedBuffers & IAR_BUFFER_NORMAL) !=0 && P->Class.Player.spState == 0 && !P->Class.Player.inAir)
				ValidPlaybackStart = true;
			
			if (P->Class.Player.state == P->Class.Player.WakeupStateRef) {
				if (P->Class.Player.spState == 4) {
					ValidPlaybackStart = true;
				}
			}
			
			if (cpu->OptionGroup == cpu->IAR_OptionGroup_CheckConfirm && spIndex == 4)
				ValidPlaybackStart = true;
		}

		if ((cpu->CurrentRecord == NULL) && (P->Class.Player.AllowedBuffers & IAR_BUFFER_NORMAL) !=0) {
			_local_CPU_RefreshCurrentRecord(cpu, gs);
			
		}				
		
		if (_Playback)
		if (cpu->CurrentRecord != NULL) {
			
			if (cpu->CurrentIndex >= cpu->CurrentRecord->len) {
				global.CPU.CurrentFrameInputs[cpu->Pside-1] = 0;
				global.CPU.CurrentFrameInputsPressed[cpu->Pside-1]  = 0;
				global.CPU.CurrentFrameInputsReleased[cpu->Pside-1] = 0;
				cpu->PreviousItem = 0;
				//cpu->Ready = false;
				cpu->GotToRecordEnd = true;
			}
		
			if (cpu->CurrentIndex < cpu->CurrentRecord->len && !cpu->GotToRecordEnd) {
				_local_CPU_AdvanceRecord(cpu, gs);
			}
			
			if (ValidPlaybackStart  && cpu->GotToRecordEnd) {
				//cpu->Ready = true;
				_local_CPU_RefreshCurrentRecord(cpu, gs);
				cpu->GotToRecordEnd = false;
				
				if (cpu->ExportData && cpu->InQueue < 20) {
					_IAR_ScoreQueueNode node;
					node.InitialScore = _local_ScoreGameState(gs, cpu->Pside);
					node.Wait = cpu->CurrentRecord->ScoreDelay;
					node.InitialContext = _local_ExtractDataRow(gs, cpu->Pside);
					node.InitialContext.Option = cpu->CurrentRecord->OptionIndex;
					clist_push(cpu->ScoreQueue, &node);
					cpu->InQueue = cpu->InQueue + 1;
					//printf("Scores queued: %d\n", cpu->InQueue);
				}
				
				_local_CPU_AdvanceRecord(cpu, gs);
			}
		}

	}
	
	if (DHEX_InputCheckPressed(global.app, DHEX_MACRO_2, 2)) {
		printf("Score for P2: %d\n", _local_ScoreGameState(gs, 2));
	}
}

IAR_CPU_AI* IAR_Create_CPU_AI(int DumpFilenameOffset, int8_t Pside) {
	IAR_CPU_AI* cpu = malloc(sizeof(IAR_CPU_AI));
	
	cpu->step = &_local_CPU_Step;
	cpu->reset = &_local_CPU_ResetState;
	cpu->CurrentRecord = NULL;
	cpu->CurrentIndex = 0;
	cpu->PreviousItem = 0;
	
	cpu->Policy = IAR_CPU_RANDOM;
	
	cpu->DumpFilenameOffset = DumpFilenameOffset;
	
	cpu->OptionGroup = cpu->IAR_OptionGroup_NeutralFar;
	cpu->nOptions = IAR_AI_NEUTRAL_FAR_OPTIONS;
	cpu->Pside = Pside;
	
	cpu->IAR_OptionGroup_NeutralFar[0] = &IAR_Option_JumpFwHK;
	cpu->IAR_OptionGroup_NeutralFar[1] = &IAR_Option_HadoLP;
	cpu->IAR_OptionGroup_NeutralFar[2] = &IAR_Option_HadoHP;
	cpu->IAR_OptionGroup_NeutralFar[3] = &IAR_Option_WalkForward15Frames;
	cpu->IAR_OptionGroup_NeutralFar[4] = &IAR_Option_BlockLow6Frames;
	cpu->IAR_OptionGroup_NeutralFar[5] = &IAR_Option_JumpFwEmpty;

	cpu->IAR_OptionGroup_NeutralMid[0]  = &IAR_Option_JumpFwMK;
	cpu->IAR_OptionGroup_NeutralMid[1]  = &IAR_Option_HadoHP;
	cpu->IAR_OptionGroup_NeutralMid[2]  = &IAR_Option_crMK_Hado;
	cpu->IAR_OptionGroup_NeutralMid[3]  = &IAR_Option_crHK_Hado;
	cpu->IAR_OptionGroup_NeutralMid[4]  = &IAR_Option_AntiAir_jMP;
	cpu->IAR_OptionGroup_NeutralMid[5]  = &IAR_Option_RandomTatsu;
	cpu->IAR_OptionGroup_NeutralMid[6]  = &IAR_Option_WalkForward6Frames;
	cpu->IAR_OptionGroup_NeutralMid[7]  = &IAR_Option_BlockLow6Frames;
	cpu->IAR_OptionGroup_NeutralMid[8]  = &IAR_Option_WalkBack8Frames;
	cpu->IAR_OptionGroup_NeutralMid[9]  = &IAR_Option_Shoryu_HP;
	cpu->IAR_OptionGroup_NeutralMid[10] = &IAR_Option_crHP;
	cpu->IAR_OptionGroup_NeutralMid[11] = &IAR_Option_stHP;
	
	cpu->IAR_OptionGroup_Close[0] = &IAR_Option_Hitconfirm;
	cpu->IAR_OptionGroup_Close[1] = &IAR_Option_crLP;
	cpu->IAR_OptionGroup_Close[2] = &IAR_Option_crMP;
	cpu->IAR_OptionGroup_Close[3] = &IAR_Option_Grab;
	cpu->IAR_OptionGroup_Close[4] = &IAR_Option_WalkKaraGrab;
	cpu->IAR_OptionGroup_Close[5] = &IAR_Option_JumpFwEmpty;
	cpu->IAR_OptionGroup_Close[6] = &IAR_Option_stMK;
	cpu->IAR_OptionGroup_Close[7] = &IAR_Option_stMP_Hado;
	cpu->IAR_OptionGroup_Close[8] = &IAR_Option_Shoryu_LP;
	
	cpu->IAR_OptionGroup_CheckConfirm[0] = &IAR_Option_BlockLow6Frames;
	cpu->IAR_OptionGroup_CheckConfirm[1] = &IAR_Option_FastHado;
	
	cpu->IAR_OptionGroup_Wakeup[0] = &IAR_Option_WakeupBlockLowTech;
	cpu->IAR_OptionGroup_Wakeup[1] = &IAR_Option_WakeupBlockLong;
	cpu->IAR_OptionGroup_Wakeup[2] = &IAR_Option_WakeupShoryuHP;
	cpu->IAR_OptionGroup_Wakeup[3] = &IAR_Option_WakeupBlockLow_crLP;
	cpu->IAR_OptionGroup_Wakeup[4] = &IAR_Option_WakeupShoryuLP;
	cpu->IAR_OptionGroup_Wakeup[5] = &IAR_Option_WakeupBlockHighTech;
	
	cpu->IAR_OptionGroup_Okizeme[0] = &IAR_Option_Oki_Block;
	cpu->IAR_OptionGroup_Okizeme[1] = &IAR_Option_Oki_IOH;
	cpu->IAR_OptionGroup_Okizeme[2] = &IAR_Option_Oki_WalkForwardGrab;
	cpu->IAR_OptionGroup_Okizeme[3] = &IAR_Option_Oki_WalkForward_crMP;
	cpu->IAR_OptionGroup_Okizeme[4] = &IAR_Option_Oki_crLK;
	cpu->IAR_OptionGroup_Okizeme[5] = &IAR_Option_Oki_crLK_stMK;
	cpu->IAR_OptionGroup_Okizeme[6] = &IAR_Option_Oki_crLP_Grab;
	cpu->IAR_OptionGroup_Okizeme[7] = &IAR_Option_Oki_fHP;
	
	cpu->IAR_OptionGroup_CheckConfirm[0] = &IAR_Option_FastHado;
	cpu->IAR_OptionGroup_CheckConfirm[1] = &IAR_Option_BlockLow6Frames;
	
	cpu->GotToRecordEnd = false;

	cpu->ScoreQueue = clist_create(_IAR_ScoreQueueNode);
	cpu->ExportData = true;
	
	cpu->InQueue = 0;
	cpu->EndgameDump = false;
	
	return cpu;
}





 //Input records will be stored assuming a P1 side. The CPU input setter will flip them accordingly
const uint16_t _IAR_RecordList_BlockLow6Frames[] = {
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D	
};
const IAR_InputRecord IAR_Option_BlockLow6Frames = {_IAR_RecordList_BlockLow6Frames, IAR_OPTION_BLOCK_LOW_6_FRAMES_LENGTH, IAR_OPTION_BLOCK_LOW_6_FRAMES, 8};

const uint16_t _IAR_RecordList_crMK[] = {
	DHEX_DIR_L | DHEX_DIR_D | DHEX_BUTTON_HK
};
const IAR_InputRecord IAR_Option_crMK = {_IAR_RecordList_crMK, IAR_OPTION_CR_MK_LENGTH, IAR_OPTION_CR_MK, 10};

const uint16_t _IAR_RecordList_WalkForward6Frames[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R
	
};
const IAR_InputRecord IAR_Option_WalkForward6Frames = {_IAR_RecordList_WalkForward6Frames, IAR_OPTION_WALK_FORWARD_6_FRAMES_LENGTH, IAR_OPTION_WALK_FORWARD_6_FRAMES, 8};

const uint16_t _IAR_RecordList_WalkForward15Frames[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R
};
const IAR_InputRecord IAR_Option_WalkForward15Frames = {_IAR_RecordList_WalkForward15Frames, IAR_OPTION_WALK_FORWARD_15_FRAMES_LENGTH, IAR_OPTION_WALK_FORWARD_6_FRAMES, 16};

const uint16_t _IAR_RecordList_WalkBack8Frames[] = {
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L
};
const IAR_InputRecord IAR_Option_WalkBack8Frames = {_IAR_RecordList_WalkBack8Frames, IAR_OPTION_WALK_BACK_8_FRAMES_LENGTH, IAR_OPTION_WALK_BACK_8_FRAMES,  10};


const uint16_t _IAR_RecordList_crHK[] = {
	DHEX_DIR_L | DHEX_DIR_D | DHEX_BUTTON_HP
};
const IAR_InputRecord IAR_Option_crHK = {_IAR_RecordList_crHK, IAR_OPTION_CR_HK_LENGTH, IAR_OPTION_CR_HK, 28};

const uint16_t _IAR_RecordList_crMK_Hado[] = {
	DHEX_DIR_L | DHEX_DIR_D | DHEX_BUTTON_HK,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_D,
	DHEX_DIR_D,
	DHEX_DIR_R | DHEX_DIR_D,
	DHEX_DIR_R | DHEX_DIR_D,
	DHEX_DIR_R,
	DHEX_DIR_R | DHEX_BUTTON_SP
	
};
const IAR_InputRecord IAR_Option_crMK_Hado = {_IAR_RecordList_crMK_Hado, IAR_OPTION_CR_MK_HADO_LENGTH, IAR_OPTION_CR_MK_HADO, 24};

const uint16_t _IAR_RecordList_crHK_Hado[] = {
	DHEX_DIR_L | DHEX_DIR_D | DHEX_BUTTON_DS,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_D,
	DHEX_DIR_D,
	DHEX_DIR_R | DHEX_DIR_D,
	DHEX_DIR_R | DHEX_DIR_D,
	DHEX_DIR_R,
	DHEX_DIR_R | DHEX_BUTTON_LP
	
};
const IAR_InputRecord IAR_Option_crHK_Hado = {_IAR_RecordList_crHK_Hado, IAR_OPTION_CR_HK_HADO_LENGTH, IAR_OPTION_CR_MK_HADO, 27};

const uint16_t _IAR_RecordList_RandomTatsu[] = {
	DHEX_DIR_D,
	DHEX_DIR_D | DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L | DHEX_BUTTON_HK
	
};
const IAR_InputRecord IAR_Option_RandomTatsu = {_IAR_RecordList_RandomTatsu, IAR_OPTION_RANDOM_TATSU_LENGTH, IAR_OPTION_RANDOM_TATSU, 25};

const uint16_t _IAR_RecordList_HadoHP[] = {
	DHEX_DIR_L,
	DHEX_DIR_D | DHEX_DIR_L,
	DHEX_DIR_D,
	DHEX_DIR_D | DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_BUTTON_SP	
};
const IAR_InputRecord IAR_Option_HadoHP = {_IAR_RecordList_HadoHP, IAR_OPTION_HADO_HP_LENGTH, IAR_OPTION_HADO_HP, 50};

const uint16_t _IAR_RecordList_HadoLP[] = {
	DHEX_DIR_L,
	DHEX_DIR_D | DHEX_DIR_L,
	DHEX_DIR_D,
	DHEX_DIR_D | DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_BUTTON_LP	
};
const IAR_InputRecord IAR_Option_HadoLP = {_IAR_RecordList_HadoLP, IAR_OPTION_HADO_LP_LENGTH, IAR_OPTION_HADO_LP, 50};

const uint16_t _IAR_RecordList_JumpFwHK[] = {
	DHEX_DIR_R | DHEX_DIR_U,
	DHEX_DIR_R | DHEX_DIR_U,
	DHEX_DIR_R | DHEX_DIR_U,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,

	DHEX_BUTTON_DS
};
const IAR_InputRecord IAR_Option_JumpFwHK = {_IAR_RecordList_JumpFwHK, IAR_OPTION_JUMP_FORWARD_HK_LENGTH, IAR_OPTION_JUMP_FORWARD_HK, 42}; 

const uint16_t _IAR_RecordList_JumpFwMK[] = {
	DHEX_DIR_R | DHEX_DIR_U,
	DHEX_DIR_R | DHEX_DIR_U,
	DHEX_DIR_R | DHEX_DIR_U,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,

	DHEX_BUTTON_HK
};
const IAR_InputRecord IAR_Option_JumpFwMK = {_IAR_RecordList_JumpFwMK, IAR_OPTION_JUMP_FORWARD_MK_LENGTH, IAR_OPTION_JUMP_FORWARD_MK, 42}; 

const uint16_t _IAR_RecordList_AntiAir_jMP[] = {
	DHEX_DIR_R | DHEX_DIR_U,
	DHEX_DIR_R | DHEX_DIR_U,
	DHEX_DIR_R | DHEX_DIR_U,
	DHEX_BUTTON_HP

};
const IAR_InputRecord IAR_Option_AntiAir_jMP = {_IAR_RecordList_AntiAir_jMP, IAR_OPTION_ANTI_AIR_J_MP_LENGTH, IAR_OPTION_ANTI_AIR_J_MP, 30}; 


const uint16_t _IAR_RecordList_ShoryuHP[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	0, 0,
	DHEX_DIR_D,
	DHEX_DIR_D,
	DHEX_DIR_D | DHEX_DIR_R,
	DHEX_DIR_D | DHEX_DIR_R | DHEX_BUTTON_SP

};
const IAR_InputRecord IAR_Option_Shoryu_HP = {_IAR_RecordList_ShoryuHP, IAR_OPTION_SHORYU_HP_LENGTH, IAR_OPTION_SHORYU_HP, 12}; 

const uint16_t _IAR_RecordList_crHP[] = {
	DHEX_DIR_D | DHEX_BUTTON_SP

};
const IAR_InputRecord IAR_Option_crHP = {_IAR_RecordList_crHP, IAR_OPTION_CR_HP_LENGTH, IAR_OPTION_CR_HP, 12}; 

const uint16_t _IAR_RecordList_stHP[] = {
	DHEX_BUTTON_SP
};
const IAR_InputRecord IAR_Option_stHP = {_IAR_RecordList_stHP, IAR_OPTION_ST_HP_LENGTH, IAR_OPTION_ST_HP, 12};

const uint16_t _IAR_RecordList_WalkKaraGrab[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R | DHEX_BUTTON_LK,
	DHEX_BUTTON_LP | DHEX_BUTTON_LK

};
const IAR_InputRecord IAR_Option_WalkKaraGrab = {_IAR_RecordList_WalkKaraGrab, IAR_OPTION_WALK_KARA_GRAB_LENGTH, IAR_OPTION_WALK_KARA_GRAB, 36}; 

const uint16_t _IAR_RecordList_crLP[] = {
	DHEX_DIR_D | DHEX_BUTTON_LP

};
const IAR_InputRecord IAR_Option_crLP = {_IAR_RecordList_crLP, IAR_OPTION_CR_LP_LENGTH, IAR_OPTION_CR_LP, 7}; 

const uint16_t _IAR_RecordList_crMP[] = {
	DHEX_DIR_D | DHEX_BUTTON_HP

};
const IAR_InputRecord IAR_Option_crMP = {_IAR_RecordList_crMP, IAR_OPTION_CR_MP_LENGTH, IAR_OPTION_CR_MP, 9}; 

const uint16_t _IAR_RecordList_Grab[] = {
	DHEX_BUTTON_LK | DHEX_BUTTON_LP

};
const IAR_InputRecord IAR_Option_Grab = {_IAR_RecordList_Grab, IAR_OPTION_GRAB_LENGTH, IAR_OPTION_GRAB, 32}; 

const uint16_t _IAR_RecordList_Hitconfirm[] = {
	DHEX_BUTTON_LK | DHEX_DIR_D,
	0, 0, 0, 0, 0, 0, 
	DHEX_BUTTON_LP | DHEX_DIR_D,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	DHEX_BUTTON_LK

};
const IAR_InputRecord IAR_Option_Hitconfirm = {_IAR_RecordList_Hitconfirm, IAR_OPTION_HITCONFIRM_LENGTH , IAR_OPTION_HITCONFIRM, 38}; 

const uint16_t _IAR_RecordList_JumpFwEmpty[] = {
	DHEX_DIR_U | DHEX_DIR_R
};
const IAR_InputRecord IAR_Option_JumpFwEmpty = {_IAR_RecordList_JumpFwEmpty, IAR_OPTION_JUMP_FORWARD_EMPTY_LENGTH, IAR_OPTION_JUMP_FORWARD_EMPTY, 40}; 

const uint16_t _IAR_RecordList_stMK[] = {
	DHEX_BUTTON_HK
};
const IAR_InputRecord IAR_Option_stMK = {_IAR_RecordList_stMK, IAR_OPTION_ST_MK_LENGTH, IAR_OPTION_ST_MK, 26}; 

const uint16_t _IAR_RecordList_stMP_Hado[] = {
	DHEX_BUTTON_HP | DHEX_DIR_L,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_D,
	DHEX_DIR_D | DHEX_DIR_R,
	DHEX_DIR_R,
	0, 0, 0,
	DHEX_BUTTON_HP
	
};
const IAR_InputRecord IAR_Option_stMP_Hado = {_IAR_RecordList_stMP_Hado, IAR_OPTION_ST_MP_HADO_LENGTH, IAR_OPTION_ST_MP_HADO, 26}; 

const uint16_t _IAR_RecordList_ShoryuLP[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	0, 0,
	DHEX_DIR_D,
	DHEX_DIR_D,
	DHEX_DIR_D | DHEX_DIR_R,
	DHEX_DIR_D | DHEX_DIR_R | DHEX_BUTTON_LP

};
const IAR_InputRecord IAR_Option_Shoryu_LP = {_IAR_RecordList_ShoryuLP, IAR_OPTION_SHORYU_LP_LENGTH, IAR_OPTION_SHORYU_LP, 12}; 

const uint16_t _IAR_RecordList_WakeupBlockLong[] = {
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D

};
const IAR_InputRecord IAR_Option_WakeupBlockLong = {_IAR_RecordList_WakeupBlockLong, IAR_OPTION_WAKEUP_BLOCK_LONG_LENGTH, IAR_OPTION_WAKEUP_BLOCK_LONG, 16}; 

const uint16_t _IAR_RecordList_WakeupBlockLowTech[] = {
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_BUTTON_LP | DHEX_BUTTON_LK

};
const IAR_InputRecord IAR_Option_WakeupBlockLowTech = {_IAR_RecordList_WakeupBlockLowTech, IAR_OPTION_WAKEUP_BLOCK_LOW_TECH_LENGTH, IAR_OPTION_WAKEUP_BLOCK_LOW_TECH, 30};

const uint16_t _IAR_RecordList_WakeupBlockHighTech[] = {
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_BUTTON_LP | DHEX_BUTTON_LK

};
const IAR_InputRecord IAR_Option_WakeupBlockHighTech = {_IAR_RecordList_WakeupBlockHighTech, IAR_OPTION_WAKEUP_BLOCK_HIGH_TECH_LENGTH, IAR_OPTION_WAKEUP_BLOCK_HIGH_TECH, 30};

const uint16_t _IAR_RecordList_WakeupBlockLow_crLP[] = {
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_BUTTON_LP | DHEX_DIR_D

};
const IAR_InputRecord IAR_Option_WakeupBlockLow_crLP = {_IAR_RecordList_WakeupBlockLow_crLP, IAR_OPTION_WAKEUP_BLOCK_LOW_CR_LP_LENGTH, IAR_OPTION_WAKEUP_BLOCK_LOW_CR_LP, 9};

const uint16_t _IAR_RecordList_WakeupShoryuLP[] = {
	DHEX_DIR_R,
	0,
	DHEX_DIR_D,
	DHEX_DIR_D | DHEX_DIR_R | DHEX_BUTTON_LP

};
const IAR_InputRecord IAR_Option_WakeupShoryuLP = {_IAR_RecordList_WakeupShoryuLP, IAR_OPTION_WAKEUP_SHORYU_LP_LENGTH, IAR_OPTION_WAKEUP_SHORYU_LP, 12};

const uint16_t _IAR_RecordList_WakeupShoryuHP[] = {
	DHEX_DIR_R,
	0,
	DHEX_DIR_D,
	DHEX_DIR_D | DHEX_DIR_R | DHEX_BUTTON_SP

};
const IAR_InputRecord IAR_Option_WakeupShoryuHP = {_IAR_RecordList_WakeupShoryuHP, IAR_OPTION_WAKEUP_SHORYU_HP_LENGTH, IAR_OPTION_WAKEUP_SHORYU_HP, 12};

const uint16_t _IAR_RecordList_Oki_crLP_Grab[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_D | DHEX_BUTTON_LP,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R | DHEX_BUTTON_LK,
	DHEX_DIR_R | DHEX_BUTTON_LP | DHEX_BUTTON_LK
	
};
const IAR_InputRecord IAR_Option_Oki_crLP_Grab = {_IAR_RecordList_Oki_crLP_Grab, IAR_OPTION_OKI_CR_LP_GRAB_LENGTH, IAR_OPTION_OKI_CR_LP_GRAB, 42};

const uint16_t _IAR_RecordList_Oki_WalkForwardGrab[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R | DHEX_BUTTON_LK,
	DHEX_DIR_R | DHEX_BUTTON_LP | DHEX_BUTTON_LK
	
};
const IAR_InputRecord IAR_Option_Oki_WalkForwardGrab = {_IAR_RecordList_Oki_WalkForwardGrab, IAR_OPTION_OKI_WALK_FORWARD_GRAB_LENGTH, IAR_OPTION_OKI_WALK_FORWARD_GRAB, 32};

const uint16_t _IAR_RecordList_Oki_WalkForward_crMP[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_D | DHEX_BUTTON_HP
	
};
const IAR_InputRecord IAR_Option_Oki_WalkForward_crMP = {_IAR_RecordList_Oki_WalkForward_crMP, IAR_OPTION_OKI_WALK_FORWARD_CR_MP_LENGTH, IAR_OPTION_OKI_WALK_FORWARD_CR_MP, 20};


const uint16_t _IAR_RecordList_Oki_Block[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D

	
};
const IAR_InputRecord IAR_Option_Oki_Block = {_IAR_RecordList_Oki_Block, IAR_OPTION_OKI_BLOCK_LENGTH, IAR_OPTION_OKI_BLOCK, 18};


const uint16_t _IAR_RecordList_Oki_fHP[] = {
	DHEX_DIR_R | DHEX_BUTTON_SP
};
const IAR_InputRecord IAR_Option_Oki_fHP = {_IAR_RecordList_Oki_fHP, IAR_OPTION_OKI_FORWARD_HP_LENGTH, IAR_OPTION_OKI_FORWARD_HP, 18};

const uint16_t _IAR_RecordList_Oki_crLK_stMK[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_D | DHEX_BUTTON_LK,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,

	DHEX_BUTTON_HK

};
const IAR_InputRecord IAR_Option_Oki_crLK_stMK = {_IAR_RecordList_Oki_crLK_stMK, IAR_OPTION_OKI_CR_LK_ST_MK_LENGTH , IAR_OPTION_OKI_CR_LK_ST_MK, 32};

const uint16_t _IAR_RecordList_Oki_crLK[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_D | DHEX_BUTTON_LK

};
const IAR_InputRecord IAR_Option_Oki_crLK = {_IAR_RecordList_Oki_crLK, IAR_OPTION_OKI_CR_LK_LENGTH , IAR_OPTION_OKI_CR_LK, 12};

const uint16_t _IAR_RecordList_Oki_IOH[] = {
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R,
	DHEX_DIR_R | DHEX_DIR_U | DHEX_BUTTON_LK	

};
const IAR_InputRecord IAR_Option_Oki_IOH = {_IAR_RecordList_Oki_IOH, IAR_OPTION_OKI_IOH_J_LK_LENGTH, IAR_OPTION_OKI_IOH_J_LK, 32};

const uint16_t _IAR_RecordList_FastHado[] = {
	DHEX_DIR_D,
	DHEX_DIR_L | DHEX_DIR_D,
	DHEX_DIR_D,
	DHEX_DIR_D | DHEX_DIR_R,
	DHEX_DIR_R | DHEX_BUTTON_SP

};
const IAR_InputRecord IAR_Option_FastHado = {_IAR_RecordList_FastHado, IAR_OPTION_FAST_HADO_LENGTH, IAR_OPTION_FAST_HADO, 20};