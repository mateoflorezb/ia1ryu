#include "DHEX_boxes.h"
#include <stdio.h>
#include <stdlib.h>

DHEX_FrameBoxes* DHEX_LoadBXG(const char* filename) {
	
	FILE* file = fopen(filename, "rb");
	if(ferror(file) != 0) {
		printf("Couldn't open file %s\n", filename);
		return NULL;
	}
	
	DHEX_FrameBoxes* frame = (DHEX_FrameBoxes*)malloc(sizeof(DHEX_FrameBoxes));
	
	char c = 'a';
	while (c != '\0') { //search for the end of the string at the start of the bxg
		fread((void*)&c, 1, 1, file);
	}
	
	for (uint8_t b = 0; b < DHEX_MAX_HBOXES; b++) { //write null terminated hurtbox list
		fread((void*)&(frame->hurtboxes[b]), sizeof(DHEX_HBox), 1, file);
		if (frame->hurtboxes[b].w == 0) break;
	}
	
	for (uint8_t b = 0; b < DHEX_MAX_HBOXES; b++) { //write null terminated hurtbox list
		fread((void*)&(frame->hitboxes[b]), sizeof(DHEX_HBox), 1, file);
		if (frame->hitboxes[b].w == 0) break;
	}
	
	fread((void*)&(frame->xcenter), sizeof(int16_t), 1, file);
	fread((void*)&(frame->ycenter), sizeof(int16_t), 1, file);		
	
	fclose(file);
	
	return frame;	

}




DHEX_FrameBoxes* DHEX_GetBXG(DHEX_App* app, const char* filename) {
	
	if (app == NULL) return NULL;
	
	clist* ref = (clist*)(app->__private.BXG_List);
	clist_iterator it = clist_start(ref);
	while (it != NULL) {
		_DHEX_APP_BXG_LIST_STRUCT st = clist_get(it,_DHEX_APP_BXG_LIST_STRUCT);
		if (strcmp(st.filename,filename) == 0) {
			return st.bxg;
		}
	
	it = clist_next(it);
	}
	
	DHEX_FrameBoxes* B = DHEX_LoadBXG(filename);
	
	if (B != NULL) {
		_DHEX_APP_BXG_LIST_STRUCT* st = malloc(sizeof(_DHEX_APP_BXG_LIST_STRUCT));
		char* tmpStr = malloc(strlen(filename)+1);
		//strcpy(tmpStr, filename);
		//strcat(tmpStr, "");
		sprintf(tmpStr, "%s\0", filename);
		st->filename = (const char*)tmpStr;
		st->bxg = B;
		clist_push(ref, st);
		
		return B;
		
	}
	
	return NULL;
	
}