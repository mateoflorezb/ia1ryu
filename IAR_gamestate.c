#include "IAR_gamestate.h"
#include "IAR_obj.h"
#include "IAR_global.h"
#include "IAR_general.h"
#include <stdio.h>

extern glob_ds global;

static IAR_Obj* _local_IAR_CloneObj(IAR_Obj* o) {
	IAR_Obj* clone = o->GetCopy(o);
	return clone;
}

IAR_GameState* IAR_CloneGameState(IAR_GameState* gs) {
	IAR_GameState* ns = malloc(sizeof(IAR_GameState));
	ns->objs = clist_create(IAR_Obj*);
	ns->CreationQueue = clist_create(IAR_Obj*);
	
	ns->cam_x = gs->cam_x;
	ns->cam_y = gs->cam_y;
	ns->SecondTimer = gs->SecondTimer;
	ns->FrameTimer = gs->FrameTimer;

	ns->InstanceCounter = gs->InstanceCounter;
	
	for (uint8_t k = 0; k < 2; k++) {
		clist* src = gs->objs;
		clist* dst = ns->objs;
		if (k == 1) {
			src = gs->CreationQueue;
			dst = ns->CreationQueue;
		}
		
		clist_iterator it = clist_start(src);
		while (it != NULL) {
			IAR_Obj* o = clist_get(it, IAR_Obj*);
			IAR_Obj* clone = _local_IAR_CloneObj(o);
			clone->gs = ns;
			clist_push(dst, &clone);
			it = clist_next(it);
		}
		
	}
	
	//find players
	bool found = false;
	for (uint8_t k = 0; k < 2; k++) {
		clist_iterator it = clist_start(ns->objs);
		if (k == 1) it = clist_start(ns->CreationQueue);
		if (found) break;
		while (it != NULL) {
			IAR_Obj* o = clist_get(it, IAR_Obj*);
			if (o->ClassGroup == IAR_GROUP_CHAR && o->Class.Player.Pside == 1) {
				ns->P1 = o;
				found = true;

			}
				
			
			else
			if (o->ClassGroup == IAR_GROUP_CHAR && o->Class.Player.Pside == 2)  {
				ns->P2 = o;

			}
			
			it = clist_next(it);
		}
	}       
	
	//ns->P1->gs = ns;
	//ns->P2->gs = ns;
	
	
	return ns;
}

void IAR_FreeGameState(IAR_GameState* gs) {
	clist_free(gs->objs);
	clist_free(gs->CreationQueue);
	free(gs);
}

static void __local_creationQueueCallback(void* a) {return;}

static void __local_free_IAR_Obj(void* obj) {
	
	IAR_Obj* inst = *(IAR_Obj**)obj;
	inst->destroy(inst);
	return;
}


IAR_GameState* IAR_NewGameState() {
	IAR_GameState* ns = malloc(sizeof(IAR_GameState));
	
	ns->objs = clist_create(IAR_Obj*);
	ns->CreationQueue = clist_create(IAR_Obj*);
	ns->objs->free_element_callback = &__local_free_IAR_Obj;	
	ns->CreationQueue->free_element_callback = &__local_creationQueueCallback;
	ns->InstanceCounter = 0;
	
	ns->P1 = IAR_InstanceCreate(IAR_CreateRyu, &(int){IAR_RYU_PAL_GRAY}, ns);
	ns->P2 = IAR_InstanceCreate(IAR_CreateRyu, &(int){IAR_RYU_PAL_BLUE}, ns); 
	ns->P1->Class.Player.Pside = 1;
	ns->P1->Class.Player.OpponentID = ns->P2->InstanceID;
	
	//printf("first and last %p  ---  %p  \n", ns->CreationQueue->first->data, ns->CreationQueue->last->data);
	//clist_print(ns->CreationQueue);
	
	ns->P2->Class.Player.Pside = 2;
	ns->P2->Class.Player.OpponentID = ns->P1->InstanceID;
	
	ns->P1->gs = ns;
	ns->P2->gs = ns;

	
	const int PlayoDistance = 80;
	ns->P1->Class.Player.x = IAR_FIXED_SCALE*(-PlayoDistance+STAGE_WIDTH/2);
	ns->P2->Class.Player.x = IAR_FIXED_SCALE*( PlayoDistance+STAGE_WIDTH/2);
	
	ns->P2->Class.Player.xdraw =-1;
	
	ns->cam_x = (STAGE_WIDTH/2)-(SCREEN_WIDTH/2);
	ns->cam_y = 0;
	
	return ns;
}

void IAR_AdvanceGameState(IAR_GameState* gs) {
	
	DHEX_DrawLayer(global.app, DHEX_LAYER_BACKGROUND0);
	DHEX_DrawSprite(global.app, global.resources.background, -gs->cam_x, -gs->cam_y, 0, 0, 1.0, 1.0, 0.0);
	
	//if (DHEX_InputCheckPressed(global.app, DHEX_MACRO_2, 2))
	//	gs->P1->Class.Player.y = 0;
	
	clist_iterator it = clist_start(gs->CreationQueue);
	
	while (it != NULL)  {
		
		
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		clist_push(gs->objs, &o);
		it = clist_next(it);
		
	}
	
	
	clist_clear(gs->CreationQueue);
	
	
	it = clist_start(gs->objs);
	
	while (it != NULL)  {
		
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		
		//printf("(while) ryu health: %d \n", o->Class.Player.health);
		if (!o->DestroyFlag) o->pre_step(o);
		it = clist_next(it);
	}
	
	
	
	it = clist_start(gs->objs);
	while (it != NULL)  {
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		if (!o->DestroyFlag) o->step(o);
		it = clist_next(it);
	}
	
	
	
	//player arbiter: check for hits/trades
	
	DHEX_DrawLayer(global.app, DHEX_LAYER_HUD2);
	DHEX_FrameBoxes bx1 = IAR_ScaleBXG(*gs->P1->Class.Player.CurrentFrame.bxg, gs->P1->Class.Player.xdraw, 1);
	DHEX_FrameBoxes bx2 = IAR_ScaleBXG(*gs->P2->Class.Player.CurrentFrame.bxg, gs->P2->Class.Player.xdraw, 1);
	char P1Scored, P2Scored;
	IAR_TestBXG_Collision(bx1, fixed_cast(gs->P1->Class.Player.x), fixed_cast(gs->P1->Class.Player.y),
	bx2, fixed_cast(gs->P2->Class.Player.x), fixed_cast(gs->P2->Class.Player.y),  &P1Scored, &P2Scored);
	
	//IAR_DrawBXG(bx1, fixed_cast(P1->Class.Player.x), fixed_cast(P1->Class.Player.y));
	//IAR_DrawBXG(bx2, fixed_cast(P2->Class.Player.x), fixed_cast(P2->Class.Player.y));
	
	for (uint8_t pside = 1; pside <= 2; pside++) {
		char Pscore = P1Scored;
		IAR_Obj* enemy = gs->P2;
		IAR_Obj* self = gs->P1;
		if (pside == 2) {
			Pscore = P2Scored;
			enemy = gs->P1;
			self  = gs->P2;
		}
		
		bool EnemyInvincible = false;
		
		if ( (enemy->Class.Player.DefenseFlags & IAR_DEFENSE_INVINCIBLE) != 0
		||
			(((enemy->Class.Player.DefenseFlags & IAR_DEFENSE_ANTI_AIR) != 0) && self->Class.Player.inAir))
			EnemyInvincible = true;
		
		if (Pscore == 1 && self->Class.Player.AttackHits > 0 && self->Class.Player.MultiHitDelay == 0 && !EnemyInvincible) {
			self->Class.Player.AttackHits--;
			self->Class.Player.MultiHitDelay = 2;
			
			bool EnemyCanBlock = false;
			if ((enemy->Class.Player.DefenseFlags & IAR_DEFENSE_CANBLOCK) != 0)
				EnemyCanBlock = true;
			
			bool EnemyHoldingBack = false;
			if (
			(IAR_IsInputFlagged(enemy->Class.Player.CurrentFrameInputs, DHEX_DIR_R) && enemy->Class.Player.xdir ==-1)
			||
			(IAR_IsInputFlagged(enemy->Class.Player.CurrentFrameInputs, DHEX_DIR_L) && enemy->Class.Player.xdir == 1)
			) {
				EnemyHoldingBack = true;
				
			}
			
			int StunTime = self->Class.Player.AppliedOnHit;
			int sparkType = IAR_SPARK_HIT1;
			if (self->Class.Player.HitLevel == IAR_HITLEVEL_MEDIUM)  sparkType = IAR_SPARK_HIT2;
			if (self->Class.Player.HitLevel == IAR_HITLEVEL_HARD)    sparkType = IAR_SPARK_HIT3;
			if (self->Class.Player.HitLevel == IAR_HITLEVEL_SPECIAL) sparkType = IAR_SPARK_HIT4;
			
			bool Blocked = false;
			if (EnemyCanBlock && EnemyHoldingBack && enemy->Class.Player.inAir == false) Blocked = true;
			
			if (!((self->Class.Player.AttackFlags & IAR_ATTACK_CANBLOCK_LOW)  != 0))  //CANT block low
			if  (IAR_IsInputFlagged(enemy->Class.Player.CurrentFrameInputs, DHEX_DIR_D))
				Blocked = false;
			
			if (!((self->Class.Player.AttackFlags & IAR_ATTACK_CANBLOCK_HIGH) != 0))  //CANT block high
			if (!(IAR_IsInputFlagged(enemy->Class.Player.CurrentFrameInputs, DHEX_DIR_D)))
				Blocked = false;
			
			
			
			if (Blocked) {
				sparkType = IAR_SPARK_BLOCK1;
				StunTime = self->Class.Player.AppliedOnBlock;
			}
			
			IAR_CreateSpark(sparkType, fixed_cast(self->Class.Player.x)+self->Class.Player.Spark_xOffset* self->Class.Player.xdraw,
							fixed_cast(self->Class.Player.y)+self->Class.Player.Spark_yOffset, self->Class.Player.xdraw, gs);
			
			IAR_PlayerApplyHit(enemy, Blocked, self->Class.Player.Launcher, self->Class.Player.ForceStand, self->Class.Player.HitStunType, 
							   self->Class.Player.AppliedOnHit, self->Class.Player.AppliedOnBlock, self->Class.Player.AppliedStopFrames,
							   self->Class.Player.AppliedPushback, self->Class.Player.AppliedJuggleHs, self->Class.Player.AppliedJuggleVs,
							   self->Class.Player.AppliedJuggleTime, self->Class.Player.xdraw, self->Class.Player.JP,
							   self->Class.Player.AppliedDamage, self->Class.Player.AppliedChip); 
			
			
			self->Class.Player.StopFrames = self->Class.Player.AppliedStopFrames;
			self->Class.Player.LastAttackLanded = true;

		}
	}
	
	it = clist_start(gs->objs);
	while (it != NULL)  {
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		
		if (!o->DestroyFlag && o->ClassID == IAR_OBJ_PROJECTILE) {
			//create bxg representing fireball hitbox
			DHEX_FrameBoxes fireball;
			fireball.xcenter = 0;
			fireball.ycenter = 0;
			fireball.hurtboxes[0].w = 0;
			
			fireball.hitboxes[0].w = o->Class.Projectile.HitboxW*2;
			fireball.hitboxes[0].h = o->Class.Projectile.HitboxH*2;
			fireball.hitboxes[0].x =-o->Class.Projectile.HitboxW;
			fireball.hitboxes[0].y =-o->Class.Projectile.HitboxH;
			
			fireball.hitboxes[1].w = 0;
			
			//IAR_DrawBXG(fireball, fixed_cast(o->Class.Projectile.x)-gs->cam_x, fixed_cast(o->Class.Projectile.y)-gs->cam_y );			
			
			IAR_Obj* enemy = gs->P1;
			if (o->Class.Projectile.Pside == 1) enemy = gs->P2;
			
			char FireballScored, Ignore;
			IAR_TestBXG_Collision(fireball, fixed_cast(o->Class.Projectile.x), fixed_cast(o->Class.Projectile.y),
			IAR_ScaleBXG(*enemy->Class.Player.CurrentFrame.bxg, enemy->Class.Player.xdraw, 1),
			fixed_cast(enemy->Class.Player.x), fixed_cast(enemy->Class.Player.y),  &FireballScored, &Ignore);
			
			if (FireballScored == 1 && o->Class.Projectile.Hits > 0) {
				
				
				bool EnemyCanBlock = false;
				if ((enemy->Class.Player.DefenseFlags & IAR_DEFENSE_CANBLOCK) != 0)
					EnemyCanBlock = true;
				
				bool EnemyHoldingBack = false;
				if (
				(IAR_IsInputFlagged(enemy->Class.Player.CurrentFrameInputs, DHEX_DIR_R) && enemy->Class.Player.xdir ==-1)
				||
				(IAR_IsInputFlagged(enemy->Class.Player.CurrentFrameInputs, DHEX_DIR_L) && enemy->Class.Player.xdir == 1)
				) {
					EnemyHoldingBack = true;
					
				}
				
				bool Blocked = false;
				if (EnemyCanBlock && EnemyHoldingBack && enemy->Class.Player.inAir == false) Blocked = true;
				
				bool EnemyInvincible = false;
		
				if ((enemy->Class.Player.DefenseFlags & IAR_DEFENSE_INVINCIBLE) != 0)
					EnemyInvincible = true;
		
				if (!EnemyInvincible) {
				
					IAR_PlayerApplyHit(enemy, Blocked, o->Class.Projectile.Launcher, false, IAR_STUNTYPE_GUT, 
								   o->Class.Projectile.AppliedOnHit, o->Class.Projectile.AppliedOnBlock, o->Class.Projectile.AppliedStopFrames,
								   o->Class.Projectile.AppliedPushback, o->Class.Projectile.AppliedJuggleHs, o->Class.Projectile.AppliedJuggleVs,
								   o->Class.Projectile.AppliedJuggleTime, o->Class.Projectile.xdraw, 1,  o->Class.Projectile.AppliedDamage, o->Class.Projectile.AppliedChip);
					
					int SparkType = IAR_SPARK_HIT4;
					if (Blocked) 
						SparkType = IAR_SPARK_BLOCK2;

					IAR_CreateSpark(SparkType, fixed_cast(o->Class.Projectile.x), fixed_cast(o->Class.Projectile.y), o->Class.Projectile.xdraw, gs);						
					
					
					enemy->Class.Player.CounterPushback = false;
					
					o->Class.Projectile.Hits--;
					if (o->Class.Projectile.Hits <= 0) {
						IAR_InstanceDestroy(o->InstanceID, o->gs);
					}
					
				}
				
				
			}
			
		}
		
		it = clist_next(it);
	}
	
	it = clist_start(gs->objs);
	while (it != NULL)  {
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		if (!o->DestroyFlag) o->post_step(o);
		it = clist_next(it);
	}
	
	IAR_CollideChars(gs->P1, gs->P2);
	
	it = clist_start(gs->objs);
	while (it != NULL)  {
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		if (!o->DestroyFlag) o->draw(o);
		it = clist_next(it);
	}
	
	IAR_DrawHealthBars(gs->P1, gs->P2);
	
	if (DHEX_InputCheckPressed(global.app, DHEX_MACRO_2, 1)) {
		gs->P1->Class.Player.health = gs->P1->Class.Player.MAX_HEALTH;
		gs->P2->Class.Player.health = gs->P2->Class.Player.MAX_HEALTH;
	}
	
	
	it = clist_start(gs->objs); 
	while (it != NULL)  { //destroy flagged objs
		clist_iterator next = clist_next(it);
		IAR_Obj* o = clist_get(it, IAR_Obj*);
		if (o->DestroyFlag) {
			clist_delete(it);
		}
		it = next;
	}
	
	int PlayerCenter = (fixed_cast(gs->P1->Class.Player.x)+fixed_cast(gs->P2->Class.Player.x))/2;
	const int cam_target = (int)iclamp(PlayerCenter-SCREEN_WIDTH/2, 0, STAGE_WIDTH - SCREEN_WIDTH);
	int deltaCam = cam_target - gs->cam_x;
	gs->cam_x += deltaCam/3;
	gs->cam_y = 0;
	
}