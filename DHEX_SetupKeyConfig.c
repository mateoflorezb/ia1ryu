#include "DHEX.h"
#include <stdio.h>
#include <stdbool.h>

#define _DHEX_KEY_CONFIG_FILENAME "DHEX_KeyConfig"

static const char* _local_GetButtonName[] = {
	"LP",
	"HP",
	"SP",
	"LK",
	"HK",
	"DS",
	"START",
	"Right",
	"Left",
	"Up",
	"Down",
	"Macro 1",
	"Macro 2"	
};

static void ClearScreen(void) {
	system("cls");
}

int main(int argc, char* argv[]) {
	DHEX_App* app = DHEX_Init(NULL, "DHEX Setup Key Config", 300, 300);
	
	int Pside = 1; // 1 or 2
	int ButtonIndex = 0; 
	bool Prompt = false;
	int ReadDelay = 0;
	
	
	while(DHEX_IsRunning(app)) {
		DHEX_UpdateState(app);
		DHEX_ClearScreen(app,0,0,0);
		
		if (ReadDelay > 0) ReadDelay--;
		
		if (!Prompt) {
			ClearScreen();
			printf("Press %s for player %d", _local_GetButtonName[ButtonIndex], Pside);
			Prompt = true;
		}
		
		if (app->__private.Input.AnyVk && ReadDelay <= 0) {
			app->__private.Input.KeyConfig[ButtonIndex][Pside - 1] = app->__private.Input.LastVk;
			Prompt = false;
			ButtonIndex++;
			ReadDelay = 7;
			
		}
		
		if (ButtonIndex >= __DHEX_MAX_BUTTONS) {
			ButtonIndex = 0;
			Pside++;
		}
		
		if (Pside >= 3) {
			DHEX_SaveKeyConfig(app, _DHEX_KEY_CONFIG_FILENAME);
			break;
		}
		
		DHEX_UpdateScreen(app);
		DHEX_Delay(25);
	}
	
	DHEX_FreeApp(app);
	
	return 0;
}