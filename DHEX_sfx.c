#include "DHEX_base.h"
#include "DHEX_sfx.h"
#include "SDL_mixer.h"

#include <stdio.h>

DHEX_Sfx* _DHEX_LoadSfx(const char* file) {
    Mix_Chunk* wav;
    wav = Mix_LoadWAV(file);
    if (wav == NULL) {
        printf("Mix_LoadWAV: %s\n", Mix_GetError() );
        return NULL;
    }

    DHEX_Sfx* s = malloc(sizeof(DHEX_Sfx));
	(*s) = (DHEX_Sfx){(void*)wav, wav->abuf, wav->alen};
    return s;
	
	
}

int8_t _DHEX_FreeSfx(DHEX_Sfx* s) {
    if (s == NULL) return -1;
    ((Mix_Chunk*)s->wav)->abuf = (uint8_t*)s->original_abuf;
    ((Mix_Chunk*)s->wav)->alen = s->original_alen;
    Mix_FreeChunk((Mix_Chunk*)s->wav);
    free(s);
    return 0;
	
}

int8_t DHEX_PlaySfx(int Channel, DHEX_Sfx* S, int Loops, uint32_t SkipBytes) {
	if (S->original_alen <= SkipBytes) return -1;
    ((Mix_Chunk*)S->wav)->abuf = (uint8_t*)(S->original_abuf+SkipBytes);
    ((Mix_Chunk*)S->wav)->alen = S->original_alen-SkipBytes;
    int8_t error = Mix_PlayChannel(Channel, (Mix_Chunk*)S->wav, Loops);
    return error;
	
}

int8_t _DHEX_SfxInit() {
    {
        uint32_t flags = MIX_INIT_OGG;
        if ((Mix_Init(MIX_INIT_OGG) & flags) != flags) {
            printf("Mix_Init: %s\n", Mix_GetError() );
            return -1;
        }
    }

	//default frequency is 22050 samples per second
	//default format is signed 16 bit
	//2 channels (stereo, nothing to do with mixing channels)
	//so, bytes per second of sfx = 22050 * 2 (bytes per sample) * 2 (channels)
	//so bps = 88200
	//bytes per frame (at 60hz) = 1470
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) < 0) {
        printf("Mix_OpenAudio: %s\n", Mix_GetError() );
        return -1;
    }
	
	Mix_AllocateChannels(_DHEX_AUDIO_CHANNEL_MAX);

    return 0;
}


DHEX_Sfx* DHEX_GetSfx(DHEX_App* app, const char* filename) {
	
	if (app == NULL) return NULL;
	
	clist* ref = (clist*)(app->__private.SfxList);
	clist_iterator it = clist_start(ref);
	while (it != NULL) {
		_DHEX_APP_SFX_LIST_STRUCT st = clist_get(it,_DHEX_APP_SFX_LIST_STRUCT);
		if (strcmp(st.filename,filename) == 0) {
			return st.sfx;
		}
	
	it = clist_next(it);
	}
	
	/*
	unsigned int tmpFilenameLen = strlen(filename)+strlen(app->WorkingDirectory)+1;
	char* tmpFilename = malloc(tmpFilenameLen);
	strcpy(tmpFilename, app->WorkingDirectory);
	strcat(tmpFilename, filename);
	tmpFilename[tmpFilenameLen-1] = '\0';
	*/
	
	DHEX_Sfx* S =_DHEX_LoadSfx(filename);
	//free(tmpFilename);
	
	
	if (S != NULL) {
		_DHEX_APP_SFX_LIST_STRUCT* st = malloc(sizeof(_DHEX_APP_SFX_LIST_STRUCT));
		char* tmpStr = malloc(strlen(filename)+1);
		strcpy(tmpStr, filename);
		strcat(tmpStr, "");
		st->filename = (const char*)tmpStr;
		st->sfx = S;
		clist_push(ref, st);
		
		return S;
		
	}
	
	return NULL;
	
}

void* DHEX_LoadMusic(DHEX_App* app, const char* filename) {
	
	unsigned int tmpFilenameLen = strlen(filename)+strlen(app->WorkingDirectory)+1;
	char* tmpFilename = malloc(tmpFilenameLen);
	strcpy(tmpFilename, app->WorkingDirectory);
	strcat(tmpFilename, filename);
	tmpFilename[tmpFilenameLen-1] = '\0';
	
	void* m = (void*)Mix_LoadMUS(tmpFilename);
	free(tmpFilename);
	
	return m;
}

void  DHEX_FreeMusic(void* music) {
	if (music == NULL) return;
	Mix_FreeMusic((Mix_Music*) music);
}

int DHEX_PlayMusic(void* music, int loops) {
	if (music == NULL) return -1;
	return Mix_PlayMusic((Mix_Music*) music, loops);
}

int8_t DHEX_VolumeMusic(int8_t volume) {
	return (int8_t) Mix_VolumeMusic((int)volume);
}

int DHEX_FadeInMusicPos(void* music, int loops, int ms, double position) {
	if (music == NULL) return -1;
	return Mix_FadeInMusicPos((Mix_Music*) music, loops, ms, position);
}


void DHEX_PauseMusic() {
	Mix_PauseMusic();
}

void DHEX_ResumeMusic() {
	Mix_ResumeMusic();
}

int DHEX_IsMusicPaused() {
	return Mix_PausedMusic();
}

int DHEX_SetMusicPosition(double position) {
	return Mix_SetMusicPosition(position);
}


