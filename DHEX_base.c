#include "DHEX_base.h"
#include "clist.h"
#include "DHEX_img.h"
#include "DHEX_sfx.h"
#include "DHEX_boxes.h"

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

#define _DHEX_JOYSTICK_ID_NULL 0xFF
#define _DHEX_NULL_DEVICE_PORT 0xFFFFFFFF
#define _DHEX_JOYSTICK_AXIS_THRESHOLD 30000

#define __CHECK_DRAW_ENABLED if (!app->Draw.Enabled) return;

enum _DHEX_DRAW_CALL {
	_DHEX_DRAW_CALL_SPRITE = 0,
	_DHEX_DRAW_CALL_FILLRECT = 1,
	_DHEX_DRAW_CALL_RECT = 2,
	_DHEX_DRAW_CALL_TEXT = 3
};


static void __local_FREE_DHEX_APP_SPRITE_RAW_LIST_STRUCT(void* data) {
	_DHEX_APP_SPRITE_RAW_LIST_STRUCT* st = (_DHEX_APP_SPRITE_RAW_LIST_STRUCT*)data;
	free((void*)st->filename);
	DHEX_FreeSprite(st->spr);
	free(data);	
	
}

static void __local_FREE_DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT(void* data) {
	_DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT* st = (_DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT*)data;
	free((void*)st->spr_filename);
	free((void*)st->pal_filename);
	DHEX_FreeSprite(st->spr);
	free(data);	
	
}

static void __local_FREE_DHEX_APP_BXG_LIST_STRUCT(void* data) {
	_DHEX_APP_BXG_LIST_STRUCT* st = (_DHEX_APP_BXG_LIST_STRUCT*)data;
	free((void*)st->filename);
	free(data);	
}

static void __local_FREE_DHEX_APP_SFX_LIST_STRUCT(void* data) {
	_DHEX_APP_SFX_LIST_STRUCT* st = (_DHEX_APP_SFX_LIST_STRUCT*)data;
	free((void*)st->filename);
	 _DHEX_FreeSfx(st->sfx);
	free(data);	
}

void DHEX_GetScreenDimensions(DHEX_App* app, int* w, int* h) {
	if (app != NULL) {
		if (w != NULL) 
			(*w) = app->__private.ScreenWidth;
			
		if (h != NULL) 
			(*h) = app->__private.ScreenHeight;		
		
	}
	
	return;
}

DHEX_App* DHEX_Init(int* ErrorState, const char* AppName, int w, int h) {
	
	DHEX_App* app = malloc(sizeof(DHEX_App));
	app->__private.SpriteRawList = NULL;
	app->__private.SpritePalettizedList = NULL;
	app->__private.MainRenderer = NULL;
	app->__private.MainWindow = NULL;
	
	int tmpError = 0;
	
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_AUDIO) < 0) {
		printf("SDL_Init: %s\n", SDL_GetError());
		tmpError = -1;
	}
    SDL_JoystickEventState(SDL_ENABLE);

	if((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
		printf("IMG_Init: %s\n", IMG_GetError());
		tmpError = -1;
	}
	
	if (_DHEX_SfxInit() < 0 ) {
		tmpError = -1;
	}
	
	app->__private.MainWindow = SDL_CreateWindow(AppName,
				                               SDL_WINDOWPOS_UNDEFINED,
				                               SDL_WINDOWPOS_UNDEFINED,
				                               w,
				                               h,
				                               SDL_WINDOW_RESIZABLE);

	if (app->__private.MainWindow == NULL) {
		printf("SDL: %s\n", SDL_GetError());
		tmpError = -1;
	}
	

	app->__private.SpriteRawList = clist_create(_DHEX_APP_SPRITE_RAW_LIST_STRUCT);
	((clist*)(app->__private.SpriteRawList))->free_element_callback = &__local_FREE_DHEX_APP_SPRITE_RAW_LIST_STRUCT;
	
	app->__private.SpritePalettizedList = clist_create(_DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT);
	((clist*)(app->__private.SpritePalettizedList))->free_element_callback = &__local_FREE_DHEX_APP_SPRITE_PALETTIZED_LIST_STRUCT;
	
	app->__private.SfxList = clist_create(_DHEX_APP_SFX_LIST_STRUCT);
	((clist*)(app->__private.SfxList))->free_element_callback = &__local_FREE_DHEX_APP_SFX_LIST_STRUCT;
	
	app->__private.BXG_List = clist_create(_DHEX_APP_BXG_LIST_STRUCT);
	((clist*)(app->__private.BXG_List))->free_element_callback = &__local_FREE_DHEX_APP_BXG_LIST_STRUCT;
	
	app->__private.MainRenderer = SDL_CreateRenderer(app->__private.MainWindow,-1,SDL_RENDERER_SOFTWARE);
	SDL_RenderSetLogicalSize(app->__private.MainRenderer, w, h);
	SDL_SetRenderDrawBlendMode(app->__private.MainRenderer,SDL_BLENDMODE_BLEND);
	
	int _tmpWorkDirLen = strlen(SDL_GetBasePath());
	char* _tmpWorkDir = malloc(_tmpWorkDirLen+6); //5 bytes for (data\) plus null terminator
	strcpy(_tmpWorkDir, (const char*)SDL_GetBasePath());
	strcat(_tmpWorkDir, "data\\");
	
	app->WorkingDirectory = (const char*)_tmpWorkDir;
	app->Draw.Enabled = true;
	
	if (ErrorState != NULL)
		(*ErrorState) = tmpError;

	app->__private.ScreenWidth = w;
	app->__private.ScreenHeight = h;
	app->Quit = false;
	
	DHEX_SetDefaultKeyConfig(app, 1);
	DHEX_SetDefaultKeyConfig(app, 2);

	for (uint8_t L = 0; L < _DHEX_DRAW_LAYER_MAX; L++)
		app->__private.DrawLayers[L] = clist_create(_DHEX_DrawCall);

	app->Draw.Layer = DHEX_LAYER_BACKGROUND0;

	if (tmpError < 0) {
		DHEX_FreeApp(app);
		app = NULL;
	}

	return app;
	
}

void DHEX_DrawLayer(DHEX_App* app, uint8_t Layer) {
	if (app == NULL) return;
	app->Draw.Layer = Layer;
}



void DHEX_FreeApp(DHEX_App* app) {
	if (app == NULL) return;
	
	clist_free(app->__private.SpriteRawList);
	clist_free(app->__private.SpritePalettizedList);
	clist_free(app->__private.SfxList);
	clist_free(app->__private.BXG_List);
	SDL_DestroyRenderer((SDL_Renderer*)(app->__private.MainRenderer));
	SDL_DestroyWindow((SDL_Window*)app->__private.MainWindow);
	free((void*)app->WorkingDirectory);
	
	IMG_Quit();
	Mix_Quit();
	SDL_Quit();
	
	return;
}


static void __local_DHEX_GetMouseCoords(DHEX_App* app, int* x, int* y) {
	
	int window_w, window_h;
	SDL_GetMouseState(x,y);
	SDL_GetWindowSize(app->__private.MainWindow, &window_w, &window_h);
	const double window_aspect_ratio = (double)window_w/(double)window_h;
	const double logic_aspect_ratio  = (double)app->__private.ScreenWidth/(double)app->__private.ScreenHeight;

	if (window_aspect_ratio >= logic_aspect_ratio) { //longer window horizontally, thus the vertical side is fit to the screen's height
		*y = (int)(app->__private.ScreenHeight*((double)*y/(double)window_h));
		double screen_scale = (double)window_h/(double)app->__private.ScreenHeight;
		int black_border = (int)floor((double)window_w - (screen_scale*(double)app->__private.ScreenWidth))/2;
		*x = (int)fmax(*x - black_border, 0);
		*x = (int)fmin(*x,window_w - 2*black_border); //clamp mouse in between non blackbar space. Mouse 0 starts from where the screen is drawn
		*x = (int)(app->__private.ScreenWidth*((double)*x/(double)(window_w - 2*black_border)));				
	}
	else {
		*x = (int)(app->__private.ScreenWidth*((double)*x/(double)window_w));
		double screen_scale = (double)window_w/(double)app->__private.ScreenWidth;
		int black_border = (int)floor((double)window_h - (screen_scale*(double)app->__private.ScreenHeight))/2;
		*y = (int)fmax(*y - black_border, 0);
		*y = (int)fmin(*y, window_h - 2*black_border); //clamp mouse in between non blackbar space. Mouse 0 starts from where the screen is drawn
		*y = (int)(app->__private.ScreenHeight*((double)*y/(double)(window_h - 2*black_border)));				
	}
	
	
}



void DHEX_SetDrawColor(DHEX_App* app, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
	if (app == NULL) return;
	SDL_SetRenderDrawColor(app->__private.MainRenderer, r,g,b,a);
	
}


void DHEX_ClearScreen(DHEX_App* app, uint8_t r, uint8_t g, uint8_t b) {
	if (app == NULL) return;
	SDL_SetRenderDrawColor(app->__private.MainRenderer, r,g,b,255);
	SDL_RenderClear(app->__private.MainRenderer);
}

void DHEX_Delay(uint16_t ms) {
	SDL_Delay(ms);
}

uint32_t DHEX_GetTicks(void) {
	return SDL_GetTicks();
}

static bool __local_DHEX_FindVirtualPort(DHEX_App* app, SDL_JoystickID joyID, uint8_t* port) {	
    for (uint8_t i = 0; i < __DHEX_MAX_DEVICES; i++) {
        if (app->__private.Input.DevicePorts[i] == joyID) {
            *port = i;
            return true;
        }
    }
    return false;

}


static void __local_DHEX_UpdateDetectedDevices(DHEX_App* app) {
	if (app == NULL) return;
	
	memset((void*)app->__private.Input.DevicePorts, _DHEX_JOYSTICK_ID_NULL, sizeof(SDL_JoystickID)*__DHEX_MAX_DEVICES);

    int devices = SDL_NumJoysticks();
    if (devices < 0) {
        printf("SDL: %s\n", SDL_GetError() );
        return;
    }

    printf("Detected %d joystick(s)\n", devices);

    if (devices == 0) return;
	
    for (uint8_t j = 0; j < (int)fmin(devices, __DHEX_MAX_DEVICES); j++) {
        SDL_Joystick* Joy = SDL_JoystickOpen(j);
		app->__private.Input.DevicePorts[j] = SDL_JoystickInstanceID(Joy);
    }


}


static bool __local_DHEX_CheckVirtualKeyHeld(DHEX_App* app, _DHEX_VirtualKey vk) {
    switch(vk.HardwareType) {

       case _DHEX_HARDWARE_TYPE_KEYBOARD:
       {
            const uint8_t* KeyboardState = SDL_GetKeyboardState(NULL);
            return (KeyboardState[vk.Device.Keyboard.KeyNumber] == 1);
       }
       break;

       case _DHEX_HARDWARE_TYPE_JOYBUTTON:

            if (app->__private.Input.DevicePorts[vk.Device.JoyButton.DevicePort] == _DHEX_NULL_DEVICE_PORT) break;
            return (SDL_JoystickGetButton(
					SDL_JoystickFromInstanceID(
					app->__private.Input.DevicePorts[vk.Device.JoyButton.DevicePort]), vk.Device.JoyButton.ButtonNumber) == 1);

        break;

        case _DHEX_HARDWARE_TYPE_JOYAXIS:
        {
            if (app->__private.Input.DevicePorts[vk.Device.JoyButton.DevicePort] == _DHEX_NULL_DEVICE_PORT) break;
            int16_t AxisValue = SDL_JoystickGetAxis(
								SDL_JoystickFromInstanceID(app->__private.Input.DevicePorts[vk.Device.JoyButton.DevicePort]),  vk.Device.JoyAxis.AxisNumber);
            bool AxisDir = false;
            if (AxisValue > 0) AxisDir = true;

            return ((abs(AxisValue) > _DHEX_JOYSTICK_AXIS_THRESHOLD) && (AxisDir == vk.Device.JoyAxis.AxisDirection));

        }
        break;

        case _DHEX_HARDWARE_TYPE_JOYHAT:
        {

            if (app->__private.Input.DevicePorts[vk.Device.JoyButton.DevicePort] == _DHEX_NULL_DEVICE_PORT) break;
            uint8_t Hat = SDL_JoystickGetHat(
						  SDL_JoystickFromInstanceID(app->__private.Input.DevicePorts[vk.Device.JoyButton.DevicePort]), vk.Device.JoyHat.HatIndex);
						  
			bool _Good = false;
			 
			if ((Hat == SDL_HAT_RIGHTUP || Hat == SDL_HAT_RIGHTDOWN || Hat == SDL_HAT_RIGHT)
				&& vk.Device.JoyHat.HatDirection == SDL_HAT_RIGHT) 
			   _Good = true;

			if ((Hat == SDL_HAT_LEFTUP || Hat == SDL_HAT_LEFTDOWN || Hat == SDL_HAT_LEFT)
				&& vk.Device.JoyHat.HatDirection == SDL_HAT_LEFT) 
			   _Good = true;
			   
			if ((Hat == SDL_HAT_LEFTUP || Hat == SDL_HAT_UP || Hat == SDL_HAT_RIGHTUP)
				&& vk.Device.JoyHat.HatDirection == SDL_HAT_UP) 
			   _Good = true;
			   
			if ((Hat == SDL_HAT_LEFTDOWN || Hat == SDL_HAT_RIGHTDOWN || Hat == SDL_HAT_DOWN)
				&& vk.Device.JoyHat.HatDirection == SDL_HAT_DOWN) 
			   _Good = true;
			
		
            return (_Good);
        }
        break;
    }

    return false;
}


static uint16_t __local_DHEX_GetButtonMask[] = {
	DHEX_BUTTON_LP,
	DHEX_BUTTON_HP,
	DHEX_BUTTON_SP,
	DHEX_BUTTON_LK,
	DHEX_BUTTON_HK,
	DHEX_BUTTON_DS,
	DHEX_BUTTON_START,
	DHEX_DIR_R,
	DHEX_DIR_L,
	DHEX_DIR_U,
	DHEX_DIR_D,
	DHEX_MACRO_1,
	DHEX_MACRO_2
};


void DHEX_UpdateState(DHEX_App* app) {
	
	if (app == NULL) return;
	
    app->__private.Input.Mouse.Pressed = false;
    app->__private.Input.AnyVk = false;
    SDL_Event e;
	
	//[frame, 0 = current, 1 = previous][player 0 = p1, 1 = p2]	
	app->__private.Input.ButtonMaskState[1][0] = app->__private.Input.ButtonMaskState[0][0]; 
	app->__private.Input.ButtonMaskState[1][1] = app->__private.Input.ButtonMaskState[0][1];
	
	while (SDL_PollEvent(&e)) {
		
			if (e.type == SDL_JOYDEVICEADDED || e.type == SDL_JOYDEVICEREMOVED) {
				__local_DHEX_UpdateDetectedDevices(app);
			}

			switch(e.type) {
				
				case SDL_QUIT:
					app->Quit = true;
				break;

				case SDL_MOUSEBUTTONDOWN:
					if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) {
						app->__private.Input.Mouse.Held = true;
						app->__private.Input.Mouse.Pressed = true;
					}
				break;

				case SDL_MOUSEBUTTONUP:
					if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) {
						app->__private.Input.Mouse.Held = false;
					}
				break;

				case SDL_KEYDOWN:
					app->__private.Input.LastVk = (_DHEX_VirtualKey){_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {e.key.keysym.scancode} };
					app->__private.Input.AnyVk = true;
				break;

				case SDL_JOYBUTTONDOWN:
				{
					uint8_t port;
					if (__local_DHEX_FindVirtualPort(app, e.jbutton.which, &port)) {
						app->__private.Input.LastVk = (_DHEX_VirtualKey){_DHEX_HARDWARE_TYPE_JOYBUTTON, .Device.JoyButton = {e.jbutton.button, port} };
						app->__private.Input.AnyVk = true;
					}
				}
				break;

				case SDL_JOYAXISMOTION:
				{
					uint8_t port;
					if ((__local_DHEX_FindVirtualPort(app, e.jaxis.which, &port)) && abs(e.jaxis.value) > _DHEX_JOYSTICK_AXIS_THRESHOLD) {
						bool axis_dir = false;
						if (e.jaxis.value > 0) axis_dir = true;
						app->__private.Input.LastVk = (_DHEX_VirtualKey){_DHEX_HARDWARE_TYPE_JOYAXIS, .Device.JoyAxis = { e.jaxis.axis, port, axis_dir}  };
						app->__private.Input.AnyVk = true;
					}
				}
				break;

				case SDL_JOYHATMOTION:
				{
					uint8_t port;
					if ( (__local_DHEX_FindVirtualPort(app, e.jhat.which, &port)) && (e.jhat.value != SDL_HAT_CENTERED) ) {
						app->__private.Input.LastVk = (_DHEX_VirtualKey){_DHEX_HARDWARE_TYPE_JOYHAT, .Device.JoyHat = {e.jhat.hat, port, e.jhat.value}  };
						app->__private.Input.AnyVk = true;
					}
				}
				break;
				
				
			}
		
	}
	
	
	__local_DHEX_GetMouseCoords(app, &app->__private.Input.Mouse.x, &app->__private.Input.Mouse.y);
	
	app->__private.Input.ButtonMaskState[0][0] = 0; 
	app->__private.Input.ButtonMaskState[0][1] = 0; 

    for (uint8_t player = 0; player <= 1; player++)
    for (uint8_t i = 0; i < __DHEX_MAX_BUTTONS; i++) {
        if (__local_DHEX_CheckVirtualKeyHeld(app, app->__private.Input.KeyConfig[i][player]))
        app->__private.Input.ButtonMaskState[0][player] = app->__private.Input.ButtonMaskState[0][player] | __local_DHEX_GetButtonMask[i];
    }
	
}


bool DHEX_InputCheck(DHEX_App* app, uint16_t ButtonMask, uint8_t player) {
	if (app == NULL) return false;
    if (player < 1 || player > 2) return false;
    if ((app->__private.Input.ButtonMaskState[0][player-1] & ButtonMask) == ButtonMask) return true;
	return false;
}

bool DHEX_InputCheckPressed(DHEX_App* app, uint16_t ButtonMask, uint8_t player) {
	if (app == NULL) return false;
    if (player < 1 || player > 2) return false;
    if (DHEX_InputCheck(app, ButtonMask, player) && !((app->__private.Input.ButtonMaskState[1][player-1] & ButtonMask) == ButtonMask))
    return true;
    return false;
}

bool DHEX_InputCheckReleased(DHEX_App* app, uint16_t ButtonMask, uint8_t player) {
	if (app == NULL) return false;
    if (player < 1 || player > 2) return false;
    if (!DHEX_InputCheck(app, ButtonMask, player) && ((app->__private.Input.ButtonMaskState[1][player-1] & ButtonMask) == ButtonMask))
    return true;
    return false;
}


bool DHEX_IsRunning(DHEX_App* app) {
	if (app == NULL) return false;
	return !app->Quit;	
}

void DHEX_GetMouseState(DHEX_App* app, int* x, int* y, bool* held, bool* pressed) {
	if (app == NULL) return;
	
	if (x != NULL)
	*x 		 = app->__private.Input.Mouse.x;

	if (y != NULL)
	*y 		 = app->__private.Input.Mouse.y;

	if (held != NULL)
	*held 	 = app->__private.Input.Mouse.Held;

	if (pressed != NULL)
	*pressed = app->__private.Input.Mouse.Pressed;
	
	return;
	
}


void DHEX_SetDefaultKeyConfig(DHEX_App* app, uint8_t player) {
	if (app == NULL) return;
	switch(player) {
		case 1:
			app->__private.Input.KeyConfig[__DHEX_BUTTON_LP_INDEX][0] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_A} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_HP_INDEX][0] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_S} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_SP_INDEX][0] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_D} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_LK_INDEX][0] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_Z} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_HK_INDEX][0] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_X} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_DS_INDEX][0] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_C} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_START_INDEX][0] = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_1} };
			app->__private.Input.KeyConfig[__DHEX_DIR_R_INDEX][0] 		 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_RIGHT} };
			app->__private.Input.KeyConfig[__DHEX_DIR_L_INDEX][0] 		 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_LEFT} };
			app->__private.Input.KeyConfig[__DHEX_DIR_U_INDEX][0] 		 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_UP} };
			app->__private.Input.KeyConfig[__DHEX_DIR_D_INDEX][0] 		 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_DOWN} };
			app->__private.Input.KeyConfig[__DHEX_MACRO_1_INDEX][0] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_Q} };
			app->__private.Input.KeyConfig[__DHEX_MACRO_2_INDEX][0] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_W} };
		break;
		
		case 2:
			app->__private.Input.KeyConfig[__DHEX_BUTTON_LP_INDEX][1] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_U} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_HP_INDEX][1] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_I} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_SP_INDEX][1] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_O} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_LK_INDEX][1] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_J} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_HK_INDEX][1] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_K} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_DS_INDEX][1] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_L} };
			app->__private.Input.KeyConfig[__DHEX_BUTTON_START_INDEX][1] = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_2} };
			app->__private.Input.KeyConfig[__DHEX_DIR_R_INDEX][1] 		 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_KP_6} };
			app->__private.Input.KeyConfig[__DHEX_DIR_L_INDEX][1] 		 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_KP_4} };
			app->__private.Input.KeyConfig[__DHEX_DIR_U_INDEX][1] 		 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_KP_8} };
			app->__private.Input.KeyConfig[__DHEX_DIR_D_INDEX][1] 		 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_KP_5} };
			app->__private.Input.KeyConfig[__DHEX_MACRO_1_INDEX][1] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_7} };
			app->__private.Input.KeyConfig[__DHEX_MACRO_2_INDEX][1] 	 = (_DHEX_VirtualKey) {_DHEX_HARDWARE_TYPE_KEYBOARD, .Device.Keyboard = {SDL_SCANCODE_8} };
		break;
		
		
	}
	
	
}


void DHEX_DrawSprite(DHEX_App* app, void* spr, int x, int y, int xOffset, int yOffset, double xscale, double yscale, double angle) {
	if (app == NULL) return;	
	__CHECK_DRAW_ENABLED
	if (app->Draw.Layer >= _DHEX_DRAW_LAYER_MAX) return; 
	
	clist_push( (clist*)app->__private.DrawLayers[app->Draw.Layer],
	&(_DHEX_DrawCall){.Type = _DHEX_DRAW_CALL_SPRITE,
	  .Call.DrawSprite = {
		spr,
		x, y,
		xOffset, yOffset,
		xscale, yscale,
		angle}  });
	
}


void DHEX_FillRect(DHEX_App* app, int x, int y, int w, int h, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
	if (app == NULL) return;
	__CHECK_DRAW_ENABLED	
	if (app->Draw.Layer >= _DHEX_DRAW_LAYER_MAX) return; 
	
	clist_push( (clist*)app->__private.DrawLayers[app->Draw.Layer],
	&(_DHEX_DrawCall){.Type = _DHEX_DRAW_CALL_FILLRECT,
	  .Call.DrawFillRect = {
		x, y,
		w, h,
		r, g, b, a}  });
	
	
}


void DHEX_DrawRect(DHEX_App* app, int x, int y, int w, int h, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
	if (app == NULL) return;	
	__CHECK_DRAW_ENABLED
	if (app->Draw.Layer >= _DHEX_DRAW_LAYER_MAX) return; 
	
	clist_push( (clist*)app->__private.DrawLayers[app->Draw.Layer],
	&(_DHEX_DrawCall){.Type = _DHEX_DRAW_CALL_RECT,
	  .Call.DrawRect = {
		x, y,
		w, h,
		r, g, b, a}  });
	
	
}


void DHEX_DrawText(DHEX_App* app, void* font, int x, int y, double xscale, double yscale, int hcenter, int vcenter, const char* string) {
	if (app == NULL) return;	
	__CHECK_DRAW_ENABLED
	if (app->Draw.Layer >= _DHEX_DRAW_LAYER_MAX) return; 
	
	char* str = malloc(strlen(string)+1);
	strcpy(str, string);
	strcat(str, "");
	
	clist_push( (clist*)app->__private.DrawLayers[app->Draw.Layer],
	&(_DHEX_DrawCall){.Type = _DHEX_DRAW_CALL_TEXT,
	  .Call.DrawText = {
		font,
		x, y,
		xscale, yscale,
		hcenter, vcenter,
		str}  });
	
}

void DHEX_UpdateScreen(DHEX_App* app) {
	if (app == NULL) return;
	for (uint8_t L = 0; L < _DHEX_DRAW_LAYER_MAX; L++) {
		clist_iterator it = clist_start(app->__private.DrawLayers[L]);
		while (it != NULL) {
			clist_iterator next = clist_next(it);
			_DHEX_DrawCall dc = clist_get(it, _DHEX_DrawCall);
			switch(dc.Type) {
				
				case _DHEX_DRAW_CALL_SPRITE:
					_DHEX_DrawSprite(app,
									(DHEX_Sprite*)dc.Call.DrawSprite.Sprite,
									dc.Call.DrawSprite.x,
									dc.Call.DrawSprite.y,
									dc.Call.DrawSprite.xOffset,
									dc.Call.DrawSprite.yOffset,
									dc.Call.DrawSprite.xscale,
									dc.Call.DrawSprite.yscale,
									dc.Call.DrawSprite.angle);
				break;
				
				case _DHEX_DRAW_CALL_FILLRECT:
					DHEX_SetDrawColor(app,
									dc.Call.DrawFillRect.r,
									dc.Call.DrawFillRect.g,
									dc.Call.DrawFillRect.b,
									dc.Call.DrawFillRect.a);
									
					_DHEX_FillRect(app,
								dc.Call.DrawFillRect.x,
								dc.Call.DrawFillRect.y,
								dc.Call.DrawFillRect.w,
								dc.Call.DrawFillRect.h);
				
				break;
				
				
				case _DHEX_DRAW_CALL_RECT:
					DHEX_SetDrawColor(app,
									dc.Call.DrawRect.r,
									dc.Call.DrawRect.g,
									dc.Call.DrawRect.b,
									dc.Call.DrawRect.a);
									
					SDL_RenderDrawRect(app->__private.MainRenderer,
								 &(const SDL_Rect){
								dc.Call.DrawRect.x,
								dc.Call.DrawRect.y,
								dc.Call.DrawRect.w,
								dc.Call.DrawRect.h 
								}
					);
				
				break;
				
				case _DHEX_DRAW_CALL_TEXT:
					_DHEX_DirectDrawFont(app,
										(DHEX_Font*)dc.Call.DrawText.font,
										dc.Call.DrawText.x,
										dc.Call.DrawText.y,
										dc.Call.DrawText.xscale,
										dc.Call.DrawText.yscale,
										(_DHEX_TEXT_HCENTER)dc.Call.DrawText.hcenter,
										(_DHEX_TEXT_VCENTER)dc.Call.DrawText.vcenter,
										dc.Call.DrawText.string);
										
										
					free((void*)dc.Call.DrawText.string);
				
				break;
				
			}
			
			clist_delete(it);
			it = next;
		}
	}
	
	SDL_RenderPresent(app->__private.MainRenderer);
	
}



void DHEX_SetWindowSize(DHEX_App* app, int w, int h) {
	if (app == NULL) return;
	SDL_SetWindowSize(app->__private.MainWindow, w, h);
}

void DHEX_CenterWindow(DHEX_App* app) {
	if (app == NULL) return;
	SDL_SetWindowPosition(app->__private.MainWindow, SDL_WINDOWPOS_CENTERED,  SDL_WINDOWPOS_CENTERED);
}


void DHEX_SaveKeyConfig(DHEX_App* app, const char* filename) {
	
	FILE* file = fopen(filename, "wb");

	if (file) {
		
		
		
		for (uint8_t pside = 0; pside < 2; pside++)
		for (uint8_t b = 0; b < __DHEX_MAX_BUTTONS; b++) {
			fwrite((const void*) &(app->__private.Input.KeyConfig[b][pside]), sizeof(_DHEX_VirtualKey), 1, file);
		}
		
		
	}
	
	fclose(file);
}

void DHEX_LoadKeyConfig(DHEX_App* app, const char* filename) {
	
	FILE* file = fopen(filename, "rb");

	
	if (file) {
		
		for (uint8_t pside = 0; pside < 2; pside++)
		for (uint8_t b = 0; b < __DHEX_MAX_BUTTONS; b++) {
			fread((void*) &(app->__private.Input.KeyConfig[b][pside]), sizeof(_DHEX_VirtualKey), 1, file);
		}
		

	}
	fclose(file);
}

void DHEX_DrawEnable(DHEX_App* app, bool enable) {
	app->Draw.Enabled = enable;
}