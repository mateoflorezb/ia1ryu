#ifndef _IAR_OBJ_INCLUDED
#define _IAR_OBJ_INCLUDED

#include "DHEX.h"
#include "IAR_Ryu.h"
#include "IAR_gamestate.h"
#include "IAR_general.h"

#define IAR_PLAYER_INPUT_HIST_SIZE 15
#define IAR_MAX_JUGGLE_COUNTER 4
#define HADOKEN_FRAMES 8

#define IAR_PLAYER_KNOCKDOWN_TIME 28

struct _IAR_Obj;
typedef struct _IAR_Obj IAR_Obj;

struct _IAR_GameState;
typedef struct _IAR_GameState IAR_GameState;

enum _IAR_OBJ_IDS {
	_IAR_OBJ_DEFAULT = 0,
	IAR_OBJ_RYU = 1,
	IAR_OBJ_TEMPVISUAL,
	IAR_OBJ_PROJECTILE
};

enum _IAR_OBJ_GROUPS {
	IAR_GROUP_NONE = 0,
	IAR_GROUP_CHAR = 1
	
};

enum _IAR_INPUT_HIST_MASKS {
	_IAR_INPUT_HIST_D = 0b00000001,
	_IAR_INPUT_HIST_U = 0b00000010,
	_IAR_INPUT_HIST_L = 0b00000100,
	_IAR_INPUT_HIST_R = 0b00001000
	
};

enum _IAR_BUFFERS {
	IAR_BUFFER_LIGHT   = 0b00000001,
	IAR_BUFFER_NORMAL  = 0b00000010,
	IAR_BUFFER_GRAB    = 0b00000100,
	IAR_BUFFER_SPECIAL = 0b00001000,
	IAR_BUFFER_SUPER   = 0b00010000
	
};

enum __IAR_PLAYER_DEFENSE_FLAGS {
	IAR_DEFENSE_CANBLOCK   = 0b00000001,
	IAR_DEFENSE_INVINCIBLE = 0b00000010,
	IAR_DEFENSE_THROW_INV  = 0b00000100,
	IAR_DEFENSE_ANTI_AIR   = 0b00001000
};


enum __IAR_PLAYER_ATTACK_FLAGS {
	IAR_ATTACK_CANBLOCK_HIGH = 0b00000001,
	IAR_ATTACK_CANBLOCK_LOW  = 0b00000010

};

enum _IAR_HITLEVELS {
	IAR_HITLEVEL_LIGHT   = 0,
	IAR_HITLEVEL_MEDIUM  = 1,
	IAR_HITLEVEL_HARD    = 2,
	IAR_HITLEVEL_SPECIAL = 3,
	IAR_HITLEVEL_SUPER 	 = 4
};

enum _IAR_PLAYER_INPUT_POLICIES {
	IAR_PLAYER_INPUT_POLICY_HARDWARE = 0,
	IAR_PLAYER_INPUT_POLICY_CPU
};

typedef struct {
	DHEX_Sprite* img;
	int xcenter;
	int ycenter;
} IAR_CenteredSprite;

typedef struct {
	DHEX_FrameBoxes* bxg; //Box gen file with hit/hurtboxes and their offset
	DHEX_Sprite* spr;
	int16_t x_offset;
	int16_t y_offset; //offset relative to the sprite's origin
	
} IAR_Frame;

typedef struct {
	uint8_t flaggedInputs; //Format: 0b0000 RLUD
	uint8_t deltaT; //frames elapsed since previous input hist
	
} InputHist; //hist aka history

struct _IAR_Obj {
	void (*pre_step)(IAR_Obj *self);
	void (*step)(IAR_Obj *self);
	void (*post_step)(IAR_Obj *self);
	void (*destroy)(IAR_Obj *self);
	void (*draw)(IAR_Obj *self);
	IAR_Obj* (*GetCopy)(IAR_Obj* self);
	
	IAR_GameState* gs;
	
	int ClassID;
	int ClassGroup;
	
	uint32_t InstanceID;
	
	bool DestroyFlag; //if flagged as to be destroyed, step wont be executed. Flagged objects are freed at the end of the frame
	
	union {
	
		struct {
			fixed x;
			fixed y;
			fixed vspeed;
			fixed hspeed;
			
			IAR_Frame CurrentFrame;
			IAR_Frame* CurrentFrameSet; //a frame set is just an array of IAR frames
			
			fixed FrameIndex; //like GMS image_index but refers to an IAR_Frame
			fixed FrameSpeed; //like GMS image_speed but refers to FrameIndex
			uint16_t CurrentMaxFrame; //so if currently idling and idle has 6 frames, this would be 6
			
			int state; //"crMP", "forward jump", etc
			
			uint16_t CurrentFrameInputs; //what buttons are held currently. Uses DHEX's masks (eg. DHEX_BUTTON_LP)
			uint16_t CurrentFrameInputsPressed;
			uint16_t CurrentFrameInputsReleased;
			
			int health;
			int MAX_HEALTH;
			
			DHEX_Sprite* GroundShadow;
			
			uint32_t DefenseScore; //increases on each succesful defensive action
			
			int8_t PreJump; //frames left, acts at 0, stops at -1
			int8_t JumpDir; //-1, 0 or 1. xdraw independent, it just means absolute speed
			
			bool inAir;
			//bool CanMove;
			uint8_t DefenseFlags; //0000 ATIB
			//B --> CanBlock
			//I --> Incinvible
			//T --> Throw inmune
			//A --> Air attack inmune
			
			uint8_t LandingFrames; //frames left. Stops at 0
			
			bool AnimationLoops; //set to true if after reaching the end the animation jumps back to the last frame
								//like idk, something like idling. Set to false if the animation just gets stuck in the last frame until
								//you swap it.
								
			
			int8_t xdir; //relative direction of the target (enemy's x coord)
			int8_t xdraw; //direction char is looking at
			
			uint16_t PushBoxW; //width  of half the pushbox
			uint16_t PushBoxH; //height the pushbox
			//pushbox is always centered at PushBoxW, PushBoxH, and located at player's x,y
			
			uint8_t GRAB_RANGE; //max distance between pushboxes that still allow for grabbing 
			
			uint16_t Juggled; //frames left. at 0 Ryu will go back into air jump animation 
			//(aka air reset). Set to a really high value for this to be just a typical juggle state
			bool AirReset; //wheter current state is an air reset
			
			uint8_t JumpCounter; //n frames since jump started
			
			uint8_t TurnAround; //frames left of turning animation. Stops at 0
			
			uint8_t MAX_TURN_AROUND;
			
			uint8_t CrouchPhase; //this is a bit of a weird one
			//(assuming the char has 3 crouch frames)
			//if standing, this var sits at 3. When going from standing to crouch this acts
			//as a frames left, stopping at 0. When going from crouch to standing, this process
			//is inversed, so we add until we reach 3 again. Basically it indicates the state of 
			//the crouching animation
			//(2 -> first to crouch frame, 1 -> second to crouch frame, 0 -> last crouching frame and the one it sits on)
			
			uint8_t MAX_CROUCH_PHASE;
			
			uint8_t CanTech; //frames left of being allowed to tech
			
			uint16_t spState; //frames left, stops at 0. An special state means ryu can't block or do other actions
			//(or maybe he can but it's specific)
			uint16_t CurrentMaxSpState;
			
			uint16_t invisible; //frames left of ignoring drawing
			uint16_t CollisionLess; //frames left of ignoring pushboxes
			
			int LastAction; //last outputed special action "stLP" "crMK"
			
			char InputPolicy; //reading inputs from hardware or CPU
			InputHist InputHistList[IAR_PLAYER_INPUT_HIST_SIZE]; //only stores dirs.
			//1390 am
			uint8_t LastHistElapsed; //frames elapsed since last input hist entry
			
			bool CanOutputBuffer; //if in the current frame we're allowed 
			int BufferedAction; //"stMP", "crMK"
			uint8_t BufferedSpecialLevel; //1, 2 or 3. aka. L M or H version
			uint8_t AllowedBuffers; //flag mask for what kind of attacks are allowed to be buffered
			//light normals -> normals -> grab -> specials -> super		
			
			uint8_t ProxyBlock; //frames left of proxy blocking
			bool ApplyProxyBlock; //wheter in this frame player can inflict proxy block to their opponent
			int ApplyProxyBlockDistance; //max distance at which it can trigger
			
			//------
			IAR_Frame* GutStunFramesRef;
			int GutStunStateRef;
			int GutStunMaxFrameRef;
			
			IAR_Frame* HeadStunFramesRef;
			int HeadStunStateRef;
			int HeadStunMaxFrameRef;
			
			IAR_Frame* CrStunFramesRef;
			int CrStunStateRef;
			int CrStunMaxFrameRef;
			
			IAR_Frame* StBlockFramesRef;
			int StBlockStateRef;
			int StBlockMaxFrameRef;
			
			IAR_Frame* CrBlockFramesRef;
			int CrBlockStateRef;
			int CrBlockMaxFrameRef;
			
			IAR_Frame* JuggledFramesRef;
			int JuggledStateRef;
			int JuggledMaxFrameRef;
			
			int KnockdownStateRef;
			int WakeupStateRef;
			int WakeupTime;
			
			int WhiffGrabStateRef;
			int ThrowForwardStateRef;
			int ThrowBackStateRef;
			
			//------
			
			
			uint8_t pDepth; //layer used for player spr
			float spFriction; //for slowing down pushback
			uint16_t StopFrames; //frames left, stops at 0. Hitstop (on dealt or received hit)	
			bool LastAttackLanded; //wheter or not the last attack done registered a hit/block
			bool CounterPushback; //wheter eaten pushblock by the corner will push the opponent back to compensate
			uint8_t MultiHitDelay; //frames left. Small counter for avoiding same active frame multi hitting
			bool AirAction; //If false, air action has already been used
			bool AirAttackSlowdown; //if false, this hasn't been applied yet
			//air attack slowdown is a propety of air attacks. Vspeed will be shortened by some factor
			//after an air attack succesfully hits
			bool ApplyGrav; //if false grav wont be applied
			
			/////Current sp Action attributes ////////---------------------------------------
			
			uint16_t AppliedStopFrames; //how much hitstop applied with the current action
			uint16_t HitLevel; //light, medium, hard, special
			int16_t Spark_xOffset;
			int16_t Spark_yOffset;
			int8_t HitStunType; //gut or head
			uint16_t AppliedOnHit;
			uint16_t AppliedOnBlock;
			uint8_t AttackHits; //number of hits left that this attack can still deal
			//hit and blockstun is now just considered an sp state
			//uint16_t StunFrames; //hit/blockstun left. Stops at 0
			
			int16_t AppliedPushback; //on int units, will get converted to fixed later on
			int16_t AppliedJuggleHs; //same
			int16_t AppliedJuggleVs; //same lmao
			bool Launcher; //launches grounded opponents
			uint16_t AppliedJuggleTime;
			
			uint16_t AppliedDamage;
			uint16_t AppliedChip;
			
			
			bool ForceStand;
			uint8_t AttackFlags; //0000 00LH
			//H-> Can be blocked high
			//L-> Can be blocked low
			
			int8_t JP; //juggle points
			
			bool CanKara;
			
			/////////////////////////////////////////-------------------------------------
			
			int8_t JuggleCounter; //number of juggle points still valid to land
			
			
			uint8_t WakeupThrowInv; //5 frames of glory
			
			IAR_CenteredSprite GutStunAtFeet;
			IAR_CenteredSprite GutStunAtChest;
			IAR_CenteredSprite JuggledAtNeck;
			
			//set on instanciation
			uint8_t Pside; //p1 or p2
			uint32_t OpponentID;
			
			
			union {
				struct {
					DHEX_Sprite* images[_IAR_RYU_IMG_MAX];
					IAR_Frame IdleFrames[_IAR_RYU_IDLE_FRAMES];
					IAR_Frame WalkForwardFrames[_IAR_RYU_WALK_FORWARD_FRAMES];
					IAR_Frame WalkBackFrames[_IAR_RYU_WALK_BACK_FRAMES];
					IAR_Frame PreJumpFrame;
					IAR_Frame JumpBackFrames[_IAR_RYU_JUMP_BACK_FRAMES];
					IAR_Frame JumpNeutralFrames[_IAR_RYU_JUMP_NEUTRAL_FRAMES];
					IAR_Frame JumpForwardFrames[_IAR_RYU_JUMP_FORWARD_FRAMES];
					IAR_Frame StandTurnFrames[_IAR_RYU_STAND_TURN_FRAMES];
					IAR_Frame ToCrouchFrames[_IAR_RYU_TO_CROUCH_FRAMES];
					IAR_Frame CrouchTurnFrames[_IAR_RYU_CROUCH_TURN_FRAMES];
					
					IAR_Frame stLP_Frames[_IAR_RYU_ST_LP_FRAMES];
					IAR_Frame stMP_Frames[_IAR_RYU_ST_MP_FRAMES];
					IAR_Frame stHP_Frames[_IAR_RYU_ST_HP_FRAMES];
					
					IAR_Frame crLP_Frames[_IAR_RYU_CR_LP_FRAMES];
					IAR_Frame crMP_Frames[_IAR_RYU_CR_MP_FRAMES];
					IAR_Frame crHP_Frames[_IAR_RYU_CR_HP_FRAMES];
					
					IAR_Frame stLK_Frames[_IAR_RYU_ST_LK_FRAMES];
					IAR_Frame stMK_Frames[_IAR_RYU_ST_MK_FRAMES];
					IAR_Frame stHK_Frames[_IAR_RYU_ST_HK_FRAMES];
					
					IAR_Frame crLK_Frames[_IAR_RYU_CR_LK_FRAMES];
					IAR_Frame crMK_Frames[_IAR_RYU_CR_MK_FRAMES];
					IAR_Frame crHK_Frames[_IAR_RYU_CR_HK_FRAMES];
					
					IAR_Frame airLP_Frames[_IAR_RYU_AIR_LP_FRAMES];
					IAR_Frame airMP_Frames[_IAR_RYU_AIR_MP_FRAMES];
					IAR_Frame airHP_Frames[_IAR_RYU_AIR_HP_FRAMES];
					
					IAR_Frame airLK_Frames[_IAR_RYU_AIR_LK_FRAMES];
					IAR_Frame airMHK_Frames[_IAR_RYU_AIR_MHK_FRAMES];
					
					IAR_Frame fHP_Frames[_IAR_RYU_F_HP_FRAMES];
					
					IAR_Frame HadokenFrames[_IAR_RYU_HADOKEN_FRAMES];
					IAR_Frame ShoryukenFrames[_IAR_RYU_SHORYUKEN_FRAMES];
					IAR_Frame TatsuFrames[_IAR_RYU_TATSU_FRAMES];
					
					IAR_Frame WhiffGrabFrames[_IAR_RYU_WHIFF_GRAB_FRAMES];
					IAR_Frame ThrowForwardFrames[_IAR_RYU_THROW_FORWARD_FRAMES];
					IAR_Frame ThrowBackFrames[_IAR_RYU_THROW_BACK_FRAMES];
					
					IAR_Frame GutStunFrames[_IAR_RYU_GUTSTUN_FRAMES];
					IAR_Frame HeadStunFrames[_IAR_RYU_HEADSTUN_FRAMES];
					IAR_Frame CrStunFrames[_IAR_RYU_CR_STUN_FRAMES];
					IAR_Frame StBlockFrames[_IAR_RYU_ST_BLOCK_FRAMES];
					IAR_Frame CrBlockFrames[_IAR_RYU_CR_BLOCK_FRAMES];
					IAR_Frame JuggledFrames[_IAR_RYU_JUGGLED_FRAMES];
					IAR_Frame KnockdownFrames[_IAR_RYU_KNOCKDOWN_FRAMES];
					IAR_Frame WakeupFrames[_IAR_RYU_WAKEUP_FRAMES];
					
					
					uint16_t tempJumpCounter;
					
					uint32_t LastHadokenID;
					
				} Ryu;
				
			} Char;
			
			
		} Player;
		
		
		struct {
			
			uint16_t TTL; //time to live
			bool AnimationLoops;
			
			DHEX_Sprite** images; //unknown size
			uint16_t nImages;
			DHEX_Sprite* CurrentImage;
			
			float xscale;
			float yscale;
			
			fixed x;
			fixed y;
			
			fixed ImageIndex;
			fixed ImageSpeed;
			
			int xOffset;
			int yOffset;
			
			int layer;
			
			
		} TempVisual;
		
		
		struct {
			fixed x;
			fixed y;
			
			fixed hspeed;
			
			int8_t xdraw;
			
			DHEX_Sprite* images[HADOKEN_FRAMES]; //--unknown size-- HAS TO BE KNOWN SIZE FOR OBJ CLONING
			uint16_t nImages;
			DHEX_Sprite* CurrentImage;
			
			fixed ImageIndex;
			fixed ImageSpeed;
			
			int xOffset;
			int yOffset;
			
			uint16_t HitboxW; //half the width
			uint16_t HitboxH; //half the height
			//projectile's hitbox is centered at x,y
			
			uint8_t Pside;
			
			int8_t Hits;
			
			uint16_t StopFrames; //applied to self
			
			uint8_t TTSmall; //time to small. Frames left to get a smaller hitbox. Acts at 1 
			
			/////////////////////////////////////////-------------------------------------
			
			uint16_t AppliedStopFrames; //how much hitstop applied with the current action
			uint16_t HitLevel; //special or super
			uint16_t AppliedOnHit;
			uint16_t AppliedOnBlock;
			
			int16_t AppliedPushback; //on int units, will get converted to fixed later on
			int16_t AppliedJuggleHs; //same
			int16_t AppliedJuggleVs; //same lmao
			uint16_t AppliedJuggleTime;
			
			uint16_t AppliedDamage;
			uint16_t AppliedChip;
			
			bool Launcher;
			
			/////////////////////////////////////////-------------------------------------
			
		} Projectile;
		
		
	} Class;
	
};

enum ___RYU_PALETTES {
	IAR_RYU_PAL_ORIGINAL,
	IAR_RYU_PAL_BLUE,
	IAR_RYU_PAL_GRAY
};

enum __IAR_STUNTYPES {
	IAR_STUNTYPE_GUT,
	IAR_STUNTYPE_HEAD,
	IAR_STUNTYPE_BLOCK
};

bool IAR_PlayerCheckMotion(IAR_Obj* self, const char* notation);
bool IAR_PlayerCheckQC(int8_t direction, IAR_Obj* self); //1 for qcf, -1 for qcb
bool IAR_PlayerCheckDPM(IAR_Obj* self); //dp forward always

void IAR_PlayerApplyStun(int type, uint16_t nframes, IAR_Obj* self);

void IAR_PlayerApplyHit(IAR_Obj* self, bool Blocked, bool Launcher, bool ForceStand, int StunType, uint16_t OnHit, uint16_t OnBlock,
						uint16_t AppliedStopFrames, int16_t AppliedPushback, int16_t AppliedJuggleHs, int16_t AppliedJuggleVs, uint16_t AppliedJuggleTime, int8_t HitDir,
						int8_t JP, uint16_t Damage, uint16_t Chip);

void IAR_PlayerTryGrab(IAR_Obj* self);
void IAR_PlayerTechGrab(IAR_Obj* self);

IAR_Obj* IAR_InstanceFind(uint32_t ID, IAR_GameState* gs); //Null if doesn't exist
IAR_Obj* IAR_InstanceCreate(IAR_Obj* (*constructor)(void* data), void* data, IAR_GameState* gs); //returns created instance address
void IAR_InstanceDestroy(uint32_t ID, IAR_GameState* gs);

void IAR_CollideChars(IAR_Obj* P1, IAR_Obj* P2);
void IAR_DrawHealthBars(IAR_Obj* P1, IAR_Obj* P2);

IAR_Obj* IAR_CreateRyu(void* data);
IAR_Obj* IAR_CreateTempVisual(void* data);
IAR_Obj* IAR_CreateHadoken(void* data);

enum _SPARK_TYPES_ {
	IAR_SPARK_BLOCK1 = 0,
	IAR_SPARK_BLOCK2 = 1,
	IAR_SPARK_BLOCK3 = 2,
	
	IAR_SPARK_HIT1 = 3,
	IAR_SPARK_HIT2 = 4,
	IAR_SPARK_HIT3 = 5,
	IAR_SPARK_HIT4 = 6
	
	
};
void IAR_CreateSpark(int type, int16_t x, int16_t y, float xscale, IAR_GameState* gs); //non fixed, real pixel coords relative to stage origin

void _IAR_PlayerResetStateDescriptors(IAR_Obj* self); //reset state vars to base (prejump, landing frames, etc)

void IAR_PlayerRefreshCurrentBXG(IAR_Obj* self);

void _IAR_PlayerDefaultPreStep(IAR_Obj* self); //things like setting if we're in Air
void _IAR_PlayerDefaultStep(IAR_Obj* self); //things like updating xy with speeds or advancing FrameIndex
void _IAR_PlayerDefaultFetchInput(IAR_Obj* self); //depending on the setting, will read inputs from hardware or from another obj (like the IA handler)
void _IAR_PlayerSetState(IAR_Obj* self, int state); //like set(idle), or set(walking forward)

void _IAR_PlayerDefaultPostChar(IAR_Obj* self); 

void _IAR_PlayerDefaultMovement(IAR_Obj* self, const fixed WalkSpeed,
											   const fixed JumpSpeed,
											   const fixed grav,
											   const fixed JumpHspeed); //walk, jump, etc

#endif