import pandas as pd
from sklearn.tree import DecisionTreeRegressor
import numpy as np

def ZerosDataDict(n):
    
    d = {
        "SelfDistCorner" : [0]*n,
        "EnemyDistCorner" : [0]*n,
        "SelfDistEnemy" : [0]*n,
        "SelfDistFireball" : [0]*n,
        "EnemyDistFireball" : [0]*n,
        "FrameAdvantage" : [0]*n,
        "SelfLife" : [0]*n,
        "EnemyLife" : [0]*n,
        "InCombo" : [0]*n,
	"EnemyInCombo" : [0]*n,
        "EnemyY" : [0]*n,
        "Option" : [0]*n,
        "RW" : [0]*n,
        "RS" : [0]*n
    }
   
    return d
    
def SetRow(d, args, nth):
    d["SelfDistCorner"][nth] = args[0]
    d["EnemyDistCorner"][nth] = args[1]
    d["SelfDistEnemy"][nth] = args[2]
    d["SelfDistFireball"][nth] = args[3]
    d["EnemyDistFireball"][nth] = args[4]
    d["FrameAdvantage"][nth] = args[5]
    d["SelfLife"][nth] = args[6]
    d["EnemyLife"][nth] = args[7]
    d["InCombo"][nth] = args[8]
    d["EnemyInCombo"][nth] = args[9]
    d["EnemyY"][nth] = args[10]
    d["Option"][nth] = args[11]
    d["RW"][nth] = args[12]
    d["RS"][nth] = args[13]

def SaveDataDictAsCSV(d, path):
    tmpDF = pd.DataFrame.from_dict(d) 
    tmpDF.to_csv(path_or_buf=path)

def GetEstimation(rx, args):
    rxInput = [[]]
    for i in range(12):
        rxInput[0].append(args[i])
    v = rx.predict(rxInput)
    return v[0]
