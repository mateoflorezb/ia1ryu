#ifndef _IAR_OPTIONS_H
#define _IAR_OPTIONS_H

#include "IAR_DataGeneration.h"
#include "IAR_gamestate.h"

#define IAR_AI_NEUTRAL_FAR_OPTIONS 6
#define IAR_AI_NEUTRAL_MID_OPTIONS 12
#define IAR_AI_CLOSE_OPTIONS 9
#define IAR_AI_CHECK_CONFIRM_OPTIONS 2
#define IAR_AI_WAKEUP_OPTIONS 6
#define IAR_AI_OKIZEME_OPTIONS 8

typedef struct {
	const uint16_t* RecordList;
	int len; //how long is the list
	int OptionIndex; //for the regressor
	uint16_t ScoreDelay; //how many frames to wait before scoring this option
	
} IAR_InputRecord;

enum _IAR_CPU_AI_POLICIES {
	IAR_CPU_OFF = 0, //Do nothing
	IAR_CPU_RANDOM = 1, //choose proper context (okizeme, neutral, etc), then give the same weight to every option
	IAR_CPU_INSTANT_REACTION = 2
};

typedef struct {
	int Wait;
	int32_t InitialScore;
	IAR_DataRow InitialContext;
	
} _IAR_ScoreQueueNode;

typedef struct {
	void (*step)(void* self, IAR_GameState* gs);
	void (*reset)(void* self, IAR_GameState* gs);
	const IAR_InputRecord* CurrentRecord;
	uint16_t CurrentIndex;
	uint16_t PreviousItem;
	
	char Policy; //data generation or actually playing
	
	int DumpFilenameOffset;
	
	const IAR_InputRecord** OptionGroup; //okizeme, wakeup, neutral active, neutral preventive, etc
	int nOptions;
	
	int8_t Pside; //which player side this thing controls
	
	const IAR_InputRecord* IAR_OptionGroup_NeutralFar[IAR_AI_NEUTRAL_FAR_OPTIONS];
	const IAR_InputRecord* IAR_OptionGroup_NeutralMid[IAR_AI_NEUTRAL_MID_OPTIONS];
	const IAR_InputRecord* IAR_OptionGroup_Close[IAR_AI_CLOSE_OPTIONS];
	const IAR_InputRecord* IAR_OptionGroup_CheckConfirm[IAR_AI_CHECK_CONFIRM_OPTIONS];
	const IAR_InputRecord* IAR_OptionGroup_Wakeup[IAR_AI_WAKEUP_OPTIONS];
	const IAR_InputRecord* IAR_OptionGroup_Okizeme[IAR_AI_OKIZEME_OPTIONS];
	
	bool GotToRecordEnd;
	
	clist* ScoreQueue;
	
	bool ExportData; //set to true if we are dumping data sets
	bool EndgameDump;
	
	int InQueue; //how many scores are waiting to be calculated
	
} IAR_CPU_AI;

IAR_CPU_AI* IAR_Create_CPU_AI(int DumpFilenameOffset, int8_t Pside);

enum IAR_OPTIONS_ALL { //unique ID for each option
	IAR_OPTION_BLOCK_LOW_6_FRAMES = 0,
	IAR_OPTION_CR_MK,
	IAR_OPTION_CR_HK,
	IAR_OPTION_CR_MK_HADO,
	IAR_OPTION_CR_HK_HADO,
	IAR_OPTION_WALK_FORWARD_6_FRAMES,
	IAR_OPTION_WALK_FORWARD_15_FRAMES,
	IAR_OPTION_WALK_BACK_8_FRAMES,
	IAR_OPTION_RANDOM_TATSU,
	IAR_OPTION_HADO_HP,
	IAR_OPTION_HADO_LP,	
	IAR_OPTION_JUMP_FORWARD_HK,
	IAR_OPTION_JUMP_FORWARD_MK,
	IAR_OPTION_ANTI_AIR_J_MP,
	IAR_OPTION_SHORYU_HP,
	IAR_OPTION_CR_HP,
	IAR_OPTION_ST_HP,
	IAR_OPTION_WALK_KARA_GRAB,
	IAR_OPTION_CR_LP,
	IAR_OPTION_CR_MP,
	IAR_OPTION_GRAB,
	IAR_OPTION_HITCONFIRM,
	IAR_OPTION_JUMP_FORWARD_EMPTY,
	IAR_OPTION_ST_MK,
	IAR_OPTION_ST_MP_HADO,
	IAR_OPTION_SHORYU_LP,
	IAR_OPTION_WAKEUP_BLOCK_LONG,
	IAR_OPTION_WAKEUP_BLOCK_LOW_TECH,
	IAR_OPTION_WAKEUP_BLOCK_HIGH_TECH,
	IAR_OPTION_WAKEUP_BLOCK_LOW_CR_LP,
	IAR_OPTION_WAKEUP_SHORYU_LP,
	IAR_OPTION_WAKEUP_SHORYU_HP,
	IAR_OPTION_OKI_CR_LP_GRAB,
	IAR_OPTION_OKI_WALK_FORWARD_GRAB,
	IAR_OPTION_OKI_WALK_FORWARD_CR_MP,
	IAR_OPTION_OKI_BLOCK,
	IAR_OPTION_OKI_FORWARD_HP,
	IAR_OPTION_OKI_CR_LK_ST_MK,
	IAR_OPTION_OKI_CR_LK,
	IAR_OPTION_OKI_IOH_J_LK,
	IAR_OPTION_FAST_HADO,
	IAR_OPTION_POST_JUMP_SHORT_CONFIRM,
	
	IAR_OPTIONS_MAX
	
};

/*
//Option groups?
//neutral and too far ->
jump forward hk,
jump forward empty
LP/HP hado,
walk forward 15 frames,
hold ground 6 frames

//neutral and mid range (mid range aka just a bit further than max cr mk range)
jump crossup,
HP hado,
cr mk hado,
cr hk hado,
air mp anti air, 
random tatsu,
walk forward 6 frames,
block 6 frames, 
walk back 8 frames
hp shoryu
cr hp
st hp

//close range (cr lp connects)
walk forward kara grab
cr lp 
cr mp
grab
cr lk cr lp st lk (go into hitconfirm state aka do nothing or hado cancel)
jump forward (escape corner lol)
st mk 
st mp hado
lp shoryu

//wakeup
block low long
block low tech
block high tech
block low cr lp
LP DP
HP DP

//okizeme -- start acting 10 frames before wakeup frame
cr lp grab 
walk forward kara grab
walk forward cr mp meaty
block
foward hp
cr lk > st mk
cr lk 
instant overhead with forward jump lk


//hitconfirming
do nothing (block)
cancel into hp hado

//post jump
walk forward kara grab
hitconfirm (short)
cr mk hado
st mk
cr lk

*/



#define IAR_OPTION_BLOCK_LOW_6_FRAMES_LENGTH 6
extern const uint16_t _IAR_RecordList_BlockLow6Frames[IAR_OPTION_BLOCK_LOW_6_FRAMES_LENGTH];
extern const IAR_InputRecord IAR_Option_BlockLow6Frames;
#define IAR_OPTION_CR_MK_LENGTH 1
extern const uint16_t _IAR_RecordList_crMK[IAR_OPTION_CR_MK_LENGTH];
extern const IAR_InputRecord IAR_Option_crMK;
#define IAR_OPTION_WALK_FORWARD_6_FRAMES_LENGTH 6
extern const uint16_t _IAR_RecordList_WalkForward6Frames[IAR_OPTION_WALK_FORWARD_6_FRAMES_LENGTH];
extern const IAR_InputRecord IAR_Option_WalkForward6Frames;
#define IAR_OPTION_WALK_FORWARD_15_FRAMES_LENGTH 15
extern const uint16_t _IAR_RecordList_WalkForward15Frames[IAR_OPTION_WALK_FORWARD_15_FRAMES_LENGTH];
extern const IAR_InputRecord IAR_Option_WalkForward15Frames;
#define IAR_OPTION_WALK_BACK_8_FRAMES_LENGTH 8
extern const uint16_t _IAR_RecordList_WalkBack8Frames[IAR_OPTION_WALK_BACK_8_FRAMES_LENGTH];
extern const IAR_InputRecord IAR_Option_WalkBack8Frames;
#define IAR_OPTION_CR_HK_LENGTH 1
extern const uint16_t _IAR_RecordList_crHK[IAR_OPTION_CR_HK_LENGTH];
extern const IAR_InputRecord IAR_Option_crHK;
#define IAR_OPTION_CR_MK_HADO_LENGTH 10
extern const uint16_t _IAR_RecordList_crMK_Hado[IAR_OPTION_CR_MK_HADO_LENGTH];
extern const IAR_InputRecord IAR_Option_crMK_Hado;
#define IAR_OPTION_CR_HK_HADO_LENGTH 11
extern const uint16_t _IAR_RecordList_crHK_Hado[IAR_OPTION_CR_HK_HADO_LENGTH];
extern const IAR_InputRecord IAR_Option_crHK_Hado;
#define IAR_OPTION_RANDOM_TATSU_LENGTH 4
extern const uint16_t _IAR_RecordList_RandomTatsu[IAR_OPTION_RANDOM_TATSU_LENGTH];
extern const IAR_InputRecord IAR_Option_RandomTatsu;
#define IAR_OPTION_HADO_HP_LENGTH 6
extern const uint16_t _IAR_RecordList_HadoHP[IAR_OPTION_HADO_HP_LENGTH];
extern const IAR_InputRecord IAR_Option_HadoHP;
#define IAR_OPTION_HADO_LP_LENGTH 6
extern const uint16_t _IAR_RecordList_HadoHP[IAR_OPTION_HADO_LP_LENGTH];
extern const IAR_InputRecord IAR_Option_HadoLP;
#define IAR_OPTION_JUMP_FORWARD_HK_LENGTH 31
extern const uint16_t _IAR_RecordList_JumpFwHK[IAR_OPTION_JUMP_FORWARD_HK_LENGTH];
extern const IAR_InputRecord IAR_Option_JumpFwHK;
#define IAR_OPTION_JUMP_FORWARD_MK_LENGTH 32
extern const uint16_t _IAR_RecordList_JumpFwMK[IAR_OPTION_JUMP_FORWARD_MK_LENGTH];
extern const IAR_InputRecord IAR_Option_JumpFwMK;
#define IAR_OPTION_ANTI_AIR_J_MP_LENGTH 4
extern const uint16_t _IAR_RecordList_AntiAir_jMP[IAR_OPTION_ANTI_AIR_J_MP_LENGTH];
extern const IAR_InputRecord IAR_Option_AntiAir_jMP;
#define IAR_OPTION_SHORYU_HP_LENGTH 8
extern const uint16_t _IAR_RecordList_ShoryuHP[IAR_OPTION_SHORYU_HP_LENGTH];
extern const IAR_InputRecord IAR_Option_Shoryu_HP;
#define IAR_OPTION_CR_HP_LENGTH 1
extern const uint16_t _IAR_RecordList_crHP[IAR_OPTION_CR_HP_LENGTH];
extern const IAR_InputRecord IAR_Option_crHP;
#define IAR_OPTION_ST_HP_LENGTH 1
extern const uint16_t _IAR_RecordList_stHP[IAR_OPTION_ST_HP_LENGTH];
extern const IAR_InputRecord IAR_Option_stHP;
#define IAR_OPTION_WALK_KARA_GRAB_LENGTH 7
extern const uint16_t _IAR_RecordList_WalkKaraGrab[IAR_OPTION_WALK_KARA_GRAB_LENGTH];
extern const IAR_InputRecord IAR_Option_WalkKaraGrab;
#define IAR_OPTION_CR_LP_LENGTH 1
extern const uint16_t _IAR_RecordList_crLP[IAR_OPTION_CR_LP_LENGTH];
extern const IAR_InputRecord IAR_Option_crLP;
#define IAR_OPTION_CR_MP_LENGTH 1
extern const uint16_t _IAR_RecordList_crMP[IAR_OPTION_CR_MP_LENGTH];
extern const IAR_InputRecord IAR_Option_crMP;
#define IAR_OPTION_GRAB_LENGTH 1
extern const uint16_t _IAR_RecordList_Grab[IAR_OPTION_GRAB_LENGTH];
extern const IAR_InputRecord IAR_Option_Grab;
#define IAR_OPTION_HITCONFIRM_LENGTH 20
extern const uint16_t _IAR_RecordList_Hitconfirm[IAR_OPTION_HITCONFIRM_LENGTH];
extern const IAR_InputRecord IAR_Option_Hitconfirm;
#define IAR_OPTION_JUMP_FORWARD_EMPTY_LENGTH 1
extern const uint16_t _IAR_RecordList_JumpFwEmpty[IAR_OPTION_JUMP_FORWARD_EMPTY_LENGTH];
extern const IAR_InputRecord IAR_Option_JumpFwEmpty;
#define IAR_OPTION_ST_MK_LENGTH 1
extern const uint16_t _IAR_RecordList_stMK[IAR_OPTION_ST_MK_LENGTH];
extern const IAR_InputRecord IAR_Option_stMK;
#define IAR_OPTION_ST_MP_HADO_LENGTH 9
extern const uint16_t _IAR_RecordList_stMP_Hado[IAR_OPTION_ST_MP_HADO_LENGTH];
extern const IAR_InputRecord IAR_Option_stMP_Hado;
#define IAR_OPTION_SHORYU_LP_LENGTH 8
extern const uint16_t _IAR_RecordList_ShoryuLP[IAR_OPTION_SHORYU_LP_LENGTH];
extern const IAR_InputRecord IAR_Option_Shoryu_LP;
#define IAR_OPTION_WAKEUP_BLOCK_LONG_LENGTH 16
extern const uint16_t _IAR_RecordList_WakeupBlockLong[IAR_OPTION_WAKEUP_BLOCK_LONG_LENGTH];
extern const IAR_InputRecord IAR_Option_WakeupBlockLong;
#define IAR_OPTION_WAKEUP_BLOCK_LOW_TECH_LENGTH 8
extern const uint16_t _IAR_RecordList_WakeupBlockLowTech[IAR_OPTION_WAKEUP_BLOCK_LOW_TECH_LENGTH];
extern const IAR_InputRecord IAR_Option_WakeupBlockLowTech;
#define IAR_OPTION_WAKEUP_BLOCK_HIGH_TECH_LENGTH 8
extern const uint16_t _IAR_RecordList_WakeupBlockHighTech[IAR_OPTION_WAKEUP_BLOCK_HIGH_TECH_LENGTH];
extern const IAR_InputRecord IAR_Option_WakeupBlockHighTech;
#define IAR_OPTION_WAKEUP_BLOCK_LOW_CR_LP_LENGTH 7
extern const uint16_t _IAR_RecordList_WakeupBlockLow_crLP[IAR_OPTION_WAKEUP_BLOCK_LOW_CR_LP_LENGTH];
extern const IAR_InputRecord IAR_Option_WakeupBlockLow_crLP;
#define IAR_OPTION_WAKEUP_SHORYU_LP_LENGTH 4
extern const uint16_t _IAR_RecordList_WakeupShoryuLP[IAR_OPTION_WAKEUP_SHORYU_LP_LENGTH];
extern const IAR_InputRecord IAR_Option_WakeupShoryuLP;
#define IAR_OPTION_WAKEUP_SHORYU_HP_LENGTH 4
extern const uint16_t _IAR_RecordList_WakeupShoryuHP[IAR_OPTION_WAKEUP_SHORYU_HP_LENGTH];
extern const IAR_InputRecord IAR_Option_WakeupShoryuHP;
#define IAR_OPTION_OKI_CR_LP_GRAB_LENGTH 28
extern const uint16_t _IAR_RecordList_Oki_crLP_Grab[IAR_OPTION_OKI_CR_LP_GRAB_LENGTH];
extern const IAR_InputRecord IAR_Option_Oki_crLP_Grab;
#define IAR_OPTION_OKI_WALK_FORWARD_GRAB_LENGTH 16
extern const uint16_t _IAR_RecordList_Oki_WalkForwardGrab[IAR_OPTION_OKI_WALK_FORWARD_GRAB_LENGTH];
extern const IAR_InputRecord IAR_Option_Oki_WalkForwardGrab;
#define IAR_OPTION_OKI_WALK_FORWARD_CR_MP_LENGTH 6
extern const uint16_t _IAR_RecordList_Oki_WalkForward_crMP[IAR_OPTION_OKI_WALK_FORWARD_CR_MP_LENGTH];
extern const IAR_InputRecord IAR_Option_Oki_WalkForward_crMP;
#define IAR_OPTION_OKI_BLOCK_LENGTH 22
extern const uint16_t _IAR_RecordList_Oki_Block[IAR_OPTION_OKI_BLOCK_LENGTH];
extern const IAR_InputRecord IAR_Option_Oki_Block;
#define IAR_OPTION_OKI_FORWARD_HP_LENGTH 1
extern const uint16_t _IAR_RecordList_Oki_fHP[IAR_OPTION_OKI_FORWARD_HP_LENGTH];
extern const IAR_InputRecord IAR_Option_Oki_fHP;
#define IAR_OPTION_OKI_CR_LK_ST_MK_LENGTH 28
extern const uint16_t _IAR_RecordList_Oki_crLK_stMK[IAR_OPTION_OKI_CR_LK_ST_MK_LENGTH];
extern const IAR_InputRecord IAR_Option_Oki_crLK_stMK;
#define IAR_OPTION_OKI_CR_LK_LENGTH 6
extern const uint16_t _IAR_RecordList_Oki_crLK[IAR_OPTION_OKI_CR_LK];
extern const IAR_InputRecord IAR_Option_Oki_crLK;
#define IAR_OPTION_OKI_IOH_J_LK_LENGTH 8
extern const uint16_t _IAR_RecordList_Oki_IOH[IAR_OPTION_OKI_IOH_J_LK_LENGTH];
extern const IAR_InputRecord IAR_Option_Oki_IOH;
#define IAR_OPTION_FAST_HADO_LENGTH 5
extern const uint16_t _IAR_RecordList_FastHado[IAR_OPTION_FAST_HADO_LENGTH];
extern const IAR_InputRecord IAR_Option_FastHado;

#endif