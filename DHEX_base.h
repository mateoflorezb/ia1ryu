#ifndef _DHEX_BASE_INCLUDED
#define _DHEX_BASE_INCLUDED
#include "clist.h"
#include <stdint.h>
#include <stdbool.h>

#define __DHEX_MAX_DEVICES 6


enum __DHEX_INPUT_BUTTON_INDEX {
	__DHEX_BUTTON_LP_INDEX = 0,
	__DHEX_BUTTON_HP_INDEX = 1,
	__DHEX_BUTTON_SP_INDEX = 2, 
	__DHEX_BUTTON_LK_INDEX = 3,
	__DHEX_BUTTON_HK_INDEX = 4,
	__DHEX_BUTTON_DS_INDEX = 5,
	
	__DHEX_BUTTON_START_INDEX = 6,
	
	__DHEX_DIR_R_INDEX = 7,
	__DHEX_DIR_L_INDEX = 8,
	__DHEX_DIR_U_INDEX = 9,
	__DHEX_DIR_D_INDEX = 10,
	
	__DHEX_MACRO_1_INDEX = 11,
	__DHEX_MACRO_2_INDEX = 12,
	__DHEX_MAX_BUTTONS
	
};

enum _DHEX_INPUT_BUTTON_MASK {
	DHEX_BUTTON_LP = 0b0000000000000001,
	DHEX_BUTTON_HP = 0b0000000000000010,
	DHEX_BUTTON_SP = 0b0000000000000100, //parry/dcancel
	DHEX_BUTTON_LK = 0b0000000000001000,
	DHEX_BUTTON_HK = 0b0000000000010000,
	DHEX_BUTTON_DS = 0b0000000000100000, //directional strike (ds feels like dust as well lmao)
	
	DHEX_BUTTON_START = 0b0000000001000000,
	
	DHEX_DIR_R = 0b0000000010000000,
	DHEX_DIR_L = 0b0000000100000000,
	DHEX_DIR_U = 0b0000001000000000,
	DHEX_DIR_D = 0b0000010000000000,
	
	DHEX_MACRO_1 = 0b0000100000000000,
	DHEX_MACRO_2 = 0b0001000000000000
	
};

enum _DHEX_HARDWARE_TYPE {
	 _DHEX_HARDWARE_TYPE_KEYBOARD 	= 0,
	 _DHEX_HARDWARE_TYPE_JOYBUTTON 	= 1,
	 _DHEX_HARDWARE_TYPE_JOYAXIS 	= 2,
	 _DHEX_HARDWARE_TYPE_JOYHAT 	= 3
};


typedef struct {
	uint8_t HardwareType;
	struct {
		
		struct {
			int16_t KeyNumber;
		} Keyboard;
		
		
		struct {
			int16_t ButtonNumber;
			uint8_t DevicePort; //index pointing to the virtual device port table
			
		} JoyButton;
		
		struct {
			int16_t AxisNumber;
			uint8_t DevicePort; //index pointing to the virtual device port table
			bool 	AxisDirection; //true for positive direction
			
		} JoyAxis;
		
		struct {
			uint8_t HatIndex;
			uint8_t DevicePort;
			uint8_t HatDirection;
			
		} JoyHat;
		
		
		
	} Device;
	
	
} _DHEX_VirtualKey;


enum _DHEX_DRAW_LAYER {

	DHEX_LAYER_BACKGROUND0 = 0,
	DHEX_LAYER_BACKGROUND1 = 1,
	DHEX_LAYER_BACKGROUND2 = 2,
	DHEX_LAYER_BACKGROUND3 = 3,
	DHEX_LAYER_BACKGROUND4 = 4,

	DHEX_LAYER_OBJ0 = 5,
	DHEX_LAYER_OBJ1 = 6,
	DHEX_LAYER_OBJ2 = 7,
	DHEX_LAYER_OBJ3 = 8,
	DHEX_LAYER_OBJ4 = 9,
	DHEX_LAYER_OBJ5 = 10,
	DHEX_LAYER_OBJ6 = 11,
	
	DHEX_LAYER_HUD0 = 12,
	DHEX_LAYER_HUD1 = 13,
	DHEX_LAYER_HUD2 = 14,
	_DHEX_DRAW_LAYER_MAX = 15
	
};

typedef struct {
	uint8_t Type;
	union {
		
		struct {
			void* Sprite;
			int x;
			int y;
			int xOffset;
			int yOffset;
			double xscale;
			double yscale;
			double angle;
			
		} DrawSprite;
		
		
		struct {
			int x;
			int y;
			uint16_t w;
			uint16_t h;
			uint8_t r;
			uint8_t g;
			uint8_t b;
			uint8_t a;
			
		} DrawFillRect;
		
		struct {
			int x;
			int y;
			uint16_t w;
			uint16_t h;
			uint8_t r;
			uint8_t g;
			uint8_t b;
			uint8_t a;
			
		} DrawRect;
		
		struct {
			void* font;
			int x;
			int y;
			double xscale;
			double yscale;
			int hcenter;
			int vcenter;
			const char* string;
			
		} DrawText;
		
		
		
	} Call;
	
} _DHEX_DrawCall;



typedef struct { //DHEX_App
	struct _DHEX_APP_PRIVATE {
		void* SpriteRawList;
		void* SpritePalettizedList;
		void* BXG_List;
		void* MainRenderer;
		void* MainWindow;
		
		void* SfxList;
		
		int ScreenWidth;
		int ScreenHeight;
		
		struct _DHEX_APP_PRIVATE_INPUT {
			uint16_t ButtonMaskState[2][2]; //[frame, 0 = current, 1 = previous][player 0 = p1, 1 = p2]
			
			_DHEX_VirtualKey KeyConfig[__DHEX_MAX_BUTTONS][2]; //[button] [player]
			
			int32_t DevicePorts[__DHEX_MAX_DEVICES];
			
			struct {
				int32_t x,y;
				bool Held, Pressed;
			} Mouse;
			
			bool AnyVk; //if any virtual key was detected PRESSED on this frame
			_DHEX_VirtualKey LastVk;
			
			
		} Input;
		
		
		void* DrawLayers[_DHEX_DRAW_LAYER_MAX];
		
		
		
	} __private;
	
	struct {
		uint8_t Layer;
		bool Enabled;
		
	} Draw;
	
	const char* WorkingDirectory;
	
	bool Quit;

	
} DHEX_App;

DHEX_App* DHEX_Init(int* ErrorState, const char* AppName, int w, int h);
void DHEX_FreeApp(DHEX_App* app);

void DHEX_GetScreenDimensions(DHEX_App* app, int* w, int* h);
void DHEX_GetMouseState(DHEX_App* app, int* x, int* y, bool* held, bool* pressed);

void DHEX_SetDrawColor(DHEX_App* app, uint8_t r, uint8_t g, uint8_t b, uint8_t a); //sets the drawing color for a few functions
void DHEX_ClearScreen(DHEX_App* app, uint8_t r, uint8_t g, uint8_t b); //sets every pixel to the draw color

void DHEX_UpdateState(DHEX_App* app); //call at the start of each frame. Polls events.

void DHEX_Delay(uint16_t ms); //sleep x miliseconds
uint32_t DHEX_GetTicks(void); //miliseconds elapsed since DHEX_Init

bool DHEX_IsRunning(DHEX_App* app); //is the app still running? use for the main while

bool DHEX_InputCheck(DHEX_App* app, uint16_t ButtonMask, uint8_t player); //pass player argument as 1 or 2
bool DHEX_InputCheckPressed(DHEX_App* app, uint16_t ButtonMask, uint8_t player);
bool DHEX_InputCheckReleased(DHEX_App* app, uint16_t ButtonMask, uint8_t player);

void DHEX_SetDefaultKeyConfig(DHEX_App* app, uint8_t player);
void DHEX_SaveKeyConfig(DHEX_App* app, const char* filename);
void DHEX_LoadKeyConfig(DHEX_App* app, const char* filename);


void DHEX_DrawLayer(DHEX_App* app, uint8_t Layer); //once set, all further draw calls will be drawn to this layer

//these don't actually draw anything, just queue a draw call in the currently set layer
void DHEX_DrawSprite(DHEX_App* app, void* spr, int x, int y, int xOffset, int yOffset, double xscale, double yscale, double angle);
void DHEX_FillRect(DHEX_App* app, int x, int y, int w, int h, uint8_t r, uint8_t g, uint8_t b, uint8_t a); //filled
void DHEX_DrawRect(DHEX_App* app, int x, int y, int w, int h, uint8_t r, uint8_t g, uint8_t b, uint8_t a); //outline
void DHEX_DrawText(DHEX_App* app, void* font, int x, int y, double xscale, double yscale, int hcenter, int vcenter, const char* string);


void DHEX_UpdateScreen(DHEX_App* app); //updates screen with all queued draw calls

void DHEX_SetWindowSize(DHEX_App* app, int w, int h);
void DHEX_CenterWindow(DHEX_App* app);

void DHEX_DrawEnable(DHEX_App* app, bool enable); //if draw is disabled all drawing functions will be ignored
//useful for some things (like not drawing rollbacks without in-app implementation)

#endif