#ifndef _IAR_DATAGENERATION_H
#define _IAR_DATAGENERATION_H

#ifndef PY_SSIZE_T_CLEAN
#define PY_SSIZE_T_CLEAN
#endif
#define Py_LIMITED_API
#include "Python.h"
/*
short back dist to corner (2)
short enemy back dist to corner (2)
short dist to enemy (2)
short dist to enemy fireball (2)
short enemy dist to self fireball (2)
short frame advantage (2)
short life lead (2) //THIS HAS TO CHANGE to absolute health
//for the formula, yeah, we use life lead. Easy to  compute.
//but the regressor needs to know if an instant overhead or a DP chip will KO or not
char self KO (1)
char enemy KO (1)
short enemy y coord(2) //not used for scoring, just for regression
*/

#define _IAR_DATA_ROW_NMEMBERS 14

typedef struct {
	int16_t SelfDistCorner;
	int16_t EnemyDistCorner;
	int16_t SelfDistEnemy;
	int16_t SelfDistFireball;
	int16_t EnemyDistFireball;
	int16_t FrameAdvantage;
	int16_t SelfLife;
	int16_t EnemyLife;
	int8_t  InCombo; //0 if opponent is in blockstun. 1 if they are in hitstun
	int8_t  EnemyInCombo; //0 if self is in blockstun. 1 if self is in hitstun
	int16_t EnemyY; //y coord lol
	int16_t Option; //enum for all the things the AI can do, jump, delay tech, cr mk etc idk
	int16_t RW;
	int16_t RS;

	
} IAR_DataRow;

void IAR_SetLocalRow(IAR_DataRow row);

void IAR_ExportLocalRowsAsCSV(IAR_DataRow data[], int nrows, const char* filename);
//data: pointer to an array with at least nrows elements
//nrows: how many rows are going to be copied into the csv file 
//filename: self explanatory

double IAR_GetEstimation(PyObject* rx, IAR_DataRow input);

#endif